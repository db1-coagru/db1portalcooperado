# **DB1 Global Software - COAGRU - Portal do Cooperado** #

## DB1PortalCooperado ##

- Java JDK 6 & JRE 8
O código é desenvolvido usando a JDK6, porém precisa da JRE8 devido as dependências do Jersey (Framework da api REST) serem compiladas na versão JDK8;

- Tomcat 6.0.33
http://tomcat.apache.org/download-60.cgi#6.0.33

- jdbc.jar, JdbcProgress.dll, wrapper.dll, procli92.dll
Se encontram na pasta /connectorLibs e devem ser colocados na pasta lib do tomcat.

- %PORTAL_COOPERADO_HOME%
Variável de ambiente que indica o caminho da pasta que deve conter o arquivo portalCooperado.properties

- portalCooperado.properties
Arquivo texto contendo as informações referente a conexão com o DB1PortalCooperadoConnector.jar e com a base progress,
  e deve estar localizado na pasta indicada pela variável %PORTAL_COOPERADO_HOME%

- Flex SDK 4.1.0
Precisa ter no minimo o player 3 (10.3) e no máximo o player 10 (10.10) para compilar no ANT.  

- iReport 3.7.2
Para desenvoler os relatórios (Compativel com a versão do jasperreports-3.7.2.jar)

- Oracle XE Database
Criar os schema PORTALCOOPERADO, PUB e SGC
	- PORTALCOOPERADO contém todas as tabelas do DB1CoreSecurity e DB1PortalCooperado.
	- PUB contém as tabelas onde o progress grava o resultado das requisições.
	- SGC contém as tabelas onde o sistema SGC (sistema da Capptan) irá conectar, o PORTALCOOPERADO se comunica com o SGC.
Dentro do projeto na pasta DB1PortalCooperadoJ/documentacao/banco encontram-se os scripts de geração dos bancos e atualização destes (estilo flyway mas manual), 
rodar na sequencia das pastas system, portalcooperado, pub e na sequencia das versoes V01__..., V02__..., V03__..., Vetc__...; Se atentar aos comentários dentro dos scripts;


## DB1PortalCooperadoConnector   

- Descrição
Aplicativo responável por receber requisições do Portal do Cooperado, para que seja executado o programa responsável por popular a tabela da base Progress.

- Operação
Para inicializar a aplicação
  - navegar até a pasta onde está localizado o DB1PortalCooperadoConnector.jar
  - executar o comando: java -jar DB1PortalCooperadoConnector.jar

- JDK/JRE 1.5
http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-javase5-419410.html#jre-1.5.0_22-oth-JPR
  
- DB1PortalCooperadoConnector.jar
disponível na pasta /connectorLibs

- Porta 10500
Deve estar disponível para receber requisições e não deve possuir serviços utilizando-a