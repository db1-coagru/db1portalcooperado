/**
 * 
 */
package br.com.db1.connector.requisicao;


import java.io.IOException;

/**
 * Classe respons�vel por realizar a requisi��o do programa progress.
 * 
 * @author Alexandre de Castro
 * @since 23/08/2011
 */
public class Requisicao {
	
	private static final String ERR_EXEC_COMANDO = "Problema ao tentar executar comando. Entre em contato com o suporte t�cnico.";
	public static final Long TIPO_POS_FINANCEIRA = 1L;
	public static final Long TIPO_ENTREGA_PRODUCAO = 2L;
	public static final String FALTA_ARGUMENTOS_EXCEPTION = "Numero incorreto de argumentos. \n" +
															"Sao necessarios 3 argumentos para o relatorio do tipo 1: " +
															"matricula, idRequisicao e tipoRequisicao, " +
															"ou 6 argumentos para relatorio do tipo 2: " +
															"matricula, idRequisicao, tipoRequisicao, tipoProduto, dataInicial e dataFinal.";
	
	private Long matricula;
	private Long id;
	private Long tpRequisicao;
	private Long tpProduto;
	private String dtInicial;
	private String dtFinal;
	private String comando;
	
	public Requisicao() {}
	
	/**
	 * Construtor da classe. Recebe um conjunto de par�metros para instanciar o objeto.
	 * 
	 * @param args conjunto de par�metros usados para instanciar o objeto
	 * @throws Exception disparada quando o n�mero de par�metros � incompat�vel com o esperado pelos relat�rios
	 */
	public Requisicao(String argss[]) throws Exception {
		//valida a quantidade de argumentos
		String args[] = {"123","1","1"};
		//String args[] = {"123","1","2","5","20/08/2011","25/08/2011"};
		
		if ((args.length != 3) && (args.length != 6)) {
			throw new Exception(FALTA_ARGUMENTOS_EXCEPTION);
		}
		//relat�rio tipo 1
		this.setMatricula(Long.parseLong(args[0]));
		this.setId(Long.parseLong(args[1]));
		this.setTpRequisicao(Long.parseLong(args[2]));
		//relat�rio tipo 2
		if (args.length == 6) {
			this.setTpProduto(Long.parseLong(args[3]));
			this.setDtInicial(args[4]);
			this.setDtFinal(args[5]);
		}
	}
	
	/**
	 * M�todo respons�vel por invocar um aplicativo.
	 * @throws Exception disparada quando o arquivo a ser lido n�o pode ser lido
	 */
	public void invocaRequisicao() throws Exception {
		try {
			Runtime.getRuntime().exec(comando);
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception(ERR_EXEC_COMANDO);
		}
	}
	
	/**
	 * Monta o comando para execu��o do progress com os valores dos atributos do
	 * objeto
	 * 
	 * @return Comando para execu��o de linha de comando do progress
	 */
	public String montarComando() {
		String caminhoProwin = Propriedades.getInstancia().getPropriedade(Propriedades.PROP_LINHA_COMANDO_PROGRESS);
		if (tpRequisicao.equals(TIPO_ENTREGA_PRODUCAO)) {
			return caminhoProwin + " " + matricula + ";" + id
			+ ";" + tpRequisicao + ";" + tpProduto + ";" + 
			dtInicial + ";" +
			dtFinal + "\"";
			//converteDataDDMMAAAA(dtInicial)+ ";" + 
			//converteDataDDMMAAAA(dtFinal)+ "\"";
			/*return caminhoProwin + " -param \"" + matricula + ";" + id
					+ ";" + tpRequisicao + ";" + tpProduto + ";" + 
					converteDataDDMMAAAA(dtInicial)+ ";" + 
					converteDataDDMMAAAA(dtFinal) + "\""+"/bdhot2/bas";*/
					
		}
		return caminhoProwin + " " + matricula + ";" + id + ";"
		+ tpRequisicao+ "\"";
		/*return caminhoProwin + " -param \"" + matricula + ";" + id + ";"
				+ tpRequisicao + "\""+"/bdhot2/bas";*/
	}
	
	/**
	 * Converte a data especificada para o formato DD/MM/AAAA
	 * 
	 * @param data
	 *            Data para ser convertida
	 * @return Data no formato DD/MM/AAAA
	 */
//	private String converteDataDDMMAAAA(Date data) {
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//		return sdf.format(data);
//	}

	/**
	 * @return the matricula
	 */
	public Long getMatricula() {
		return matricula;
	}

	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the tpRequisicao
	 */
	public Long getTpRequisicao() {
		return tpRequisicao;
	}

	/**
	 * @param tpRequisicao the tpRequisicao to set
	 */
	public void setTpRequisicao(Long tpRequisicao) {
		this.tpRequisicao = tpRequisicao;
	}

	/**
	 * @return the tpProduto
	 */
	public Long getTpProduto() {
		return tpProduto;
	}

	/**
	 * @param tpProduto the tpProduto to set
	 */
	public void setTpProduto(Long tpProduto) {
		this.tpProduto = tpProduto;
	}

	/**
	 * @return the dtInicial
	 */
	public String getDtInicial() {
		return dtInicial;
	}

	/**
	 * @param dtInicial the dtInicial to set
	 */
	public void setDtInicial(String dtInicial) {
		this.dtInicial = dtInicial;
	}

	/**
	 * @return the dtFinal
	 */
	public String getDtFinal() {
		return dtFinal;
	}

	/**
	 * @param dtFinal the dtFinal to set
	 */
	public void setDtFinal(String dtFinal) {
		this.dtFinal = dtFinal;
	}

	/**
	 * @return the comando
	 */
	public String getComando() {
		return comando;
	}

	/**
	 * @param comando the comando to set
	 */
	public void setComando(String comando) {
		this.comando = comando;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("---------------------------------------------\n");
		sb.append("Matricula: " + matricula + "\n");
		sb.append("Id: " + id + "\n");
		sb.append("Tipo Requisicao: " + tpRequisicao + "\n");
		if (tpProduto != null) {
			sb.append("Tipo Produto: " + tpProduto + "\n");
			sb.append("Data Inicial: " + dtInicial + "\n");
			sb.append("Data Final: " + dtFinal + "\n");
		}
		sb.append("---------------------------------------------");
		
		return sb.toString();
	}
	
}
