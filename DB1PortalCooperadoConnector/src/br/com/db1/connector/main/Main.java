/**
 *
 */
package br.com.db1.connector.main;

import br.com.db1.connector.requisicao.Propriedades;
import br.com.db1.connector.requisicao.Requisicao;

import java.io.File;
import java.io.IOException;

/**
 * Classe principal do sistema.
 *
 * @author Alexandre de Castro
 * @since 23/08/2011
 */
public class Main {

	private static final String STRING_VAZIA = "";

	private static final String PORTAL_COOPERADO_HOME_LINUX = "PORTAL_COOPERADO_HOME_LINUX";

	private static final String NOME_ARQUIVO_PROPRIEDADES = "portalCooperado.properties";

	private Main() {

	}

	/**
	 * M�thodo a ser chamado quando a aplica��o for executada.
	 *
	 * @param args par�metros passados pela invoca��o da aplica��o
	 */
	public static void main(String[] args) {
		//matricula, id_requisicao, tipoReq, tpProduto, dt_inicial, dt_final
		Requisicao requisicao;
		try {
			carregaArquivoProperties();
			requisicao = new Requisicao(args);
			requisicao.setComando(requisicao.montarComando());
			requisicao.invocaRequisicao();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * M�thodo respons�vel por carregar o arquivo que cont�m propriedades para manipula��o do sistema.
	 */
	private static void carregaArquivoProperties() throws IOException {
		String portalCoopHome = System.getenv(PORTAL_COOPERADO_HOME_LINUX);
		if (portalCoopHome != null && !STRING_VAZIA.equals(portalCoopHome.trim())) {
			String caminhoArquivo = portalCoopHome.concat(File.separator.concat(NOME_ARQUIVO_PROPRIEDADES));
			Propriedades.getInstancia().carregar(caminhoArquivo);
		}
		throw new IllegalArgumentException("Pasta HOME n�o encontrada.");
	}

}
