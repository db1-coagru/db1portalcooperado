/**
 *
 */
package br.com.db1.connector.socket;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Classe que representa o servidor na recep��o de chamadas remotas.
 *
 * @author Alexandre de Castro
 * @since 25/08/2011
 */
public class ConnectorServer implements Runnable {

	private Socket client;

	/**
	 * Construtor da classe.
	 *
	 * @param client socket a ser instanciado
	 */
	public ConnectorServer(Socket client) {
		this.client = client;
	}

	/**
	 * M�todo a ser executado quando a aplica��o for iniciada. Respons�vel por
	 * inicializar o socket na porta 10500
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			ServerSocket server = new ServerSocket(10500);
			while (true) {
				Socket client = server.accept();
				Thread t = new Thread(new ConnectorServer(client));
				t.start();
				System.out.println("Conex�o estabelecida: " + client);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		String comando = null;
		BufferedReader br;
		Process proc = null;
		try {
			// recebe a linha de comando enviada pelo cliente
			br = new BufferedReader(new InputStreamReader(
					client.getInputStream()));
			comando = br.readLine();
			System.out.println(comando);
		}
		catch (Exception e) {
			e.printStackTrace();
			return;
		}

		StringBuffer sb = new StringBuffer();

		try {
			// executa o comando na m�quina do server
			sb.append("Executando comando: " + comando);
			System.out.print(sb.toString());
			proc = Runtime.getRuntime().exec(comando);

			int code = proc.waitFor();

			sb.append("Resultado: ");
			sb.append(code);
			sb.append("\n");

			BufferedReader saida = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			String line = null;

			do {
				line = saida.readLine();
				if (line == null)
					break;
				sb.append(line);
				sb.append("\n");
			} while (true);

			saida = new BufferedReader(new InputStreamReader(proc.getErrorStream()));

			do {
				line = saida.readLine();
				if (line == null)
					break;
				sb.append(line);
				sb.append("\n");
			} while (true);

			System.out.println(sb.toString());
			System.out.println("Comando executado");
		}
		catch (Exception e) {
			sb.append("ERRO: ");
			sb.append(e.getMessage());
			sb.append("\n");
			e.printStackTrace();
		}
		try {
			PrintWriter retorno = new PrintWriter(client.getOutputStream());
			retorno.print(sb.toString());
			retorno.flush();

			System.out.println("Fechando socket..");
			client.close();
			System.out.println("Fechado? " + client.isClosed());
		}
		catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

}
