package br.com.db1.portalcooperado.components {
import br.com.db1.controls.edit.DB1Edit;

import flash.events.KeyboardEvent;
import flash.ui.Keyboard;

import mx.formatters.PhoneFormatter;

public class TelefoneFormat {

    public static function format(txt:DB1Edit, event:KeyboardEvent):void {
        if (Keyboard.BACKSPACE !== event.keyCode
                && Keyboard.LEFT !== event.keyCode
                && Keyboard.RIGHT !== event.keyCode) {

            var actualText:String = getActualText(txt);
            var formatter:PhoneFormatter = new PhoneFormatter();
            var position:int = 0;
            if (actualText.length === 1) {
                formatter.formatString = "(#)";
                position = 1;
            }
            else if (actualText.length === 2) {
                formatter.formatString = "(##)";
                position = 2;
            }
            else if (actualText.length === 3) {
                formatter.formatString = "(##)#";
            }
            else if (actualText.length === 4) {
                formatter.formatString = "(##)##";
            }
            else if (actualText.length === 5) {
                formatter.formatString = "(##)###";
            }
            else if (actualText.length === 6) {
                formatter.formatString = "(##)####-";
                position = 1;
            }
            else if (actualText.length === 7) {
                formatter.formatString = "(##)####-#";
            }
            else if (actualText.length === 8) {
                formatter.formatString = "(##)####-##";
            }
            else if (actualText.length === 9) {
                formatter.formatString = "(##)####-###";
            }
            else if (actualText.length === 10) {
                formatter.formatString = "(##)####-####";
            }
            else if (actualText.length === 11) {
                formatter.formatString = "(##)#####-####";
            }
            txt.text = formatter.format(actualText);

            txt.setSelection(txt.selectionActivePosition + position, txt.selectionActivePosition + position);
        }
    }

    private static function getActualText(txt:DB1Edit):String {
        var parEsq:RegExp = /\(/g;
        var parDir:RegExp = /\)/g;
        var hifen:RegExp = /\-/g;

        var str:String = txt.text.replace(parEsq, "");
        str = str.replace(parDir, "");
        str = str.replace(hifen, "");
        return str;
    }
}
}
