package br.com.db1.portalcooperado.components
{
	import flash.display.Sprite;
	import flash.events.Event;

	[Bindable]
	public class Cultura extends Sprite
	{
		public static const SELECTED_ALL:String = "SelectedAll";
		
		public function Cultura(label:String, codigo:String)
		{
			this.label=label;
			this.codigo=codigo;
			
		}
		public var isSelectedAll:Boolean = false;
		public var label:String;
		private var _selected:Boolean=true;
		public var codigo:String;
		
		public function set selected(value:Boolean):void{
			this._selected = value;
			dispatchEvent(new Event(SELECTED_ALL));
			dispatchEvent(new Event("manageSelection"));
			
		}
		
		[Bindable("manageSelection")]
		public function get selected():Boolean{
			return this._selected;			
		}
		
		public function unselected(value:Boolean):void {
			this._selected = value;
			dispatchEvent(new Event("manageSelection"));
		}
		
		public function selectedAllFunction(functionSelectAll:Function):void{	
			isSelectedAll = true;
			this.addEventListener(SELECTED_ALL, functionSelectAll);
		}
	}
}