package br.com.db1.portalcooperado.view.preloader
{
	import br.com.db1.dialogs.DB1SplashScreen;
	
	public class DB1PortalCooperadoSplashScreen extends DB1SplashScreen
	{
		
		[ Embed(source="../../image/logoCoagruSplashScreen.png", mimeType="application/octet-stream") ]
		public var SplashScreenImage:Class;
		
		public function DB1PortalCooperadoSplashScreen()
		{
		}
		
		override public function loadImage():void{
			this.SplashScreenGraphic = SplashScreenImage;
			this.timeAutoClose = 120;
			super.loadImage();
		}

	}
}