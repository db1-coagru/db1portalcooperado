package br.com.db1.portalcooperado.controller {

import br.com.db1.core.action.Action;
import br.com.db1.core.action.ActionForm;
import br.com.db1.core.action.SecurityActionType;
import br.com.db1.core.application.DB1ApplicationConfig;
import br.com.db1.core.controller.ApplicationControllerDefault;
import br.com.db1.core.resource.DB1ResourceManager;
import br.com.db1.core.rpc.DB1Responder;
import br.com.db1.core.security.DB1SecurityManager;
import br.com.db1.core.security.DB1UserSession;
import br.com.db1.dialogs.DB1Alert;
import br.com.db1.dialogs.DB1InvokingWindow;
import br.com.db1.dialogs.SearchDialogFactory;
import br.com.db1.filter.DB1FilterBuilderFactory;
import br.com.db1.forms.DB1CustomLogin;
import br.com.db1.portalcooperado.model.builder.RequisicaoBuilder;
import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.service.RequisicaoService;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import br.com.db1.portalcooperado.view.AboutForm;
import br.com.db1.portalcooperado.view.AcessoCooperadoForm;
import br.com.db1.portalcooperado.view.AcessoUsuarioLogForm;
import br.com.db1.portalcooperado.view.DesignForm;
import br.com.db1.portalcooperado.view.EntregaProducaoForm;
import br.com.db1.portalcooperado.view.IntegracaoManualAviculturaForm;
import br.com.db1.portalcooperado.view.PosicaoFinanceiraForm;
import br.com.db1.portalcooperado.view.UsuarioLogForm;
import br.com.db1.portalcooperado.view.searchDialog.CooperanteSearchDialog;
import br.com.db1.portalcooperado.view.searchDialog.FuncionarioSearchDialog;
import br.com.db1.portalcooperado.view.searchDialog.IndicadoSearchDialog;
import br.com.db1.portalcooperado.view.searchDialog.ProfileSearchDialog;
import br.com.db1.portalcooperado.view.template.AlteraSenhaTemplateForm;
import br.com.db1.portalcooperado.view.template.HomeTemplateForm;
import br.com.db1.portalcooperado.view.template.UsuarioTemplateForm;

import flash.events.Event;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import mx.controls.Alert;
import mx.core.FlexGlobals;
import mx.core.UIComponent;
import mx.events.CloseEvent;
import mx.managers.PopUpManager;
import mx.rpc.AsyncToken;
import mx.rpc.events.FaultEvent;

[ResourceBundle("DB1PortalCooperado")]
public class ApplicationController extends ApplicationControllerDefault {

  [Bindable]
  public var actSobre:Action;

  [Bindable]
  public var actUsuario:Action;

  [Bindable]
  public var actHome:Action;

  [Bindable]
  public var actPosicaoFinanceira:Action;

  [Bindable]
  public var actEntregaProducao:Action;

  [Bindable]
  public var actAcessoCooperado:Action;

  [Bindable]
  public var actAlteraSenha:Action;

  [Bindable]
  public var actDesign:Action;

  [Bindable]
  public var actUsuarioLog:Action;

  [Bindable]
  public var actAcessoUsuarioLog:Action;

  [Bindable]
  public var actIntegracaoManualAvicultura:Action;

  [Bindable]
  public var idSystem:Number;

  [Bindable]
  public var idProfile:Number;

  //SearchDialogs******************************************
  [Bindable]
  public var perfilSearchDialogFactory:SearchDialogFactory;

  [Bindable]
  public var cooperanteSearchDialogFactory:SearchDialogFactory;

  [Bindable]
  public var funcionarioSearchDialogFactory:SearchDialogFactory;

  [Bindable]
  public var indicadoSearchDialogFactory:SearchDialogFactory;

  public var parentForm:UIComponent = UIComponent(application);

  public var searchWindow:DB1InvokingWindow = new DB1InvokingWindow();

  public static function getInstance():ApplicationController {
    if (!instance) {
      allowInstantiation = true;
      instance = new ApplicationController();
      allowInstantiation = false;
    }

    return instance as ApplicationController;
  }


  public function ApplicationController() {
    super();
    versao = CONFIG::versao;
    DB1FilterBuilderFactory.getInstance().useColumnName = true;
    DB1SecurityManager.getInstance().defaultAllow = false;
    DB1ApplicationConfig.getInstance().defaultSecurityActionType = SecurityActionType.INVISIBLE;
  }

  override public function set userSession(value:DB1UserSession):void {
    super.userSession = value;
    dispatchEvent(new Event("refreshStatusBar"));
  }

  // Obrigatório extender . retorna referencia para um customLogin
  override protected function get db1Login():DB1CustomLogin {
    return application.loginScreen;
  }

  override public function getVersion():String {
    return CONFIG::versao;
  }

  // Actions do sistema
  override protected function initActions():void {
    super.initActions();
    actSobre = Action.newAction("actSobre", DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actSobre.lbl'), executeActionActSobre);
    actPosicaoFinanceira = Action.newSecurityAction("actPosicaoFinanceira", null, executeActionActPosicaoFinanceira, DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actPosicaoFinanceira.lbl'), DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actPosicaoFinanceira.ttp'));
    actEntregaProducao = Action.newSecurityAction("actEntregaProducao", null, executeActionActEntregaProducao, DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actEntregaProducao.lbl'), DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actEntregaProducao.ttp'));
    actAcessoCooperado = Action.newSecurityAction("actAcessoCooperado", null, executeActionActAcessoCooperado, DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actAcessoCooperado.lbl'), DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actAcessoCooperado.ttp'));
    actUsuarioLog = Action.newSecurityAction("actUsuarioLog", null, executeActionActUsuarioLog, DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actAcessoCooperado.lbl'), DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actUsuarioLog.ttp'));
    actAcessoUsuarioLog = Action.newSecurityAction("actAcessoUsuarioLog", null, executeActionActAcessoUsuarioLog, DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actAcessoCooperado.lbl'), DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actAcessoUsuarioLog.ttp'));
    actIntegracaoManualAvicultura = Action.newSecurityAction("actIntegracaoManualAvicultura", null, executeActionActIntegracaoManualAvicultura, DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actIntegracaoManualAvicultura.lbl'), DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.portalcooperado.controller.ApplicationController.actIntegracaoManualAvicultura.ttp'));
    actHome = new ActionForm(HomeTemplateForm);
    actUsuario = new ActionForm(UsuarioTemplateForm);
    actAlteraSenha = new ActionForm(AlteraSenhaTemplateForm);

    this.actionList.add(actHome);
    this.actionList.add(actUsuario);
    this.actionList.add(actAlteraSenha);

    // Search Dialog
    perfilSearchDialogFactory = new SearchDialogFactory(DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.core.security.model.entity.Profile.ttl'), ["code", "description"], ProfileSearchDialog, "PerfilSearch");
    cooperanteSearchDialogFactory = new SearchDialogFactory(DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.core.security.model.entity.Cooperante.ttl'), ["matricula", "nome"], CooperanteSearchDialog, "CooperanteSearch");
    funcionarioSearchDialogFactory = new SearchDialogFactory(DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.core.security.model.entity.Cooperante.ttl'), ["matricula", "nome"], FuncionarioSearchDialog, "FuncionarioSearch");
    indicadoSearchDialogFactory = new SearchDialogFactory(DB1ResourceManager.getInstance().getResourceString('DB1PortalCooperado', 'br.com.db1.core.security.model.entity.Cooperante.ttl'), ["matricula", "nome"], IndicadoSearchDialog, "IndicadoSearch");
  }

  // Resource Manager
  override protected function initResourceManager():void {
    super.initResourceManager();
  }

  // Application Config
  override protected function initApplicationConfig():void {
    super.initApplicationConfig();
    DB1ApplicationConfig.getInstance().applicationResourceBundleName = "DB1PortalCooperado";
  }

  // Aplicacao principal. Somente para facilitar acesso
  // pois é equivalente a Application.application
  public function get application():DB1PortalCooperado {
    return FlexGlobals.topLevelApplication as DB1PortalCooperado;
  }

  // Show na Tela de Login
  override protected function showLoginScreen():void {
    application.viewsMenu.selectedChild = db1Login;
  }

  // Show tela da aplicação
  override protected function showApplicationScreen():void {
    application.viewsMenu.selectedChild = application.applicationScreen;
    idSystem = getInstance().userSession.systemSeg.idSystem;
    var usuarioController:UsuarioController = new UsuarioController();
    usuarioController.buscaPerfil();

  }

  private function onLogoutConfirmation(event:CloseEvent):void {
    if (event.detail == Alert.YES) {
      logout();
      navigateToURL(new URLRequest('http://www.coagru.com.br'), '_self');
    }
  }

  /**
   * Método responsável por tratar a requisição de logout do sistema
   * pelo usuário.
   * Sobrescrito para chamar o site da Coagru após realizar a saída do sistema.
   */
  override public function logout():void {
    super.logout();
    navigateToURL(new URLRequest('http://www.coagru.com.br'), '_self');
  }


  public function executeActionHome(sender:Object):void {
    showApplicationScreen();
  }

  /**
   * Método executado pela Action do relatório de Posição Financeira
   */
  public function executeActionActPosicaoFinanceira(sender:Object):void {
    var posicaoFinanceiraForm:PosicaoFinanceiraForm = new PosicaoFinanceiraForm();
    PopUpManager.addPopUp(posicaoFinanceiraForm, application, true);
    PopUpManager.centerPopUp(posicaoFinanceiraForm);
  }


  public function chamaRelatorio(builder:RequisicaoBuilder, form:UIComponent):void {
    var requisicaoService:RequisicaoService = new RequisicaoService();
    var args:Array = new Array();
    args.push(builder.reportName);
    args.push(builder.reportType);
    args.push(searchWindow);

    var async:AsyncToken = requisicaoService.geraRequisicao(builder);
    async.addResponder(new DB1Responder(this, esperarRespostaDoRelatorio, faultFindContrato, args));
    searchWindow.showSearchWindow(form, "Gerando Relatório...");
  }

  /**
   * Irá fazer requisições no java para verificar se está disponível o relatório
   * especificado da requisição.
   *
   * Caso o relatório ainda não esteja disponível fará uma chamada recursiva
   * para verificar novamente.
   */
  public function esperarRespostaDoRelatorio(result:Requisicao, args:Array):void {
    var requisicaoService:RequisicaoService = new RequisicaoService();
    var async:AsyncToken = requisicaoService.retornoRequisicao(result.id);
    async.addResponder(new DB1Responder(this, function (nomeArquivo:String, args:Array):void {
      if (nomeArquivo !== null) {
        PortalCooperadoUtil.navigateToReport(nomeArquivo, args);
      }
      else {
        esperarRespostaDoRelatorio(result, args);
      }
    }, faultFindContrato, args));
  }

  public function faultFindContrato(fault:Object):void {
    DB1Alert.error(FaultEvent(fault).fault.faultString);
    searchWindow.hideSearchWindow();
  }

  public function executeActionActEntregaProducao(sender:Object):void {
    var relEntregaProducao:EntregaProducaoForm = new EntregaProducaoForm();
    PopUpManager.addPopUp(relEntregaProducao, application, true);
    PopUpManager.centerPopUp(relEntregaProducao);

  }

  public function executeActionActSobre(sender:Object):void {
    var popUpAbout:AboutForm = new AboutForm();
    PopUpManager.addPopUp(popUpAbout, application, true);
    PopUpManager.centerPopUp(popUpAbout);

  }

  public function executeActionActDesign(sender:Object):void {
    var popUpDesign:DesignForm = new DesignForm();
    PopUpManager.addPopUp(popUpDesign, application, true);
    PopUpManager.centerPopUp(popUpDesign);

  }

  // Código deste sistema no sistema de segurança
  override public function get securitySystemName():String {
    return "DB1PORTALCOOPERADO";
  }

  public function executeActionActAcessoCooperado(sender:Object):void {
    var relAcessoCooperado:AcessoCooperadoForm = new AcessoCooperadoForm();
    PopUpManager.addPopUp(relAcessoCooperado, application, true);
    PopUpManager.centerPopUp(relAcessoCooperado);
  }

  public function executeActionActUsuarioLog(sender:Object):void {
    var usuarioLogForm:UsuarioLogForm = new UsuarioLogForm();
    PopUpManager.addPopUp(usuarioLogForm, application, true);
    PopUpManager.centerPopUp(usuarioLogForm);
  }

  /**
   * Método responsável por executar a Action referente ao relatório
   * de log de acessos por cooperado.
   */
  public function executeActionActAcessoUsuarioLog(sender:Object):void {
    var acessoUsuarioLogForm:AcessoUsuarioLogForm = new AcessoUsuarioLogForm();
    PopUpManager.addPopUp(acessoUsuarioLogForm, application, true);
    PopUpManager.centerPopUp(acessoUsuarioLogForm);
  }

  public function executeActionActIntegracaoManualAvicultura(sender:Object):void {
    var integracaoManualAvicultura:IntegracaoManualAviculturaForm = new IntegracaoManualAviculturaForm();
    PopUpManager.addPopUp(integracaoManualAvicultura, application, true);
    PopUpManager.centerPopUp(integracaoManualAvicultura);
  }

}
}
