package br.com.db1.portalcooperado.controller
{
	import br.com.db1.core.controller.FormController;
	import br.com.db1.dialogs.DB1Alert;
	
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	
	import mx.managers.CursorManager;

	
	public class DesignController extends FormController
	{
		
		[Bindable]
		public var lblNovos: String = "";
		
		[Bindable]
		public var fileName:String;
		
	
		[Bindable]
		public var fileSize:Number = 0;
		
		[Bindable]
		public var fileProgressUpload:Number = 0;
		
		[Bindable]
		public var fileUploading:Boolean = false;
		
		[Bindable]
		public var fileUploaded:Boolean = false;
		
		[Bindable]
		public var fileImported:Boolean = false;
		
		private var fileTypes:FileFilter = new FileFilter("Todos os Arquivos", "*.*");
		
		public var fileRef:FileReference;
		
		public function DesignController(logOwner:Object = null)
		{
			super(logOwner);
			fileRef = new FileReference();
		}
		
		public function loadFile():void
		{
			fileRef.addEventListener(Event.SELECT, onFileSelected);
			fileRef.addEventListener(Event.CANCEL, onFileCancel);
			fileRef.browse([fileTypes]);
		}
		
		private function onFileCancel(evt:Event):void
		{
			fileUploading = false;
			CursorManager.removeBusyCursor();
		}
		
		private function onFileSelected(evt:Event):void
		{
			fileUploading = true;
			CursorManager.setBusyCursor();
			//progressLabel = "Carregando Arquivo...";
			fileProgressUpload = 0;
			fileName = null;
			fileUploaded = false;
			fileImported = false;
			fileRef.addEventListener(ProgressEvent.PROGRESS, onProgress);
			fileRef.addEventListener(Event.COMPLETE, onComplete);
			fileRef.load();
		}
		
		private function onProgress(evt:ProgressEvent):void
		{
			fileSize = evt.bytesTotal;
			fileProgressUpload = evt.bytesLoaded;
		}
		
		private function onComplete(evt:Event):void
		{
			if (fileRef && fileRef.data) {
				fileName = fileRef.name;
			} else {
				fileName = null;
			}
			//fileUploading = false;
			CursorManager.removeBusyCursor();
		}
		
		public function importFile():void
		{
			lblNovos = "";
			if (fileRef && fileRef.data) {
				DB1Alert.information("Arquivo para importação.");
			} else {
				DB1Alert.information("Primeiramente selecione o arquivo para importação.");
			}
		}
	}
}