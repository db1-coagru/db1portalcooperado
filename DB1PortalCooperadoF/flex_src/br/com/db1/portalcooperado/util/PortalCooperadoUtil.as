package br.com.db1.portalcooperado.util
{
	import br.com.db1.core.application.DB1ApplicationConfig;
	import br.com.db1.core.application.DB1DateManager;
	import br.com.db1.dialogs.DB1InvokingWindow;
	import br.com.db1.objectserver.core.model.Application;
	import br.com.db1.util.HTTPUtils;

	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;

	import mx.collections.ArrayCollection;
	import mx.utils.StringUtil;

	public class PortalCooperadoUtil
	{


		public function PortalCooperadoUtil()
		{

		}



		public static function convertNumberToCurrencyValueStr(value:Number):String
		{
			return value.toFixed(2).replace('.', ',');
		}

		public static function isBlankOrNull(string:String):Boolean
		{
			if (string == null)
			{
				return true;
			}

			if (StringUtil.trim(string).length == 0)
			{
				return true;
			}
			return false;
		}


		/**Args		 *
		 * 0 - Nome Relatorio
		 * 1 - Tipo Arquivo
		 * 2 - Search Window*/
		public static function navigateToReport(result:String, args:Array):void
		{
			var nomeArquivo:String=args[0];
			var tipoArquivo:String=args[1]
			var searchWindow:DB1InvokingWindow=args[2] as DB1InvokingWindow;

			var request:URLRequest=new URLRequest();
			var applicationServerURL:String=DB1ApplicationConfig.getInstance()["applicationServerURL"];
			var variables:URLVariables=new URLVariables();


			if (applicationServerURL)
			{

				applicationServerURL=HTTPUtils.replaceTokenContext(applicationServerURL);
				applicationServerURL=HTTPUtils.replaceTokenServerName(applicationServerURL);
				applicationServerURL=HTTPUtils.replaceTokenServerPort(applicationServerURL);
			}

			variables.fileId=result;
			variables.fileType=tipoArquivo;
			variables.fileName=nomeArquivo + "." + tipoArquivo;
			request.data=variables;
			request.method="post";
			request.url=applicationServerURL + "/download-file";
			
			if (searchWindow != null)
			{
				searchWindow.hideSearchWindow();

			}

			navigateToURL(request,"_blank");  
			
		}

		public static function getCurrentOperationDate():Date
		{
			return DB1DateManager.instance.getCurrentOperationDate();
		}

	}
}