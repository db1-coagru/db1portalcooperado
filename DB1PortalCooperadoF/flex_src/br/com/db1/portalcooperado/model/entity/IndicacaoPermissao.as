package br.com.db1.portalcooperado.model.entity {

import br.com.db1.portalcooperado.model.entity.generated._IndicacaoPermissao;

[Bindable]
[RemoteClass(alias="br.com.db1.portalcooperado.model.entity.IndicacaoPermissao")]
[Table(name="COOP_INDICACAO_PERMISSAO")]
public class IndicacaoPermissao extends _IndicacaoPermissao {

    public function IndicacaoPermissao() {
        // construtor
    }

}

}