package br.com.db1.portalcooperado.model.entity.generated
{

		import br.com.db1.core.type.DB1Date; 
		import br.com.db1.portalcooperado.model.entity.*;

 		[Bindable]
 		public class _EntregaProducao {		

			public function _EntregaProducao() {
                // construtor
			}

			private var _idRequisicao:Number; 
			private var _dtEntrega:DB1Date; 
			private var _dsProduto:String; 
			private var _cdOrdenacao:Number; 
			private var _dsLocal:String; 
			private var _nrRomaneio:String; 
			private var _nrSequencia:Number; 
			private var _nrCadPro:String; 
			private var _placa:String; 
			private var _dtEntrada:DB1Date; 
			private var _nrPadrao:Number; 
			private var _nrTipo:Number; 
			private var _pesoBruto:Number; 
			private var _pesoLiquido:Number; 
			private var _pcImpureza:Number; 
			private var _pcQuebrado:Number; 
			private var _pcArdido:Number; 
			private var _pcUmidade:Number; 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="ID_REQUISICAO" )]
			public function get idRequisicao():Number{ 
				return _idRequisicao; 
			} 

			public function set idRequisicao(value :Number):void{ 
				_idRequisicao =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="DT_ENTREGA" )]
			public function get dtEntrega():DB1Date{ 
				return _dtEntrega; 
			} 

			public function set dtEntrega(value :DB1Date):void{ 
				_dtEntrega =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="DS_PRODUTO" )]
			public function get dsProduto():String{ 
				return _dsProduto; 
			} 

			public function set dsProduto(value :String):void{ 
				_dsProduto =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="CD_ORDENACAO" )]
			public function get cdOrdenacao():Number{ 
				return _cdOrdenacao; 
			} 

			public function set cdOrdenacao(value :Number):void{ 
				_cdOrdenacao =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="DS_LOCAL" )]
			public function get dsLocal():String{ 
				return _dsLocal; 
			} 

			public function set dsLocal(value :String):void{ 
				_dsLocal =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="NR_ROMANEIO" )]
			public function get nrRomaneio():String{ 
				return _nrRomaneio; 
			} 

			public function set nrRomaneio(value :String):void{ 
				_nrRomaneio =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="NR_SEQUENCIA" )]
			public function get nrSequencia():Number{ 
				return _nrSequencia; 
			} 

			public function set nrSequencia(value :Number):void{ 
				_nrSequencia =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="NR_CAD_PRO" )]
			public function get nrCadPro():String{ 
				return _nrCadPro; 
			} 

			public function set nrCadPro(value :String):void{ 
				_nrCadPro =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="PL_VEICULO" )]
			public function get placa():String{ 
				return _placa; 
			} 

			public function set placa(value :String):void{ 
				_placa =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="DT_ENTRADA" )]
			public function get dtEntrada():DB1Date{ 
				return _dtEntrada; 
			} 

			public function set dtEntrada(value :DB1Date):void{ 
				_dtEntrada =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="NR_PADRAO" )]
			public function get nrPadrao():Number{ 
				return _nrPadrao; 
			} 

			public function set nrPadrao(value :Number):void{ 
				_nrPadrao =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="NR_TIPO" )]
			public function get nrTipo():Number{ 
				return _nrTipo; 
			} 

			public function set nrTipo(value :Number):void{ 
				_nrTipo =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="PS_BRUTO" )]
			public function get pesoBruto():Number{ 
				return _pesoBruto; 
			} 

			public function set pesoBruto(value :Number):void{ 
				_pesoBruto =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="PS_LIQUIDO" )]
			public function get pesoLiquido():Number{ 
				return _pesoLiquido; 
			} 

			public function set pesoLiquido(value :Number):void{ 
				_pesoLiquido =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="PC_IMPUREZA" )]
			public function get pcImpureza():Number{ 
				return _pcImpureza; 
			} 

			public function set pcImpureza(value :Number):void{ 
				_pcImpureza =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="PC_QUEBRADO" )]
			public function get pcQuebrado():Number{ 
				return _pcQuebrado; 
			} 

			public function set pcQuebrado(value :Number):void{ 
				_pcQuebrado =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="PC_ARDIDO" )]
			public function get pcArdido():Number{ 
				return _pcArdido; 
			} 

			public function set pcArdido(value :Number):void{ 
				_pcArdido =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="PC_UMIDADE" )]
			public function get pcUmidade():Number{ 
				return _pcUmidade; 
			} 

			public function set pcUmidade(value :Number):void{ 
				_pcUmidade =  value; 
			} 


		}

}