package br.com.db1.portalcooperado.model.entity.generated
{

		import br.com.db1.core.type.DB1Date;
		import br.com.db1.portalcooperado.model.entity.*;

 		[Bindable]
 		public class _Requisicao {		

			public function _Requisicao() {
                // construtor
			}

			private var _id:Number; 
			private var _matricula:Number; 
			private var _tpRequisicao:Number; 
			private var _tpAcao:Number; 
			private var _tpProduto:Number; 
			private var _dtInicial:DB1Date; 
			private var _dtFinal:DB1Date; 
			private var _dtRequisicao:DB1Date;
			private var _ipUsuario:String;
			private var _hrRequisicao:String;
			

			[Id]
			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator" )]
			[Column( name="ID_REQUISICAO" )]
			public function get id():Number{ 
				return _id; 
			} 

			public function set id(value :Number):void{ 
				_id =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator", length = 6, nullable = false )]
			[Column( name="MAT_USUARIO" )]
			public function get matricula():Number{ 
				return _matricula; 
			} 

			public function set matricula(value :Number):void{ 
				_matricula =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator", length = 4, nullable = false )]
			[Column( name="TP_REQUISICAO" )]
			public function get tpRequisicao():Number{ 
				return _tpRequisicao; 
			} 

			public function set tpRequisicao(value :Number):void{ 
				_tpRequisicao =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator", length = 4, nullable = false )]
			[Column( name="TP_ACAO" )]
			public function get tpAcao():Number{ 
				return _tpAcao; 
			} 

			public function set tpAcao(value :Number):void{ 
				_tpAcao =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator", length = 4, nullable = false )]
			[Column( name="TP_PRODUTO" )]
			public function get tpProduto():Number{ 
				return _tpProduto; 
			} 

			public function set tpProduto(value :Number):void{ 
				_tpProduto =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator", nullable = false )]
			[Column( name="DT_INICIAL" )]
			public function get dtInicial():DB1Date{ 
				return _dtInicial; 
			} 

			public function set dtInicial(value :DB1Date):void{ 
				_dtInicial =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator", nullable = false )]
			[Column( name="DT_FINAL" )]
			public function get dtFinal():DB1Date{ 
				return _dtFinal; 
			} 

			public function set dtFinal(value :DB1Date):void{ 
				_dtFinal =  value; 
			} 
			
			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator", nullable = false )]
			[Column( name="DT_REQUISICAO" )]
			public function get dtRequisicao():DB1Date{ 
				return _dtRequisicao; 
			} 
			
			public function set dtRequisicao(value :DB1Date):void{ 
				_dtRequisicao =  value; 
			} 

			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator", length = 15, nullable = false )]
			[Column( name="IP_USUARIO" )]
			public function get ipUsuario():String{ 
				return _ipUsuario; 
			} 
			
			public function set ipUsuario(value :String):void{ 
				_ipUsuario =  value; 
			} 
			
			[Validate( name="br.com.db1.core.validator.DB1ColumnValidator", length = 8, nullable = false )]
			[Column( name="HR_REQUISICAO" )]
			public function get hrRequisicao():String{ 
				return _hrRequisicao; 
			} 
			
			public function set hrRequisicao(value :String):void{ 
				_hrRequisicao =  value; 
			} 
		}

}