package br.com.db1.portalcooperado.model.entity.generated {

import br.com.db1.core.security.model.entity.Organization;
import br.com.db1.core.security.model.entity.Profile;
import br.com.db1.core.type.DB1Boolean;
import br.com.db1.core.type.DB1Date;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.TipoPessoa;

import mx.collections.ArrayCollection;

[Bindable]
public class _Usuario {

    public function _Usuario() {
        // construtor
    }

    private var _id:Number;
    private var _matricula:Number;
    private var _nome:String;
    private var _ativo:DB1Boolean;
    private var _email:String;
    private var _dtNascimento:DB1Date;
    private var _cpfCnpj:String;
    private var _tpPessoa:TipoPessoa;
    private var _dtCadastro:DB1Date;
    private var _hrCadastro:String;
    private var _usuarioCadastro:String;
    private var _dtAlteracao:DB1Date;
    private var _hrAlteracao:String;
    private var _usuarioAlteracao:String;
    private var _profile:Profile;
    private var _senha:String;
    private var _contraSenha:String;
    private var _alteraSenha:DB1Boolean;
    private var _organization:Organization;
    private var _idUser:Number;
    private var _enviaEmail:DB1Boolean;
    private var _mobile:DB1Boolean;
    private var _telefone:String;
    private var _indicacoes:ArrayCollection;
    private var _indicacoesIndicado:ArrayCollection;
    private var _indicado:Usuario;

    public function get descricao():String {
        return matricula + " - " + nome;
    }

    [Id]
    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator")]
    [Column(name="ID_USUARIO")]
    public function get id():Number {
        return _id;
    }

    public function set id(value:Number):void {
        _id = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=6, nullable=false)]
    [Column(name="MAT_USUARIO")]
    public function get matricula():Number {
        return _matricula;
    }

    public function set matricula(value:Number):void {
        _matricula = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=100, nullable=false)]
    [Column(name="NM_USUARIO")]
    public function get nome():String {
        return _nome;
    }

    public function set nome(value:String):void {
        _nome = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    [Column(name="BO_ATIVO")]
    public function get ativo():DB1Boolean {
        return _ativo;
    }

    public function set ativo(value:DB1Boolean):void {
        _ativo = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    [Column(name="BO_MOBILE")]
    public function get mobile():DB1Boolean {
        return _mobile;
    }

    public function set mobile(value:DB1Boolean):void {
        _mobile = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=60, nullable=false)]
    [Column(name="EM_USUARIO")]
    public function get email():String {
        return _email;
    }

    public function set email(value:String):void {
        _email = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    [Column(name="DT_NASC_USUARIO")]
    public function get dtNascimento():DB1Date {
        return _dtNascimento;
    }

    public function set dtNascimento(value:DB1Date):void {
        _dtNascimento = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false, length=14)]
    [Column(name="CPF_CNPJ_USUARIO")]
    public function get cpfCnpj():String {
        return _cpfCnpj;
    }

    public function set cpfCnpj(value:String):void {
        _cpfCnpj = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator")]
    [Column(name="TP_PESSOA")]
    public function get tpPessoa():TipoPessoa {
        return _tpPessoa;
    }

    public function set tpPessoa(value:TipoPessoa):void {
        _tpPessoa = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    [Column(name="DT_CADASTRO")]
    public function get dtCadastro():DB1Date {
        return _dtCadastro;
    }

    public function set dtCadastro(value:DB1Date):void {
        _dtCadastro = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=8)]
    [Column(name="HR_CADASTRO")]
    public function get hrCadastro():String {
        return _hrCadastro;
    }

    public function set hrCadastro(value:String):void {
        _hrCadastro = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false, length=30)]
    [Column(name="USUARIO_CADASTRO")]
    public function get usuarioCadastro():String {
        return _usuarioCadastro;
    }

    public function set usuarioCadastro(value:String):void {
        _usuarioCadastro = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator")]
    [Column(name="DT_ALTERACAO")]
    public function get dtAlteracao():DB1Date {
        return _dtAlteracao;
    }

    public function set dtAlteracao(value:DB1Date):void {
        _dtAlteracao = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=8)]
    [Column(name="HR_ALTERACAO")]
    public function get hrAlteracao():String {
        return _hrAlteracao;
    }

    public function set hrAlteracao(value:String):void {
        _hrAlteracao = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=30)]
    [Column(name="USUARIO_ALTERACAO")]
    public function get usuarioAlteracao():String {
        return _usuarioAlteracao;
    }

    public function set usuarioAlteracao(value:String):void {
        _usuarioAlteracao = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    [Column(name="ID_PROFILE")]
    public function get profile():Profile {
        return _profile;
    }

    public function set profile(value:Profile):void {
        _profile = value;
    }

    public function get senha():String {
        return _senha;
    }

    public function set senha(value:String):void {
        _senha = value;
    }

    public function get contraSenha():String {
        return _contraSenha;
    }

    public function set contraSenha(value:String):void {
        _contraSenha = value;
    }

    public function get alteraSenha():DB1Boolean {
        return _alteraSenha;
    }

    public function set alteraSenha(value:DB1Boolean):void {
        _alteraSenha = value;
    }

    public function get enviaEmail():DB1Boolean {
        return _enviaEmail;
    }

    public function set enviaEmail(value:DB1Boolean):void {
        _enviaEmail = value;
    }

    public function get organization():Organization {
        return _organization;
    }

    public function set organization(value:Organization):void {
        _organization = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator")]
    [Column(name="ID_SEC_USER")]
    public function get idUser():Number {
        return _idUser;
    }

    public function set idUser(value:Number):void {
        _idUser = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=true, length=14)]
    [Column(name="TELEFONE_USUARIO")]
    public function get telefone():String {
        return _telefone;
    }

    public function set telefone(value:String):void {
        _telefone = value;
    }


    [ArrayElementType("br.com.db1.portalcooperado.model.entity.generated._Indicacao")]
    public function get indicacoes():ArrayCollection {
        return _indicacoes;
    }

    public function set indicacoes(value:ArrayCollection):void {
        _indicacoes = value;
    }


    [ArrayElementType("br.com.db1.portalcooperado.model.entity.generated._Indicacao")]
    public function get indicacoesIndicado():ArrayCollection {
        return _indicacoesIndicado;
    }

    public function set indicacoesIndicado(value:ArrayCollection):void {
        _indicacoesIndicado = value;
    }

    public function get indicado():Usuario {
        return _indicado;
    }

    public function set indicado(value:Usuario):void {
        _indicado = value;
    }

}

}