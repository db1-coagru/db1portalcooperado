package br.com.db1.portalcooperado.model.entity {

import br.com.db1.portalcooperado.model.entity.generated._EntregaProducao;

[Bindable]
[RemoteClass(alias="br.com.db1.portalcooperado.model.entity.EntregaProducao")]
[Table(name="COOP_ENT_PROD_AGR")]
public class EntregaProducao extends _EntregaProducao {

    public function EntregaProducao() {
        // construtor
    }

}

}