package br.com.db1.portalcooperado.model.entity.generated {

import br.com.db1.portalcooperado.model.entity.*;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;

[Bindable]
public class _IndicacaoPermissao {

    public function _IndicacaoPermissao() {
        // construtor
    }

    private var _id:Number;
    private var _indicacao:Indicacao;
    private var _permissao:PermissaoIndicado;


    [Id]
    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator")]
    [Column(name="ID_INDICACAO_PERMISSAO")]
    public function get id():Number {
        return _id;
    }

    public function set id(value:Number):void {
        _id = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    [Column(name="ID_INDICACAO")]
    public function get indicacao():Indicacao {
        return _indicacao;
    }

    public function set indicacao(value:Indicacao):void {
        _indicacao = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    [Column(name="DS_PERMISSAO")]
    public function get permissao():PermissaoIndicado {
        return _permissao;
    }

    public function set permissao(value:PermissaoIndicado):void {
        _permissao = value;
    }
}

}