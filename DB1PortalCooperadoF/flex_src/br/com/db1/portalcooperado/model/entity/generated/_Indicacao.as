package br.com.db1.portalcooperado.model.entity.generated {

import br.com.db1.portalcooperado.model.entity.*;

import mx.collections.ArrayCollection;

[Bindable]
public class _Indicacao {

    public function _Indicacao() {
        // construtor
    }

    private var _id:Number;
    private var _indicador:Usuario;
    private var _indicado:Usuario;
    private var _permissoes:ArrayCollection;


    public function get permissoesAsString():String {
        var permissoesAsString:String = "";
        var count:Number = 0;
        for each (var permissao:IndicacaoPermissao in _permissoes) {
            if (permissoesAsString !== "") {
                var mediator:String = " - ";
                if(count === 3){
                    mediator = "\n";
                    count = 0;
                }
                permissoesAsString = permissoesAsString + mediator + permissao.permissao.label;
            } else {
                permissoesAsString = permissao.permissao.label;
            }
            count = count + 1;
        }
        return permissoesAsString;
    }

    [Id]
    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator")]
    [Column(name="ID_INDICACAO")]
    public function get id():Number {
        return _id;
    }

    public function set id(value:Number):void {
        _id = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    [Column(name="ID_INDICADOR")]
    public function get indicador():Usuario {
        return _indicador;
    }

    public function set indicador(value:Usuario):void {
        _indicador = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    [Column(name="ID_INDICADO")]
    public function get indicado():Usuario {
        return _indicado;
    }

    public function set indicado(value:Usuario):void {
        _indicado = value;
    }


    [ArrayElementType("br.com.db1.portalcooperado.model.entity.generated._IndicacaoPermissao")]
    public function get permissoes():ArrayCollection {
        return _permissoes;
    }

    public function set permissoes(value:ArrayCollection):void {
        _permissoes = value;
    }
}

}