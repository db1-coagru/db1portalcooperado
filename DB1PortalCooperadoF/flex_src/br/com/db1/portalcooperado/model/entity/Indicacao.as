package br.com.db1.portalcooperado.model.entity {

import br.com.db1.portalcooperado.model.entity.generated._Indicacao;

[Bindable]
[RemoteClass(alias="br.com.db1.portalcooperado.model.entity.Indicacao")]
[Table(name="COOP_INDICACAO")]
public class Indicacao extends _Indicacao {

    public function Indicacao() {
        // construtor
    }

}

}