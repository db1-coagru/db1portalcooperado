package br.com.db1.portalcooperado.model.entity {

import br.com.db1.portalcooperado.model.entity.generated._Usuario;

[Bindable]
[RemoteClass(alias="br.com.db1.portalcooperado.model.entity.Usuario")]
[Table(name="COOP_USUARIO")]
public class Usuario extends _Usuario {

    public function Usuario() {
        // construtor
    }

}

}