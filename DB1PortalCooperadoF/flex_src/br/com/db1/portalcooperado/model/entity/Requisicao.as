package br.com.db1.portalcooperado.model.entity {

import br.com.db1.portalcooperado.model.entity.generated._Requisicao;

[Bindable]
[RemoteClass(alias="br.com.db1.portalcooperado.model.entity.Requisicao")]
[Table(Name="COOP_REQUISICAO")]
public class Requisicao extends _Requisicao {

    public function Requisicao() {
        // construtor
    }

}

}