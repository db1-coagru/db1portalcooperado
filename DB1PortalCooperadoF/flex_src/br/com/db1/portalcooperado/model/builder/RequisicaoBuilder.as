package br.com.db1.portalcooperado.model.builder {
[Bindable]
[RemoteClass(alias="br.com.db1.portalcooperado.model.builder.RequisicaoBuilder")]
public class RequisicaoBuilder {

    private var _matriculaUsuario:Number;

    private var _tipoRequisicao:Number;

    private var _tipoAcao:Number;

    private var _tipoProduto:Number;

    private var _dataInicial:Date;

    private var _dataFinal:Date;

    private var _matriculaIndicador:Number;

    private var _reportType:String;

    private var _reportName:String;

    public function RequisicaoBuilder() {
        // construtor
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=6, nullable=false)]
    public function get matriculaUsuario():Number {
        return _matriculaUsuario;
    }

    public function set matriculaUsuario(value:Number):void {
        _matriculaUsuario = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=4, nullable=false)]
    public function get tipoRequisicao():Number {
        return _tipoRequisicao;
    }

    public function set tipoRequisicao(value:Number):void {
        _tipoRequisicao = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=4, nullable=false)]
    public function get tipoAcao():Number {
        return _tipoAcao;
    }

    public function set tipoAcao(value:Number):void {
        _tipoAcao = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=4, nullable=false)]
    public function get tipoProduto():Number {
        return _tipoProduto;
    }

    public function set tipoProduto(value:Number):void {
        _tipoProduto = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    public function get dataInicial():Date {
        return _dataInicial;
    }

    public function set dataInicial(value:Date):void {
        _dataInicial = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", nullable=false)]
    public function get dataFinal():Date {
        return _dataFinal;
    }

    public function set dataFinal(value:Date):void {
        _dataFinal = value;
    }

    [Validate(name="br.com.db1.core.validator.DB1ColumnValidator", length=6, nullable=true)]
    public function get matriculaIndicador():Number {
        return _matriculaIndicador;
    }

    public function set matriculaIndicador(value:Number):void {
        _matriculaIndicador = value;
    }


    public function get reportType():String {
        return _reportType;
    }

    public function set reportType(value:String):void {
        _reportType = value;
    }


    public function get reportName():String {
        return _reportName;
    }

    public function set reportName(value:String):void {
        _reportName = value;
    }

}

}