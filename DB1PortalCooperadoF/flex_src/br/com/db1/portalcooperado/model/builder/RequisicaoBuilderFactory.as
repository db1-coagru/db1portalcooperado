/**
 * Created by rafael.rasso on 06/09/2016.
 */
package br.com.db1.portalcooperado.model.builder {

public class RequisicaoBuilderFactory {

    public function RequisicaoBuilderFactory() {
        // construtor padrao
    }

    public static function posicaoFinanceira(reportType:String):RequisicaoBuilder {
        var builder:RequisicaoBuilder = new RequisicaoBuilder();
        builder.reportName = "PosicaoFinanceira";
        builder.reportType = reportType;
        builder.tipoRequisicao = 1;
        return  builder;
    }

    public static function entregaProducao(reportType:String):RequisicaoBuilder {
        var builder:RequisicaoBuilder = new RequisicaoBuilder();
        builder.reportName = "EntregaProducao";
        builder.reportType = reportType;
        builder.tipoRequisicao = 2;
        return  builder;
    }

}

}
