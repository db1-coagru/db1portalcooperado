package br.com.db1.portalcooperado.model.types {

import br.com.db1.core.type.DB1Enum;
import br.com.db1.core.type.DB1EnumClass;

[RemoteClass(alias="br.com.db1.portalcooperado.model.types.PermissaoIndicado")]
public class PermissaoIndicado extends DB1Enum {

  private static var _this:DB1EnumClass = DB1Enum.enumOf(PermissaoIndicado);


  public static const ACTEVENTOS:PermissaoIndicado = create("Eventos", 0);

  public static const ACTPOSICAOFINANCEIRA:PermissaoIndicado = create("Extrato financeiro", 3);

  public static const ACTENTREGAPRODUCAO:PermissaoIndicado = create("Extrato produção agricola", 2);

  public static const ACTFIXACAOFRANGO:PermissaoIndicado = create("Fechamento de frango", 11);

  public static const ACTNOTICIAS:PermissaoIndicado = create("Notícias", 1);

  public static const ACTRECOMENDACAOTECNICA:PermissaoIndicado = create("Recomendação técnica", 4);

  public static const ACTCAVACOMARAVALHA:PermissaoIndicado = create("Venda de cavaco/maravalha", 10);

  public static const ACTVENDAINSUMOS:PermissaoIndicado = create("Venda de insumos", 6);

  public static const ACTVENDAPECAS:PermissaoIndicado = create("Venda de peças", 8);

  public static const ACTRACAOSUPLEMENTO:PermissaoIndicado = create("Venda de ração/suplemento", 9);

  public static const ACTVENDAVETERNARIA:PermissaoIndicado = create("Venda veterinária", 7);

  public static const ACTAVICULTURA:PermissaoIndicado = create("Avicultura", 12);

  private var _codigo:Number;

  private static function create(name:String, codigo:Number):PermissaoIndicado {
    var permissaoIndicado:PermissaoIndicado = PermissaoIndicado(_this.states(name));
    permissaoIndicado._codigo = codigo;
    return permissaoIndicado;
  }

  public function PermissaoIndicado(name:String = null) {
    super(_this, name);
  }

  public static function get values():Array {
    return _this.values.slice();
  }

  public function get codigo():Number {
    return _codigo;
  }
}
}