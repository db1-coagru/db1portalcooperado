package br.com.db1.portalcooperado.model.types
{

		import br.com.db1.core.type.DB1EnumClass;
		import br.com.db1.core.type.DB1Enum;

		[RemoteClass(alias = "br.com.db1.portalcooperado.model.types.TipoPessoa")]
		public class TipoPessoa extends DB1Enum {

			private static var _this:DB1EnumClass = DB1Enum.enumOf(TipoPessoa);

			public static const F:TipoPessoa = create("FÍSICA");
			public static const J:TipoPessoa = create("JURÍDICA");

			private static function create(name:String):TipoPessoa {
				return TipoPessoa(_this.states(name));
			}

			public function TipoPessoa(name:String = null) {
				super(_this, name);
			}

			public static function get values():Array {
				return _this.values.slice();
			}

		}
}