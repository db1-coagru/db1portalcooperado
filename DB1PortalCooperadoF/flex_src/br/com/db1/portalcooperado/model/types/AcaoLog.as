package br.com.db1.portalcooperado.model.types
{

		import br.com.db1.core.type.DB1EnumClass;
		import br.com.db1.core.type.DB1Enum;

		[RemoteClass(alias = "br.com.db1.portalcooperado.model.types.AcaoLog")]
		public class AcaoLog extends DB1Enum {

			private static var _this:DB1EnumClass = DB1Enum.enumOf(AcaoLog);

			public static const INSERT:AcaoLog = create("INSERT");
			public static const DELETE:AcaoLog = create("DELETE");
			public static const UPDATE:AcaoLog = create("UPDATE");
			public static const SELECT:AcaoLog = create("SELECT");

			private static function create(name:String):AcaoLog {
				return AcaoLog(_this.states(name));
			}

			public function AcaoLog(name:String = null) {
				super(_this, name);
			}

			public static function get values():Array {
				return _this.values.slice();
			}

		}
}