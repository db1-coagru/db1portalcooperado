package br.com.db1.portalcooperado.model.types {

import br.com.db1.core.type.DB1Enum;
import br.com.db1.core.type.DB1EnumClass;

[RemoteClass(alias="br.com.db1.portalcooperado.relatorio.types.LogAcessoTipoCanal")]
public class LogAcessoTipoCanal extends DB1Enum {

    private static var _this:DB1EnumClass = DB1Enum.enumOf(LogAcessoTipoCanal);

    public static const T:LogAcessoTipoCanal = create("Todos");
    public static const P:LogAcessoTipoCanal = create("Portal");
    public static const M:LogAcessoTipoCanal = create("Mobile");

    private static function create(name:String):LogAcessoTipoCanal {
        return LogAcessoTipoCanal(_this.states(name));
    }

    public function LogAcessoTipoCanal(name:String = null) {
        super(_this, name);
    }

    public static function get values():Array {
        return [T, P, M];
    }


}
}