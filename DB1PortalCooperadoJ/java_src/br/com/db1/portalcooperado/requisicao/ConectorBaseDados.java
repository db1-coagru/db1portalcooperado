package br.com.db1.portalcooperado.requisicao;

import br.com.db1.exception.DB1Exception;
import br.com.db1.portalcooperado.model.entity.*;
import br.com.db1.portalcooperado.requisicao.consultas.ConectorBaseDadosFiltro;
import br.com.db1.portalcooperado.requisicao.consultas.ConectorBaseDadosFiltroFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.*;
import java.util.List;

import static br.com.db1.portalcooperado.requisicao.Propriedades.*;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

/**
 * Gerenciador da conexão com a base de dados externa <br />
 * Esta classe fará as consultas à base externa à aplicação para verificar
 * retorno das chamadas assíncronas
 *
 * @author João Fabrício Filho
 * @since 21/07/2011
 */
public class ConectorBaseDados {

	private static final Log log = LogFactory.getLog(ConectorBaseDados.class);

	private static final String N = "N";

	private static final String UM = "1";

	private static final String MSG_ERRO_CARREGAR_DRIVER = "Problema ao tentar carregar o driver '%s'";

	private static final String MSG_SQL_EXCEPTION = "Erro ao executar o comando SQL solicitado: '%s'";

	private static final String MSG_SQL_EXCEPTION_CONEXAO = "Problema ao tentar conectar à base de dados: '%s'";

	private static final String MSG_NAO_CONECTADO = "Não foi possível executar o comando SQL solicitado, verifique a conexão à base de dados";

	private static final String SQL_RETORNO_REQUISICAO = "SELECT BO_ACESSA_USUARIO FROM pub.COOP_INF_COMPL WHERE ID_REQUISICAO = ?";

	private static ConectorBaseDados instancia;

	private static String nomeDriver = Propriedades.getInstancia().getPropriedade(PROP_DRIVER_PROGRESS);

	private Connection conexao;

	/**
	 * Construtor privado para instanciar a conexão
	 *
	 * @throws SQLException           Exceção gerada em caso de erro de conexão
	 * @throws ClassNotFoundException Exceção gerada caso haja erro ao carregar o Driver
	 */
	private ConectorBaseDados() throws SQLException, ClassNotFoundException {
		openConnection();
	}

	/**
	 * Instancia o {@link ConectorBaseDados}, conectando à base externa à
	 * aplicação
	 */
	private static void instanciar() {
		try {
			instancia = new ConectorBaseDados();
		}
		catch (SQLException e) {
			log.error(String.format(MSG_SQL_EXCEPTION_CONEXAO, e.getMessage()), e);
			throw new DB1Exception(e);
		}
		catch (ClassNotFoundException e) {
			log.error(String.format(MSG_ERRO_CARREGAR_DRIVER, nomeDriver), e);
			throw new DB1Exception(e);
		}
	}

	/**
	 * Retorna a única referência de {@link ConectorBaseDados}
	 *
	 * @return Única referência de {@link ConectorBaseDados}
	 */
	public static ConectorBaseDados getInstancia() {
		if (instancia == null) {
			instanciar();
		}
		return instancia;
	}

	private void openConnection() throws ClassNotFoundException, SQLException {
		String url = Propriedades.getInstancia().getPropriedade(PROP_URL_BASE_PROGRESS);
		String usuario = Propriedades.getInstancia().getPropriedade(PROP_USUARIO_BASE_PROGRESS);
		String senha = Propriedades.getInstancia().getPropriedade(PROP_SENHA_BASE_PROGRESS);
		String banco = Propriedades.getInstancia().getPropriedade(PROP_DRIVER_PROGRESS);
		Class.forName(banco);
		conexao = DriverManager.getConnection(url, usuario, senha);
	}

	/**
	 * Verifica se a {@link Requisicao} com o id especificado obteve retorno
	 *
	 * @param idRequisicao Identificador da {@link Requisicao}
	 * @return Se a {@link Requisicao} obteve retorno
	 */
	public boolean existeRetorno(Long idRequisicao) {
		try {
			log.info(SQL_RETORNO_REQUISICAO);
			openConnection();
			checkConnection();
			PreparedStatement statement = conexao.prepareStatement(SQL_RETORNO_REQUISICAO);
			statement.setObject(1, idRequisicao);
			ResultSet resultSet = statement.executeQuery();

			if (resultSet.next()) {
				String str = resultSet.getString("BO_ACESSA_USUARIO");
				if (N.equalsIgnoreCase(str)) {
					resultSet.close();
					statement.close();
					conexao.close();
					throw new DB1Exception("Esta matrícula não esta habilitada para utilizar este sistema. " +
							"Por favor, entre em contato com a área de cadastro da sua unidade.");
				}
				if (UM.equalsIgnoreCase(str)) {
					resultSet.close();
					statement.close();
					conexao.close();
					return true;
				}
				else {
					resultSet.close();
					statement.close();
					conexao.close();
					throw new DB1Exception("Você não tem permissão para visualizar o relatório solicitado.");
				}
			}
			resultSet.close();
			statement.close();
			conexao.close();
			return false;
		}
		catch (Exception e) {
			log.error(String.format(MSG_SQL_EXCEPTION, e.getMessage()), e);
			throw new DB1Exception(e);
		}
	}

	public PosicaoFinanceiraMensagem remoteFindPosicaoFinanceiraMensagemById(Long id) {
		return executeFiltro(ConectorBaseDadosFiltroFactory.mensagem(id));
	}

	public List<PosicaoFinanceiraDebitos> remoteFindPosicaoFinanceiraDebitosByCondition(Long id, boolean orderBy) {
		return executeFiltro(ConectorBaseDadosFiltroFactory.debitos(id, orderBy));
	}

	public List<PosicaoFinanceiraCredito> remoteFindPosicaoFinanceiraCreditoByCondition(Long id) {
		return executeFiltro(ConectorBaseDadosFiltroFactory.creditos(id));
	}

	public List<PosicaoFinanceiraOutros> remoteFindPosicaoFinanceiraOutrosByCondition(Long id) {
		return executeFiltro(ConectorBaseDadosFiltroFactory.outros(id));
	}

	public List<EntregaProducao> remoteFindEntregaProducaoByCondition(Long id) {
		return executeFiltro(ConectorBaseDadosFiltroFactory.entregasProducao(id));
	}

	private <T> T executeFiltro(ConectorBaseDadosFiltro<T> filtro) {
		checkNotNull(filtro, "Filtro deve ser informado.");
		String comandoSQL = filtro.comandoSql();
		log.info(comandoSQL);
		T result;
		try {
			openConnection();
			checkConnection();
			PreparedStatement statement = conexao.prepareStatement(comandoSQL);
			appendParameters(filtro, statement);
			ResultSet resultSet = statement.executeQuery();
			result = filtro.handleResultSet(resultSet);
			statement.close();
			resultSet.close();
			conexao.close();
		}
		catch (Exception e) {
			log.error(String.format(MSG_SQL_EXCEPTION, e.getMessage()), e);
			throw new DB1Exception(e);
		}
		return result;
	}

	private <T> void appendParameters(ConectorBaseDadosFiltro<T> filtro, PreparedStatement statement)
			throws SQLException {
		Object[] params = filtro.parameters();
		for (int i = 0; i < params.length; i++) {
			statement.setObject(i + 1, params[i]);
		}
	}

	private void checkConnection() throws SQLException {
		if (conexao == null) {
			throw new SQLException(MSG_NAO_CONECTADO);
		}
	}
}
