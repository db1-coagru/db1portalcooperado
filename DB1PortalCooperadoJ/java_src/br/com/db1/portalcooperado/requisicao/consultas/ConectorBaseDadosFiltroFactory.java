package br.com.db1.portalcooperado.requisicao.consultas;

import br.com.db1.portalcooperado.model.entity.*;

import java.util.List;

public class ConectorBaseDadosFiltroFactory {

	private ConectorBaseDadosFiltroFactory() {

	}

	public static ConectorBaseDadosFiltro<PosicaoFinanceiraMensagem> mensagem(Long idRequisicao) {
		return PosicaoFinanceiraMensagemFiltro.of(idRequisicao);
	}

	public static ConectorBaseDadosFiltro<List<PosicaoFinanceiraDebitos>> debitos(Long idRequisicao, Boolean orderBy) {
		return PosicaoFinanceiraDebitosFiltro.of(idRequisicao, orderBy);
	}

	public static ConectorBaseDadosFiltro<List<PosicaoFinanceiraCredito>> creditos(Long idRequisicao) {
		return PosicaoFinanceiraCreditosFiltro.of(idRequisicao);
	}

	public static ConectorBaseDadosFiltro<List<PosicaoFinanceiraOutros>> outros(Long idRequisicao) {
		return PosicaoFinanceiraOutrosFiltro.of(idRequisicao);
	}

	public static ConectorBaseDadosFiltro<List<EntregaProducao>> entregasProducao(Long idRequisicao) {
		return EntregasProducaoFiltro.of(idRequisicao);
	}

}
