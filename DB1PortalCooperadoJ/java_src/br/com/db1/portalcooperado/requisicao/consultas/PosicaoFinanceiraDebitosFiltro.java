package br.com.db1.portalcooperado.requisicao.consultas;

import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraDebitos;
import jersey.repackaged.com.google.common.collect.Lists;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

final class PosicaoFinanceiraDebitosFiltro implements ConectorBaseDadosFiltro<List<PosicaoFinanceiraDebitos>> {

	private final Long idRequisicao;

	private final Boolean orderBy;

	PosicaoFinanceiraDebitosFiltro(Long idRequisicao, Boolean orderBy) {
		this.idRequisicao = idRequisicao;
		this.orderBy = orderBy;
	}

	static PosicaoFinanceiraDebitosFiltro of(Long idRequisicao, Boolean orderBy) {
		return new PosicaoFinanceiraDebitosFiltro(idRequisicao, orderBy);
	}

	@Override
	public String comandoSql() {
		if (orderBy) {
			return "SELECT * FROM pub.COOP_POS_FIN_DEB WHERE ID_REQUISICAO = ? and bo_credito = 1";
		}
		return "SELECT * FROM pub.COOP_POS_FIN_DEB WHERE ID_REQUISICAO = ? AND bo_credito <> 1";
	}

	@Override
	public Object[] parameters() {
		return new Object[] { idRequisicao };
	}

	@Override
	public List<PosicaoFinanceiraDebitos> handleResultSet(ResultSet resultSet) throws SQLException {
		List<PosicaoFinanceiraDebitos> result = Lists.newLinkedList();
		while (resultSet.next()) {
			PosicaoFinanceiraDebitos debitos = new PosicaoFinanceiraDebitos();
			debitos.setIdRequisicao(resultSet.getLong("ID_REQUISICAO"));
			debitos.setNrDocumento(resultSet.getString("NR_DOCUMENTO"));
			debitos.setSerie(resultSet.getString("SERIE"));
			debitos.setNrParcela(resultSet.getLong("NR_PARCELA"));
			debitos.setDsProduto(resultSet.getString("DS_PRODUTO"));
			debitos.setNrFilial(resultSet.getLong("NR_FILIAL"));
			debitos.setDtLancamento(resultSet.getDate("DT_LANCAMENTO"));
			debitos.setDtVencimento(resultSet.getDate("DT_VENCIMENTO"));
			debitos.setVlProduto(resultSet.getBigDecimal("VL_PRODUTO"));
			debitos.setVlJuros(resultSet.getBigDecimal("VL_JUROS"));
			debitos.setVlLiquido(resultSet.getBigDecimal("VL_LIQUIDO"));
			debitos.setCredito(resultSet.getBoolean("BO_CREDITO"));
			result.add(debitos);
		}
		return result;
	}

}
