package br.com.db1.portalcooperado.requisicao.consultas;

import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraCredito;
import jersey.repackaged.com.google.common.collect.Lists;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

final class PosicaoFinanceiraCreditosFiltro implements ConectorBaseDadosFiltro<List<PosicaoFinanceiraCredito>> {

	private final Long idRequisicao;

	PosicaoFinanceiraCreditosFiltro(Long idRequisicao) {
		this.idRequisicao = idRequisicao;
	}

	static PosicaoFinanceiraCreditosFiltro of(Long idRequisicao) {
		return new PosicaoFinanceiraCreditosFiltro(idRequisicao);
	}

	@Override
	public String comandoSql() {
		return "SELECT * FROM pub.COOP_POS_FIN_CRED where ID_REQUISICAO = ?";
	}

	@Override
	public Object[] parameters() {
		return new Object[] { idRequisicao };
	}

	@Override
	public List<PosicaoFinanceiraCredito> handleResultSet(ResultSet resultSet) throws SQLException {
		List<PosicaoFinanceiraCredito> result = Lists.newLinkedList();
		while (resultSet.next()) {
			PosicaoFinanceiraCredito credito = new PosicaoFinanceiraCredito();
			credito.setIdRequisicao(resultSet.getLong("ID_REQUISICAO"));
			credito.setNrRomaneio(resultSet.getString("NR_ROMANEIO"));
			credito.setNrSequencia(resultSet.getLong("NR_SEQUENCIA"));
			credito.setDsLocal(resultSet.getString("DS_LOCAL"));
			credito.setDsProduto(resultSet.getString("DS_PRODUTO"));
			credito.setNrPadrao(resultSet.getLong("NR_PADRAO"));
			credito.setNrTipo(resultSet.getLong("NR_TIPO"));
			credito.setPesoLiquido(resultSet.getBigDecimal("PS_LIQUIDO"));
			credito.setVlDia(resultSet.getBigDecimal("VL_DIA"));
			credito.setVlLiquido(resultSet.getBigDecimal("VL_LIQUIDO"));
			result.add(credito);
		}
		return result;
	}

}
