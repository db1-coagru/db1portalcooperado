package br.com.db1.portalcooperado.requisicao.consultas;

import br.com.db1.portalcooperado.model.entity.EntregaProducao;
import jersey.repackaged.com.google.common.collect.Lists;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

final class EntregasProducaoFiltro implements ConectorBaseDadosFiltro<List<EntregaProducao>> {

	private final Long idRequisicao;

	EntregasProducaoFiltro(Long idRequisicao) {
		this.idRequisicao = idRequisicao;
	}

	static EntregasProducaoFiltro of(Long idRequisicao) {
		return new EntregasProducaoFiltro(idRequisicao);
	}

	@Override
	public String comandoSql() {
		return "SELECT * FROM pub.COOP_ENT_PROD_AGR WHERE ID_REQUISICAO = ? ORDER BY DS_PRODUTO, DT_ENTREGA";
	}

	@Override
	public Object[] parameters() {
		return new Object[] { idRequisicao };
	}

	@Override
	public List<EntregaProducao> handleResultSet(ResultSet resultSet) throws SQLException {
		List<EntregaProducao> result = Lists.newLinkedList();
		while (resultSet.next()) {
			EntregaProducao entregaProducao = new EntregaProducao();
			entregaProducao.setIdRequisicao(resultSet.getLong("ID_REQUISICAO"));
			entregaProducao.setDtEntrega(resultSet.getDate("DT_ENTREGA"));
			entregaProducao.setDsProduto(resultSet.getString("DS_PRODUTO"));
			entregaProducao.setCdOrdenacao(resultSet.getLong("CD_ORDENACAO"));
			entregaProducao.setDsLocal(resultSet.getString("DS_LOCAL"));
			entregaProducao.setNrRomaneio(resultSet.getString("NR_ROMANEIO"));
			entregaProducao.setNrSequencia(resultSet.getLong("NR_SEQUENCIA"));
			entregaProducao.setNrCadPro(resultSet.getString("NR_CAD_PRO"));
			entregaProducao.setPlaca(resultSet.getString("PL_VEICULO"));
			entregaProducao.setNrPadrao(resultSet.getLong("NR_PADRAO"));
			entregaProducao.setNrTipo(resultSet.getLong("NR_TIPO"));
			entregaProducao.setPesoBruto(resultSet.getBigDecimal("PS_BRUTO"));
			entregaProducao.setPesoLiquido(resultSet.getBigDecimal("PS_LIQUIDO"));
			entregaProducao.setPcImpureza(resultSet.getBigDecimal("PC_IMPUREZA"));
			entregaProducao.setPcQuebrado(resultSet.getBigDecimal("PC_QUEBRADO"));
			entregaProducao.setPcArdido(resultSet.getBigDecimal("PC_ARDIDO"));
			entregaProducao.setPcUmidade(resultSet.getBigDecimal("PC_UMIDADE"));
			result.add(entregaProducao);
		}
		Collections.sort(result, orderBy());
		return result;
	}

	private Comparator<EntregaProducao> orderBy() {
		return new Comparator<EntregaProducao>() {
			@Override
			public int compare(EntregaProducao e1, EntregaProducao e2) {
				return e1.getCdOrdenacao().compareTo(e2.getCdOrdenacao()) < 0 ? +1 :
						(e1.getCdOrdenacao().compareTo(e2.getCdOrdenacao()) > 0 ? -1 :
								Integer.parseInt(e1.getNrRomaneio()) < Integer.parseInt(e2.getNrRomaneio()) ?
										-1 : 1);
			}
		};
	}

}
