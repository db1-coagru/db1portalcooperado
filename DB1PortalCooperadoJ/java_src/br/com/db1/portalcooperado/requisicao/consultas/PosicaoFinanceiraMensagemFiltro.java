package br.com.db1.portalcooperado.requisicao.consultas;

import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraMensagem;

import java.sql.ResultSet;
import java.sql.SQLException;

final class PosicaoFinanceiraMensagemFiltro implements ConectorBaseDadosFiltro<PosicaoFinanceiraMensagem> {

	private final Long idRequisicao;

	PosicaoFinanceiraMensagemFiltro(Long idRequisicao) {
		this.idRequisicao = idRequisicao;
	}

	static PosicaoFinanceiraMensagemFiltro of(Long idRequisicao) {
		return new PosicaoFinanceiraMensagemFiltro(idRequisicao);
	}

	@Override
	public String comandoSql() {
		return "SELECT * FROM pub.COOP_POS_FIN_CRED_OBS WHERE COOP_POS_FIN_CRED_OBS.ID_REQUISICAO = ?";
	}

	@Override
	public Object[] parameters() {
		return new Object[] { idRequisicao };
	}

	@Override
	public PosicaoFinanceiraMensagem handleResultSet(ResultSet resultSet) throws SQLException {
		if (resultSet.next()) {
			PosicaoFinanceiraMensagem mensagem = new PosicaoFinanceiraMensagem();
			mensagem.setIdRequisicao(resultSet.getLong("ID_REQUISICAO"));
			mensagem.setMensagem(resultSet.getString("MSG_CREDITO"));
			return mensagem;
		}
		return null;
	}

}
