package br.com.db1.portalcooperado.requisicao.consultas;

import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraOutros;
import jersey.repackaged.com.google.common.collect.Lists;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

final class PosicaoFinanceiraOutrosFiltro implements ConectorBaseDadosFiltro<List<PosicaoFinanceiraOutros>> {

	private final Long idRequisicao;

	PosicaoFinanceiraOutrosFiltro(Long idRequisicao) {
		this.idRequisicao = idRequisicao;
	}

	static PosicaoFinanceiraOutrosFiltro of(Long idRequisicao) {
		return new PosicaoFinanceiraOutrosFiltro(idRequisicao);
	}

	@Override
	public String comandoSql() {
		return "SELECT * FROM pub.COOP_POS_FIN_OUT WHERE ID_REQUISICAO = ?";
	}

	@Override
	public Object[] parameters() {
		return new Object[] { idRequisicao };
	}

	@Override
	public List<PosicaoFinanceiraOutros> handleResultSet(ResultSet resultSet) throws SQLException {
		List<PosicaoFinanceiraOutros> result = Lists.newLinkedList();
		while (resultSet.next()) {
			PosicaoFinanceiraOutros outros = new PosicaoFinanceiraOutros();
			outros.setIdRequisicao(resultSet.getLong("ID_REQUISICAO"));
			outros.setDsTipo(resultSet.getString("DS_TIPO"));
			outros.setDsProduto(resultSet.getString("DS_PRODUTO"));
			outros.setVlQuantidade(resultSet.getBigDecimal("VL_QUANTIDADE"));
			outros.setDtVencimento(resultSet.getDate("DT_VENCIMENTO"));
			outros.setVlProduto(resultSet.getBigDecimal("VL_PRODUTO"));
			result.add(outros);
		}
		return result;
	}

}
