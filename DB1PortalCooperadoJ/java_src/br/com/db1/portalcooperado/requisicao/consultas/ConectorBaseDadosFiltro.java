package br.com.db1.portalcooperado.requisicao.consultas;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface ConectorBaseDadosFiltro<T> {

	public String comandoSql();

	public Object[] parameters();

	public T handleResultSet(ResultSet resultSet) throws SQLException;

}
