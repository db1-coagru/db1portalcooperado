package br.com.db1.portalcooperado.requisicao;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import static br.com.db1.portalcooperado.requisicao.Propriedades.*;

/**
 * Socket Client
 */
public class SocketClient {

	private final Socket socket;

	private SocketClient(Socket socket) {
		this.socket = socket;
	}

	public static SocketClient newInstance() throws IOException {
		Socket client = new Socket(getPropriedadeNovo(PROP_URL_SOCKET_CONNECTOR), PROP_PORT_SOCKET_CONNECTOR);
		return new SocketClient(client);
	}

	public void close() throws IOException {
		this.socket.close();
	}

	public OutputStream getOutputStream() throws IOException {
		return socket.getOutputStream();
	}

	public InputStream getInputStream() throws IOException {
		return socket.getInputStream();
	}

}
