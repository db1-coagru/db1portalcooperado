package br.com.db1.portalcooperado.requisicao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericValidator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Servlet que inicializa juntamente com o servidor.
 * <p/>
 * Carregará o arquivo de propriedades e inicializará os executores.
 *
 * @author João Fabrício
 * @since 19/07/2011
 */
public class InicializacaoPortalCooperado extends HttpServlet {

	private static final String MSG_ENV_VAR_NAO_ATRIBUIDA = "Não foi atribuída variável de ambiente '%s'";

	private static final String MSG_CARREGANDO = "Carregando arquivo de propriedades '%s'";

	private static final String MSG_ARQUIVO_NAO_ENCONTRADO = "Arquivo '%s' não encontrado";

	private static final String MSG_ERRO_IO = "Problema ao ler o arquivo '%s'";

	private static final String PORTAL_COOPERADO_HOME = "PORTAL_COOPERADO_HOME";

	private static final String NOME_ARQUIVO_PROPRIEDADES = "portalCooperado.properties";

	private static final long serialVersionUID = -6089578695075124033L;

	Log log = LogFactory.getLog(this.getClass());

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init() throws ServletException {
		String portalCoopHome = System.getenv(PORTAL_COOPERADO_HOME);
		if (GenericValidator.isBlankOrNull(portalCoopHome)) {
			log.error(String.format(MSG_ENV_VAR_NAO_ATRIBUIDA, PORTAL_COOPERADO_HOME));
		}
		else {
			log.info(String.format(MSG_CARREGANDO, NOME_ARQUIVO_PROPRIEDADES));
			String caminhoArquivo = portalCoopHome.concat(File.separator.concat(NOME_ARQUIVO_PROPRIEDADES));
			try {
				Propriedades.getInstancia().carregar(caminhoArquivo);
			}
			catch (FileNotFoundException e) {
				log.error(String.format(MSG_ARQUIVO_NAO_ENCONTRADO, caminhoArquivo), e);
			}
			catch (IOException e) {
				log.error(String.format(MSG_ERRO_IO, caminhoArquivo), e);
			}
		}
		super.init();
	}

}
