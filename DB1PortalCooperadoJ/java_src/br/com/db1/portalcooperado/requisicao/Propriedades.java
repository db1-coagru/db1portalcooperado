package br.com.db1.portalcooperado.requisicao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Representação do arquivo de propriedades carregado ao iniciar a aplicação.
 *
 * @author João Fabrício
 * @since 19/07/2011
 */
public class Propriedades {

	public static final String PROP_LINHA_COMANDO_PROGRESS = "linha.comando.progress";

	public static final String PROP_URL_BASE_PROGRESS = "url.base.progress";

	public static final String PROP_USUARIO_BASE_PROGRESS = "usuario.base.progress";

	public static final String PROP_SENHA_BASE_PROGRESS = "senha.base.progress";

	public static final String PROP_DRIVER_PROGRESS = "driver.progress";

	public static final Integer PROP_PORT_SOCKET_CONNECTOR = 10500;

	public static final String PROP_URL_SOCKET_CONNECTOR = "socket.url.connector";

	public static final String PROP_URL_API_CAPPTAN = "url.api.capptan";

	public static final String PROP_RECOMENDACAO_TECNICA_PROCESSADO_FOLDER = "recomendacao.tecnica.processado.folder";

	/**
	 * Única referência de {@link Propriedades}
	 */
	private static final Propriedades INSTANCIA = new Propriedades();

	/**
	 * {@link Properties} carregado do arquivo
	 */
	private Properties properties = new Properties();

	/**
	 * Nome do arquivo carregado
	 */
	private String nomeArquivoPropriedades;

	/**
	 * Construtor privado
	 */
	private Propriedades() {
	}

	/**
	 * Retorna a única instância de {@link Propriedades}
	 *
	 * @return Referência para a única instância de {@link Propriedades}
	 */
	public static Propriedades getInstancia() {
		return INSTANCIA;
	}

	public static String getPropriedadeNovo(String nomePropriedade) {
		Propriedades instancia = getInstancia();
		if (instancia.contemPropriedade(nomePropriedade)) {
			return instancia.properties.getProperty(nomePropriedade);
		}
		return "";
	}

	/**
	 * Carrega as propriedades do arquivo com o nome parametrizado.
	 *
	 * @param nomeArquivoPropriedades Nome do arquivo de propriedades a ser carregado
	 * @throws FileNotFoundException Exceção gerada caso o arquivo não seja encontrado
	 * @throws IOException           Exceção de E/S
	 */
	public void carregar(String nomeArquivoPropriedades) throws IOException {
		this.nomeArquivoPropriedades = nomeArquivoPropriedades;
		properties.load(new FileInputStream(nomeArquivoPropriedades));
	}

	/**
	 * Retorna o valor da propriedade com o nome especificado, ou String vazia
	 * caso esta não exista
	 *
	 * @param nomePropriedade Nome da propriedade a ser buscada
	 * @return Valor da propriedade ou String vazia se ela não existir
	 */
	public String getPropriedade(String nomePropriedade) {
		if (contemPropriedade(nomePropriedade)) {
			return properties.getProperty(nomePropriedade);
		}
		return "";
	}

	/**
	 * Verifica se a propriedade com o nome especificado existe no
	 * {@link Properties} carregado
	 *
	 * @param paramNomePropriedade Nome da propriedade a ser verificada
	 * @return Se a propriedade existe ou não
	 */
	private boolean contemPropriedade(final String paramNomePropriedade) {
		return properties.containsKey(paramNomePropriedade);
	}

	/**
	 * Retorna o nome do arquivo de propriedades carregado
	 *
	 * @return Nome do arquivo de propriedades carregado
	 */
	public String getNomeArquivoPropriedades() {
		return nomeArquivoPropriedades;
	}

}
