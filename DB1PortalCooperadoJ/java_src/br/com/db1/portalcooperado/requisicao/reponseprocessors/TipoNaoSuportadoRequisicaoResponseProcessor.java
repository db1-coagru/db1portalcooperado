package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.responses.Responses;

import javax.ws.rs.core.Response;

class TipoNaoSuportadoRequisicaoResponseProcessor implements RequisicaoResponseProcessor {

	private TipoNaoSuportadoRequisicaoResponseProcessor() {

	}

	public static TipoNaoSuportadoRequisicaoResponseProcessor of() {
		return new TipoNaoSuportadoRequisicaoResponseProcessor();
	}

	@Override
	public Response processar(Requisicao requisicao, RequisicaoJson requisicaoJson) {
		return Responses.notAcceptable(MensagemJson.of("Tipo de requisição não suportado!"));
	}

}
