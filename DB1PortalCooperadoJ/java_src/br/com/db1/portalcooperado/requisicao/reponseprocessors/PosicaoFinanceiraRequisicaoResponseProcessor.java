package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.service.PosicaoFinanceiraService;
import br.com.db1.portalcooperado.util.RequisicaoWaitReturn;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response;

class PosicaoFinanceiraRequisicaoResponseProcessor extends AbstractRequisicaoResponseProcessor {

	private final PosicaoFinanceiraService posicaoFinanceiraService;

	@Autowired
	public PosicaoFinanceiraRequisicaoResponseProcessor(PosicaoFinanceiraService posicaoFinanceiraService) {
		super(TipoRequisicao.POSICAO_FINANCEIRA);
		this.posicaoFinanceiraService = posicaoFinanceiraService;
	}

	static PosicaoFinanceiraRequisicaoResponseProcessor of(PosicaoFinanceiraService posicaoFinanceiraService) {
		return new PosicaoFinanceiraRequisicaoResponseProcessor(posicaoFinanceiraService);
	}

	@Override
	public Response processar(Requisicao requisicao, RequisicaoJson requisicaoJson) {
		validarTipo(requisicaoJson.getTipoRequisicao());
		validarTipo(requisicao.getTipoRequisicao());
		RequisicaoWaitReturn.waitReturn(requisicao);
		return posicaoFinanceiraService.posicaoFinanceiraResponseOf(requisicao, requisicaoJson.isExportaPdf());
	}

}
