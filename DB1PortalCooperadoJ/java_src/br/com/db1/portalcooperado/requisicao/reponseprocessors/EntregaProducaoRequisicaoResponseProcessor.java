package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.service.EntregaProducaoService;
import br.com.db1.portalcooperado.util.RequisicaoWaitReturn;

import javax.ws.rs.core.Response;

class EntregaProducaoRequisicaoResponseProcessor extends AbstractRequisicaoResponseProcessor {

	private final EntregaProducaoService entregaProducaoService;

	private EntregaProducaoRequisicaoResponseProcessor(EntregaProducaoService entregaProducaoService) {
		super(TipoRequisicao.ENTREGAS_PRODUCAO);
		this.entregaProducaoService = entregaProducaoService;
	}

	static EntregaProducaoRequisicaoResponseProcessor of(EntregaProducaoService entregaProducaoService) {
		return new EntregaProducaoRequisicaoResponseProcessor(entregaProducaoService);
	}

	@Override
	public Response processar(Requisicao requisicao, RequisicaoJson requisicaoJson) {
		validarTipo(requisicaoJson.getTipoRequisicao());
		validarTipo(requisicao.getTipoRequisicao());
		RequisicaoWaitReturn.waitReturn(requisicao);
		return entregaProducaoService.entregasProducaoResponseOf(requisicao, requisicaoJson.isExportaPdf());
	}

}
