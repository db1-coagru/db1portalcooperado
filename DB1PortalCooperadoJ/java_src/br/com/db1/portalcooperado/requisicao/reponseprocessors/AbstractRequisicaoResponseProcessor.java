package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;

import static jersey.repackaged.com.google.common.base.Preconditions.checkArgument;

abstract class AbstractRequisicaoResponseProcessor implements RequisicaoResponseProcessor {

	private static final String MENSAGEM_TIPO_IMCOMPATIVEL = "Tipos incompatíves. Esperado %s - Recebido %s";

	private final TipoRequisicao processo;

	AbstractRequisicaoResponseProcessor(TipoRequisicao processo) {
		this.processo = processo;
	}

	void validarTipo(TipoRequisicao processar) {
		checkArgument(processo.equals(processar), String.format(MENSAGEM_TIPO_IMCOMPATIVEL, processo, processar));
	}

}
