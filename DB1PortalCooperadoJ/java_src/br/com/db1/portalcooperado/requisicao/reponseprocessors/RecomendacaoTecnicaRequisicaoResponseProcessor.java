package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.service.RecomendacaoTecnicaService;

import javax.ws.rs.core.Response;

class RecomendacaoTecnicaRequisicaoResponseProcessor extends AbstractRequisicaoResponseProcessor {

	private final RecomendacaoTecnicaService recomendacaoTecnicaService;

	private RecomendacaoTecnicaRequisicaoResponseProcessor(RecomendacaoTecnicaService recomendacaoTecnicaService) {
		super(TipoRequisicao.RECOMENDACAO_TECNICA);
		this.recomendacaoTecnicaService = recomendacaoTecnicaService;
	}

	public static RecomendacaoTecnicaRequisicaoResponseProcessor of(
			RecomendacaoTecnicaService recomendacaoTecnicaService) {
		return new RecomendacaoTecnicaRequisicaoResponseProcessor(recomendacaoTecnicaService);
	}

	@Override
	public Response processar(Requisicao requisicao, RequisicaoJson requisicaoJson) {
		validarTipo(requisicaoJson.getTipoRequisicao());
		validarTipo(requisicao.getTipoRequisicao());
		Long matricula = requisicao.getMatricula();
		Long idRecomendacaoTecnica = requisicaoJson.getIdRecomendacaoTecnica();
		return recomendacaoTecnicaService.recomendacoesTecnicasResponseOf(matricula, idRecomendacaoTecnica);
	}

}
