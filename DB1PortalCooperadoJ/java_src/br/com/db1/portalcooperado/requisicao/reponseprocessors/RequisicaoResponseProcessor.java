package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;

import javax.ws.rs.core.Response;

public interface RequisicaoResponseProcessor {

	Response processar(Requisicao requisicao, RequisicaoJson requisicaoJson);

}
