package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.dao.AviculturaDao;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturasJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.infrastructure.rest.responses.Responses;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaToAviculturaJsonTranslator;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturasToAviculturasJsonTranslator;
import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;

import javax.ws.rs.core.Response;
import java.util.List;

class AviculturaRequisicaoResponseProcessor extends AbstractRequisicaoResponseProcessor {

	private final AviculturaDao aviculturaDao;

	private final AviculturasToAviculturasJsonTranslator aviculturasToAviculturasJsonTranslator;

	private final AviculturaToAviculturaJsonTranslator aviculturaToAviculturaJsonTranslator;

	private AviculturaRequisicaoResponseProcessor(AviculturaDao aviculturaDao,
			AviculturasToAviculturasJsonTranslator aviculturasToAviculturasJsonTranslator,
			AviculturaToAviculturaJsonTranslator aviculturaToAviculturaJsonTranslator) {
		super(TipoRequisicao.AVICULTURA);
		this.aviculturaDao = aviculturaDao;
		this.aviculturasToAviculturasJsonTranslator = aviculturasToAviculturasJsonTranslator;
		this.aviculturaToAviculturaJsonTranslator = aviculturaToAviculturaJsonTranslator;
	}

	static AviculturaRequisicaoResponseProcessor of(AviculturaDao aviculturaDao,
			AviculturasToAviculturasJsonTranslator aviculturasToAviculturasJsonTranslator,
			AviculturaToAviculturaJsonTranslator aviculturaToAviculturaJsonTranslator) {
		return new AviculturaRequisicaoResponseProcessor(aviculturaDao, aviculturasToAviculturasJsonTranslator,
				aviculturaToAviculturaJsonTranslator);
	}

	@Override
	public Response processar(Requisicao requisicao, RequisicaoJson requisicaoJson) {
		validarTipo(requisicao.getTipoRequisicao());
		validarTipo(requisicaoJson.getTipoRequisicao());
		Long idAvicultura = requisicaoJson.getIdAvicultura();
		if (idAvicultura == null) {
			List<Avicultura> aviculturas = aviculturaDao.findAllByMatricula(requisicao.getMatricula());
			AviculturasJson aviculturasJson = aviculturasToAviculturasJsonTranslator.translate(aviculturas);
			return Responses.ok(aviculturasJson);
		}
		Avicultura avicultura = aviculturaDao.findById(idAvicultura);
		AviculturaJson aviculturaJson = aviculturaToAviculturaJsonTranslator.translate(avicultura);
		return Responses.ok(aviculturaJson);
	}

}
