package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.dao.AviculturaDao;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaToAviculturaJsonTranslator;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturasToAviculturasJsonTranslator;
import br.com.db1.portalcooperado.service.EntregaProducaoService;
import br.com.db1.portalcooperado.service.PosicaoFinanceiraService;
import br.com.db1.portalcooperado.service.RecomendacaoTecnicaService;

public final class RequisicaoResponseProcessorFactory {

	private RequisicaoResponseProcessorFactory() {

	}

	public static RequisicaoResponseProcessor posicaoFinanceira(PosicaoFinanceiraService posicaoFinanceiraService) {
		return PosicaoFinanceiraRequisicaoResponseProcessor.of(posicaoFinanceiraService);
	}

	public static RequisicaoResponseProcessor entregaProducao(EntregaProducaoService entregaProducaoService) {
		return EntregaProducaoRequisicaoResponseProcessor.of(entregaProducaoService);
	}

	public static RequisicaoResponseProcessor recomendacaoTecnica(
			RecomendacaoTecnicaService recomendacaoTecnicaService) {
		return RecomendacaoTecnicaRequisicaoResponseProcessor.of(recomendacaoTecnicaService);
	}

	public static RequisicaoResponseProcessor avicultura(AviculturaDao aviculturaDao,
			AviculturasToAviculturasJsonTranslator aviculturasToAviculturasJsonTranslator,
			AviculturaToAviculturaJsonTranslator aviculturaToAviculturaJsonTranslator) {
		return AviculturaRequisicaoResponseProcessor
				.of(aviculturaDao, aviculturasToAviculturasJsonTranslator, aviculturaToAviculturaJsonTranslator);
	}

	public static RequisicaoResponseProcessor tipoNaoSuportado() {
		return TipoNaoSuportadoRequisicaoResponseProcessor.of();
	}
}
