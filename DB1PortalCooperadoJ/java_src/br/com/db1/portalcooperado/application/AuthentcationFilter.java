package br.com.db1.portalcooperado.application;

import br.com.db1.portalcooperado.infrastructure.rest.annotations.Secured;
import br.com.db1.portalcooperado.infrastructure.rest.exceptions.CustomUnauthorizedWebApplicationException;
import br.com.db1.portalcooperado.service.PortalTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Esta classe é responsável por filtrar as requisições anotadadas com @Secured
 * e validar o token existente no header é válido.
 */
@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
@Component
public class AuthentcationFilter implements ContainerRequestFilter {

	@Autowired
	private PortalTokenService portalTokenService;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String authorizationHeader = obtemAuthorizationHeader(requestContext);
		validateToken(authorizationHeader);
	}

	private String obtemAuthorizationHeader(ContainerRequestContext requestContext) {
		String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		if (authorizationHeader == null) {
			throw new CustomUnauthorizedWebApplicationException("Token de autenticação deve ser informado. Contate o suporte.");
		}
		return authorizationHeader;
	}

	private void validateToken(String token) {
		if (!portalTokenService.validateToken(token)) {
			throw new CustomUnauthorizedWebApplicationException("Token do usuário expirado ou inválido. Efetue o login novamente.");
		}
	}
}
