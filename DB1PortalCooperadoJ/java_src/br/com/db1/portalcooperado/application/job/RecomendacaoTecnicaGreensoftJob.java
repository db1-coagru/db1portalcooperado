package br.com.db1.portalcooperado.application.job;

import br.com.db1.portalcooperado.infrastructure.job.AbstractJob;
import br.com.db1.portalcooperado.model.task.RecomendacaoTecnicaGreensoftTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class RecomendacaoTecnicaGreensoftJob extends AbstractJob {

	@Value(value = "${recomendacao.tecnica.greensoft.job.enable}")
	private Boolean enabled;

	@Autowired
	protected RecomendacaoTecnicaGreensoftJob(RecomendacaoTecnicaGreensoftTask recomendacaoTecnicaGreensoftTask) {
		super(recomendacaoTecnicaGreensoftTask);
	}

	@Override
	public Boolean isEnabled() {
		return enabled;
	}

}
