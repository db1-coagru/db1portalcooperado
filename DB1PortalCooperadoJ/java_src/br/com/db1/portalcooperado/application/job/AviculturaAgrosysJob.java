package br.com.db1.portalcooperado.application.job;

import br.com.db1.portalcooperado.infrastructure.job.AbstractJob;
import br.com.db1.portalcooperado.model.task.AviculturaAgrosysTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class AviculturaAgrosysJob extends AbstractJob {

	@Value(value = "${avicultura.agrosys.job.enable}")
	private Boolean enabled;

	@Autowired
	protected AviculturaAgrosysJob(AviculturaAgrosysTask aviculturaAgrosysTask) {
		super(aviculturaAgrosysTask);
	}

	@Override
	public Boolean isEnabled() {
		return enabled;
	}

}
