package br.com.db1.portalcooperado.application.job;

import br.com.db1.portalcooperado.infrastructure.job.AbstractJob;
import br.com.db1.portalcooperado.model.task.ProgressNotificacaoERPTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class ProgressNotificacaoERPJob extends AbstractJob {

	@Value(value = "${progress.notificacao.erp.job.enable}")
	private Boolean enabled;

	@Autowired
	protected ProgressNotificacaoERPJob(ProgressNotificacaoERPTask progressNotificacaoERPTask) {
		super(progressNotificacaoERPTask);
	}

	@Override
	public Boolean isEnabled() {
		return enabled;
	}

}
