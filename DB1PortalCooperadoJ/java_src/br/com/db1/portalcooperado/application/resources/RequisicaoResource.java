package br.com.db1.portalcooperado.application.resources;

import br.com.db1.exception.DB1Exception;
import br.com.db1.portalcooperado.service.impl.ProcessadorRequisicaoService;
import br.com.db1.portalcooperado.infrastructure.rest.annotations.Secured;
import br.com.db1.portalcooperado.infrastructure.rest.exceptions.CustomNotAcceptableWebApplicationException;
import br.com.db1.portalcooperado.infrastructure.rest.exceptions.CustomServerErrorWebApplicationException;
import br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.responses.Responses;
import flex.messaging.FlexContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@Component
@Path("requisicao")
public class RequisicaoResource {

	private static final String ERRO_GERAR_REQUISICAO = "Erro ao gerar requisição.";

	private static final String ERRO_RECUPERAR_RELATORIO = "Erro ao recuperar o relatório.";

	private static final Log logger = LogFactory.getLog(RequisicaoResource.class);

	private final ProcessadorRequisicaoService processadorRequisicaoService;

	@Autowired
	public RequisicaoResource(ProcessadorRequisicaoService processadorRequisicaoService) {
		this.processadorRequisicaoService = processadorRequisicaoService;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional
	@Secured
	public Response request(RequisicaoJson json, @HeaderParam("authorization") String authorization) {
		if (json != null) {
			try {
				return processadorRequisicaoService.processResponse(json, authorization);
			}
			catch (DB1Exception dbex) {
				logger.error(ERRO_GERAR_REQUISICAO, dbex);
				return Responses.notAcceptable(dbex);
			}
			catch (WebApplicationException wbex) {
				logger.error(ERRO_GERAR_REQUISICAO, wbex);
				return wbex.getResponse();
			}
			catch (Exception ex) {
				logger.error(ERRO_GERAR_REQUISICAO, ex);
				return Responses.serverError();
			}
		}
		logger.error("Erro ao gerar requisição - JSON não encontrado");
		return Responses.notAcceptable(MensagemJson.newMensagemJsonNaoLocalizado());
	}

	@Path("/export")
	@GET
	@Produces({ "application/pdf", MediaType.APPLICATION_JSON })
	@Secured
	public Response request(@QueryParam("idreport") String idReport) {
		try {
			checkNotNull(idReport, "Id do relatório deve ser informado.");
			Object report = FlexContext.getServletContext().getAttribute(idReport);
			checkNotNull(report, "Relatório não encontrado no servidor. Contate o suporte!");
			return Response.ok(report, "application/pdf").build();

		}
		catch (DB1Exception dbex) {
			logger.error(ERRO_RECUPERAR_RELATORIO, dbex);
			throw new CustomNotAcceptableWebApplicationException(dbex);
		}
		catch (Exception ex) {
			logger.error(ERRO_RECUPERAR_RELATORIO, ex);
			throw new CustomServerErrorWebApplicationException(ex);
		}
	}

}
