package br.com.db1.portalcooperado.application.resources;

import br.com.db1.exception.DB1Exception;
import br.com.db1.portalcooperado.model.entity.Token;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.infrastructure.rest.exceptions.CustomNotAcceptableWebApplicationException;
import br.com.db1.portalcooperado.infrastructure.rest.exceptions.CustomServerErrorWebApplicationException;
import br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.CredencialJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson;
import br.com.db1.portalcooperado.infrastructure.rest.responses.Responses;
import br.com.db1.portalcooperado.service.PortalTokenService;
import br.com.db1.portalcooperado.service.UsuarioService;
import br.com.db1.security.model.entity.User;
import br.com.db1.security.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("login")
public class LoginResource {

	private static final Log logger = LogFactory.getLog(LoginResource.class);

	private final UserService userService;

	private final PortalTokenService portalTokenService;

	private final UsuarioService usuarioService;

	@Autowired
	public LoginResource(UserService userService,
			PortalTokenService portalTokenService,
			UsuarioService usuarioService) {
		this.userService = userService;
		this.portalTokenService = portalTokenService;
		this.usuarioService = usuarioService;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Transactional
	public Response authenticate(CredencialJson credencialJson) {
		if (credencialJson != null) {
			try {
				String matricula = credencialJson.getMatricula();
				User userSecurity = userService
						.authenticate(matricula, credencialJson.getSenha(), credencialJson.getDomain());
				if (userSecurity != null) {
					Usuario usuarioPortal = usuarioService.buscaUsuarioFull(matricula);
					return processResponse(usuarioPortal, userSecurity);
				}
			}
			catch (DB1Exception dbex) {
				logger.error("Erro ao autenticar usuário", dbex);
				throw new CustomNotAcceptableWebApplicationException(dbex);
			}
			catch (Exception ex) {
				logger.error("Erro ao autenticar usuário", ex);
				throw new CustomServerErrorWebApplicationException(ex);
			}
		}
		logger.error("Erro ao autenticar usuário - JSON não localizado");
		return Responses.notAcceptable(MensagemJson.newMensagemJsonNaoLocalizado());
	}

	private Response processResponse(Usuario usuarioPortal, User userSecurity) {
		if (usuarioPortal.getMobile()) {
			Token token = portalTokenService.generateTokenTo(userSecurity);
			UsuarioJson usuarioJson = UsuarioJson.of(usuarioPortal, token.toJson());
			return Responses.ok(usuarioJson);
		}
		return Responses.unauthorized(MensagemJson.newMensagemSemAcessoMobile());
	}

}
