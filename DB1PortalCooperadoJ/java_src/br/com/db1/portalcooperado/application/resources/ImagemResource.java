package br.com.db1.portalcooperado.application.resources;

import br.com.db1.portalcooperado.requisicao.Propriedades;
import br.com.db1.portalcooperado.infrastructure.rest.responses.Responses;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;

@Component
@Path("imagem")
public class ImagemResource {

    private static final Log logger = LogFactory.getLog(ImagemResource.class);

    private String caminho = Propriedades.getPropriedadeNovo(Propriedades.PROP_RECOMENDACAO_TECNICA_PROCESSADO_FOLDER);

    @GET
    @Produces({"image/jpeg", "image/gif", "image/png", "image/jpg", "imagem/svg"})
    @Transactional
    public Response getImage(@QueryParam(value = "name") String name) {
        try {
            File file = new File(caminho, name);
            if (file.exists()) {
                FileInputStream stream = new FileInputStream(file);
                byte[] bytesFile = new byte[(int) file.length()];
                int read = stream.read(bytesFile);
                stream.close();
                checkArgument(read > 0, "Problema na leitura da imagem.");
                return Responses.ok(bytesFile);
            } else {
                logger.info(String.format("Imagem %s não encontrada em %s", name, caminho));
            }
        } catch (Exception e) {
            logger.error("Não foi possível recuperar a imagem.", e);
        }
        return Responses.ok(null);
    }

}
