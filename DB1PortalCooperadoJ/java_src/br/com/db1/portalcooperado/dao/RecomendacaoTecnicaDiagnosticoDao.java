package br.com.db1.portalcooperado.dao;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaDiagnostico;

public interface RecomendacaoTecnicaDiagnosticoDao extends GenericMyBatisDao<RecomendacaoTecnicaDiagnostico, Long> {

}