package br.com.db1.portalcooperado.dao;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDao;
import br.com.db1.portalcooperado.model.entity.Token;

public interface TokenDao extends GenericMyBatisDao<Token, Long> {

	Token findByAccessToken(String accessToken);

}