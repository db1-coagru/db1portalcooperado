package br.com.db1.portalcooperado.dao;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDao;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItem;

import java.util.List;

public interface AviculturaItemDao extends GenericMyBatisDao<AviculturaItem, Long> {

	void save(List<AviculturaItem> itens, Avicultura avicultura);

}