package br.com.db1.portalcooperado.dao;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDao;
import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXml;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;

import java.util.List;

public interface AviculturaDao extends GenericMyBatisDao<Avicultura, Long> {

	List<Avicultura> findAllByMatricula(Object matricula);

	Avicultura findOneByAviculturaXml(AviculturaXml aviculturaXml);

}