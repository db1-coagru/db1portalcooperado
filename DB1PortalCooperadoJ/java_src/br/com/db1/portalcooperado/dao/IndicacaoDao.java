package br.com.db1.portalcooperado.dao;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDao;
import br.com.db1.portalcooperado.model.entity.Indicacao;

import java.util.List;

public interface IndicacaoDao extends GenericMyBatisDao<Indicacao, Long> {

	void saveOrUpdateAll(List<Indicacao> indicacoes);

	void removeAll(List<Indicacao> indicacoesRemover);
}
