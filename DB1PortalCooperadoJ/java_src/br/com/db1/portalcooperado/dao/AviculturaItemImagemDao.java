package br.com.db1.portalcooperado.dao;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDao;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItem;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItemImagem;

import java.util.List;

public interface AviculturaItemImagemDao extends GenericMyBatisDao<AviculturaItemImagem, Long> {

	void save(List<AviculturaItemImagem> imagens,
			AviculturaItem item);

}