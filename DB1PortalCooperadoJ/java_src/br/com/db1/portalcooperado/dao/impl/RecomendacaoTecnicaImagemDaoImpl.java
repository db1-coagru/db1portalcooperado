package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaImagemDao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaImagem;
import org.springframework.stereotype.Repository;

@Repository("recomendacaoTecnicaImagemDao")
public class RecomendacaoTecnicaImagemDaoImpl extends GenericMyBatisDaoImpl<RecomendacaoTecnicaImagem, Long>
		implements RecomendacaoTecnicaImagemDao {

	/**
	 * Construtor padrão da implementação do <code>Dao</code>.
	 */
	public RecomendacaoTecnicaImagemDaoImpl() {
		super(RecomendacaoTecnicaImagem.class);
	}

}