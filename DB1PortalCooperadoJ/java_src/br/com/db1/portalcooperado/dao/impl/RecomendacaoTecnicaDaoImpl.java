package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.myBatisPersistence.util.MyBatisUtil;
import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaDao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository("recomendacaoTecnicaDao")
public class RecomendacaoTecnicaDaoImpl extends GenericMyBatisDaoImpl<RecomendacaoTecnica, Long>
		implements RecomendacaoTecnicaDao {

	private static final String FIND_BY_NOME_ARQUIVO = "findByNomeArquivo";

	private static final String FIND_LAST_NINETY_DAYS_BY_MATRICULA = "findLastNinetyDaysByMatricula";

	private static final String PARAM_NOME_ARQUIVO = "nomeArquivo";

	private static final String PARAM_MATRICULA = "matricula";

	private static final String PARAM_INICIO = "inicio";

	private static final String PARAM_FIM = "fim";

	/**
	 * Construtor padrão da implementação do <code>Dao</code>.
	 */
	public RecomendacaoTecnicaDaoImpl() {
		super(RecomendacaoTecnica.class);
	}

	@Override
	public RecomendacaoTecnica findOneByFileName(String fileName) {
		return findOne(FIND_BY_NOME_ARQUIVO, MyBatisUtil.createMap(PARAM_NOME_ARQUIVO, fileName));
	}

	@Override
	public List<RecomendacaoTecnica> findLastNinetyDaysByMatricula(Long matricula) {
		Date fim = PortalCooperadoUtil.getDataAtEndDay();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, -90);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date inicio = calendar.getTime();

		Map<String, Object> map = MyBatisUtil.createMap(PARAM_MATRICULA, matricula, PARAM_INICIO, inicio, PARAM_FIM, fim);
		return find(FIND_LAST_NINETY_DAYS_BY_MATRICULA, map);
	}
}