package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.myBatisPersistence.util.MyBatisUtil;
import br.com.db1.portalcooperado.dao.AviculturaDao;
import br.com.db1.portalcooperado.dao.AviculturaItemDao;
import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXml;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import br.com.db1.portalcooperado.util.DateFormatter;
import jersey.repackaged.com.google.common.base.MoreObjects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.testng.util.Strings;

import java.util.List;
import java.util.Map;

@Repository("aviculturaDao")
public class AviculturaDaoImpl extends GenericMyBatisDaoImpl<Avicultura, Long> implements AviculturaDao {

	private static final String FIND_ALL_BY_MATRICULA = "findAllByMatricula";

	private static final String FIND_ONE_BY_AVICULTURA_XML = "findOneByAviculturaXml";

	private static final String FIND_ONE_BY_AVICULTURA_XML_COM_LINHAGEM_NULA = "findOneByAviculturaXmlComLinhagemNula";

	private static final String PARAM_MATRICULA = "matricula";

	private final AviculturaItemDao aviculturaItemDao;

	@Autowired
	public AviculturaDaoImpl(AviculturaItemDao aviculturaItemDao) {
		super(Avicultura.class);
		this.aviculturaItemDao = aviculturaItemDao;
	}

	@Override
	public Avicultura save(Avicultura avicultura) {
		Avicultura saved = super.saveOrUpdate(avicultura);
		aviculturaItemDao.save(avicultura.getItens(), saved);
		return saved;
	}

	@Override
	public List<Avicultura> findAllByMatricula(Object matricula) {
		Object nonNullMatricula = MoreObjects.firstNonNull(matricula, "");
		return find(FIND_ALL_BY_MATRICULA, MyBatisUtil.createMap(PARAM_MATRICULA, nonNullMatricula.toString()));
	}

	@Override
	public Avicultura findOneByAviculturaXml(AviculturaXml aviculturaXml) {
		Map<String, Object> params = MyBatisUtil.createMap();
		params.put("cpfcnpj", aviculturaXml.getCpfCnpj());
		params.put("galpao", aviculturaXml.getGalpao());
		params.put("lote", aviculturaXml.getLote());
		params.put("sexo", aviculturaXml.getSexo());
		params.put("quantidade", aviculturaXml.getQuantidade());
		params.put("data", DateFormatter.DD_MM_YYYY.format(aviculturaXml.getData()));
		params.put("linhagem", aviculturaXml.getLinhagem());
		params.put("nomeTecnico", aviculturaXml.getNomeTecnico());
		if (Strings.isNullOrEmpty(aviculturaXml.getLinhagem())) {
			return findOne(FIND_ONE_BY_AVICULTURA_XML_COM_LINHAGEM_NULA, params);
		}
		return findOne(FIND_ONE_BY_AVICULTURA_XML, params);
	}

}