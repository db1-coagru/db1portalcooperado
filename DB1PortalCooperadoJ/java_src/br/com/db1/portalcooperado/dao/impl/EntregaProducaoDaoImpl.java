package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.EntregaProducaoDao;
import br.com.db1.portalcooperado.model.entity.EntregaProducao;
import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import org.springframework.stereotype.Repository;


/**
 * Implementação da <code>Interface</code> que define os métodos de acesso a dados Genéricos.
 * Define o nome do <code>Repository</code> que será criado no <code>Spring</code>.
 *
 * @author DB1 Informatica - Generated by DB1 Eclipse Plugin
 *
 */
@Repository("entregaProducaoDao")
public class EntregaProducaoDaoImpl extends GenericMyBatisDaoImpl<EntregaProducao, Long> implements EntregaProducaoDao {

	/**
	 * Construtor padrão da implementação do <code>Dao</code>.
	 */
	public EntregaProducaoDaoImpl() {
		super(EntregaProducao.class); 
	} 

}