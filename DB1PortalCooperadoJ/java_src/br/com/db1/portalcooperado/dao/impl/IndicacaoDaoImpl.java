package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.portalcooperado.dao.IndicacaoDao;
import br.com.db1.portalcooperado.model.entity.Indicacao;
import br.com.db1.portalcooperado.model.entity.IndicacaoPermissao;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.MatriculasEnviarNotificacao;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.NotificacaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.indicacao.IndicacaoAcao;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.indicacao.IndicacaoParametrosJson;
import br.com.db1.portalcooperado.service.NotificacaoMobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("indicacaoDao")
public class IndicacaoDaoImpl extends GenericMyBatisDaoImpl<Indicacao, Long> implements IndicacaoDao {

    @Autowired
    private NotificacaoMobileService notificacaoMobileService;

    public IndicacaoDaoImpl() {
        super(Indicacao.class);
    }

    @Override
    public void saveOrUpdateAll(List<Indicacao> indicacoes) {
        for (Indicacao indicacao : indicacoes) {
            IndicacaoAcao acao = indicacao.getAcao();
            Indicacao saved = saveOrUpdate(indicacao);
            List<IndicacaoPermissao> permissoes = indicacao.getPermissoes();
            for (IndicacaoPermissao permissao : permissoes) {
                permissao.setIndicacao(saved);
            }
            enviaNotificacaoMobile(indicacao, acao);

        }
    }

    @Override
    public void removeAll(List<Indicacao> indicacoesRemover) {
        for (Indicacao indicacao : indicacoesRemover) {
            remove(indicacao);
            enviaNotificacaoMobile(indicacao, IndicacaoAcao.EXCLUSAO);
        }
    }

    private void enviaNotificacaoMobile(Indicacao indicacao, IndicacaoAcao acao) {
        List<PermissaoIndicado> permissoes = indicacao.getPermissoesIndicado();
        indicacao.getAcao();
        NotificacaoJson notificacaoJson = NotificacaoJson.builder()
                .withTitulo(acao.getTitulo())
                .withMensagem(acao.mensagemOf(indicacao.getNomeIndicador()))
                .withMatriculas(MatriculasEnviarNotificacao.of(indicacao.getMatriculaIndicado()))
                .withParametros(parametrosOf(acao, indicacao.getMatriculaIndicadorAsLong(), permissoes))
                .build();
        notificacaoMobileService.enviarNotificacaoMobile(notificacaoJson);
    }

    private IndicacaoParametrosJson parametrosOf(IndicacaoAcao acao, Long indicador, List<PermissaoIndicado> permissoes) {
        return IndicacaoParametrosJson.builder()
                .withTipo(TipoNotificacao.INDICACOES)
                .withAcao(acao)
                .withIndicador(indicador)
                .withPermissoes(permissoes)
                .build();
    }
}
