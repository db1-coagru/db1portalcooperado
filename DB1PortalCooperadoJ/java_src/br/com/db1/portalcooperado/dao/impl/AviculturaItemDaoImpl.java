package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.portalcooperado.dao.AviculturaItemDao;
import br.com.db1.portalcooperado.dao.AviculturaItemImagemDao;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("aviculturaItemDao")
public class AviculturaItemDaoImpl extends GenericMyBatisDaoImpl<AviculturaItem, Long> implements AviculturaItemDao {

	private final AviculturaItemImagemDao aviculturaItemImagemDao;

	@Autowired
	public AviculturaItemDaoImpl(AviculturaItemImagemDao aviculturaItemImagemDao) {
		super(AviculturaItem.class);
		this.aviculturaItemImagemDao = aviculturaItemImagemDao;
	}

	@Override
	public AviculturaItem save(AviculturaItem item) {
		AviculturaItem saved = super.saveOrUpdate(item);
		aviculturaItemImagemDao.save(item.getImagens(), saved);
		return saved;
	}

	@Override
	public void save(List<AviculturaItem> itens, Avicultura avicultura) {
		for (AviculturaItem item : itens) {
			item.setAvicultura(avicultura);
			save(item);
		}
	}

}