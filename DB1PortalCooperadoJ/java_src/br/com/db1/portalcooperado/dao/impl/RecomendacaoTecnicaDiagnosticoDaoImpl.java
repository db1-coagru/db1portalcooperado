package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaDiagnosticoDao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaDiagnostico;
import org.springframework.stereotype.Repository;

@Repository("recomendacaoTecnicaDiagnoticoDao")
public class RecomendacaoTecnicaDiagnosticoDaoImpl extends GenericMyBatisDaoImpl<RecomendacaoTecnicaDiagnostico, Long>
		implements RecomendacaoTecnicaDiagnosticoDao {

	/**
	 * Construtor padrão da implementação do <code>Dao</code>.
	 */
	public RecomendacaoTecnicaDiagnosticoDaoImpl() {
		super(RecomendacaoTecnicaDiagnostico.class);
	}

}