package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.portalcooperado.dao.NotificacaoDao;
import br.com.db1.portalcooperado.model.entity.Notificacao;
import org.springframework.stereotype.Repository;

@Repository("notificacaoDao")
public class NotificacaoDaoImpl extends GenericMyBatisDaoImpl<Notificacao, Long>
		implements NotificacaoDao {

	public NotificacaoDaoImpl() {
		super(Notificacao.class);
	}

	@Override
	public Notificacao update(Notificacao object) {
		return super.update("updateNotificacao", object);
	}
}