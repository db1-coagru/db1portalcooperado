package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.portalcooperado.dao.TokenDao;
import br.com.db1.portalcooperado.model.entity.Token;
import br.com.db1.portalcooperado.model.specifications.Tokens;
import org.springframework.stereotype.Repository;

@Repository("tokenDao")
public class TokenDaoImpl extends GenericMyBatisDaoImpl<Token, Long>
		implements TokenDao {

	public TokenDaoImpl() {
		super(Token.class);
	}

	@Override
	public Token findByAccessToken(String accessToken) {
		return findOne(Tokens.FIND_BY_ACCESS_TOKEN, Tokens.withAccessToken(accessToken));
	}

}