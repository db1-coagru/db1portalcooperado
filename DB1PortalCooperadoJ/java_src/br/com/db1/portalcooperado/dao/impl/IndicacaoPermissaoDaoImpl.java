package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.portalcooperado.dao.IndicacaoPermissaoDao;
import br.com.db1.portalcooperado.model.entity.IndicacaoPermissao;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("indicacaoPermissaoDao")
public class IndicacaoPermissaoDaoImpl extends GenericMyBatisDaoImpl<IndicacaoPermissao, Long>
		implements IndicacaoPermissaoDao {

	public IndicacaoPermissaoDaoImpl() {
		super(IndicacaoPermissao.class);
	}

	@Override
	public void saveOrUpdateAll(List<IndicacaoPermissao> permissoes) {
		for (IndicacaoPermissao indicacaoPermissao : permissoes) {
			saveOrUpdate(indicacaoPermissao);
		}
	}

	@Override
	public void removeAll(List<IndicacaoPermissao> permissoes) {
		for (IndicacaoPermissao indicacaoPermissao : permissoes) {
			remove(indicacaoPermissao);
		}
	}
}
