package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.portalcooperado.dao.AviculturaItemImagemDao;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItem;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItemImagem;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("aviculturaItemImagemDao")
public class AviculturaItemImagemDaoImpl extends GenericMyBatisDaoImpl<AviculturaItemImagem, Long>
		implements AviculturaItemImagemDao {

	public AviculturaItemImagemDaoImpl() {
		super(AviculturaItemImagem.class);
	}

	@Override
	public void save(List<AviculturaItemImagem> imagens, AviculturaItem item) {
		for (AviculturaItemImagem imagem : imagens) {
			imagem.setAviculturaItem(item);
			saveOrUpdate(imagem);
		}
	}

}