package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaProdutoDao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaProduto;
import org.springframework.stereotype.Repository;

@Repository("recomendacaoTecnicaProdutoDao")
public class RecomendacaoTecnicaProdutoDaoImpl extends GenericMyBatisDaoImpl<RecomendacaoTecnicaProduto, Long>
		implements RecomendacaoTecnicaProdutoDao {

	/**
	 * Construtor padrão da implementação do <code>Dao</code>.
	 */
	public RecomendacaoTecnicaProdutoDaoImpl() {
		super(RecomendacaoTecnicaProduto.class);
	}

}