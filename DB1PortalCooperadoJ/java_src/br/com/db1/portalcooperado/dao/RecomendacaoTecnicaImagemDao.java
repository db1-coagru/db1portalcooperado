package br.com.db1.portalcooperado.dao;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaImagem;

public interface RecomendacaoTecnicaImagemDao extends GenericMyBatisDao<RecomendacaoTecnicaImagem, Long> {

}