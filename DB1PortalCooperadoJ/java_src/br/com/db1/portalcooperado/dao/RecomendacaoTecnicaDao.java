package br.com.db1.portalcooperado.dao;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;

import java.util.List;

public interface RecomendacaoTecnicaDao extends GenericMyBatisDao<RecomendacaoTecnica, Long> {

	RecomendacaoTecnica findOneByFileName(String fileName);

	List<RecomendacaoTecnica> findLastNinetyDaysByMatricula(Long matricula);

}