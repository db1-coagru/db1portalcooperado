package br.com.db1.portalcooperado.dao;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDao;
import br.com.db1.portalcooperado.model.entity.IndicacaoPermissao;

import java.util.List;

public interface IndicacaoPermissaoDao extends GenericMyBatisDao<IndicacaoPermissao, Long> {

	void saveOrUpdateAll(List<IndicacaoPermissao> permissoes);

	void removeAll(List<IndicacaoPermissao> permissoes);
}
