package br.com.db1.portalcooperado.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.model.types.AcaoLog;

/**
 * Classe que controla as alterações realizadas no cadastro de usuário.
 * 
 * @author henrique.fernandes
 * @since 22/07/2011
 */
@Entity
@Table(name = "COOP_USUARIO_LOG")
@MBClass
public class UsuarioLog {

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_USUARIO_LOG")
	@Column(name = "ID_LOG", insertable = true, updatable = false, unique = true, length = 19)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "DS_ACAO", length = 6, nullable = false, updatable = false)
	private AcaoLog dsAcao;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_LOG", nullable = false, insertable = true, updatable = false)
	private Date dtLog;

	@Column(name = "HR_LOG", nullable = false, updatable = false, length = 8)
	private String hrLog;

	@Column(name = "IP_ORIGEM", nullable = false, updatable = false, length = 39)
	private String ipOrigem;

	@ManyToOne
	@JoinColumn(name = "ID_USUARIO_AFETADO", referencedColumnName = "ID_USUARIO", nullable = false)
	private Usuario usuarioAfetado;

	@ManyToOne
	@JoinColumn(name = "ID_USUARIO", referencedColumnName = "ID_USUARIO", nullable = false)
	private Usuario usuario;

	@Column(name = "MAT_USUARIO_DEL", nullable = true, length = 10)
	private Long matUsuarioDeletado;

	@Column(name = "NM_USUARIO_DEL", nullable = true, length = 100)
	private String nmUsuarioDeletado;

	@Column(name = "CPF_CNPJ_USUARIO_DEL", nullable = true, length = 14)
	private String cpfCnpjUsuarioDeletado;

	/**
	 * Retorna o valor do campo id.
	 * 
	 * @return valor do campo id.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Atribui um novo valor para o campo id.
	 * 
	 * @param id
	 *            novo valor para o campo id.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Retorna o valor do campo dsAcao.
	 * 
	 * @return valor do campo dsAcao.
	 */
	public AcaoLog getDsAcao() {
		return dsAcao;
	}

	/**
	 * Atribui um novo valor para o campo dsAcao.
	 * 
	 * @param dsAcao
	 *            novo valor para o campo dsAcao.
	 */
	public void setDsAcao(AcaoLog dsAcao) {
		this.dsAcao = dsAcao;
	}

	/**
	 * Retorna o valor do campo dtLog.
	 * 
	 * @return valor do campo dtLog.
	 */
	public Date getDtLog() {
		return dtLog;
	}

	/**
	 * Atribui um novo valor para o campo dtLog.
	 * 
	 * @param dtLog
	 *            novo valor para o campo dtLog.
	 */
	public void setDtLog(Date dtLog) {
		this.dtLog = dtLog;
	}

	/**
	 * Retorna o valor do campo hrLog.
	 * 
	 * @return valor do campo hrLog.
	 */
	public String getHrLog() {
		return hrLog;
	}

	/**
	 * Atribui um novo valor para o campo hrLog.
	 * 
	 * @param hrLog
	 *            novo valor para o campo hrLog.
	 */
	public void setHrLog(String hrLog) {
		this.hrLog = hrLog;
	}

	/**
	 * Retorna o valor do campo ipOrigem.
	 * 
	 * @return valor do campo ipOrigem.
	 */
	public String getIpOrigem() {
		return ipOrigem;
	}

	/**
	 * Atribui um novo valor para o campo ipOrigem.
	 * 
	 * @param ipOrigem
	 *            novo valor para o campo ipOrigem.
	 */
	public void setIpOrigem(String ipOrigem) {
		this.ipOrigem = ipOrigem;
	}

	/**
	 * Retorna o valor do campo usuarioAfetado.
	 * 
	 * @return valor do campo usuarioAfetado.
	 */
	public Usuario getUsuarioAfetado() {
		return usuarioAfetado;
	}

	/**
	 * Atribui um novo valor para o campo usuarioAfetado.
	 * 
	 * @param usuarioAfetado
	 *            novo valor para o campo usuarioAfetado.
	 */
	public void setUsuarioAfetado(Usuario usuarioAfetado) {
		this.usuarioAfetado = usuarioAfetado;
	}

	/**
	 * Retorna o valor do campo usuario.
	 * 
	 * @return valor do campo usuario.
	 */
	public Usuario getUsuario() {
		return usuario;
	}

	/**
	 * Atribui um novo valor para o campo usuario.
	 * 
	 * @param usuario
	 *            novo valor para o campo usuario.
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * Retorna o valor do campo matUsuarioDeletado.
	 * 
	 * @return valor do campo matUsuarioDeletado.
	 */
	public Long getMatUsuarioDeletado() {
		return matUsuarioDeletado;
	}

	/**
	 * Atribui um novo valor para o campo matUsuarioDeletado.
	 * 
	 * @param matUsuarioDeletado
	 *            novo valor para o campo matUsuarioDeletado.
	 */
	public void setMatUsuarioDeletado(Long matUsuarioDeletado) {
		this.matUsuarioDeletado = matUsuarioDeletado;
	}

	/**
	 * Retorna o valor do campo nmUsuarioDeletado.
	 * 
	 * @return valor do campo nmUsuarioDeletado.
	 */
	public String getNmUsuarioDeletado() {
		return nmUsuarioDeletado;
	}

	/**
	 * Atribui um novo valor para o campo nmUsuarioDeletado.
	 * 
	 * @param nmUsuarioDeletado
	 *            novo valor para o campo nmUsuarioDeletado.
	 */
	public void setNmUsuarioDeletado(String nmUsuarioDeletado) {
		this.nmUsuarioDeletado = nmUsuarioDeletado;
	}

	/**
	 * Retorna o valor do campo cpfCnpjUsuarioDeletado.
	 * 
	 * @return valor do campo cpfCnpjUsuarioDeletado.
	 */
	public String getCpfCnpjUsuarioDeletado() {
		return cpfCnpjUsuarioDeletado;
	}

	/**
	 * Atribui um novo valor para o campo cpfCnpjUsuarioDeletado.
	 * 
	 * @param cpfCnpjUsuarioDeletado
	 *            novo valor para o campo cpfCnpjUsuarioDeletado.
	 */
	public void setCpfCnpjUsuarioDeletado(String cpfCnpjUsuarioDeletado) {
		this.cpfCnpjUsuarioDeletado = cpfCnpjUsuarioDeletado;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioLog other = (UsuarioLog) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
