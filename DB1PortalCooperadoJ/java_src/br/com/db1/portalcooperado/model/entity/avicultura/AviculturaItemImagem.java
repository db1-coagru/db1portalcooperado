package br.com.db1.portalcooperado.model.entity.avicultura;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import jersey.repackaged.com.google.common.base.MoreObjects;
import jersey.repackaged.com.google.common.base.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COOP_AVICULTURA_ITEM_IMAG")
@MBClass
public class AviculturaItemImagem {

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_AVICULTURA_ITEM_IMAG")
	@Column(name = "ID_AVICULTURA_ITEM_IMAG", updatable = false, unique = true)
	private Long id;

	@Column(name = "ID_AVICULTURA_ITEM", nullable = false, updatable = false)
	private AviculturaItem aviculturaItem;

	@Column(name = "DS_NOME", nullable = false, updatable = false)
	private String nome;

	protected AviculturaItemImagem() {
		// for frameworks
	}

	public AviculturaItemImagem(String nome) {
		this.nome = nome;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof AviculturaItemImagem) {
			AviculturaItemImagem that = (AviculturaItemImagem) o;
			return Objects.equal(aviculturaItem, that.aviculturaItem) &&
					Objects.equal(nome, that.nome);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(aviculturaItem, nome);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("aviculturaItem", aviculturaItem)
				.add("nome", nome)
				.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AviculturaItem getAviculturaItem() {
		return aviculturaItem;
	}

	public void setAviculturaItem(AviculturaItem aviculturaItem) {
		this.aviculturaItem = aviculturaItem;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
