package br.com.db1.portalcooperado.model.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.db1.myBatisPersistence.annotations.MBClass;

/**
 * Classe para lista os débitos de um cooperante no relatório.
 * 
 * @author Wagner Mendes Voltz
 * @since 10/07/2011
 */
@Entity
@Table(name = "COOP_POS_FIN_DEB")
@MBClass
public class PosicaoFinanceiraDebitos {

	@Column(name = "ID_REQUISICAO", nullable = false)
	private Long idRequisicao;

	@Column(name = "NR_DOCUMENTO", nullable = false)
	private String nrDocumento;

	@Column(name = "SERIE", nullable = false)
	private String serie;

	@Column(name = "NR_PARCELA", nullable = false)
	private Long nrParcela;

	@Column(name = "DS_PRODUTO", nullable = false)
	private String dsProduto;

	@Column(name = "NR_FILIAL", nullable = false)
	private Long nrFilial;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_LANCAMENTO", nullable = false)
	private Date dtLancamento;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_VENCIMENTO", nullable = false)
	private Date dtVencimento;

	@Column(name = "VL_PRODUTO", nullable = false)
	private BigDecimal vlProduto;

	@Column(name = "VL_JUROS", nullable = false)
	private BigDecimal vlJuros;

	@Column(name = "VL_LIQUIDO", nullable = false)
	private BigDecimal vlLiquido;

	@Column(name = "BO_CREDITO", nullable = false)
	private Boolean credito;

	/**
	 * Retorna o valor do campo idRequisicao.
	 * 
	 * @return valor do campo idRequisicao.
	 */
	public Long getIdRequisicao() {
		return idRequisicao;
	}

	/**
	 * Atribui um novo valor para o campo idRequisicao.
	 * 
	 * @param idRequisicao
	 *            novo valor para o campo idRequisicao.
	 */
	public void setIdRequisicao(Long idRequisicao) {
		this.idRequisicao = idRequisicao;
	}

	/**
	 * Retorna o valor do campo nrDocumento.
	 * 
	 * @return valor do campo nrDocumento.
	 */
	public String getNrDocumento() {
		return nrDocumento;
	}

	/**
	 * Atribui um novo valor para o campo nrDocumento.
	 * 
	 * @param nrDocumento
	 *            novo valor para o campo nrDocumento.
	 */
	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	/**
	 * Retorna o valor do campo serie.
	 * 
	 * @return valor do campo serie.
	 */
	public String getSerie() {
		return serie;
	}

	/**
	 * Atribui um novo valor para o campo serie.
	 * 
	 * @param serie
	 *            novo valor para o campo serie.
	 */
	public void setSerie(String serie) {
		this.serie = serie;
	}

	/**
	 * Retorna o valor do campo nrParcela.
	 * 
	 * @return valor do campo nrParcela.
	 */
	public Long getNrParcela() {
		return nrParcela;
	}

	/**
	 * Atribui um novo valor para o campo nrParcela.
	 * 
	 * @param nrParcela
	 *            novo valor para o campo nrParcela.
	 */
	public void setNrParcela(Long nrParcela) {
		this.nrParcela = nrParcela;
	}

	/**
	 * Retorna o valor do campo dsProduto.
	 * 
	 * @return valor do campo dsProduto.
	 */
	public String getDsProduto() {
		return dsProduto;
	}

	/**
	 * Atribui um novo valor para o campo dsProduto.
	 * 
	 * @param dsProduto
	 *            novo valor para o campo dsProduto.
	 */
	public void setDsProduto(String dsProduto) {
		this.dsProduto = dsProduto;
	}

	/**
	 * Retorna o valor do campo nrFilial.
	 * 
	 * @return valor do campo nrFilial.
	 */
	public Long getNrFilial() {
		return nrFilial;
	}

	/**
	 * Atribui um novo valor para o campo nrFilial.
	 * 
	 * @param nrFilial
	 *            novo valor para o campo nrFilial.
	 */
	public void setNrFilial(Long nrFilial) {
		this.nrFilial = nrFilial;
	}

	/**
	 * Retorna o valor do campo dtLancamento.
	 * 
	 * @return valor do campo dtLancamento.
	 */
	public Date getDtLancamento() {
		return dtLancamento;
	}

	/**
	 * Atribui um novo valor para o campo dtLancamento.
	 * 
	 * @param dtLancamento
	 *            novo valor para o campo dtLancamento.
	 */
	public void setDtLancamento(Date dtLancamento) {
		this.dtLancamento = dtLancamento;
	}

	/**
	 * Retorna o valor do campo dtVencimento.
	 * 
	 * @return valor do campo dtVencimento.
	 */
	public Date getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Atribui um novo valor para o campo dtVencimento.
	 * 
	 * @param dtVencimento
	 *            novo valor para o campo dtVencimento.
	 */
	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	/**
	 * Retorna o valor do campo vlProduto.
	 * 
	 * @return valor do campo vlProduto.
	 */
	public BigDecimal getVlProduto() {
		return vlProduto;
	}

	/**
	 * Atribui um novo valor para o campo vlProduto.
	 * 
	 * @param vlProduto
	 *            novo valor para o campo vlProduto.
	 */
	public void setVlProduto(BigDecimal vlProduto) {
		this.vlProduto = vlProduto;
	}

	/**
	 * Retorna o valor do campo vlJuros.
	 * 
	 * @return valor do campo vlJuros.
	 */
	public BigDecimal getVlJuros() {
		return vlJuros;
	}

	/**
	 * Atribui um novo valor para o campo vlJuros.
	 * 
	 * @param vlJuros
	 *            novo valor para o campo vlJuros.
	 */
	public void setVlJuros(BigDecimal vlJuros) {
		this.vlJuros = vlJuros;
	}

	/**
	 * Retorna o valor do campo vlLiquido.
	 * 
	 * @return valor do campo vlLiquido.
	 */
	public BigDecimal getVlLiquido() {
		return vlLiquido;
	}

	/**
	 * Atribui um novo valor para o campo vlLiquido.
	 * 
	 * @param vlLiquido
	 *            novo valor para o campo vlLiquido.
	 */
	public void setVlLiquido(BigDecimal vlLiquido) {
		this.vlLiquido = vlLiquido;
	}

	/**
	 * Retorna o valor do campo credito.
	 * 
	 * @return valor do campo credito.
	 */
	public Boolean getCredito() {
		return credito;
	}

	/**
	 * Atribui um novo valor para o campo credito.
	 * 
	 * @param credito
	 *            novo valor para o campo credito.
	 */
	public void setCredito(Boolean credito) {
		this.credito = credito;
	}

}
