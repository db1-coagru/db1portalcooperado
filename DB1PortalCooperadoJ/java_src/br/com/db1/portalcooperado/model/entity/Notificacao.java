package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.model.types.Quantidade;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import br.com.db1.portalcooperado.util.DateFormatter;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.base.MoreObjects;
import org.assertj.core.util.Strings;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "COOP_NOTIFICACAO")
@MBClass
public class Notificacao {

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_NOTIFICACAO")
	@Column(name = "ID_NOTIFICACAO", insertable = true, updatable = false, unique = true)
	private Long id;

	@Column(name = "MAT_MATRICULA", insertable = false, updatable = false)
	private Long matricula;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_DATA", insertable = false, updatable = false)
	private Date data;

	@Column(name = "HR_HORA", insertable = false, updatable = false)
	private String hora;

	@Column(name = "TP_TIPO", insertable = false, updatable = false)
	private TipoNotificacao tipo;

	@Column(name = "DS_FILIAL", insertable = false, updatable = false)
	private String filial;

	@Column(name = "DS_LOTE", insertable = false, updatable = false)
	private String lote;

	@Column(name = "NR_DOCUMENTO", insertable = false, updatable = false)
	private Long documento;

	@Column(name = "DS_PRODUTO", insertable = false, updatable = false)
	private String produto;

	@Column(name = "DS_PLACA", insertable = false, updatable = false)
	private String placa;

	@Column(name = "PS_PESO", insertable = false, updatable = false)
	private Long peso;

	@Column(name = "VL_VALOR", insertable = false, updatable = false)
	private BigDecimal valor;

	@Column(name = "TP_NOTA", insertable = false, updatable = false)
	private String tipoNota;

	@Column(name = "NR_STATUS", insertable = false, updatable = true)
	private Long status;

	protected Notificacao() {
		//utilizado por frameworks
	}

	public static Notificacao of() {
		return new Notificacao();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Notificacao) {
			Notificacao other = (Notificacao) obj;
			return PortalCooperadoUtil.areEquals(this.matricula, other.matricula) &&
					PortalCooperadoUtil.areEquals(this.data, other.data) &&
					PortalCooperadoUtil.areEquals(this.hora, other.hora) &&
					PortalCooperadoUtil.areEquals(this.tipo, other.tipo);

		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(this.matricula, this.data, this.hora, this.tipo);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("matricula", matricula)
				.add("data", data)
				.add("hora", hora)
				.add("tipo", tipo)
				.add("filial", filial)
				.add("lote", lote)
				.add("documento", documento)
				.add("produto", produto)
				.add("placa", placa)
				.add("peso", peso)
				.add("valor", valor)
				.add("tipoNota", tipoNota)
				.add("status", status)
				.toString();
	}

	public boolean isDeRomaneioAgricola() {
		return tipo.isRomaneioAgricola();
	}

	public boolean isRecomendacaoTecnica() {
		return tipo.isRecomendacaoTecnica();
	}

	public String descricaoTipoNotificacao() {
		return tipo.getDescricao();
	}

	public String dataAsString() {
		return DateFormatter.DD_MM_YYYY.format(data);
	}

	public String matriculaAsString() {
		return matricula.toString();
	}

	public boolean possuiLote() {
		return !Strings.isNullOrEmpty(lote) && !lote.trim().isEmpty();
	}

	private boolean possuiFilial() {
		return !Strings.isNullOrEmpty(filial) && !filial.trim().isEmpty();
	}

	public boolean possuiTipo(TipoNotificacao tipoNotificacao) {
		return tipo.equals(tipoNotificacao);
	}

	public String getFilialLowercase() {
		return possuiFilial() ? filial.toLowerCase() : "";
	}

	public String getLoteLowercase() {
		return possuiLote() ? lote.toLowerCase() : "";
	}

	public PermissaoIndicado getPermissao() {
		return tipo.getPermissao();
	}

	public void setValor(Quantidade valor) {
		this.valor = valor.toBigDecimal();
	}

	public String getTitulo() {
		return tipo.getTitulo();
	}

	public Long getMatricula() {
		return matricula;
	}

	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public TipoNotificacao getTipo() {
		return tipo;
	}

	public void setTipo(TipoNotificacao tipo) {
		this.tipo = tipo;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public Long getDocumento() {
		return documento;
	}

	public void setDocumento(Long documento) {
		this.documento = documento;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Long getPeso() {
		return peso;
	}

	public void setPeso(Long peso) {
		this.peso = peso;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getTipoNota() {
		return tipoNota;
	}

	public void setTipoNota(String tipoNota) {
		this.tipoNota = tipoNota;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

}
