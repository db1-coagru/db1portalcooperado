package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Classe para lista os créditos de um cooperante no relatório.
 * 
 * @author Wagner Mendes Voltz
 * @since 10/07/2011
 */
@Entity
@Table(name = "COOP_POS_FIN_CRED")
@MBClass
public class PosicaoFinanceiraCredito {

	@Column(name = "ID_REQUISICAO")
	private Long idRequisicao;

	@Column(name = "NR_ROMANEIO")
	private String nrRomaneio;
	
	@Column(name = "NR_SEQUENCIA")
	private Long nrSequencia;
	
	@Column(name = "DS_LOCAL")
	private String dsLocal;
	
	@Column(name = "DS_PRODUTO")
	private String dsProduto;
	
	@Column(name = "NR_PADRAO")
	private Long nrPadrao;
	
	@Column(name = "NR_TIPO")
	private Long nrTipo;
	
	@Column(name = "PS_LIQUIDO")
	private BigDecimal pesoLiquido;
	
	@Column(name = "VL_DIA")
	private BigDecimal vlDia;
	
	@Column(name = "VL_LIQUIDO")
	private BigDecimal vlLiquido;



	/**
	 * Retorna o valor do campo idRequisicao.
	 *
	 * @return valor do campo idRequisicao.
	 */
	public Long getIdRequisicao() {
		return idRequisicao;
	}

	/**
	 * Atribui um novo valor para o campo idRequisicao.
	 *
	 * @param idRequisicao novo valor para o campo idRequisicao.
	 */
	public void setIdRequisicao(Long idRequisicao) {
		this.idRequisicao = idRequisicao;
	}

	/**
	 * Retorna o valor do campo nrRomaneio.
	 *
	 * @return valor do campo nrRomaneio.
	 */
	public String getNrRomaneio() {
		return nrRomaneio;
	}

	/**
	 * Atribui um novo valor para o campo nrRomaneio.
	 *
	 * @param nrRomaneio novo valor para o campo nrRomaneio.
	 */
	public void setNrRomaneio(String nrRomaneio) {
		this.nrRomaneio = nrRomaneio;
	}

	/**
	 * Retorna o valor do campo nrSequencia.
	 *
	 * @return valor do campo nrSequencia.
	 */
	public Long getNrSequencia() {
		return nrSequencia;
	}

	/**
	 * Atribui um novo valor para o campo nrSequencia.
	 *
	 * @param nrSequencia novo valor para o campo nrSequencia.
	 */
	public void setNrSequencia(Long nrSequencia) {
		this.nrSequencia = nrSequencia;
	}

	/**
	 * Retorna o valor do campo dsLocal.
	 *
	 * @return valor do campo dsLocal.
	 */
	public String getDsLocal() {
		return dsLocal;
	}

	/**
	 * Atribui um novo valor para o campo dsLocal.
	 *
	 * @param dsLocal novo valor para o campo dsLocal.
	 */
	public void setDsLocal(String dsLocal) {
		this.dsLocal = dsLocal;
	}

	/**
	 * Retorna o valor do campo dsProduto.
	 *
	 * @return valor do campo dsProduto.
	 */
	public String getDsProduto() {
		return dsProduto;
	}

	/**
	 * Atribui um novo valor para o campo dsProduto.
	 *
	 * @param dsProduto novo valor para o campo dsProduto.
	 */
	public void setDsProduto(String dsProduto) {
		this.dsProduto = dsProduto;
	}

	/**
	 * Retorna o valor do campo nrTipo.
	 *
	 * @return valor do campo nrTipo.
	 */
	public Long getNrTipo() {
		return nrTipo;
	}

	/**
	 * Atribui um novo valor para o campo nrTipo.
	 *
	 * @param nrTipo novo valor para o campo nrTipo.
	 */
	public void setNrTipo(Long nrTipo) {
		this.nrTipo = nrTipo;
	}

	/**
	 * Retorna o valor do campo pesoLiquido.
	 *
	 * @return valor do campo pesoLiquido.
	 */
	public BigDecimal getPesoLiquido() {
		return pesoLiquido;
	}

	/**
	 * Atribui um novo valor para o campo pesoLiquido.
	 *
	 * @param pesoLiquido novo valor para o campo pesoLiquido.
	 */
	public void setPesoLiquido(BigDecimal pesoLiquido) {
		this.pesoLiquido = pesoLiquido;
	}

	/**
	 * Retorna o valor do campo vlDia.
	 *
	 * @return valor do campo vlDia.
	 */
	public BigDecimal getVlDia() {
		return vlDia;
	}

	/**
	 * Atribui um novo valor para o campo vlDia.
	 *
	 * @param vlDia novo valor para o campo vlDia.
	 */
	public void setVlDia(BigDecimal vlDia) {
		this.vlDia = vlDia;
	}

	/**
	 * Retorna o valor do campo vlLiquido.
	 *
	 * @return valor do campo vlLiquido.
	 */
	public BigDecimal getVlLiquido() {
		return vlLiquido;
	}

	/**
	 * Atribui um novo valor para o campo vlLiquido.
	 *
	 * @param vlLiquido novo valor para o campo vlLiquido.
	 */
	public void setVlLiquido(BigDecimal vlLiquido) {
		this.vlLiquido = vlLiquido;
	}

	/**
	 * Retorna o valor do campo nrPadrao.
	 *
	 * @return valor do campo nrPadrao.
	 */
	public Long getNrPadrao() {
		return nrPadrao;
	}

	/**
	 * Atribui um novo valor para o campo nrPadrao.
	 *
	 * @param nrPadrao novo valor para o campo nrPadrao.
	 */
	public void setNrPadrao(Long nrPadrao) {
		this.nrPadrao = nrPadrao;
	}
	
	

}
