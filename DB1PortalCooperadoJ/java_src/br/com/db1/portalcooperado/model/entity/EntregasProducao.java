package br.com.db1.portalcooperado.model.entity;

import java.util.Date;
import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class EntregasProducao {

	private final Requisicao requisicao;

	private final List<EntregaProducao> values;

	private EntregasProducao(Requisicao requisicao, List<EntregaProducao> values) {
		this.requisicao = requisicao;
		this.values = values;
	}

	public static EntregasProducao of(Requisicao requisicao, List<EntregaProducao> entregas) {
		checkNotNull(requisicao, "Requisição deve ser informada.");
		return new EntregasProducao(requisicao, entregas);
	}

	public Long getIdRequisicao() {
		return requisicao.getId();
	}

	public String getMatricula() {
		return requisicao.getMatricula().toString();
	}

	public Date getDtInicial() {
		return requisicao.getDtInicial();
	}

	public Date getDtFinal() {
		return requisicao.getDtFinal();
	}

	public Requisicao getRequisicao() {
		return requisicao;
	}

	public List<EntregaProducao> toList() {
		return values;
	}

}
