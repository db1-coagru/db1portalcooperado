package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.base.MoreObjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COOP_RECOMENDACAO_TECNICA_DIAG")
@MBClass
public class RecomendacaoTecnicaDiagnostico {

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_RECOMENDACAO_TECNICA_DIAG")
	@Column(name = "ID_RECOMENDACAO_DIAG", insertable = true, updatable = false, unique = true)
	private Long id;

	@Column(name = "ID_RECOMENDACAO", nullable = false, insertable = true, updatable = false)
	private RecomendacaoTecnica recomendacaoTecnica;

	@Column(name = "DS_DESCRICAO", nullable = false, insertable = true, updatable = false)
	private String descricao;

	@Column(name = "DS_OBSERVACAO", nullable = false, insertable = true, updatable = false)
	private String observacao;

	protected RecomendacaoTecnicaDiagnostico() {
		//construtor padrao utilizado por frameworks
	}

	public static RecomendacaoTecnicaDiagnostico of() {
		return new RecomendacaoTecnicaDiagnostico();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RecomendacaoTecnicaDiagnostico) {
			RecomendacaoTecnicaDiagnostico other = (RecomendacaoTecnicaDiagnostico) obj;
			return PortalCooperadoUtil.areEquals(this.recomendacaoTecnica, other.recomendacaoTecnica)
					&& PortalCooperadoUtil.areEquals(this.descricao, other.descricao)
					&& PortalCooperadoUtil.areEquals(this.observacao, other.observacao);

		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(recomendacaoTecnica, descricao, observacao);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("recomendacaoTecnica", recomendacaoTecnica)
				.add("descricao", descricao)
				.add("observacao", observacao)
				.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RecomendacaoTecnica getRecomendacaoTecnica() {
		return recomendacaoTecnica;
	}

	public void setRecomendacaoTecnica(RecomendacaoTecnica recomendacaoTecnica) {
		this.recomendacaoTecnica = recomendacaoTecnica;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
