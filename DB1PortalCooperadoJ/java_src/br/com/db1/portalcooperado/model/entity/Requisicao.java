package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.model.builder.RequisicaoBuilder;
import br.com.db1.portalcooperado.model.types.Canal;
import br.com.db1.portalcooperado.requisicao.Propriedades;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;

import javax.persistence.*;
import java.util.Date;

import static br.com.db1.portalcooperado.requisicao.Propriedades.PROP_LINHA_COMANDO_PROGRESS;
import static br.com.db1.portalcooperado.util.DateFormatter.DD_MM_YYYY;

/**
 * Classe que irá controlar o numero de pedidos de relatorios por cooperados.
 *
 * @author Wagner Mendes Voltz
 * @since 19/07/2011
 */
@Entity
@Table(name = "COOP_REQUISICAO")
@MBClass
public class Requisicao {

	public static final Long TP_ACAO = 1L;

	public static final Long TIPO_POS_FINANCEIRA = 1L;

	public static final Long TIPO_ENTREGA_PRODUCAO = 2L;

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_REQUISICAO")
	@Column(name = "ID_REQUISICAO", insertable = true, updatable = false, unique = true)
	private Long id;

	@Column(name = "MAT_USUARIO", insertable = true, length = 6, nullable = false, updatable = false)
	private Long matricula;

	@Column(name = "TP_REQUISICAO", length = 4, nullable = false, insertable = true, updatable = false)
	private Long tpRequisicao;

	@Column(name = "TP_ACAO", length = 4, nullable = false, insertable = true, updatable = false)
	private Long tpAcao;

	@Column(name = "TP_PRODUTO", length = 4, nullable = false, insertable = true, updatable = false)
	private Long tpProduto;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_INICIAL", nullable = false, insertable = true, updatable = false)
	private Date dtInicial;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_FINAL", nullable = false, insertable = true, updatable = false)
	private Date dtFinal;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_REQUISICAO", nullable = false, insertable = true, updatable = false)
	private Date dtRequisicao;

	@Column(name = "IP_USUARIO", nullable = false, insertable = true, updatable = false, length = 15)
	private String ipUsuario;

	@Column(name = "HR_REQUISICAO", nullable = false, insertable = true, updatable = false, length = 8)
	private String hrRequisicao;

	@Column(name = "TP_CANAL", nullable = false, length = 1, insertable = true, updatable = false)
	private Long tpCanal;

	public Requisicao() {
		// usado pelo flex pra fazer o biding
	}

	public Requisicao(RequisicaoBuilder builder) {
		this.matricula = builder.getMatricula();
		this.tpRequisicao = builder.getTipoRequisicao();
		this.tpAcao = TP_ACAO;
		this.tpProduto = builder.getTipoProduto();
		this.dtInicial = builder.getDataInicial();
		this.dtFinal = builder.getDataFinal();
		this.dtRequisicao = builder.getDataRequisicao();
		this.ipUsuario = builder.getIp();
		this.hrRequisicao = builder.getHoraRequisicao();
		this.tpCanal = builder.getCanal();
	}

	public static RequisicaoBuilder builder(Canal canal) {
		return RequisicaoBuilder.of(canal);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Requisicao other = (Requisicao) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		}
		else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	public Boolean isOf(TipoRequisicao tipoRequisicao) {
		return this.tpRequisicao.equals(tipoRequisicao.toLongValue());
	}

	/**
	 * Monta o comando para execução do progress com os valores dos atributos do
	 * objeto
	 *
	 * @return Comando para execução de linha de comando do progress
	 */
	public String montarComando() {
		String caminhoProwin = Propriedades.getInstancia().getPropriedade(PROP_LINHA_COMANDO_PROGRESS);
		if (tpRequisicao.equals(TIPO_ENTREGA_PRODUCAO)) {
			return caminhoProwin + " '" + matricula + ";" + id + ";" + tpRequisicao + ";" + tpProduto + ";" +
					DD_MM_YYYY.format(dtInicial) + ";" + DD_MM_YYYY.format(dtFinal) + "'";

		}
		return caminhoProwin + " '" + matricula + ";" + id + ";" + tpRequisicao + "'";
	}

	public void setTpCanal(Canal tpCanal) {
		this.tpCanal = tpCanal.longValue();
	}

	/**
	 * @return String utilizada nos jaspers para exibição da descricao via reflection
	 */
	public String getTpCanalDescricao() {
		return Canal.fromLongValue(tpCanal).getDescricao();
	}

	public TipoRequisicao getTipoRequisicao() {
		return TipoRequisicao.fromLongValue(tpRequisicao);
	}

	/**
	 * Retorna o valor do campo id.
	 *
	 * @return valor do campo id.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Atribui um novo valor para o campo id.
	 *
	 * @param id novo valor para o campo id.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Retorna o valor do campo codMatricula.
	 *
	 * @return valor do campo codMatricula.
	 */
	public Long getMatricula() {
		return matricula;
	}

	/**
	 * Atribui um novo valor para o campo codMatricula.
	 *
	 * @param matricula novo valor para o campo codMatricula.
	 */
	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}

	/**
	 * Retorna o valor do campo tpRequisicao.
	 *
	 * @return valor do campo tpRequisicao.
	 */
	public Long getTpRequisicao() {
		return tpRequisicao;
	}

	/**
	 * Atribui um novo valor para o campo tpRequisicao.
	 *
	 * @param tpRequisicao novo valor para o campo tpRequisicao.
	 */
	public void setTpRequisicao(Long tpRequisicao) {
		this.tpRequisicao = tpRequisicao;
	}

	/**
	 * Retorna o valor do campo tpAcao.
	 *
	 * @return valor do campo tpAcao.
	 */
	public Long getTpAcao() {
		return tpAcao;
	}

	/**
	 * Atribui um novo valor para o campo tpAcao.
	 *
	 * @param tpAcao novo valor para o campo tpAcao.
	 */
	public void setTpAcao(Long tpAcao) {
		this.tpAcao = tpAcao;
	}

	/**
	 * Retorna o valor do campo tpProduto.
	 *
	 * @return valor do campo tpProduto.
	 */
	public Long getTpProduto() {
		return tpProduto;
	}

	/**
	 * Atribui um novo valor para o campo tpProduto.
	 *
	 * @param tpProduto novo valor para o campo tpProduto.
	 */
	public void setTpProduto(Long tpProduto) {
		this.tpProduto = tpProduto;
	}

	/**
	 * Retorna o valor do campo dtInicial.
	 *
	 * @return valor do campo dtInicial.
	 */
	public Date getDtInicial() {
		return dtInicial;
	}

	/**
	 * Atribui um novo valor para o campo dtInicial.
	 *
	 * @param dtInicial novo valor para o campo dtInicial.
	 */
	public void setDtInicial(Date dtInicial) {
		this.dtInicial = dtInicial;
	}

	/**
	 * Retorna o valor do campo dtFinal.
	 *
	 * @return valor do campo dtFinal.
	 */
	public Date getDtFinal() {
		return dtFinal;
	}

	/**
	 * Atribui um novo valor para o campo dtFinal.
	 *
	 * @param dtFinal novo valor para o campo dtFinal.
	 */
	public void setDtFinal(Date dtFinal) {
		this.dtFinal = dtFinal;
	}

	/**
	 * Retorna o valor do campo dtRequisicao.
	 *
	 * @return valor do campo dtRequisicao.
	 */
	public Date getDtRequisicao() {
		return dtRequisicao;
	}

	/**
	 * Atribui um novo valor para o campo dtRequisicao.
	 *
	 * @param dtRequisicao novo valor para o campo dtRequisicao.
	 */
	public void setDtRequisicao(Date dtRequisicao) {
		this.dtRequisicao = dtRequisicao;
	}

	/**
	 * Retorna o valor do campo ipUsuario.
	 *
	 * @return valor do campo ipUsuario.
	 */
	public String getIpUsuario() {
		return ipUsuario;
	}

	/**
	 * Atribui um novo valor para o campo ipUsuario.
	 *
	 * @param ipUsuario novo valor para o campo ipUsuario.
	 */
	public void setIpUsuario(String ipUsuario) {
		this.ipUsuario = ipUsuario;
	}

	/**
	 * Retorna o valor do campo hrRequisicao.
	 *
	 * @return valor do campo hrRequisicao.
	 */
	public String getHrRequisicao() {
		return hrRequisicao;
	}

	/**
	 * Atribui um novo valor para o campo hrRequisicao.
	 *
	 * @param hrRequisicao novo valor para o campo hrRequisicao.
	 */
	public void setHrRequisicao(String hrRequisicao) {
		this.hrRequisicao = hrRequisicao;
	}

	public Long getTpCanal() {
		return tpCanal;
	}

	public void setTpCanal(Long tpCanal) {
		this.tpCanal = tpCanal;
	}

}
