package br.com.db1.portalcooperado.model.entity.avicultura;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.model.entity.Usuario;
import jersey.repackaged.com.google.common.base.MoreObjects;
import jersey.repackaged.com.google.common.base.Objects;
import jersey.repackaged.com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "COOP_AVICULTURA")
@MBClass
public class Avicultura {

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_AVICULTURA")
	@Column(name = "ID_AVICULTURA", updatable = false, unique = true)
	private Long id;

	@Column(name = "ID_PRODUTOR", nullable = false, updatable = false)
	private Usuario produtor;

	@Column(name = "NM_GALPAO", length = 100, nullable = false)
	private String galpao;

	@Column(name = "DS_LOTE", length = 100, nullable = false)
	private String lote;

	@Column(name = "SEXO", length = 1, nullable = false)
	private String sexo;

	@Column(name = "NR_QUANTIDADE", nullable = false)
	private Integer quantidade;

	@Column(name = "DT_DATA", nullable = false)
	private Date data;

	@Column(name = "DS_LINHAGEM", length = 100, nullable = false)
	private String linhagem;

	@Column(name = "NM_TECNICO", length = 250, nullable = false)
	private String nomeTecnico;

	@OneToMany(mappedBy = "avicultura")
	private List<AviculturaItem> itens = Lists.newLinkedList();

	public Avicultura() {
		// for frameworks
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Avicultura) {
			Avicultura other = (Avicultura) obj;
			return Objects.equal(produtor, other.produtor) &&
					Objects.equal(galpao, other.galpao) &&
					Objects.equal(lote, other.lote) &&
					Objects.equal(sexo, other.sexo) &&
					Objects.equal(quantidade, other.quantidade) &&
					Objects.equal(nomeTecnico, other.nomeTecnico) &&
					Objects.equal(data, other.data);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(produtor, galpao, lote, sexo, quantidade, nomeTecnico, data);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("produtor", produtor)
				.add("galpao", galpao)
				.add("lote", lote)
				.add("sexo", sexo)
				.add("quantidade", quantidade)
				.add("data", data)
				.add("linhagem", linhagem)
				.add("nomeTecnico", nomeTecnico)
				.toString();
	}

	public void addDifferentItensKeepingOld(List<AviculturaItem> itens) {
		itens.removeAll(this.itens);
		this.itens.addAll(itens);
	}

	public String matriculaAsString() {
		return produtor.getMatricula().toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getProdutor() {
		return produtor;
	}

	public void setProdutor(Usuario produtor) {
		this.produtor = produtor;
	}

	public String getGalpao() {
		return galpao;
	}

	public void setGalpao(String galpao) {
		this.galpao = galpao;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getLinhagem() {
		return linhagem;
	}

	public void setLinhagem(String linhagem) {
		this.linhagem = linhagem;
	}

	public String getNomeTecnico() {
		return nomeTecnico;
	}

	public void setNomeTecnico(String nomeTecnico) {
		this.nomeTecnico = nomeTecnico;
	}

	public List<AviculturaItem> getItens() {
		return itens;
	}

	public void setItens(List<AviculturaItem> itens) {
		this.itens = itens;
	}

}
