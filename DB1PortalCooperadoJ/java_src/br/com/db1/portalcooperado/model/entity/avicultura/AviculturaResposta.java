package br.com.db1.portalcooperado.model.entity.avicultura;

import br.com.db1.portalcooperado.util.Mensagem;
import jersey.repackaged.com.google.common.collect.ImmutableMap;

import java.util.Map;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public enum AviculturaResposta {

	CONFORMIDADE("CONFORMIDADE"),

	NAO_CONFORMIDADE("NAO CONFORMIDADE");

	private static final Map<String, AviculturaResposta> CODIGO_VALUE_MAP;

	static {
		ImmutableMap.Builder<String, AviculturaResposta> builder = ImmutableMap.builder();
		for (AviculturaResposta resposta : values()) {
			builder.put(resposta.codigo, resposta);
		}
		CODIGO_VALUE_MAP = builder.build();
	}

	private final String codigo;

	AviculturaResposta(String codigo) {
		this.codigo = codigo;
	}

	public static AviculturaResposta fromCodigo(String codigo) {
		checkNotNull(codigo, Mensagem.campoObrigatorioMasculino("Tipo de resposta da avicultura"));
		checkArgument(CODIGO_VALUE_MAP.containsKey(codigo), "Tipo de resposta da avicultura inexistente.");
		return CODIGO_VALUE_MAP.get(codigo);
	}

}
