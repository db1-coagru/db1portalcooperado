package br.com.db1.portalcooperado.model.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.db1.myBatisPersistence.annotations.MBClass;

/**
 * Classe para lista os outros tipos de movimentacoes de um cooperante no relatório.
 * 
 * @author Wagner Mendes Voltz
 * @since 10/07/2011
 */
@Entity
@Table(name = "COOP_POS_FIN_OUT")
@MBClass
public class PosicaoFinanceiraOutros {

	@Column(name = "ID_REQUISICAO")
	private Long idRequisicao;

	@Column(name = "DS_TIPO")
	private String dsTipo;
	
	@Column(name = "DS_PRODUTO")
	private String dsProduto;
	
	@Column(name = "VL_QUANTIDADE")
	private BigDecimal vlQuantidade;
	
	@Column(name = "VL_PRODUTO")
	private BigDecimal vlProduto;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DT_VENCIMENTO")
	private Date dtVencimento;

	/**
	 * Retorna o valor do campo idRequisicao.
	 *
	 * @return valor do campo idRequisicao.
	 */
	public Long getIdRequisicao() {
		return idRequisicao;
	}

	/**
	 * Atribui um novo valor para o campo idRequisicao.
	 *
	 * @param idRequisicao novo valor para o campo idRequisicao.
	 */
	public void setIdRequisicao(Long idRequisicao) {
		this.idRequisicao = idRequisicao;
	}

	/**
	 * Retorna o valor do campo dsTipo.
	 *
	 * @return valor do campo dsTipo.
	 */
	public String getDsTipo() {
		return dsTipo;
	}

	/**
	 * Atribui um novo valor para o campo dsTipo.
	 *
	 * @param dsTipo novo valor para o campo dsTipo.
	 */
	public void setDsTipo(String dsTipo) {
		this.dsTipo = dsTipo;
	}

	/**
	 * Retorna o valor do campo dsProduto.
	 *
	 * @return valor do campo dsProduto.
	 */
	public String getDsProduto() {
		return dsProduto;
	}

	/**
	 * Atribui um novo valor para o campo dsProduto.
	 *
	 * @param dsProduto novo valor para o campo dsProduto.
	 */
	public void setDsProduto(String dsProduto) {
		this.dsProduto = dsProduto;
	}

	/**
	 * Retorna o valor do campo vlQuantidade.
	 *
	 * @return valor do campo vlQuantidade.
	 */
	public BigDecimal getVlQuantidade() {
		return vlQuantidade;
	}

	/**
	 * Atribui um novo valor para o campo vlQuantidade.
	 *
	 * @param vlQuantidade novo valor para o campo vlQuantidade.
	 */
	public void setVlQuantidade(BigDecimal vlQuantidade) {
		this.vlQuantidade = vlQuantidade;
	}

	/**
	 * Retorna o valor do campo vlProduto.
	 *
	 * @return valor do campo vlProduto.
	 */
	public BigDecimal getVlProduto() {
		return vlProduto;
	}

	/**
	 * Atribui um novo valor para o campo vlProduto.
	 *
	 * @param vlProduto novo valor para o campo vlProduto.
	 */
	public void setVlProduto(BigDecimal vlProduto) {
		this.vlProduto = vlProduto;
	}

	/**
	 * Retorna o valor do campo dtVencimento.
	 *
	 * @return valor do campo dtVencimento.
	 */
	public Date getDtVencimento() {
		return dtVencimento;
	}

	/**
	 * Atribui um novo valor para o campo dtVencimento.
	 *
	 * @param dtVencimento novo valor para o campo dtVencimento.
	 */
	public void setDtVencimento(Date dtVencimento) {
		this.dtVencimento = dtVencimento;
	}

	
}
