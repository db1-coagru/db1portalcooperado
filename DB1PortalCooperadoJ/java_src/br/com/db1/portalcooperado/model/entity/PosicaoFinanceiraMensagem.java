package br.com.db1.portalcooperado.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import br.com.db1.myBatisPersistence.annotations.MBClass;

/**
 * Classe para lista os outros tipos de movimentacoes de um cooperante no relatório.
 * 
 * @author Wagner Mendes Voltz
 * @since 10/07/2011
 */
@Entity
@Table(name = "COOP_POS_FIN_CRED_OBS")
@MBClass
public class PosicaoFinanceiraMensagem {

	@Id
	@Column(name = "ID_REQUISICAO")
	private Long idRequisicao;

	@Column(name = "MSG_CREDITO")
	private String mensagem;

	/**
	 * Retorna o valor do campo idRequisicao.
	 *
	 * @return valor do campo idRequisicao.
	 */
	public Long getIdRequisicao() {
		return idRequisicao;
	}

	/**
	 * Atribui um novo valor para o campo idRequisicao.
	 *
	 * @param idRequisicao novo valor para o campo idRequisicao.
	 */
	public void setIdRequisicao(Long idRequisicao) {
		this.idRequisicao = idRequisicao;
	}

	/**
	 * Retorna o valor do campo mensagem.
	 *
	 * @return valor do campo mensagem.
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Atribui um novo valor para o campo mensagem.
	 *
	 * @param mensagem novo valor para o campo mensagem.
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
