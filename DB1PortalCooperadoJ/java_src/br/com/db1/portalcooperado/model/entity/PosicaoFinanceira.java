package br.com.db1.portalcooperado.model.entity;

import br.com.db1.portalcooperado.requisicao.ConectorBaseDados;
import br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira.PosicaoFinanceiraJson;

import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class PosicaoFinanceira {

	private final Requisicao requisicao;

	private final List<PosicaoFinanceiraCredito> creditos;

	private final List<PosicaoFinanceiraDebitos> debitos;

	private final List<PosicaoFinanceiraDebitos> debitosCreditos;

	private final PosicaoFinanceiraMensagem mensagemCreditos;

	private final List<PosicaoFinanceiraOutros> outros;

	private PosicaoFinanceira(Builder builder) {
		this.requisicao = builder.requisicao;
		this.creditos = builder.creditos;
		this.debitos = builder.debitos;
		this.debitosCreditos = builder.debitosCreditos;
		this.mensagemCreditos = builder.mensagemCreditos;
		this.outros = builder.outros;
	}

	public static Builder builder(Requisicao requisicao) {
		checkNotNull(requisicao, "Requisição deve ser informada para geração da posição financeira.");
		return new Builder(requisicao);
	}

	public boolean hasMensagem() {
		return mensagemCreditos != null;
	}

	public String getMensagem() {
		return hasMensagem() ? mensagemCreditos.getMensagem() : "";
	}

	public List<PosicaoFinanceiraDebitos> geDebitos() {
		return debitos;
	}

	public List<PosicaoFinanceiraCredito> getCreditos() {
		return creditos;
	}

	public List<PosicaoFinanceiraDebitos> getDebitosCreditos() {
		return debitosCreditos;
	}

	public List<PosicaoFinanceiraOutros> getOutros() {
		return outros;
	}

	public PosicaoFinanceiraJson toJson() {
		return PosicaoFinanceiraJson.builder(this).build();
	}

	public Requisicao getRequisicao() {
		return requisicao;
	}

	public static class Builder {

		private final Requisicao requisicao;

		private List<PosicaoFinanceiraCredito> creditos;

		private List<PosicaoFinanceiraDebitos> debitos;

		private List<PosicaoFinanceiraDebitos> debitosCreditos;

		private PosicaoFinanceiraMensagem mensagemCreditos;

		private List<PosicaoFinanceiraOutros> outros;

		private Builder(Requisicao requisicao) {
			this.requisicao = requisicao;
		}

		public PosicaoFinanceira build() {
			ConectorBaseDados instancia = ConectorBaseDados.getInstancia();
			Long redId = requisicao.getId();
			this.mensagemCreditos = instancia.remoteFindPosicaoFinanceiraMensagemById(redId);
			this.debitos = instancia.remoteFindPosicaoFinanceiraDebitosByCondition(redId, false);
			this.creditos = instancia.remoteFindPosicaoFinanceiraCreditoByCondition(redId);
			this.debitosCreditos = instancia.remoteFindPosicaoFinanceiraDebitosByCondition(redId, true);
			this.outros = instancia.remoteFindPosicaoFinanceiraOutrosByCondition(redId);
			return new PosicaoFinanceira(this);
		}
	}

}
