package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.model.types.StatusToken;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import br.com.db1.security.model.entity.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.oauth2.common.util.RandomValueStringGenerator;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

import static jersey.repackaged.com.google.common.base.Preconditions.checkNotNull;

/**
 * Classe que irá controlar os tokens de autenticação do usuário na API REST.
 *
 * @author rafael.rasso
 * @since 19/07/2011
 */
@Entity
@Table(name = "COOP_TOKEN")
@MBClass
public class Token {

	private static final Integer LENGTH = 8;

	private static final String PATTERN = "%s.%s.%s";

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_TOKEN")
	@Column(name = "ID_TOKEN", insertable = true, updatable = false, unique = true)
	private Long id;

	@Column(name = "ID_SEC_USER", nullable = false)
	private Long idUser;

	@Column(name = "DS_ACCESS_TOKEN", nullable = false, unique = true)
	private String accessToken;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_GERACAO", nullable = false)
	private Date geracao;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_EXPIRACAO", nullable = false)
	private Date expiracao;

	@Enumerated(EnumType.STRING)
	@Column(name = "TP_STATUS", length = 8, nullable = false)
	private StatusToken status = StatusToken.VALIDO;

	/**
	 * Consrutor padrão utilizado pelo framework de persistência
	 */
	protected Token() {

	}

	private Token(Builder builder) {
		this.idUser = builder.usuario.getIdUser();
		this.accessToken = builder.accessToken;
		this.geracao = builder.geracao;
		this.expiracao = builder.expiracao;
	}

	/**
	 * FactoryMethod que retorná um builder do token para a geração do mesmo.
	 *
	 * @param usuario
	 * @return Builder
	 */
	public static Builder builder(User usuario) {
		checkNotNull(usuario, "Usuário deve ser informado para geração do token.");
		return new Builder(usuario);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Token) {
			Token other = (Token) obj;
			return accessToken.equals(other.accessToken);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return accessToken.hashCode();
	}

	/**
	 * Retorna o token como uma representação JSON
	 *
	 * @return TokenJson
	 */
	public TokenJson toJson() {
		return TokenJson.of(this);
	}

	/**
	 * Invalida um token mesmo que já inválido
	 */
	public void invalidar() {
		this.status = StatusToken.INVALIDO;
	}

	/**
	 * Retorna true se o token já expirou e false caso ainda seja válido
	 *
	 * @return boolean
	 */
	public boolean isExpirado() {
		return expiracao.before(new Date());
	}

	/**
	 * Retorna true se o token tem status igual a válido e false caso o status seja inválido.
	 *
	 * @return boolean
	 */
	public boolean isValido() {
		return StatusToken.VALIDO.equals(status);
	}

	/**
	 * Retorna a data de geracao do token em milisegundos.
	 *
	 * @return Long
	 */
	public Long getGeracaoMillis() {
		return PortalCooperadoUtil.timeInMillisOf(geracao);
	}

	/**
	 * Retorna a data de expiracao do token em milisegundos.
	 *
	 * @return Long
	 */
	public Long getExpiracaoMillis() {
		return PortalCooperadoUtil.timeInMillisOf(expiracao);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Date getGeracao() {
		return geracao;
	}

	public void setGeracao(Date geracao) {
		this.geracao = geracao;
	}

	public Date getExpiracao() {
		return expiracao;
	}

	public void setExpiracao(Date expiracao) {
		this.expiracao = expiracao;
	}

	public StatusToken getStatus() {
		return status;
	}

	public void setStatus(StatusToken status) {
		this.status = status;
	}

	/**
	 * Classe construtora de tokens
	 */
	public static class Builder {

		public static final int TIME_EXPIRATION = 30;

		private final User usuario;

		private Date geracao;

		private Date expiracao;

		private String accessToken;

		private Builder(User usuario) {
			this.usuario = usuario;
		}

		/**
		 * Method responsável por construir um novo token.
		 *
		 * @return Token
		 */
		public Token build() {
			String randomValue = generateRandomValue();
			geracao = new Date();
			expiracao = generateExpiration();
			accessToken = DigestUtils.sha1Hex(String.format(PATTERN, randomValue, geracao, expiracao));
			return new Token(this);
		}

		private String generateRandomValue() {
			RandomValueStringGenerator generator = new RandomValueStringGenerator();
			generator.setLength(LENGTH);
			return generator.generate();
		}

		private Date generateExpiration() {
			Date expiration = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(expiration);
			calendar.add(Calendar.DATE, TIME_EXPIRATION);
			expiration = calendar.getTime();
			return expiration;
		}

	}

}
