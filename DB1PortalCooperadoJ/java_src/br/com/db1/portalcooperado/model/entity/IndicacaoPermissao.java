package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;

import javax.persistence.*;

@Entity
@Table(name = "COOP_INDICACAO_PERMISSAO")
@MBClass
public class IndicacaoPermissao {

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_INDICACAO_PERMISSAO")
	@Column(name = "ID_INDICACAO_PERMISSAO", insertable = true, updatable = false, unique = true)
	private Long id;

	@Column(name = "ID_INDICACAO", insertable = true, updatable = false, nullable = false)
	private Indicacao indicacao;

	@Enumerated(EnumType.STRING)
	@Column(name = "DS_PERMISSAO", insertable = true, updatable = false, nullable = false)
	private PermissaoIndicado permissao;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IndicacaoPermissao) {
			IndicacaoPermissao other = (IndicacaoPermissao) obj;
			return PortalCooperadoUtil.areEquals(this.indicacao, other.indicacao)
					&& PortalCooperadoUtil.areEquals(this.permissao, other.permissao);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(indicacao, permissao);
	}

	public void setPermissao(Integer permissao) {
		this.permissao = PermissaoIndicado.fromCodigo(permissao);
	}

	public String getDescricaoPermissao() {
		return permissao.getDescricao();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Indicacao getIndicacao() {
		return indicacao;
	}

	public void setIndicacao(Indicacao indicacao) {
		this.indicacao = indicacao;
	}

	public PermissaoIndicado getPermissao() {
		return permissao;
	}

	public void setPermissao(PermissaoIndicado permissao) {
		this.permissao = permissao;
	}

}
