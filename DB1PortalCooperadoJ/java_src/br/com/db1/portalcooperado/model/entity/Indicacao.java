package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.indicacao.IndicacaoAcao;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;

@Entity
@Table(name = "COOP_INDICACAO")
@MBClass
public class Indicacao {

    @Id
    @MBSequenceGenerator(sequenceName = "SEQ_INDICACAO")
    @Column(name = "ID_INDICACAO", insertable = true, updatable = false, unique = true)
    private Long id;

    @Column(name = "ID_INDICADOR", insertable = true, updatable = true, nullable = false)
    private Usuario indicador;

    @Column(name = "ID_INDICADO", insertable = true, updatable = true, nullable = false)
    private Usuario indicado;

    @OneToMany(mappedBy = "indicacao")
    private List<IndicacaoPermissao> permissoes;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Indicacao) {
            Indicacao other = (Indicacao) obj;
            return PortalCooperadoUtil.areEquals(this.indicador, other.indicador)
                    && PortalCooperadoUtil.areEquals(this.indicado, other.indicado);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return PortalCooperadoUtil.hashCode(indicador, indicado);
    }

    public void editPermissoes(List<Integer> permissaoIndicados) {
        checkArgument(!permissaoIndicados.isEmpty(), "Ao menos uma permissão deve ser informada.");
        permissoes = Lists.newLinkedList();
        for (Integer permissaoIndicado : permissaoIndicados) {
            IndicacaoPermissao indicacaoPermissao = new IndicacaoPermissao();
            indicacaoPermissao.setIndicacao(this);
            indicacaoPermissao.setPermissao(permissaoIndicado);
            permissoes.add(indicacaoPermissao);
        }
    }

    public boolean hasPermissaoTo(String matricula, PermissaoIndicado permissaoIndicado) {
        return indicador.getMatricula().toString().equals(matricula)
                && contains(permissaoIndicado);
    }

    private boolean contains(PermissaoIndicado permissaoIndicado) {
        for (IndicacaoPermissao permissao : permissoes) {
            if (permissao.getPermissao().equals(permissaoIndicado)) {
                return true;
            }
        }
        return false;
    }

    public IndicacaoAcao getAcao() {
        if (id != null) {
            return IndicacaoAcao.ALTERACAO;
        }
        return IndicacaoAcao.INCLUSAO;
    }

    public Long getIdIndicador() {
        return indicador.getId();
    }

    public Long getMatriculaIndicadorAsLong() {
        return indicador.getMatricula();
    }

    public String getMatriculaIndicador() {
        return indicador.getMatricula().toString();
    }

    public String getMatriculaIndicado() {
        return indicado.getMatricula().toString();
    }

    public String getNomeIndicador() {
        return indicador.getNome();
    }

    public List<PermissaoIndicado> getPermissoesIndicado() {
        ImmutableList.Builder<PermissaoIndicado> builder = ImmutableList.builder();
        for (IndicacaoPermissao indicacaoPermissao : permissoes) {
            builder.add(indicacaoPermissao.getPermissao());
        }
        return builder.build();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getIndicador() {
        return indicador;
    }

    public void setIndicador(Usuario indicador) {
        this.indicador = indicador;
    }

    public Usuario getIndicado() {
        return indicado;
    }

    public void setIndicado(Usuario indicado) {
        this.indicado = indicado;
    }

    public List<IndicacaoPermissao> getPermissoes() {
        return permissoes;
    }

    public void setPermissoes(List<IndicacaoPermissao> permissoes) {
        this.permissoes = permissoes;
    }

}
