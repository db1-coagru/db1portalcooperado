package br.com.db1.portalcooperado.model.entity.avicultura;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import jersey.repackaged.com.google.common.base.MoreObjects;
import jersey.repackaged.com.google.common.base.Objects;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "COOP_AVICULTURA_ITEM")
@MBClass
public class AviculturaItem {

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_AVICULTURA_ITEM")
	@Column(name = "ID_AVICULTURA_ITEM", updatable = false, unique = true)
	private Long id;

	@Column(name = "ID_AVICULTURA", nullable = false)
	private Avicultura avicultura;

	@Column(name = "DS_DESCRICAO", nullable = false)
	private String descricao;

	@Column(name = "DS_RESPOSTA", length = 100, nullable = false)
	private AviculturaResposta resposta;

	@Column(name = "DS_OBSERVACAO", length = 5000)
	private String observacao;

	@OneToMany(mappedBy = "aviculturaItem")
	private List<AviculturaItemImagem> imagens = Lists.newLinkedList();

	public AviculturaItem() {
		// for frameworks
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AviculturaItem) {
			AviculturaItem other = (AviculturaItem) obj;
			return Objects.equal(this.descricao, other.descricao) &&
					Objects.equal(this.resposta, other.resposta) &&
					Objects.equal(this.getObservacao(), other.getObservacao());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(descricao, resposta, getObservacao());
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("id", id)
				.add("avicultura", avicultura)
				.add("descricao", descricao)
				.add("resposta", resposta)
				.add("observacao", observacao)
				.toString();
	}

	public void addImagem(AviculturaItemImagem imagem) {
		if (!imagens.contains(imagem)) {
			imagens.add(imagem);
		}
	}

	public List<String> getNomesImagens() {
		ImmutableList.Builder<String> builder = ImmutableList.builder();
		for (AviculturaItemImagem imagem : imagens) {
			builder.add(imagem.getNome());
		}
		return builder.build();
	}

	public void setResposta(String resposta) {
		this.resposta = AviculturaResposta.fromCodigo(resposta);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Avicultura getAvicultura() {
		return avicultura;
	}

	public void setAvicultura(Avicultura avicultura) {
		this.avicultura = avicultura;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public AviculturaResposta getResposta() {
		return resposta;
	}

	public void setResposta(AviculturaResposta resposta) {
		this.resposta = resposta;
	}

	public String getObservacao() {
		return MoreObjects.firstNonNull(observacao, "");
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public List<AviculturaItemImagem> getImagens() {
		return imagens;
	}

	public void setImagens(List<AviculturaItemImagem> imagens) {
		this.imagens = imagens;
	}

}
