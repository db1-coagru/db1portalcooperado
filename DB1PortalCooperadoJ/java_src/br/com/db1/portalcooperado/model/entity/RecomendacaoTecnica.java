package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.model.types.Produto;
import br.com.db1.portalcooperado.model.types.Quantidade;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static jersey.repackaged.com.google.common.base.MoreObjects.firstNonNull;
import static jersey.repackaged.com.google.common.base.MoreObjects.toStringHelper;

@Entity
@Table(name = "COOP_RECOMENDACAO_TECNICA")
@MBClass
public class RecomendacaoTecnica {

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_RECOMENDACAO_TECNICA")
	@Column(name = "ID_RECOMENDACAO", insertable = true, updatable = false, unique = true)
	private Long id;

	@Column(name = "ID_USUARIO", nullable = false, insertable = true, updatable = false)
	private Usuario usuario;

	@Column(name = "DS_PRODUTO", nullable = false, insertable = true, updatable = false)
	private Produto produto;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_CRIACAO", nullable = false, insertable = true, updatable = false)
	private Date criacao;

	@Column(name = "QT_TOTAL_AREA", nullable = false, insertable = true, updatable = false)
	private Quantidade totalArea;

	@Column(name = "QT_EFICIENCIA", nullable = false, insertable = true, updatable = false)
	private Quantidade eficiencia;

	@Column(name = "QT_VOLUME", nullable = false, insertable = true, updatable = false)
	private Quantidade volume;

	@Column(name = "DS_RECOMENDACAO", insertable = true, updatable = false, length = 2000)
	private String recomendacao;

	@Column(name = "DS_NOME_ARQUIVO", insertable = true, updatable = false, length = 100)
	private String nomeArquivo;

	@Column(name = "BO_URGENTE", length = 1)
	private Boolean urgente;

	@Column(name = "DS_MENSAGEM_CUSTOMIZADA", length = 2000)
	private String mensagemCustomizada;

	@OneToMany(mappedBy = "recomendacaoTecnica")
	private List<RecomendacaoTecnicaDiagnostico> diagnosticos = Lists.newLinkedList();

	@OneToMany(mappedBy = "recomendacaoTecnica")
	private List<RecomendacaoTecnicaProduto> produtos = Lists.newLinkedList();

	@OneToMany(mappedBy = "recomendacaoTecnica")
	private List<RecomendacaoTecnicaImagem> imagens = Lists.newLinkedList();

	protected RecomendacaoTecnica() {
		//construtor padrao utilizado por frameworks
	}

	public static RecomendacaoTecnica of() {
		return new RecomendacaoTecnica();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RecomendacaoTecnica) {
			RecomendacaoTecnica other = (RecomendacaoTecnica) obj;
			return PortalCooperadoUtil.areEquals(this.nomeArquivo, other.nomeArquivo);

		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(this.nomeArquivo);
	}

	@Override
	public String toString() {
		return toStringHelper(this)
				.add("produto", produto)
				.add("criacao", criacao)
				.add("totalArea", totalArea)
				.add("eficiencia", eficiencia)
				.add("volume", volume)
				.add("recomendacao", recomendacao)
				.add("nomeArquivo", nomeArquivo)
				.toString();
	}

	public Long matricula() {
		return usuario.getMatricula();
	}

	public String matriculaAsString() {
		return matricula().toString();
	}

	public String descricaoProduto() {
		return produto.getDescricao();
	}

	public String totalAreaAsString() {
		return totalArea.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Date getCriacao() {
		return criacao;
	}

	public void setCriacao(Date criacao) {
		this.criacao = criacao;
	}

	public Quantidade getTotalArea() {
		return totalArea;
	}

	public void setTotalArea(Quantidade totalArea) {
		this.totalArea = totalArea;
	}

	public Quantidade getEficiencia() {
		return eficiencia;
	}

	public void setEficiencia(Quantidade eficiencia) {
		this.eficiencia = eficiencia;
	}

	public Quantidade getVolume() {
		return volume;
	}

	public void setVolume(Quantidade volume) {
		this.volume = volume;
	}

	public String getRecomendacao() {
		return recomendacao;
	}

	public void setRecomendacao(String recomendacao) {
		this.recomendacao = recomendacao;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

	public Boolean getUrgente() {
		return urgente;
	}

	public void setUrgente(Boolean urgente) {
		this.urgente = urgente;
	}

	public String getMensagemCustomizada() {
		return mensagemCustomizada;
	}

	public void setMensagemCustomizada(String mensagemCustomizada) {
		this.mensagemCustomizada = mensagemCustomizada;
	}

	public List<RecomendacaoTecnicaDiagnostico> getDiagnosticos() {
		return diagnosticos;
	}

	public void setDiagnosticos(List<RecomendacaoTecnicaDiagnostico> diagnosticos) {
		this.diagnosticos = firstNonNull(diagnosticos, Lists.<RecomendacaoTecnicaDiagnostico>newLinkedList());
	}

	public List<RecomendacaoTecnicaProduto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<RecomendacaoTecnicaProduto> produtos) {
		this.produtos = firstNonNull(produtos, Lists.<RecomendacaoTecnicaProduto>newLinkedList());
	}

	public List<RecomendacaoTecnicaImagem> getImagens() {
		return imagens;
	}

	public void setImagens(List<RecomendacaoTecnicaImagem> imagens) {
		this.imagens = imagens;
	}

}
