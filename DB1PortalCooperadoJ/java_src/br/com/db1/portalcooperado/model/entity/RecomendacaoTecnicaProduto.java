package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.model.types.Quantidade;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.base.MoreObjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COOP_RECOMENDACAO_TECNICA_PROD")
@MBClass
public class RecomendacaoTecnicaProduto {

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_RECOMENDACAO_TECNICA_PROD")
	@Column(name = "ID_RECOMENDACAO_PROD", insertable = true, updatable = false, unique = true)
	private Long id;

	@Column(name = "ID_RECOMENDACAO", nullable = false, insertable = true, updatable = false)
	private RecomendacaoTecnica recomendacaoTecnica;

	@Column(name = "CD_PRODUTO", nullable = false, insertable = true, updatable = false)
	private Long codigo;

	@Column(name = "DS_NOME", nullable = false, insertable = true, updatable = false)
	private String nome;

	@Column(name = "NR_ORDEM", nullable = false, insertable = true, updatable = false)
	private Long ordem;

	@Column(name = "QT_QUANTIDADE", nullable = false, insertable = true, updatable = false)
	private Quantidade quantidade;

	@Column(name = "DS_UNIDADE_MEDIDA", nullable = false, insertable = true, updatable = false)
	private String unidadeMedida;

	@Column(name = "QT_TOTAL", nullable = false, insertable = true, updatable = false)
	private Quantidade total;

	@Column(name = "QT_BOMBA", nullable = false, insertable = true, updatable = false)
	private Quantidade bomba;

	protected RecomendacaoTecnicaProduto() {
		//construtor padrao utilizado por frameworks
	}

	public RecomendacaoTecnicaProduto(Builder builder) {
		this.recomendacaoTecnica = builder.recomendacaoTecnica;
		this.codigo = builder.codigo;
		this.nome = builder.nome;
		this.ordem = builder.ordem;
		this.quantidade = builder.quantidade;
		this.unidadeMedida = builder.unidadeMedida;
		this.total = builder.calcularTotal();
		this.bomba = builder.calcularBomba();
	}

	public static Builder builder() {
		return new Builder();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RecomendacaoTecnicaProduto) {
			RecomendacaoTecnicaProduto other = (RecomendacaoTecnicaProduto) obj;
			return PortalCooperadoUtil.areEquals(this.recomendacaoTecnica, other.recomendacaoTecnica)
					&& PortalCooperadoUtil.areEquals(this.codigo, other.codigo);

		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(recomendacaoTecnica, codigo);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("recomendacaoTecnica", recomendacaoTecnica)
				.add("codigo", codigo)
				.add("nome", nome)
				.add("ordem", ordem)
				.add("quantidade", quantidade)
				.add("unidadeMedida", unidadeMedida)
				.add("total", total)
				.add("bomba", bomba)
				.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RecomendacaoTecnica getRecomendacaoTecnica() {
		return recomendacaoTecnica;
	}

	public void setRecomendacaoTecnica(RecomendacaoTecnica recomendacaoTecnica) {
		this.recomendacaoTecnica = recomendacaoTecnica;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getOrdem() {
		return ordem;
	}

	public void setOrdem(Long ordem) {
		this.ordem = ordem;
	}

	public Quantidade getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Quantidade quantidade) {
		this.quantidade = MoreObjects.firstNonNull(quantidade, Quantidade.zero());
	}

	public String getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(String unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public Quantidade getTotal() {
		return total;
	}

	public void setTotal(Quantidade total) {
		this.total = MoreObjects.firstNonNull(total, Quantidade.zero(3));
	}

	public Quantidade getBomba() {
		return bomba;
	}

	public void setBomba(Quantidade bomba) {
		this.bomba = MoreObjects.firstNonNull(bomba, Quantidade.of(3));
	}

	public static class Builder {

		private RecomendacaoTecnica recomendacaoTecnica;

		private Long codigo;

		private String nome;

		private Long ordem;

		private Quantidade totalArea = Quantidade.zero(3);

		private Quantidade eficiencia = Quantidade.zero(3);

		private Quantidade quantidade = Quantidade.zero();

		private String unidadeMedida;

		private Builder() {

		}

		public Builder withRecomendacaoTecnica(RecomendacaoTecnica recomendacaoTecnica) {
			this.recomendacaoTecnica = recomendacaoTecnica;
			return this;
		}

		public Builder withCodigo(Long codigo) {
			this.codigo = codigo;
			return this;
		}

		public Builder withNome(String nome) {
			this.nome = nome;
			return this;
		}

		public Builder withOrdem(Long ordem) {
			this.ordem = ordem;
			return this;
		}

		public Builder withTotalArea(Quantidade totalArea) {
			this.totalArea = MoreObjects.firstNonNull(totalArea, Quantidade.zero(3));
			return this;
		}

		public Builder withEficiencia(Quantidade eficiencia) {
			this.eficiencia = MoreObjects.firstNonNull(eficiencia, Quantidade.zero(3));
			return this;
		}

		public Builder withQuantidade(Quantidade quantidade) {
			this.quantidade = MoreObjects.firstNonNull(quantidade, Quantidade.zero());
			return this;
		}

		public Builder withUnidadeMedida(String unidadeMedida) {
			this.unidadeMedida = unidadeMedida;
			return this;
		}

		private Quantidade calcularTotal() {
			return totalArea.multiply(quantidade);
		}

		private Quantidade calcularBomba() {
			Quantidade total = calcularTotal();
			Quantidade indice = totalArea.multiply(eficiencia);
			if (total.isZero() || indice.isZero()) {
				return Quantidade.zero(3);
			}
			return total.divide(indice);
		}

		public RecomendacaoTecnicaProduto build() {
			return new RecomendacaoTecnicaProduto(this);
		}

	}
}
