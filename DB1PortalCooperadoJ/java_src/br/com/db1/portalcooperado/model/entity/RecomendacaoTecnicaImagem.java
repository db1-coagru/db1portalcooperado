package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.base.MoreObjects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "COOP_RECOMENDACAO_TECNICA_IMAG")
@MBClass
public class RecomendacaoTecnicaImagem {

	@Id
	@MBSequenceGenerator(sequenceName = "SEQ_RECOMENDACAO_TECNICA_IMAG")
	@Column(name = "ID_RECOMENDACAO_ITEM_IMAG", insertable = true, updatable = false, unique = true)
	private Long id;

	@Column(name = "ID_RECOMENDACAO", nullable = false, insertable = true, updatable = false)
	private RecomendacaoTecnica recomendacaoTecnica;

	@Column(name = "DS_NOME", nullable = false, insertable = true, updatable = false)
	private String nome;

	protected RecomendacaoTecnicaImagem() {
		//construtor padrao utilizado por frameworks
	}

	public static RecomendacaoTecnicaImagem of() {
		return new RecomendacaoTecnicaImagem();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RecomendacaoTecnicaImagem) {
			RecomendacaoTecnicaImagem other = (RecomendacaoTecnicaImagem) obj;
			return PortalCooperadoUtil.areEquals(this.nome, other.nome);

		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(nome);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("recomendacaoTecnica", recomendacaoTecnica)
				.add("nome", nome)
				.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RecomendacaoTecnica getRecomendacaoTecnica() {
		return recomendacaoTecnica;
	}

	public void setRecomendacaoTecnica(RecomendacaoTecnica recomendacaoTecnica) {
		this.recomendacaoTecnica = recomendacaoTecnica;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
