package br.com.db1.portalcooperado.model.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.db1.myBatisPersistence.annotations.MBClass;

/**
 * Classe para listar as entregas de produção
 * 
 * @author Wagner Mendes Voltz
 * @since 10/07/2011
 */
@Entity
@Table(name = "COOP_ENT_PROD_AGR")
@MBClass
public class EntregaProducao {

	@Column(name = "ID_REQUISICAO")
	private Long idRequisicao;

	@Temporal(TemporalType.DATE)
	@Column(name = "DT_ENTREGA")
	private Date dtEntrega;

	@Column(name = "DS_PRODUTO")
	private String dsProduto;

	@Column(name = "CD_ORDENACAO")
	private Long cdOrdenacao;

	@Column(name = "DS_LOCAL")
	private String dsLocal;

	@Column(name = "NR_ROMANEIO")
	private String nrRomaneio;

	@Column(name = "NR_SEQUENCIA")
	private Long nrSequencia;

	@Column(name = "NR_CAD_PRO")
	private String nrCadPro;

	@Column(name = "PL_VEICULO")
	private String placa;

	@Column(name = "NR_PADRAO")
	private Long nrPadrao;

	@Column(name = "NR_TIPO")
	private Long nrTipo;

	@Column(name = "PS_BRUTO")
	private BigDecimal pesoBruto;

	@Column(name = "PS_LIQUIDO")
	private BigDecimal pesoLiquido;

	@Column(name = "PC_IMPUREZA")
	private BigDecimal pcImpureza;

	@Column(name = "PC_QUEBRADO")
	private BigDecimal pcQuebrado;

	@Column(name = "PC_ARDIDO")
	private BigDecimal pcArdido;

	@Column(name = "PC_UMIDADE")
	private BigDecimal pcUmidade;

	/**
	 * Retorna o valor do campo idRequisicao.
	 * 
	 * @return valor do campo idRequisicao.
	 */
	public Long getIdRequisicao() {
		return idRequisicao;
	}

	/**
	 * Atribui um novo valor para o campo idRequisicao.
	 * 
	 * @param idRequisicao
	 *            novo valor para o campo idRequisicao.
	 */
	public void setIdRequisicao(Long idRequisicao) {
		this.idRequisicao = idRequisicao;
	}

	/**
	 * Retorna o valor do campo dtEntrega.
	 * 
	 * @return valor do campo dtEntrega.
	 */
	public Date getDtEntrega() {
		return dtEntrega;
	}

	/**
	 * Atribui um novo valor para o campo dtEntrega.
	 * 
	 * @param dtEntrega
	 *            novo valor para o campo dtEntrega.
	 */
	public void setDtEntrega(Date dtEntrega) {
		this.dtEntrega = dtEntrega;
	}

	/**
	 * Retorna o valor do campo dsProduto.
	 * 
	 * @return valor do campo dsProduto.
	 */
	public String getDsProduto() {
		return dsProduto;
	}

	/**
	 * Atribui um novo valor para o campo dsProduto.
	 * 
	 * @param dsProduto
	 *            novo valor para o campo dsProduto.
	 */
	public void setDsProduto(String dsProduto) {
		this.dsProduto = dsProduto;
	}

	/**
	 * Retorna o valor do campo cdOrdenacao.
	 * 
	 * @return valor do campo cdOrdenacao.
	 */
	public Long getCdOrdenacao() {
		return cdOrdenacao;
	}

	/**
	 * Atribui um novo valor para o campo cdOrdenacao.
	 * 
	 * @param cdOrdenacao
	 *            novo valor para o campo cdOrdenacao.
	 */
	public void setCdOrdenacao(Long cdOrdenacao) {
		this.cdOrdenacao = cdOrdenacao;
	}

	/**
	 * Retorna o valor do campo dsLocal.
	 * 
	 * @return valor do campo dsLocal.
	 */
	public String getDsLocal() {
		return dsLocal;
	}

	/**
	 * Atribui um novo valor para o campo dsLocal.
	 * 
	 * @param dsLocal
	 *            novo valor para o campo dsLocal.
	 */
	public void setDsLocal(String dsLocal) {
		this.dsLocal = dsLocal;
	}

	/**
	 * Retorna o valor do campo nrRomaneio.
	 * 
	 * @return valor do campo nrRomaneio.
	 */
	public String getNrRomaneio() {
		return nrRomaneio;
	}

	/**
	 * Atribui um novo valor para o campo nrRomaneio.
	 * 
	 * @param nrRomaneio
	 *            novo valor para o campo nrRomaneio.
	 */
	public void setNrRomaneio(String nrRomaneio) {
		this.nrRomaneio = nrRomaneio;
	}

	/**
	 * Retorna o valor do campo nrSequencia.
	 * 
	 * @return valor do campo nrSequencia.
	 */
	public Long getNrSequencia() {
		return nrSequencia;
	}

	/**
	 * Atribui um novo valor para o campo nrSequencia.
	 * 
	 * @param nrSequencia
	 *            novo valor para o campo nrSequencia.
	 */
	public void setNrSequencia(Long nrSequencia) {
		this.nrSequencia = nrSequencia;
	}

	/**
	 * Retorna o valor do campo nrCadPro.
	 * 
	 * @return valor do campo nrCadPro.
	 */
	public String getNrCadPro() {
		return nrCadPro;
	}

	/**
	 * Atribui um novo valor para o campo nrCadPro.
	 * 
	 * @param nrCadPro
	 *            novo valor para o campo nrCadPro.
	 */
	public void setNrCadPro(String nrCadPro) {
		this.nrCadPro = nrCadPro;
	}

	/**
	 * Retorna o valor do campo placa.
	 * 
	 * @return valor do campo placa.
	 */
	public String getPlaca() {
		return placa;
	}

	/**
	 * Atribui um novo valor para o campo placa.
	 * 
	 * @param placa
	 *            novo valor para o campo placa.
	 */
	public void setPlaca(String placa) {
		this.placa = placa;
	}



	/**
	 * Retorna o valor do campo nrPadrao.
	 * 
	 * @return valor do campo nrPadrao.
	 */
	public Long getNrPadrao() {
		return nrPadrao;
	}

	/**
	 * Atribui um novo valor para o campo nrPadrao.
	 * 
	 * @param nrPadrao
	 *            novo valor para o campo nrPadrao.
	 */
	public void setNrPadrao(Long nrPadrao) {
		this.nrPadrao = nrPadrao;
	}

	/**
	 * Retorna o valor do campo nrTipo.
	 * 
	 * @return valor do campo nrTipo.
	 */
	public Long getNrTipo() {
		return nrTipo;
	}

	/**
	 * Atribui um novo valor para o campo nrTipo.
	 * 
	 * @param nrTipo
	 *            novo valor para o campo nrTipo.
	 */
	public void setNrTipo(Long nrTipo) {
		this.nrTipo = nrTipo;
	}

	/**
	 * Retorna o valor do campo pesoBruto.
	 * 
	 * @return valor do campo pesoBruto.
	 */
	public BigDecimal getPesoBruto() {
		return pesoBruto;
	}

	/**
	 * Atribui um novo valor para o campo pesoBruto.
	 * 
	 * @param pesoBruto
	 *            novo valor para o campo pesoBruto.
	 */
	public void setPesoBruto(BigDecimal pesoBruto) {
		this.pesoBruto = pesoBruto;
	}

	/**
	 * Retorna o valor do campo pesoLiquido.
	 * 
	 * @return valor do campo pesoLiquido.
	 */
	public BigDecimal getPesoLiquido() {
		return pesoLiquido;
	}

	/**
	 * Atribui um novo valor para o campo pesoLiquido.
	 * 
	 * @param pesoLiquido
	 *            novo valor para o campo pesoLiquido.
	 */
	public void setPesoLiquido(BigDecimal pesoLiquido) {
		this.pesoLiquido = pesoLiquido;
	}

	/**
	 * Retorna o valor do campo pcImpureza.
	 * 
	 * @return valor do campo pcImpureza.
	 */
	public BigDecimal getPcImpureza() {
		return pcImpureza;
	}

	/**
	 * Atribui um novo valor para o campo pcImpureza.
	 * 
	 * @param pcImpureza
	 *            novo valor para o campo pcImpureza.
	 */
	public void setPcImpureza(BigDecimal pcImpureza) {
		this.pcImpureza = pcImpureza;
	}

	/**
	 * Retorna o valor do campo pcQuebrado.
	 * 
	 * @return valor do campo pcQuebrado.
	 */
	public BigDecimal getPcQuebrado() {
		return pcQuebrado;
	}

	/**
	 * Atribui um novo valor para o campo pcQuebrado.
	 * 
	 * @param pcQuebrado
	 *            novo valor para o campo pcQuebrado.
	 */
	public void setPcQuebrado(BigDecimal pcQuebrado) {
		this.pcQuebrado = pcQuebrado;
	}

	/**
	 * Retorna o valor do campo pcArdido.
	 * 
	 * @return valor do campo pcArdido.
	 */
	public BigDecimal getPcArdido() {
		return pcArdido;
	}

	/**
	 * Atribui um novo valor para o campo pcArdido.
	 * 
	 * @param pcArdido
	 *            novo valor para o campo pcArdido.
	 */
	public void setPcArdido(BigDecimal pcArdido) {
		this.pcArdido = pcArdido;
	}

	/**
	 * Retorna o valor do campo pcUmidade.
	 * 
	 * @return valor do campo pcUmidade.
	 */
	public BigDecimal getPcUmidade() {
		return pcUmidade;
	}

	/**
	 * Atribui um novo valor para o campo pcUmidade.
	 * 
	 * @param pcUmidade
	 *            novo valor para o campo pcUmidade.
	 */
	public void setPcUmidade(BigDecimal pcUmidade) {
		this.pcUmidade = pcUmidade;
	}

}
