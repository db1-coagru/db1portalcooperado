package br.com.db1.portalcooperado.model.entity;

import br.com.db1.myBatisPersistence.annotations.MBClass;
import br.com.db1.myBatisPersistence.annotations.MBSequenceGenerator;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.model.types.TipoPessoa;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import br.com.db1.security.model.entity.Organization;
import br.com.db1.security.model.entity.Privilege;
import br.com.db1.security.model.entity.Profile;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

/**
 * Classe cadastro de usuário
 *
 * @author Wagner Mendes Voltz
 * @since 01/07/2011
 */
@Entity
@Table(name = "COOP_USUARIO")
@MBClass
public class Usuario {

    public static final String USUARIO_SEM_PERMISSAO = "Você não possui permissão de acesso a essa funcionalidade.";

    public static final String INIDCACAO_NAO_INFORMADA = "Indicação deve ser informada.";

    public static final String EDICAO_NAO_PERMITIDA = "Impossível editar. Indicação inexistente para este usuário.";

    public static final String INDICACAO_EXISTENTE = "Já existe uma indicação para este usuário.";

    public static final String REMOCAO_NAO_PERMITIDA = "Impossível remover. Indicação inexistente para este usuário.";

    @Id
    @MBSequenceGenerator(sequenceName = "SEQ_USUARIO")
    @Column(name = "ID_USUARIO", insertable = true, updatable = false, unique = true)
    private Long id;

    @Column(name = "MAT_USUARIO", insertable = true, updatable = true, length = 6, unique = true, nullable = false)
    private Long matricula;

    @Column(name = "NM_USUARIO", length = 100, nullable = false, insertable = true, updatable = true)
    private String nome;

    @Column(name = "BO_ATIVO", nullable = false)
    private Boolean ativo;

    @Column(name = "EM_USUARIO", length = 60, insertable = true, updatable = true, nullable = false)
    private String email;

    @Temporal(TemporalType.DATE)
    @Column(name = "DT_NASC_USUARIO", nullable = false)
    private Date dtNascimento;

    @Column(name = "CPF_CNPJ_USUARIO", nullable = false, length = 14)
    private String cpfCnpj;

    @Enumerated(EnumType.STRING)
    @Column(name = "TP_PESSOA")
    private TipoPessoa tpPessoa;

    @Temporal(TemporalType.DATE)
    @Column(name = "DT_CADASTRO", nullable = false)
    private Date dtCadastro;

    @Column(name = "HR_CADASTRO", nullable = false, length = 8)
    private String hrCadastro;

    @Column(name = "USUARIO_CADASTRO", nullable = false, length = 30)
    private String usuarioCadastro;

    @Temporal(TemporalType.DATE)
    @Column(name = "DT_ALTERACAO")
    private Date dtAlteracao;

    @Column(name = "HR_ALTERACAO", length = 8)
    private String hrAlteracao;

    @Column(name = "USUARIO_ALTERACAO", length = 30)
    private String usuarioAlteracao;

    @Column(name = "ID_PROFILE", nullable = false)
    private Profile profile;

    @Column(name = "ID_SEC_USER")
    private Long idUser;

    @Column(name = "BO_MOBILE", nullable = false)
    private Boolean mobile;

    @Column(name = "TELEFONE_USUARIO", nullable = true, length = 14)
    private String telefone;

    @OneToMany(mappedBy = "indicador")
    private List<Indicacao> indicacoes = Lists.newLinkedList();

    @OneToMany(mappedBy = "indicado")
    private List<Indicacao> indicacoesIndicado = Lists.newLinkedList();

    @Transient
    private String senha;

    @Transient
    private String contraSenha;

    @Transient
    private Boolean alteraSenha;

    @Transient
    private Boolean enviaEmail;

    @Transient
    private Organization organization;

    @Transient
    private Usuario indicado;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Usuario) {
            Usuario other = (Usuario) obj;
            return PortalCooperadoUtil.areEquals(this.matricula, other.matricula);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return PortalCooperadoUtil.hashCode(this.matricula);
    }

    public void add(Indicacao indicacao) {
        checkNotNull(indicacao, INIDCACAO_NAO_INFORMADA);
        indicacao.setIndicador(this);
        checkArgument(!getIndicacoes().contains(indicacao), INDICACAO_EXISTENTE);
        getIndicacoes().add(indicacao);
    }

    public void edit(Indicacao indicacao) {
        checkNotNull(indicacao, INIDCACAO_NAO_INFORMADA);
        checkArgument(getIndicacoes().contains(indicacao), EDICAO_NAO_PERMITIDA);
        int index = getIndicacoes().indexOf(indicacao);
        getIndicacoes().set(index, indicacao);
    }

    public void remove(Indicacao indicacao) {
        checkNotNull(indicacao, INIDCACAO_NAO_INFORMADA);
        checkArgument(getIndicacoes().contains(indicacao), REMOCAO_NAO_PERMITIDA);
        getIndicacoes().remove(indicacao);
    }

    public boolean hasPermissaoThrowsExceptionIfFalse(Long matricula, PermissaoIndicado permissaoIndicado) {
        boolean temPermissao = this.matricula.equals(matricula) ?
                checkPrivilegiosUsuario(permissaoIndicado) :
                checkPermissoesIndicado(matricula.toString(), permissaoIndicado);
        checkArgument(temPermissao, USUARIO_SEM_PERMISSAO);
        return temPermissao;
    }

    private boolean checkPrivilegiosUsuario(PermissaoIndicado permissaoIndicado) {
        for (Privilege privilege : getProfile().getPrivileges()) {
            if (permissaoIndicado.name().equals(privilege.getIdentifier())) {
                return true;
            }
        }
        return false;
    }

    private boolean checkPermissoesIndicado(String matriculaIndicador, PermissaoIndicado permissaoIndicado) {
        for (Indicacao indicacao : indicacoesIndicado) {
            if (indicacao.hasPermissaoTo(matriculaIndicador, permissaoIndicado)) {
                return true;
            }
        }
        return false;
    }

    public boolean isPerfilNaoPermiteIndicacoes() {
        return profile.getIdProfile() == 1 && !indicacoes.isEmpty();
    }

    public List<String> matriculasIndicadosOf(PermissaoIndicado permissao) {
        ImmutableList.Builder<String> builder = ImmutableList.builder();
        for (Indicacao indicacao : indicacoes) {
            String matriculaIndicador = indicacao.getMatriculaIndicador();
            if (indicacao.hasPermissaoTo(matriculaIndicador, permissao)) {
                builder.add(indicacao.getMatriculaIndicado());
            }
        }
        return builder.build();
    }

    public List<Indicacao> getIndicacoes() {
        if (indicacoes == null) {
            indicacoes = Lists.newLinkedList();
        }
        return indicacoes;
    }

    public void setIndicacoes(List<Indicacao> indicacoes) {
        this.indicacoes = indicacoes;
    }

    public List<IndicacaoPermissao> getPermissoes() {
        List<IndicacaoPermissao> permissoes = Lists.newLinkedList();
        for (Indicacao indicacao : indicacoes) {
            permissoes.addAll(indicacao.getPermissoes());
        }
        return permissoes;
    }


    /**
     * Retorna o valor do campo id.
     *
     * @return valor do campo id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Atribui um novo valor para o campo id.
     *
     * @param id novo valor para o campo id.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Retorna o valor do campo matricula.
     *
     * @return valor do campo matricula.
     */
    public Long getMatricula() {
        return matricula;
    }

    /**
     * Atribui um novo valor para o campo matricula.
     *
     * @param matricula novo valor para o campo matricula.
     */
    public void setMatricula(Long matricula) {
        this.matricula = matricula;
    }

    /**
     * Retorna o valor do campo nome.
     *
     * @return valor do campo nome.
     */
    public String getNome() {
        return nome;
    }

    /**
     * Atribui um novo valor para o campo nome.
     *
     * @param nome novo valor para o campo nome.
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Retorna o valor do campo ativo.
     *
     * @return valor do campo ativo.
     */
    public Boolean getAtivo() {
        return ativo;
    }

    /**
     * Atribui um novo valor para o campo ativo.
     *
     * @param ativo novo valor para o campo ativo.
     */
    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    /**
     * Retorna o valor do campo email.
     *
     * @return valor do campo email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Atribui um novo valor para o campo email.
     *
     * @param email novo valor para o campo email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Retorna o valor do campo dtNascimento.
     *
     * @return valor do campo dtNascimento.
     */
    public Date getDtNascimento() {
        return dtNascimento;
    }

    /**
     * Atribui um novo valor para o campo dtNascimento.
     *
     * @param dtNascimento novo valor para o campo dtNascimento.
     */
    public void setDtNascimento(Date dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    /**
     * Retorna o valor do campo cpfCnpj.
     *
     * @return valor do campo cpfCnpj.
     */
    public String getCpfCnpj() {
        return cpfCnpj;
    }

    /**
     * Atribui um novo valor para o campo cpfCnpj.
     *
     * @param cpfCnpj novo valor para o campo cpfCnpj.
     */
    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    /**
     * Retorna o valor do campo tpPessoa.
     *
     * @return valor do campo tpPessoa.
     */
    public TipoPessoa getTpPessoa() {
        return tpPessoa;
    }

    /**
     * Atribui um novo valor para o campo tpPessoa.
     *
     * @param tpPessoa novo valor para o campo tpPessoa.
     */
    public void setTpPessoa(TipoPessoa tpPessoa) {
        this.tpPessoa = tpPessoa;
    }

    /**
     * Retorna o valor do campo dtCadastro.
     *
     * @return valor do campo dtCadastro.
     */
    public Date getDtCadastro() {
        return dtCadastro;
    }

    /**
     * Atribui um novo valor para o campo dtCadastro.
     *
     * @param dtCadastro novo valor para o campo dtCadastro.
     */
    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    /**
     * Retorna o valor do campo hrCadastro.
     *
     * @return valor do campo hrCadastro.
     */
    public String getHrCadastro() {
        return hrCadastro;
    }

    /**
     * Atribui um novo valor para o campo hrCadastro.
     *
     * @param hrCadastro novo valor para o campo hrCadastro.
     */
    public void setHrCadastro(String hrCadastro) {
        this.hrCadastro = hrCadastro;
    }

    /**
     * Retorna o valor do campo usuarioCadastro.
     *
     * @return valor do campo usuarioCadastro.
     */
    public String getUsuarioCadastro() {
        return usuarioCadastro;
    }

    /**
     * Atribui um novo valor para o campo usuarioCadastro.
     *
     * @param usuarioCadastro novo valor para o campo usuarioCadastro.
     */
    public void setUsuarioCadastro(String usuarioCadastro) {
        this.usuarioCadastro = usuarioCadastro;
    }

    /**
     * Retorna o valor do campo dtAlteracao.
     *
     * @return valor do campo dtAlteracao.
     */
    public Date getDtAlteracao() {
        return dtAlteracao;
    }

    /**
     * Atribui um novo valor para o campo dtAlteracao.
     *
     * @param dtAlteracao novo valor para o campo dtAlteracao.
     */
    public void setDtAlteracao(Date dtAlteracao) {
        this.dtAlteracao = dtAlteracao;
    }

    /**
     * Retorna o valor do campo hrAlteracao.
     *
     * @return valor do campo hrAlteracao.
     */
    public String getHrAlteracao() {
        return hrAlteracao;
    }

    /**
     * Atribui um novo valor para o campo hrAlteracao.
     *
     * @param hrAlteracao novo valor para o campo hrAlteracao.
     */
    public void setHrAlteracao(String hrAlteracao) {
        this.hrAlteracao = hrAlteracao;
    }

    /**
     * Retorna o valor do campo usuarioAlteracao.
     *
     * @return valor do campo usuarioAlteracao.
     */
    public String getUsuarioAlteracao() {
        return usuarioAlteracao;
    }

    /**
     * Atribui um novo valor para o campo usuarioAlteracao.
     *
     * @param usuarioAlteracao novo valor para o campo usuarioAlteracao.
     */
    public void setUsuarioAlteracao(String usuarioAlteracao) {
        this.usuarioAlteracao = usuarioAlteracao;
    }

    /**
     * Retorna o valor do campo profile.
     *
     * @return valor do campo profile.
     */
    public Profile getProfile() {
        return profile;
    }

    /**
     * Atribui um novo valor para o campo profile.
     *
     * @param profile novo valor para o campo profile.
     */
    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    /**
     * Retorna o valor do campo senha.
     *
     * @return valor do campo senha.
     */
    public String getSenha() {
        return senha;
    }

    /**
     * Atribui um novo valor para o campo senha.
     *
     * @param senha novo valor para o campo senha.
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * Retorna o valor do campo contraSenha.
     *
     * @return valor do campo contraSenha.
     */
    public String getContraSenha() {
        return contraSenha;
    }

    /**
     * Atribui um novo valor para o campo contraSenha.
     *
     * @param contraSenha novo valor para o campo contraSenha.
     */
    public void setContraSenha(String contraSenha) {
        this.contraSenha = contraSenha;
    }

    /**
     * Retorna o valor do campo organization.
     *
     * @return valor do campo organization.
     */
    public Organization getOrganization() {
        return organization;
    }

    /**
     * Atribui um novo valor para o campo organization.
     *
     * @param organization novo valor para o campo organization.
     */
    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    /**
     * Retorna o valor do campo idUser.
     *
     * @return valor do campo idUser.
     */
    public Long getIdUser() {
        return idUser;
    }

    /**
     * Atribui um novo valor para o campo idUser.
     *
     * @param idUser novo valor para o campo idUser.
     */
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    /**
     * Retorna o valor do campo alteraSenha.
     *
     * @return valor do campo alteraSenha.
     */
    public Boolean getAlteraSenha() {
        if (alteraSenha == null) {
            alteraSenha = false;
        }
        return alteraSenha;
    }

    /**
     * Atribui um novo valor para o campo alteraSenha.
     *
     * @param alteraSenha novo valor para o campo alteraSenha.
     */
    public void setAlteraSenha(Boolean alteraSenha) {
        this.alteraSenha = alteraSenha;
    }

    /**
     * Retorna o valor do campo enviaEmail.
     *
     * @return valor do campo enviaEmail.
     */
    public Boolean getEnviaEmail() {
        return enviaEmail;
    }

    /**
     * Atribui um novo valor para o campo enviaEmail.
     *
     * @param enviaEmail novo valor para o campo enviaEmail.
     */
    public void setEnviaEmail(Boolean enviaEmail) {
        this.enviaEmail = enviaEmail;
    }

    /**
     * Retorna o valor do campo mobile.
     *
     * @return valor do campo mobile.
     */
    public Boolean getMobile() {
        return mobile;
    }

    /**
     * Atribui um novo valor para o campo mobile.
     *
     * @param mobile novo valor para o campo mobile.
     */
    public void setMobile(Boolean mobile) {
        this.mobile = mobile;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Usuario getIndicado() {
        return indicado;
    }

    public void setIndicado(Usuario indicado) {
        this.indicado = indicado;
    }

    public List<Indicacao> getIndicacoesIndicado() {
        return indicacoesIndicado;
    }

    public void setIndicacoesIndicado(List<Indicacao> indicacoesIndicado) {
        this.indicacoesIndicado = indicacoesIndicado;
    }


}
