package br.com.db1.portalcooperado.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.db1.myBatisPersistence.annotations.MBClass;

/**
 * Classe que irá guardar alguns valores que sempre são alterados mediante a
 * requisições.
 * 
 * @author wagner mendes voltz
 * @since 11/07/2011
 */
@Entity
@Table(name = "COOP_INF_COMPL")
@MBClass
public class InformacoesComplementares {

	@Id
	@Column(name = "ID_REQUISICAO")
	private Long idRequisicao;

	@Column(name = "DS_RETORNO_LOCAL_ORIGEM")
	private String localOrigem;

	@Column(name = "BO_ACESSA_USUARIO")
	private Boolean acessoLiberado;

	/**
	 * Retorna o valor do campo localOrigem.
	 * 
	 * @return valor do campo localOrigem.
	 */
	public String getLocalOrigem() {
		return localOrigem;
	}

	/**
	 * Atribui um novo valor para o campo localOrigem.
	 * 
	 * @param localOrigem
	 *            novo valor para o campo localOrigem.
	 */
	public void setLocalOrigem(String localOrigem) {
		this.localOrigem = localOrigem;
	}

	/**
	 * Retorna o valor do campo acessoLiberado.
	 * 
	 * @return valor do campo acessoLiberado.
	 */
	public Boolean getAcessoLiberado() {
		return acessoLiberado;
	}

	/**
	 * Atribui um novo valor para o campo acessoLiberado.
	 * 
	 * @param acessoLiberado
	 *            novo valor para o campo acessoLiberado.
	 */
	public void setAcessoLiberado(Boolean acessoLiberado) {
		this.acessoLiberado = acessoLiberado;
	}

	/**
	 * Retorna o valor do campo idRequisicao.
	 * 
	 * @return valor do campo idRequisicao.
	 */
	public Long getIdRequisicao() {
		return idRequisicao;
	}

	/**
	 * Atribui um novo valor para o campo idRequisicao.
	 * 
	 * @param idRequisicao
	 *            novo valor para o campo idRequisicao.
	 */
	public void setIdRequisicao(Long idRequisicao) {
		this.idRequisicao = idRequisicao;
	}

}
