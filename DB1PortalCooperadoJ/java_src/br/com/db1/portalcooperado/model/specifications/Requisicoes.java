package br.com.db1.portalcooperado.model.specifications;

import java.util.Date;

public class Requisicoes {

	private Requisicoes() {

	}

	public static String relatorioAcessoCooperado() {
		return new StringBuilder()
				.append("MAT_USUARIO = :pMatricula ")
				.append("AND DT_REQUISICAO BETWEEN :pDtInicial ")
				.append("AND :pDtFinal ")
				.append("ORDER BY TP_REQUISICAO, DT_REQUISICAO")
				.toString();
	}

	public static Object[] withMatriculaAndDataBetween(Long matricula, Date dtInicial, Date dtFinal) {
		return new Object[] { "pMatricula", matricula, "pDtInicial", dtInicial, "pDtFinal", dtFinal };
	}

}
