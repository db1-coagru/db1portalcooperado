package br.com.db1.portalcooperado.model.specifications;

import br.com.db1.myBatisPersistence.util.MyBatisUtil;

import java.util.Map;

public class Usuarios {

	public static final String QUERY_EXISTE_CPF_CNPJ = "existeCpfCnpj";

	public static final String QUERY_EXISTE_MATRICULA = "existeMatricula";

	public static final String QUERY_BUSCAR_USUARIO_FULL = "buscarUsuarioFull";

	public static final String QUERY_BUSCAR_USUARIO_COM_TOKEN = "buscarUsuarioComToken";

	public static final String QUERY_BUSCAR_USUARIO_POR_CPF_CNPJ = "buscarUsuarioPorCpfCnpj";

	static final String PARAM_ID = "id";

	static final String PARAM_CPF_CNPJ = "cpfCnpj";

	static final String PARAM_MATRICULA = "matricula";

	static final String PARAM_ACCESS_TOKEN = "accessToken";

	private Usuarios() {

	}

	public static Map<String, Object> withCpfCnpj(String cpfCnpj, Long idUsuario) {
		return MyBatisUtil.createMap(PARAM_CPF_CNPJ, cpfCnpj, PARAM_ID, idUsuario);
	}

	public static Map<String, Object> withLogin(String matricula, Long idUsuario) {
		return MyBatisUtil.createMap(PARAM_MATRICULA, matricula, PARAM_ID, idUsuario);
	}

	public static Map<String, Object> withLogin(String login) {
		return MyBatisUtil.createMap(PARAM_MATRICULA, login);
	}

	public static Map<String, Object> withAccessToken(String accessToken) {
		return MyBatisUtil.createMap(PARAM_ACCESS_TOKEN, accessToken);
	}

	public static Map<String, Object> withMatricula(String login) {
		return MyBatisUtil.createMap(PARAM_MATRICULA, login);
	}

	public static Map<String, Object> withCpfCnpj(String cpfCnpj) {
		return MyBatisUtil.createMap(PARAM_CPF_CNPJ, cpfCnpj);
	}

}
