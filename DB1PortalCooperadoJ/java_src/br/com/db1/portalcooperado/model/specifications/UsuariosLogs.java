package br.com.db1.portalcooperado.model.specifications;

import br.com.db1.portalcooperado.relatorio.types.LogAcessoTipoCanal;

import java.util.Date;

import static br.com.db1.portalcooperado.util.DateFormatter.DD_MM_YYYY_HH_MM_SS;

public class UsuariosLogs {

	public static final String QUERY_LOG_ACESSO_COOPERANTE = "logAcessoCooperante";

	private UsuariosLogs() {

	}

	public static String withCanalAndBetween(LogAcessoTipoCanal canal, Date dtInicial, Date dtFinal) {
		StringBuilder builder = new StringBuilder();
		builder.append(" WHERE req.dt_requisicao BETWEEN");
		builder.append(" to_date('").append(DD_MM_YYYY_HH_MM_SS.format(dtInicial)).append("','dd/MM/yyyy hh24:mi:ss')");
		builder.append(" AND");
		builder.append(" to_date('").append(DD_MM_YYYY_HH_MM_SS.format(dtFinal)).append("','dd/MM/yyyy hh24:mi:ss')");
		canal.append(builder);
		builder.append(")");
		builder.append(" GROUP BY matricula, nome, canal)");
		builder.append(" ORDER BY TRIM(nome), canal");
		return builder.toString();
	}

}
