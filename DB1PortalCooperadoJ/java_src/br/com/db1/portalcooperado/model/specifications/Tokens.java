package br.com.db1.portalcooperado.model.specifications;

import br.com.db1.myBatisPersistence.util.MyBatisUtil;

import java.util.Map;

public class Tokens {

	public static final String FIND_BY_ACCESS_TOKEN = "findByAccessToken";

	public static final String PARAM_ACCESS_TOKEN = "accessToken";

	private Tokens() {

	}

	public static Map<String, Object> withAccessToken(String accessToken) {
		return MyBatisUtil.createMap(PARAM_ACCESS_TOKEN, accessToken);
	}

}
