package br.com.db1.portalcooperado.model.types;

import br.com.db1.security.model.entity.Privilege;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;

public enum PermissaoIndicado {

	ACTEVENTOS("Eventos", 0),

	ACTPOSICAOFINANCEIRA("Extrato Financeiro", 3),

	ACTENTREGAPRODUCAO("Extrato Produção Agricola", 2),

	ACTFIXACAOFRANGO("Fechamento de Frango", 11),

	ACTNOTICIAS("Notícias", 1),

	ACTRECOMENDACAOTECNICA("Recomendação Técnica", 4),

	ACTVENDACAVACOMARAVALHA("Venda de Cavaco/Maravalha", 10),

	ACTVENDAINSUMOS("Venda de Insumos", 6),

	ACTVENDAPECAS("Venda de peças", 8),

	ACTVENDARACAOSUPLEMENTO("Venda de Ração/Suplemento Mineral", 9),

	ACTVENDAVETERINARIA("Venda veterinária", 7),

	ACTAVICULTURA("Avicultura", 12);

	private static final Map<String, PermissaoIndicado> NAME_VALUE_MAP;

	private static final Map<Integer, PermissaoIndicado> CODIGOS_VALUE_MAP;

	static {
		ImmutableMap.Builder<String, PermissaoIndicado> builderName = ImmutableMap.builder();
		ImmutableMap.Builder<Integer, PermissaoIndicado> builderCodigos = ImmutableMap.builder();
		for (PermissaoIndicado permissaoIndicado : values()) {
			builderName.put(permissaoIndicado.name(), permissaoIndicado);
			builderCodigos.put(permissaoIndicado.codigo, permissaoIndicado);
		}
		NAME_VALUE_MAP = builderName.build();
		CODIGOS_VALUE_MAP = builderCodigos.build();
	}

	private final String descricao;

	private final Integer codigo;

	PermissaoIndicado(String descricao, Integer codigo) {
		this.descricao = descricao;
		this.codigo = codigo;
	}

	public static PermissaoIndicado fromCodigo(Integer codigo) {
		checkArgument(CODIGOS_VALUE_MAP.containsKey(codigo), "Tipo de permissão de indicado inexistente.");
		return CODIGOS_VALUE_MAP.get(codigo);
	}

	public static List<PermissaoIndicado> valuesOf(List<Privilege> privileges) {
		ImmutableList.Builder<PermissaoIndicado> builder = ImmutableList.builder();
		for (Privilege privilege : privileges) {
			appendIfExists(builder, privilege.getIdentifier());
		}
		return builder.build();
	}

	private static void appendIfExists(ImmutableList.Builder<PermissaoIndicado> builder, String permissao) {
		PermissaoIndicado permissaoIndicado = NAME_VALUE_MAP.get(permissao);
		if (permissaoIndicado != null) {
			builder.add(permissaoIndicado);
		}
	}

	public String getDescricao() {
		return descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}
}
