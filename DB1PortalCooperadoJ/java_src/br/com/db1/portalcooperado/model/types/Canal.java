package br.com.db1.portalcooperado.model.types;

import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.collect.ImmutableMap;

import java.util.Date;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;

public enum Canal {

	PORTAL(1L, "Portal") {
		@Override
		public Date getDataRequisicao() {
			return PortalCooperadoUtil.getDataOperacao();
		}

		@Override
		public String defineIp(String ipAtual) {
			return PortalCooperadoUtil.getIpOrigem();
		}

		@Override
		public String getHoraRequisicao() {
			return PortalCooperadoUtil.getHoraOperacao();
		}
	},

	MOBILE(2L, "Mobile") {
		@Override
		public Date getDataRequisicao() {
			return new Date();
		}

		@Override
		public String defineIp(String ipAtual) {
			return ipAtual; // quando mobile o ip será setado no builder atraves do rest
		}

		@Override
		public String getHoraRequisicao() {
			return PortalCooperadoUtil.getHora();
		}
	};

	private static final ImmutableMap<Long, Canal> valueMap;

	static {
		ImmutableMap.Builder<Long, Canal> builder = ImmutableMap.builder();
		for (Canal canal : values()) {
			builder.put(canal.longValue(), canal);
		}
		valueMap = builder.build();
	}

	private final Long longValue;

	private final String descricao;

	Canal(Long longValue, String descricao) {
		this.longValue = longValue;
		this.descricao = descricao;
	}

	public static Canal fromLongValue(Long longValue) {
		checkArgument(valueMap.containsKey(longValue), "Tipo de canal não suportado.");
		return valueMap.get(longValue);
	}

	public abstract Date getDataRequisicao();

	public abstract String defineIp(String ipAtual);

	public abstract String getHoraRequisicao();

	public Long longValue() {
		return longValue;
	}

	public String getDescricao() {
		return descricao;
	}

}
