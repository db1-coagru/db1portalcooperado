package br.com.db1.portalcooperado.model.types;

import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.entregaproducao.EntregaProducaoParametrosJson;
import br.com.db1.portalcooperado.util.Mensagem;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public enum Produto {

    SOJA(2L, "Soja") {
        @Override
        protected boolean test(boolean soja, boolean milho, boolean trigo) {
            return soja;
        }

        @Override
        public void append(EntregaProducaoParametrosJson.Builder builder) {
            builder.withSoja();
        }

        @Override
        protected List<String> getOutrasDescricoes() {
            return ImmutableList.of("Soja Industria", "Soja Semente");
        }
    },

    MILHO(3L, "Milho") {
        @Override
        protected boolean test(boolean soja, boolean milho, boolean trigo) {
            return milho;
        }

        @Override
        public void append(EntregaProducaoParametrosJson.Builder builder) {
            builder.withMilho();
        }

        @Override
        protected List<String> getOutrasDescricoes() {
            return ImmutableList.of("Quebrado de milho");
        }
    },

    TRIGO(4L, "Trigo") {
        @Override
        protected boolean test(boolean soja, boolean milho, boolean trigo) {
            return trigo;
        }

        @Override
        public void append(EntregaProducaoParametrosJson.Builder builder) {
            builder.withTrigo();
        }

        @Override
        protected List<String> getOutrasDescricoes() {
            return ImmutableList.of("Trigo Industria", "Trigo semente", "Triguilho");
        }
    };

    private static final Map<String, Produto> descricaoValueMap;

    private final Long codigo;

    private final String descricao;

    Produto(Long codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    static {
        ImmutableMap.Builder<String, Produto> builder = ImmutableMap.builder();
        for (Produto produto : values()) {
            builder.put(produto.descricao.toUpperCase(), produto);
            for (String outraDescricao : produto.getOutrasDescricoes()) {
                builder.put(outraDescricao.toUpperCase(), produto);
            }
        }
        descricaoValueMap = builder.build();
    }

    public static Long buildCodigoFrom(boolean soja, boolean milho, boolean trigo) {
        StringBuilder builder = new StringBuilder();
        for (Produto tipo : values()) {
            if (tipo.test(soja, milho, trigo)) {
                builder.append(tipo.codigo);
            }
        }
        String codigos = builder.toString();
        return !codigos.isEmpty() ? Long.valueOf(codigos) : 0L;
    }

    public static Produto fromStringValue(String value) {
        checkNotNull(value, Mensagem.campoObrigatorioMasculino("Tipo de produto"));
        String valueUpperCase = value.toUpperCase();
        checkArgument(descricaoValueMap.containsKey(valueUpperCase), "Tipo de produto inexistente.");
        return descricaoValueMap.get(valueUpperCase);
    }

    protected abstract boolean test(boolean soja, boolean milho, boolean trigo);

    public abstract void append(EntregaProducaoParametrosJson.Builder builder);

    protected abstract List<String> getOutrasDescricoes();

    public String getDescricaoLowerCase() {
        return descricao.toLowerCase();
    }

    public String getDescricao() {
        return descricao;
    }

}
