package br.com.db1.portalcooperado.model.types;

public enum AcaoLog {

	INSERT("INSERT"),

	DELETE("DELETE"),

	UPDATE("UPDATE"),

	SELECT("SELECT");

	private final String tipo;

	AcaoLog(String tipo) {
		this.tipo = tipo;
	}

}
