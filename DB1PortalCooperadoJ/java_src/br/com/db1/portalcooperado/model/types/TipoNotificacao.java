package br.com.db1.portalcooperado.model.types;

import br.com.db1.portalcooperado.util.Mensagem;
import jersey.repackaged.com.google.common.collect.ImmutableMap;

import java.util.Map;

import static br.com.db1.portalcooperado.model.types.PermissaoIndicado.*;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public enum TipoNotificacao {

	ROMANEIO_AGRICOLA(1L, "Romaneio agrícola") {
		@Override
		public String getTitulo() {
			return "Novo romaneio agrícola";
		}
	},

	VENDA_INSUMOS(2L, "Venda de insumos") {
		@Override
		public String getTitulo() {
			return "Nova venda de insumos";
		}
	},

	VENDA_VETERINARIA(3L, "Venda veterinária") {
		@Override
		public String getTitulo() {
			return "Nova venda veterinária";
		}
	},

	VENDA_PECAS(4L, "Venda de peças") {
		@Override
		public String getTitulo() {
			return "Nova venda de peças";
		}
	},

	VENDA_RACAO_SUPLEMENTO(5L, "Venda de ração/suplemento mineral") {
		@Override
		public String getTitulo() {
			return "Nova venda de ração/suplemento mineral";
		}
	},

	VENDA_CAVACO_MARAVALHA(6L, "Venda de cavaco/maravalha") {
		@Override
		public String getTitulo() {
			return "Nova venda de cavaco/maravalha";
		}
	},

	FECHAMENTO_DE_FRANGO(7L, "Fechamento de frango") {
		@Override
		public String getTitulo() {
			return "Novo fechamento de frango";
		}
	},

	RECOMENDACAO_TECNICA(10L, "Recomendação técnica") {
		@Override
		public String getTitulo() {
			return "Nova recomendação técnica";
		}

		@Override
		public Boolean isEntregaProducao() {
			return Boolean.FALSE;
		}

		@Override
		public PermissaoIndicado getPermissao() {
			return ACTRECOMENDACAOTECNICA;
		}
	},

	INDICACOES(11L, "Indicações") {
		@Override
		public String getTitulo() {
			return "";
		}

		@Override
		public Boolean isIndicacao() {
			return Boolean.TRUE;
		}

		@Override
		public PermissaoIndicado getPermissao() {
			return null;
		}
	},

	AVICULTURA(12L, "Avicultura") {
		@Override
		public String getTitulo() {
			return "Nova visita técnica no aviário";
		}

		@Override
		public Boolean isEntregaProducao() {
			return Boolean.FALSE;
		}

		@Override
		public PermissaoIndicado getPermissao() {
			return ACTAVICULTURA;
		}
	};

	private static final Map<Long, TipoNotificacao> longValueMap;

	static {
		ImmutableMap.Builder<Long, TipoNotificacao> builder = ImmutableMap.builder();
		for (TipoNotificacao tipoNotificacao : values()) {
			builder.put(tipoNotificacao.codigo, tipoNotificacao);
		}
		longValueMap = builder.build();
	}

	private final Long codigo;

	private final String descricao;

	TipoNotificacao(Long codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public static TipoNotificacao fromCodigo(Long codigo) {
		checkNotNull(codigo, Mensagem.campoObrigatorioMasculino("Tipo de notificação"));
		checkArgument(longValueMap.containsKey(codigo), "Tipo de notificação inexistente.");
		return longValueMap.get(codigo);
	}

	public abstract String getTitulo();

	public Boolean isRomaneioAgricola() {
		return ROMANEIO_AGRICOLA.equals(this);
	}

	public Boolean isRecomendacaoTecnica() {
		return RECOMENDACAO_TECNICA.equals(this);
	}

	public Boolean isEntregaProducao() {
		return Boolean.TRUE;
	}

	public Boolean isAvicultura() {
		return AVICULTURA.equals(this);
	}

	public Boolean isIndicacao() {
		return Boolean.FALSE;
	}

	public Long getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public PermissaoIndicado getPermissao() {
		return ACTENTREGAPRODUCAO;
	}

}
