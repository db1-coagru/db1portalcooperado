package br.com.db1.portalcooperado.model.types;

public enum TipoPessoa {

	F("FÍSICA"),

	J("JURÍDICA");

	private final String tipo;

	TipoPessoa(String tipo) {
		this.tipo = tipo;
	}

}
