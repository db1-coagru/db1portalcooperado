package br.com.db1.portalcooperado.model.types;

import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.base.MoreObjects;
import org.assertj.core.util.Strings;

import java.io.Serializable;
import java.math.BigDecimal;

import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioFeminino;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.*;

public class Quantidade implements Serializable {

	private static final long serialVersionUID = -4950788629987919058L;

	private static final int DEFAULT_SCALE = 2;

	private final BigDecimal value;

	private Quantidade(BigDecimal value) {
		this.value = value;
	}

	public static Quantidade zero() {
		return new Quantidade(convertToBigDecimal(BigDecimal.ZERO, DEFAULT_SCALE));
	}

	public static Quantidade zero(Integer scale) {
		return new Quantidade(convertToBigDecimal(BigDecimal.ZERO, scale));
	}

	public static Quantidade of(Object obj) {
		return new Quantidade(convertToBigDecimal(obj, DEFAULT_SCALE));
	}

	public static Quantidade of(Object obj, Integer scale) {
		return new Quantidade(convertToBigDecimal(obj, scale));
	}

	private static BigDecimal convertToBigDecimal(Object value, Integer scale) {
		Object obj = MoreObjects.firstNonNull(value, "");
		String asString = obj.toString().trim();
		checkArgument(!Strings.isNullOrEmpty(asString), campoObrigatorioFeminino("Quantidade"));
		checkNotNull(scale, campoObrigatorioFeminino("Escala"));
		return new BigDecimal(asString).setScale(scale, BigDecimal.ROUND_HALF_EVEN);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Quantidade) {
			Quantidade other = (Quantidade) obj;
			return areEquals(value, other.value);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(value);
	}

	@Override
	public String toString() {
		return value.toString();
	}

	public Quantidade setScale(Integer scale) {
		return Quantidade.of(value, scale);
	}

	public BigDecimal toBigDecimal() {
		return convertToBigDecimal(value, value.scale());
	}

	public Boolean isZero() {
		return value.signum() == 0;
	}

	public Integer scale() {
		return value.scale();
	}

	public Quantidade multiply(Quantidade quantidade) {
		BigDecimal result = value.multiply(quantidade.value);
		return Quantidade.of(result, scale());
	}

	public Quantidade divide(Quantidade quantidade) {
		BigDecimal result = value.divide(quantidade.value, BigDecimal.ROUND_HALF_EVEN);
		return Quantidade.of(result, scale());
	}

}
