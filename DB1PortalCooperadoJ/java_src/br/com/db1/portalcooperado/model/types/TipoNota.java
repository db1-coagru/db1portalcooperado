package br.com.db1.portalcooperado.model.types;

public enum TipoNota {

	V("Venda"),

	D("Devolução");

	private final String descricao;

	private TipoNota(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
