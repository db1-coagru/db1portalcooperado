package br.com.db1.portalcooperado.model.task;

import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXml;
import br.com.db1.portalcooperado.infrastructure.job.AbstractTask;
import br.com.db1.portalcooperado.infrastructure.rest.translators.xmltoentity.avicultura.AviculturaXmlToAviculturaTranslator;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import br.com.db1.portalcooperado.service.AviculturaAgrosysService;
import br.com.db1.portalcooperado.service.AviculturaService;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;
import java.util.List;

public class AviculturaAgrosysTask extends AbstractTask {

	private final AviculturaAgrosysService aviculturaAgrosysWebService;

	private final AviculturaXmlToAviculturaTranslator aviculturaXmlToAviculturaTranslator;

	private final AviculturaService aviculturaService;

	@Value("${avicultura.agrosys.qtde.dias.retroativo}")
	private Integer quantidadeDiasRetroativos;

	@Autowired
	protected AviculturaAgrosysTask(AviculturaAgrosysService aviculturaAgrosysWebService,
			AviculturaXmlToAviculturaTranslator aviculturaXmlToAviculturaTranslator,
			AviculturaService aviculturaService) {
		this.aviculturaAgrosysWebService = aviculturaAgrosysWebService;
		this.aviculturaXmlToAviculturaTranslator = aviculturaXmlToAviculturaTranslator;
		this.aviculturaService = aviculturaService;
	}

	@Override
	protected void execute() {
		Date hoje = new Date();
		Date diasAtras = PortalCooperadoUtil.getDateSubtractingDays(quantidadeDiasRetroativos);
		List<AviculturaXml> aviculturasXml = aviculturaAgrosysWebService.findAllAviculturasOf(diasAtras, hoje);
		for (AviculturaXml avicultura : aviculturasXml) {
			execute(avicultura);
		}
	}

	private void execute(AviculturaXml aviculturaXml) {
		try {
			statistics.addProcessado();
			Avicultura avicultura = aviculturaXmlToAviculturaTranslator.translate(aviculturaXml);
			if (avicultura.getProdutor() != null) {
				Long idAvicultura = avicultura.getId();
				Avicultura saved = aviculturaService.save(avicultura);
				if (idAvicultura == null) {
					aviculturaService.enviarNotificacaoMobile(saved);
				}
				statistics.addSucesso();
			}
			else {
				statistics.addIgnorado();
			}
			Thread.sleep(50);
		} catch (Exception ex) {
			statistics.addErro();
			logger.erro("Erro ao processar aviculutra", ex);
		}
	}

}
