package br.com.db1.portalcooperado.model.task;

import br.com.db1.portalcooperado.infrastructure.job.AbstractTask;
import br.com.db1.portalcooperado.model.entity.Notificacao;
import br.com.db1.portalcooperado.service.NotificacaoService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProgressNotificacaoERPTask extends AbstractTask {

	private final NotificacaoService notificacaoService;

	@Autowired
	protected ProgressNotificacaoERPTask(NotificacaoService notificacaoService) {
		this.notificacaoService = notificacaoService;
	}

	@Override
	protected void execute() throws InterruptedException {
		List<Notificacao> filaNotificacoesERP = buscaNotificacoesAEnviar();
		if (!filaNotificacoesERP.isEmpty()) {
			enviaNotificacacoesFilaMobile(filaNotificacoesERP);
		}
	}

	private List<Notificacao> buscaNotificacoesAEnviar() {
		return notificacaoService.findByCondition("NR_STATUS = 0 ORDER BY DT_DATA, HR_HORA");
	}

	private void enviaNotificacacoesFilaMobile(List<Notificacao> filaNotificacoesERP) throws InterruptedException {
		for (Notificacao notificacao : filaNotificacoesERP) {
			statistics.addProcessado();
			enviaNotificacaoFilaMobile(notificacao);
			logger.info("Notificação enviada: " + notificacao.getId());
			Thread.sleep(50);
		}
	}

	private void enviaNotificacaoFilaMobile(Notificacao notificacao) {
		try {
			notificacaoService.enviarNotificacaoMobileConsumindoFila(notificacao);
			statistics.addSucesso();
		}
		catch (Exception ex) {
			logger.itemErro(notificacao, ex);
			statistics.addErro();
		}
	}

}
