package br.com.db1.portalcooperado.model.task;

import br.com.db1.portalcooperado.infrastructure.folders.files.FileMover;
import br.com.db1.portalcooperado.infrastructure.folders.files.FolderFiles;
import br.com.db1.portalcooperado.infrastructure.folders.files.filters.ImagemFileFilter;
import br.com.db1.portalcooperado.infrastructure.folders.files.filters.XmlFileFilter;
import br.com.db1.portalcooperado.infrastructure.folders.files.writers.FileWriter;
import br.com.db1.portalcooperado.infrastructure.folders.files.writers.FileWriterFactory;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.infrastructure.job.AbstractTask;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.service.FolderService;
import br.com.db1.portalcooperado.service.RecomendacaoTecnicaService;
import jersey.repackaged.com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.List;

public class RecomendacaoTecnicaGreensoftTask extends AbstractTask {

	private final FileFilter xmlFileFilter = XmlFileFilter.of();

	private final FileWriter txtWriter = FileWriterFactory.newTxtWriter();

	private final RecomendacaoTecnicaService recomendacaoTecnicaService;

	private final FolderService folderService;

	@Value(value = "${recomendacao.tecnica.processar.folder}")
	private String processarFolder;

	@Value(value = "${recomendacao.tecnica.processado.folder}")
	private String processadoFolder;

	@Value(value = "${recomendacao.tecnica.erro.folder}")
	private String erroFolder;

	@Autowired
	protected RecomendacaoTecnicaGreensoftTask(RecomendacaoTecnicaService recomendacaoTecnicaService,
			FolderService folderService) {
		this.recomendacaoTecnicaService = recomendacaoTecnicaService;
		this.folderService = folderService;
	}

	@Override
	protected void execute() throws InterruptedException, IOException, JAXBException {
		FolderFiles<File> files = readFiles();
		if (!files.isEmpty()) {
			createRecomendacoesTecnicas(files.values());
		}
	}

	private FolderFiles<File> readFiles() throws JAXBException {
		return folderService.readFolderOnServer(processarFolder, xmlFileFilter);
	}

	private void createRecomendacoesTecnicas(List<File> files) throws InterruptedException, IOException,
			JAXBException {
		for (File file : files) {
			statistics.addProcessado();
			createRecomendacaoTecnica(file);
		}
	}

	private void createRecomendacaoTecnica(File file) throws IOException {
		RecomendacaoTecnica recomendacaoTecnica = null;
		ServiceRecord serviceRecord = null;
		List<File> imagens = Lists.newLinkedList();
		try {
			serviceRecord = ServiceRecord.of(file);
			imagens = loadImages(serviceRecord);
			recomendacaoTecnica = recomendacaoTecnicaService.processaRecomendacaoTecnica(serviceRecord, imagens);
			statistics.addSucesso();
			moveFiles(file, imagens, processadoFolder);
			logger.info("Recomendação gerada: " + recomendacaoTecnica.getId());
			Thread.sleep(50);
		}
		catch (Exception ex) {
			logger.itemErro(file.getName(), ex);
			statistics.addErro();
			moveFiles(file, imagens, erroFolder);
			txtWriter.write(erroFolder, file.getName().replace(".xml", "ERRO"), ex);
		}
	}

	private List<File> loadImages(ServiceRecord serviceRecord) throws JAXBException {
		ImagemFileFilter imagemFileFilter = ImagemFileFilter.of(serviceRecord.getFileName());
		FolderFiles<File> files = folderService.readFolderOnServer(processarFolder, imagemFileFilter);
		return files.values();
	}

	private void moveFiles(File oldFile, List<File> imagens, String destino) throws IOException {
		File file = new File(processarFolder, oldFile.getName());
		FileMover.move(file, destino);
		FileMover.move(imagens, destino);
	}

}
