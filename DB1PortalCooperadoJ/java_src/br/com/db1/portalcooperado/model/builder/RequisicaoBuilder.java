package br.com.db1.portalcooperado.model.builder;

import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.Canal;
import br.com.db1.portalcooperado.relatorio.types.ReportType;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;

import java.util.Date;

import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioFeminino;
import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioMasculino;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;
import static jersey.repackaged.com.google.common.base.MoreObjects.firstNonNull;

public class RequisicaoBuilder {

	private Long matriculaUsuario;

	private Long tipoRequisicao;

	private Long tipoProduto;

	private Date dataInicial;

	private Date dataFinal;

	private Long matriculaIndicador;

	private String reportType;

	private Usuario usuario;

	private String ip;

	private Long canal = Canal.PORTAL.longValue();

	private Date dataRequisicao;

	private String horaRequisicao;

	public RequisicaoBuilder() {
		// usado pelo flex pra fazer o biding
	}

	private RequisicaoBuilder(Canal canal) {
		this.canal = canal.longValue();

	}

	public static RequisicaoBuilder of(Canal canal) {
		checkNotNull(canal, campoObrigatorioMasculino("Canal de emissão da requisição"));
		return new RequisicaoBuilder(canal);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RequisicaoBuilder) {
			RequisicaoBuilder other = (RequisicaoBuilder) obj;
			return PortalCooperadoUtil.areEquals(this.matriculaUsuario, other.matriculaUsuario)
					&& PortalCooperadoUtil.areEquals(this.tipoRequisicao, other.tipoRequisicao);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(matriculaUsuario, tipoRequisicao);
	}

	public Requisicao build() {
		checkNotNull(matriculaUsuario, campoObrigatorioFeminino("Matrícula da requisição"));
		checkNotNull(tipoRequisicao, campoObrigatorioMasculino("Tipo da requisição"));
		checkNotNull(reportType, campoObrigatorioMasculino("Tipo do relatório"));
		checkNotNull(usuario, campoObrigatorioMasculino("Usuário"));
		checkNotNull(canal, campoObrigatorioMasculino("Canal de emissão da requisição"));
		TipoRequisicao.fromLongValue(this.tipoRequisicao).hasPermissaoThrowsExceptionIfFalse(getMatricula(), usuario);
		setDadosLogRequisicao();
		checkNotNull(ip, campoObrigatorioMasculino("Ip da requisição"));
		return new Requisicao(this);
	}

	private void setDadosLogRequisicao() {
		Canal canalType = Canal.fromLongValue(canal);
		this.dataRequisicao = canalType.getDataRequisicao();
		this.ip = canalType.defineIp(this.ip);
		this.horaRequisicao = canalType.getHoraRequisicao();
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType.getValor();
	}

	public String getMatriculaUsuarioAsString() {
		return matriculaUsuario.toString();
	}

	public Long getMatricula() {
		return firstNonNull(matriculaIndicador, matriculaUsuario);
	}

	public Long getMatriculaUsuario() {
		return matriculaUsuario;
	}

	public void setMatriculaUsuario(Long matriculaUsuario) {
		this.matriculaUsuario = matriculaUsuario;
	}

	public Long getTipoRequisicao() {
		return tipoRequisicao;
	}

	public void setTipoRequisicao(Long tipoRequisicao) {
		this.tipoRequisicao = tipoRequisicao;
	}

	public Long getTipoProduto() {
		return tipoProduto;
	}

	public void setTipoProduto(Long tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public Long getMatriculaIndicador() {
		return matriculaIndicador;
	}

	public void setMatriculaIndicador(Long matriculaIndicador) {
		this.matriculaIndicador = matriculaIndicador;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Long getCanal() {
		return canal;
	}

	public void setCanal(Long canal) {
		this.canal = canal;
	}

	public Date getDataRequisicao() {
		return dataRequisicao;
	}

	public String getHoraRequisicao() {
		return horaRequisicao;
	}

}
