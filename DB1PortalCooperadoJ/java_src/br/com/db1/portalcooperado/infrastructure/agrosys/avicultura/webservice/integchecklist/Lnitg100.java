package br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.webservice.integchecklist;

import javax.xml.bind.annotation.*;

/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vdtinicial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vdtfinal" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"vdtinicial",
		"vdtfinal"
})
@XmlRootElement(name = "lnitg100")
public class Lnitg100 {

	@XmlElement(required = true, nillable = true)
	protected String vdtinicial;

	@XmlElement(required = true, nillable = true)
	protected String vdtfinal;

	/**
	 * Gets the value of the vdtinicial property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getVdtinicial() {
		return vdtinicial;
	}

	/**
	 * Sets the value of the vdtinicial property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setVdtinicial(String value) {
		this.vdtinicial = value;
	}

	/**
	 * Gets the value of the vdtfinal property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getVdtfinal() {
		return vdtfinal;
	}

	/**
	 * Sets the value of the vdtfinal property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setVdtfinal(String value) {
		this.vdtfinal = value;
	}

}
