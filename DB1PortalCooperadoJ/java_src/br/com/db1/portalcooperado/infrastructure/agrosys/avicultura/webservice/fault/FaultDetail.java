package br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.webservice.fault;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="errorMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"errorMessage",
		"requestID"
})
@XmlRootElement(name = "FaultDetail")
public class FaultDetail implements Serializable {

	private static final long serialVersionUID = -6575503755111090167L;

	@XmlElement(required = true)
	protected String errorMessage;

	@XmlElement(required = true)
	protected String requestID;

	/**
	 * Gets the value of the errorMessage property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Sets the value of the errorMessage property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setErrorMessage(String value) {
		this.errorMessage = value;
	}

	/**
	 * Gets the value of the requestID property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getRequestID() {
		return requestID;
	}

	/**
	 * Sets the value of the requestID property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setRequestID(String value) {
		this.requestID = value;
	}

}
