package br.com.db1.portalcooperado.infrastructure.agrosys.avicultura;

import jersey.repackaged.com.google.common.base.MoreObjects;
import jersey.repackaged.com.google.common.base.Objects;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;

import javax.xml.bind.annotation.XmlElement;
import java.util.Date;
import java.util.List;

public class AviculturaXml {

	@XmlElement(name = "cpfcnpj")
	private String cpfCnpj;

	@XmlElement(name = "galpao")
	private String galpao;

	@XmlElement(name = "lote")
	private String lote;

	@XmlElement(name = "sexo")
	private String sexo;

	@XmlElement(name = "qtde")
	private Integer quantidade;

	@XmlElement(name = "data")
	private Date data;

	@XmlElement(name = "linhagem")
	private String linhagem;

	@XmlElement(name = "tecnico")
	private String nomeTecnico;

	@XmlElement(name = "item")
	private List<AviculturaItemXml> itens = Lists.newLinkedList();

	protected AviculturaXml() {
		// for frameworks
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AviculturaXml) {
			AviculturaXml other = (AviculturaXml) obj;
			return Objects.equal(cpfCnpj, other.cpfCnpj) &&
					Objects.equal(lote, other.lote) &&
					Objects.equal(data, other.data);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(cpfCnpj, lote, data);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("cpfCnpj", cpfCnpj)
				.add("galpao", galpao)
				.add("lote", lote)
				.add("sexo", sexo)
				.add("quantidade", quantidade)
				.add("data", data)
				.add("linhagem", linhagem)
				.add("nomeTecnico", nomeTecnico)
				.toString();
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public String getGalpao() {
		return galpao;
	}

	public String getLote() {
		return lote;
	}

	public String getSexo() {
		return sexo;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public Date getData() {
		return data;
	}

	public String getLinhagem() {
		return linhagem;
	}

	public String getNomeTecnico() {
		return nomeTecnico;
	}

	public List<AviculturaItemXml> getItens() {
		return itens;
	}

}

