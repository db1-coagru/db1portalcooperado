package br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.webservice.integchecklist;

import javax.xml.bind.annotation.*;

/**
 * <p>Java class for anonymous complex type.
 * <p>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="vxmlRet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"result",
		"vxmlRet"
})
@XmlRootElement(name = "lnitg100Response")
public class Lnitg100Response {

	@XmlElement(required = true, nillable = true)
	protected String result;

	@XmlElement(required = true, nillable = true)
	protected String vxmlRet;

	/**
	 * Gets the value of the result property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getResult() {
		return result;
	}

	/**
	 * Sets the value of the result property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setResult(String value) {
		this.result = value;
	}

	/**
	 * Gets the value of the vxmlRet property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getVxmlRet() {
		return vxmlRet;
	}

	/**
	 * Sets the value of the vxmlRet property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setVxmlRet(String value) {
		this.vxmlRet = value;
	}

}
