package br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.webservice.integchecklist;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the progress.services.agrosys.integchecklist package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: progress.services.agrosys.integchecklist
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link Lnitg100Response }
	 */
	public Lnitg100Response createLnitg100Response() {
		return new Lnitg100Response();
	}

	/**
	 * Create an instance of {@link Lnitg100 }
	 */
	public Lnitg100 createLnitg100() {
		return new Lnitg100();
	}

}
