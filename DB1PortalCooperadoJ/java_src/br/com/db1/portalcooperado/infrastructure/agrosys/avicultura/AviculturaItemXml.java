package br.com.db1.portalcooperado.infrastructure.agrosys.avicultura;

import jersey.repackaged.com.google.common.base.MoreObjects;
import jersey.repackaged.com.google.common.base.Objects;
import org.testng.util.Strings;

import javax.xml.bind.annotation.XmlElement;

public class AviculturaItemXml {

	@XmlElement(name = "descricao")
	private String descricao;

	@XmlElement(name = "resposta")
	private String resposta;

	@XmlElement(name = "observacao")
	private String observacao;

	@XmlElement(name = "imagem")
	private String imagem;

	protected AviculturaItemXml() {
		// for frameworks
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AviculturaItemXml) {
			AviculturaItemXml other = (AviculturaItemXml) obj;
			return Objects.equal(descricao, other.descricao) &&
					Objects.equal(resposta, other.resposta);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(descricao, resposta);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("descricao", descricao)
				.add("resposta", resposta)
				.add("observacao", observacao)
				.add("imagem", imagem)
				.toString();
	}

	boolean isIntegravel() {
		return "NAO CONFORMIDADE".equals(resposta) || !Strings.isNullOrEmpty(observacao);
	}

	public String getDescricao() {
		return descricao;
	}

	public String getResposta() {
		return resposta;
	}

	public String getObservacao() {
		return observacao;
	}

	public String getImagem() {
		return imagem;
	}

}
