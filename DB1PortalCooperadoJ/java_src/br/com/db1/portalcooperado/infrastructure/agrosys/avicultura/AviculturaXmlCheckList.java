package br.com.db1.portalcooperado.infrastructure.agrosys.avicultura;

import jersey.repackaged.com.google.common.base.Objects;
import jersey.repackaged.com.google.common.collect.Lists;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.StringReader;
import java.util.List;

@XmlRootElement(name = "CheckList")
public class AviculturaXmlCheckList {

	@XmlElement(name = "cabecalho")
	private List<AviculturaXml> itens;

	private AviculturaXmlCheckList() {

	}

	public static AviculturaXmlCheckList of(String xml) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(AviculturaXmlCheckList.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		StringReader stringReader = new StringReader(xml);
		return (AviculturaXmlCheckList) unmarshaller.unmarshal(stringReader);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof AviculturaXmlCheckList) {
			AviculturaXmlCheckList checkList = (AviculturaXmlCheckList) o;
			return Objects.equal(itens, checkList.itens);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(itens);
	}

	public List<AviculturaXml> getItens() {
		if (itens == null) {
			this.itens = Lists.newLinkedList();
		}
		return itens;
	}
}
