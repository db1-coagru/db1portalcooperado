package br.com.db1.portalcooperado.infrastructure.folders.files;

import br.com.db1.portalcooperado.util.Mensagem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class FileMover {

	private FileMover() {

	}

	public static boolean move(File actualFile, String pathDestination) throws IOException {
		checkNotNull(actualFile, Mensagem.campoObrigatorioMasculino("Arquivo"));
		checkNotNull(pathDestination, Mensagem.campoObrigatorioMasculino("Destino do arquivo"));
		checkArgument(actualFile.exists(), "Arquivo informado é inexistente.");
		checkArgument(actualFile.isFile(), "Arquivo informado é inválido.");

		File destination = new File(pathDestination);
		checkArgument(destination.exists(), "Destino informado é inexistente.");
		checkArgument(destination.isDirectory(), "Destino informado é inválido. Somente pastas podem ser informadas.");

		File newFile = new File(destination, actualFile.getName());

		FileInputStream fileInputStream = new FileInputStream(actualFile);
		FileOutputStream fileOutputStream = new FileOutputStream(newFile);
		FileChannel inChannel = fileInputStream.getChannel();
		FileChannel outChannel = fileOutputStream.getChannel();
		outChannel.transferFrom(inChannel, 0, inChannel.size());

		fileInputStream.close();
		fileOutputStream.close();

		return newFile.exists() && actualFile.delete();
	}

	public static void move(List<File> files, String pathDestination) throws IOException {
		for (File file : files) {
			move(file, pathDestination);
		}
	}

}
