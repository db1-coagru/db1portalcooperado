package br.com.db1.portalcooperado.infrastructure.folders.files;

import javax.xml.bind.JAXBException;
import java.io.File;

public interface FileReader<T> {

	public T read(File file) throws JAXBException;

}
