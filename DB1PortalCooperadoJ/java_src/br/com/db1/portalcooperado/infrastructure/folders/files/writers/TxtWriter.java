package br.com.db1.portalcooperado.infrastructure.folders.files.writers;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

class TxtWriter implements FileWriter {

	static TxtWriter of() {
		return new TxtWriter();
	}

	@Override
	public void write(String pathName, String fileName, Exception exception) throws IOException {
		File file = new File(pathName, fileName.concat(".txt"));
		java.io.FileWriter fileWriter = new java.io.FileWriter(file);
		PrintWriter printWriter = new PrintWriter(fileWriter);
		exception.printStackTrace(printWriter);
		fileWriter.close();
	}

}
