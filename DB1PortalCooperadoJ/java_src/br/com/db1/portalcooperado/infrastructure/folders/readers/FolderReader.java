package br.com.db1.portalcooperado.infrastructure.folders.readers;

import br.com.db1.portalcooperado.infrastructure.folders.files.FileReader;
import br.com.db1.portalcooperado.infrastructure.folders.files.FolderFiles;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileFilter;

public interface FolderReader {

	public <T> FolderFiles<T> read(String path, FileReader<T> fileReader, FileFilter fileFilter)
			throws JAXBException;

	public FolderFiles<File> read(String path, FileFilter fileFilter) throws JAXBException;

}
