package br.com.db1.portalcooperado.infrastructure.folders.files.filters;

import br.com.db1.portalcooperado.util.Mensagem;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class ImagemFileFilter implements FileFilter {

	private final String simpleName;

	private Pattern pattern = Pattern.compile("([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)");

	public ImagemFileFilter(String simpleName) {
		this.simpleName = simpleName;
	}

	public static ImagemFileFilter of(String fileName) {
		checkNotNull(fileName, Mensagem.campoObrigatorioMasculino("Nome do arquivo"));
		String simpleName = fileName.replace(".xml", "");
		return new ImagemFileFilter(simpleName);
	}

	@Override
	public boolean accept(File file) {
		return file != null
				&& file.getName().startsWith(simpleName)
				&& pattern.matcher(file.getName()).matches();
	}

}
