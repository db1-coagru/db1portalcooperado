package br.com.db1.portalcooperado.infrastructure.folders.readers;

public class FolderReaderFactory {

	private FolderReaderFactory() {

	}

	public static FolderReader newServerFolderReader(){
		return ServerFolderReader.of();
	}

}
