package br.com.db1.portalcooperado.infrastructure.folders.files.filters;

import java.io.File;
import java.io.FileFilter;
import java.io.Serializable;

public class XmlFileFilter implements FileFilter, Serializable {

	private static final long serialVersionUID = 5177438970916424L;

	private XmlFileFilter() {

	}

	public static XmlFileFilter of() {
		return new XmlFileFilter();
	}

	@Override
	public boolean accept(File file) {
		return file != null && file.getName().endsWith("xml");
	}

}
