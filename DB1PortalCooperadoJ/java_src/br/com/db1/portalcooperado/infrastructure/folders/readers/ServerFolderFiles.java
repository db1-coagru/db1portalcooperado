package br.com.db1.portalcooperado.infrastructure.folders.readers;

import br.com.db1.portalcooperado.infrastructure.folders.files.FolderFiles;
import jersey.repackaged.com.google.common.collect.Lists;

import java.util.List;

class ServerFolderFiles<T> implements FolderFiles<T> {

	private final List<T> values;

	private ServerFolderFiles(List<T> values) {
		this.values = values;
	}

	static <T> ServerFolderFiles<T> of(List<T> values) {
		return new ServerFolderFiles<T>(Lists.newLinkedList(values));
	}

	@Override
	public List<T> values() {
		return values;
	}

	@Override
	public Integer size() {
		return values.size();
	}

	@Override
	public boolean isEmpty() {
		return values.isEmpty();
	}

}
