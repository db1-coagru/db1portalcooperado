package br.com.db1.portalcooperado.infrastructure.folders.readers;

import br.com.db1.portalcooperado.infrastructure.folders.files.FileReader;
import br.com.db1.portalcooperado.infrastructure.folders.files.FolderFiles;
import jersey.repackaged.com.google.common.base.MoreObjects;
import jersey.repackaged.com.google.common.collect.ImmutableList;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileFilter;

import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioFeminino;
import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioMasculino;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

class ServerFolderReader implements FolderReader {

	protected ServerFolderReader() {
		//construtor padrao utilizado pelo spring
	}

	static ServerFolderReader of() {
		return new ServerFolderReader();
	}

	@Override
	public <T> FolderFiles<T> read(String pathFolder, FileReader<T> fileReader, FileFilter fileFilter)
			throws JAXBException {
		checkNotNull(pathFolder, campoObrigatorioFeminino("Caminho da pasta"));
		checkNotNull(fileReader, campoObrigatorioMasculino("Conversor de arquivo"));
		File folder = new File(pathFolder);
		ImmutableList.Builder<T> builder = ImmutableList.builder();
		checkArgument(folder.exists(), String.format("O caminho informado não existe. %s", pathFolder));
		checkArgument(folder.isDirectory(), "O caminho informado é inválido.");

		File[] files = MoreObjects.firstNonNull(folder.listFiles(fileFilter), new File[0]);
		for (File file : files) {
			T obj = fileReader.read(file);
			builder.add(obj);
		}
		return ServerFolderFiles.of(builder.build());
	}

	@Override
	public FolderFiles<File> read(String pathFolder, FileFilter fileFilter) throws JAXBException {
		FileReader<File> fileReader = new FileReader<File>() {
			@Override
			public File read(File file) throws JAXBException {
				return file;
			}
		};
		return read(pathFolder, fileReader, fileFilter);
	}
}
