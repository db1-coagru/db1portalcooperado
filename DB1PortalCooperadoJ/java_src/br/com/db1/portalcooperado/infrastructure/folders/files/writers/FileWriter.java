package br.com.db1.portalcooperado.infrastructure.folders.files.writers;

import java.io.IOException;

public interface FileWriter {

	void write(String pathName, String fileName, Exception content) throws IOException;

}
