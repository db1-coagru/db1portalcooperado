package br.com.db1.portalcooperado.infrastructure.folders.files;

import java.util.List;

public interface FolderFiles<T> {

	List<T> values();

	Integer size();

	boolean isEmpty();
}
