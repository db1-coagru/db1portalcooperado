package br.com.db1.portalcooperado.infrastructure.folders.files.writers;

public class FileWriterFactory {

	private FileWriterFactory(){

	}

	public static FileWriter newTxtWriter(){
		return TxtWriter.of();
	}
}
