package br.com.db1.portalcooperado.infrastructure.jaxb.xmladapters.quantidade;

import br.com.db1.portalcooperado.model.types.Quantidade;
import org.assertj.core.util.Strings;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public abstract class AbstractQuantidadeXmlAdapter extends XmlAdapter<String, Quantidade> {

	@Override
	public Quantidade unmarshal(String value) throws Exception {
		if (value != null && !Strings.isNullOrEmpty(value.trim())) {
			return Quantidade.of(value, scale());
		}
		return Quantidade.zero(scale());
	}

	@Override
	public String marshal(Quantidade quantidade) throws Exception {
		if (quantidade != null) {
			return quantidade.toString();
		}
		return null;
	}

	protected abstract Integer scale();
	
}
