package br.com.db1.portalcooperado.infrastructure.jaxb.xmladapters.quantidade;

public class DuasDecimaisQuantidadeXmlAdapter extends AbstractQuantidadeXmlAdapter {

	private static final int SCALE = 2;

	@Override
	protected Integer scale() {
		return SCALE;
	}

}
