package br.com.db1.portalcooperado.infrastructure.jaxb.xmladapters.quantidade;

public class TresDecimaisQuantidadeXmlAdapter extends AbstractQuantidadeXmlAdapter {

	private static final int SCALE = 3;

	@Override
	protected Integer scale() {
		return SCALE;
	}

}
