package br.com.db1.portalcooperado.infrastructure.mybatis.typehandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class AbstractLongTypeHandler extends BaseTypeHandler {

	@Override
	public final void setNonNullParameter(PreparedStatement preparedStatement, int index, Object obj, JdbcType jdbcType)
			throws SQLException {
		preparedStatement.setLong(index, getAsLong(obj));
	}

	@Override
	public final Object getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
		Long result = resultSet.getLong(columnName);
		return getAsObject(result);
	}

	@Override
	public final Object getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
		Long result = callableStatement.getLong(columnIndex);
		return getAsObject(result);
	}

	@Override
	public void setParameter(PreparedStatement preparedStatement, int index, Object parameter, JdbcType jdbcType)
			throws SQLException {
		super.setParameter(preparedStatement, index, getAsLong(parameter), JdbcType.NUMERIC);
	}

	protected abstract Long getAsLong(Object obj);

	protected abstract Object getAsObject(Long obj);
}
