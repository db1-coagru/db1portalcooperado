package br.com.db1.portalcooperado.infrastructure.mybatis.typehandler.quantidade;

import br.com.db1.portalcooperado.model.types.Quantidade;
import org.apache.ibatis.type.BigDecimalTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class AbstractQuantidadeTypeHandler extends BigDecimalTypeHandler {

	@Override
	public final void setNonNullParameter(PreparedStatement preparedStatement, int index, Object obj, JdbcType jdbcType)
			throws SQLException {
		if (obj != null) {
			Quantidade quantidade = (Quantidade) obj;
			quantidade = quantidade.setScale(scale());
			super.setNonNullParameter(preparedStatement, index, quantidade.toBigDecimal(), jdbcType);
		}
	}

	@Override
	public final Object getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
		BigDecimal result = resultSet.getBigDecimal(columnName);
		if (result != null) {
			return Quantidade.of(result, scale());
		}
		return null;
	}

	@Override
	public final Object getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
		BigDecimal result = callableStatement.getBigDecimal(columnIndex);
		if (result != null) {
			return Quantidade.of(result, scale());
		}
		return null;
	}

	protected abstract Integer scale();

}
