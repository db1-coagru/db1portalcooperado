package br.com.db1.portalcooperado.infrastructure.mybatis.typehandler.quantidade;

public class TresDecimaisQuantidadeTypeHandler extends AbstractQuantidadeTypeHandler {

	@Override
	protected Integer scale() {
		return 3;
	}

}
