package br.com.db1.portalcooperado.infrastructure.mybatis.typehandler;

import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TipoNotificacaoTypeHandler extends BaseTypeHandler {

	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int index, Object obj, JdbcType jdbcType)
			throws SQLException {
		TipoNotificacao tipoNotificacao = (TipoNotificacao) obj;
		preparedStatement.setLong(index, tipoNotificacao.getCodigo());
	}

	@Override
	public Object getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
		Long result = resultSet.getLong(columnName);
		return TipoNotificacao.fromCodigo(result);
	}

	@Override
	public Object getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
		Long result = callableStatement.getLong(columnIndex);
		return TipoNotificacao.fromCodigo(result);
	}

}
