package br.com.db1.portalcooperado.infrastructure.mybatis.typehandler.quantidade;

public class DefaultDecimaisQuantidadeTypeHandler extends AbstractQuantidadeTypeHandler {

	@Override
	protected Integer scale() {
		return 2;
	}

}
