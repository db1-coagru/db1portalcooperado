package br.com.db1.portalcooperado.infrastructure.mybatis.typehandler;

import br.com.db1.portalcooperado.model.types.Produto;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.assertj.core.util.Strings;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProdutoTypeHandler extends BaseTypeHandler {

	@Override
	public void setNonNullParameter(PreparedStatement preparedStatement, int index, Object obj, JdbcType jdbcType)
			throws SQLException {
		Produto produto = (Produto) obj;
		preparedStatement.setString(index, produto.getDescricao());
	}

	@Override
	public Object getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
		String result = resultSet.getString(columnName);
		if (!Strings.isNullOrEmpty(result)) {
			return Produto.fromStringValue(result);
		}
		return null;
	}

	@Override
	public Object getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
		String result = callableStatement.getString(columnIndex);
		if (!Strings.isNullOrEmpty(result)) {
			return Produto.fromStringValue(result);
		}
		return null;
	}

}
