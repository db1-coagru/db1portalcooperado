package br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.productinfo;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;

import javax.xml.bind.annotation.XmlElement;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class ProductInfo {

	@XmlElement(name = "VendorId", namespace = ServiceRecord.NAMESPACE)
	private Long vendorId;

	@XmlElement(name = "Name", namespace = ServiceRecord.NAMESPACE)
	private String name;

	@XmlElement(name = "Unit", namespace = ServiceRecord.NAMESPACE)
	private String unit;

	public Long getVendorId() {
		checkNotNull(vendorId, ServiceRecord
				.errorMessage("ProductInfo > VendorId"));
		return vendorId;
	}

	public String getName() {
		checkNotNull(name, ServiceRecord
				.errorMessage("ProductInfo > Name"));
		return name;
	}

	public String getUnit() {
		checkNotNull(name, ServiceRecord
				.errorMessage("ProductInfo > Unit"));
		return unit;
	}
}
