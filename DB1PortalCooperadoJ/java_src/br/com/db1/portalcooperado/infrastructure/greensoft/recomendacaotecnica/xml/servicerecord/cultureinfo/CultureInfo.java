package br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.cultureinfo;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;

import javax.xml.bind.annotation.XmlElement;

public class CultureInfo {

	@XmlElement(name = "Description", namespace = ServiceRecord.NAMESPACE)
	private String description;

	protected CultureInfo() {
		// construtor padrão utilizado pelo jaxb
	}

	public String getDescription() {
		PortalCooperadoUtil.checkNotNull(description, ServiceRecord
				.errorMessage("Description"));
		return description;
	}
}
