package br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.operationinfo;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;

import javax.xml.bind.annotation.XmlElement;
import java.util.Date;

public class OperationInfo {

	@XmlElement(name = "CustomerId", namespace = ServiceRecord.NAMESPACE)
	private String customerId;

	@XmlElement(name = "CreateDate", namespace = ServiceRecord.NAMESPACE)
	private Date createDate;

	protected OperationInfo() {
		// construtor padrão utilizado pelo jaxb
	}

	public String getCustomerId() {
		PortalCooperadoUtil.checkNotNull(customerId, ServiceRecord
				.errorMessage("CustomerId"));
		return customerId;
	}

	public Date getCreateDate() {
		PortalCooperadoUtil.checkNotNull(createDate, ServiceRecord
				.errorMessage("CreateDate"));
		return createDate;
	}

}
