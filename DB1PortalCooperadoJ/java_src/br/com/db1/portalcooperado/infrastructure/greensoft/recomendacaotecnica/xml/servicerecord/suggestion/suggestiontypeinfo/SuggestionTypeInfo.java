package br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.suggestiontypeinfo;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;

import javax.xml.bind.annotation.XmlElement;

public class SuggestionTypeInfo {

	@XmlElement(name = "Description", namespace = ServiceRecord.NAMESPACE)
	private String description;

	protected SuggestionTypeInfo() {
		// construtor padrão utilizado pelo jaxb
	}

	public String getDescription() {
		return description;
	}
}
