package br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.cultureinfo.CultureInfo;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.operationinfo.OperationInfo;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.Suggestion;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.TypeInfo;
import br.com.db1.portalcooperado.infrastructure.jaxb.xmladapters.quantidade.TresDecimaisQuantidadeXmlAdapter;
import br.com.db1.portalcooperado.model.types.Quantidade;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.base.MoreObjects;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.File;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "ServiceRecord")
public class ServiceRecord {

	public static final String NAMESPACE = "http://tempuri.org/";

	private static final String MENSAGEM_ERRO = "Problema ao recuperar o %s. Verifique se o mesmo foi informado com valor correto ou se o namespace está correto (http://tempuri.org/)";

	private String fileName;

	@XmlElement(name = "CultureInfo", namespace = NAMESPACE)
	private CultureInfo cultureInfo;

	@XmlElement(name = "OperationInfo", namespace = NAMESPACE)
	private OperationInfo operationInfo;

	@XmlElement(name = "TotalArea", namespace = NAMESPACE)
	@XmlJavaTypeAdapter(value = TresDecimaisQuantidadeXmlAdapter.class)
	private Quantidade totalArea;

	@XmlElement(name = "Efficiency", namespace = NAMESPACE)
	@XmlJavaTypeAdapter(value = TresDecimaisQuantidadeXmlAdapter.class)
	private Quantidade efficiency;

	@XmlElementWrapper(name = "Suggestions", namespace = NAMESPACE)
	@XmlElement(name = "Suggestion", namespace = NAMESPACE)
	private List<Suggestion> suggestions = Lists.newLinkedList();

	@XmlElement(name = "Volume", namespace = NAMESPACE)
	@XmlJavaTypeAdapter(value = TresDecimaisQuantidadeXmlAdapter.class)
	private Quantidade volume;

	@XmlElement(name = "Recommendation", namespace = NAMESPACE)
	private String recommendation;

	@XmlElement(name = "Urgent", namespace = NAMESPACE)
	private Boolean urgent;

	@XmlElement(name = "CustomMessage", namespace = NAMESPACE)
	private String customMessage;

	protected ServiceRecord() {
		// construtor padrão utilizado pelo jaxb
	}

	public static ServiceRecord of(File file) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(ServiceRecord.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		ServiceRecord serviceRecord = (ServiceRecord) unmarshaller.unmarshal(file);
		serviceRecord.fileName = file.getName();
		return serviceRecord;
	}

	public static String errorMessage(String field) {
		return String.format(MENSAGEM_ERRO, field);
	}

	public List<Suggestion> suggestionsOf(TypeInfo typeInfo) {
		ImmutableList.Builder<Suggestion> builder = ImmutableList.builder();
		for (Suggestion suggestion : getSuggestions()) {
			if (suggestion.typeInfoIs(typeInfo)) {
				builder.add(suggestion);
			}
		}
		return builder.build();
	}

	public String getCultureInfoDescription() {
		PortalCooperadoUtil.checkNotNull(cultureInfo, errorMessage("CultureInfo"));
		return cultureInfo.getDescription();
	}

	public Date getOperationInfoCreateDate() {
		return getOperationInfo().getCreateDate();
	}

	public String getCustomerId() {
		return getOperationInfo().getCustomerId();
	}

	private OperationInfo getOperationInfo() {
		PortalCooperadoUtil.checkNotNull(operationInfo, errorMessage("OperationInfo"));
		return operationInfo;
	}

	public Quantidade getTotalArea() {
		PortalCooperadoUtil.checkNotNull(totalArea, errorMessage("TotalArea"));
		return totalArea;
	}

	public Quantidade getEfficiency() {
		return efficiency;
	}

	public Quantidade getVolume() {
		PortalCooperadoUtil.checkNotNull(volume, errorMessage("Volume"));
		return volume;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public String getFileName() {
		return fileName;
	}

	public List<Suggestion> getSuggestions() {
		return MoreObjects.firstNonNull(suggestions, Lists.<Suggestion>newLinkedList());
	}

	public Boolean getUrgent() {
		return urgent;
	}

	public String getCustomMessage() {
		return customMessage;
	}
}

