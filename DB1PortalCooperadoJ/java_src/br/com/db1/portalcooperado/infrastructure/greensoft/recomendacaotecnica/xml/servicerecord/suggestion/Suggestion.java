package br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.suggestiontypeinfo.SuggestionTypeInfo;
import br.com.db1.portalcooperado.infrastructure.jaxb.xmladapters.quantidade.DuasDecimaisQuantidadeXmlAdapter;
import br.com.db1.portalcooperado.model.types.Quantidade;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.productinfo.ProductInfo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class Suggestion {

	@XmlElement(name = "TypeInfo", namespace = ServiceRecord.NAMESPACE)
	private TypeInfo typeInfo;

	@XmlElement(name = "SuggestionTypeInfo", namespace = ServiceRecord.NAMESPACE)
	private SuggestionTypeInfo suggestionTypeInfo;

	@XmlElement(name = "Note", namespace = ServiceRecord.NAMESPACE)
	private String note;

	@XmlElement(name = "ProductInfo", namespace = ServiceRecord.NAMESPACE)
	private ProductInfo productInfo;

	@XmlElement(name = "Order", namespace = ServiceRecord.NAMESPACE)
	private Long order;

	@XmlElement(name = "Quantity", namespace = ServiceRecord.NAMESPACE)
	@XmlJavaTypeAdapter(value = DuasDecimaisQuantidadeXmlAdapter.class)
	private Quantidade quantity;

	protected Suggestion() {
		// construtor padrão utilizado pelo jaxb
	}

	private Suggestion(TypeInfo typeInfo) {
		this.typeInfo = typeInfo;
	}

	public static Suggestion of(TypeInfo typeInfo) {
		checkTypeInfo(typeInfo);
		return new Suggestion(typeInfo);
	}

	private static void checkTypeInfo(TypeInfo typeInfo) {
		checkNotNull(typeInfo, ServiceRecord
				.errorMessage("Suggetion > TypeInfo"));
	}

	public boolean typeInfoIs(TypeInfo typeInfo) {
		checkTypeInfo(typeInfo);
		return this.typeInfo != null && this.typeInfo.equals(typeInfo);
	}

	public String getSuggestionTypeInfoDescripton() {
		checkNotNull(suggestionTypeInfo, ServiceRecord
				.errorMessage("Suggetion > SuggetsionTypeInfo"));
		return suggestionTypeInfo.getDescription();
	}

	public String getNote() {
		checkNotNull(note, ServiceRecord
				.errorMessage("Suggetion > Note"));
		return note;
	}

	public Long getProductInfoVendorId() {
		return getProductInfo().getVendorId();
	}

	public String getProductInfoName() {
		return getProductInfo().getName();
	}

	public String getUnit() {
		return getProductInfo().getUnit();
	}

	private ProductInfo getProductInfo() {
		checkNotNull(productInfo, ServiceRecord
				.errorMessage("Suggetion > ProductInfo"));
		return productInfo;
	}

	public Long getTypeInfoCode() {
		checkTypeInfo(typeInfo);
		return typeInfo.getCodigo();
	}

	public Long getOrder() {
		checkNotNull(order, ServiceRecord
				.errorMessage("Suggetion > Order"));
		return order;
	}

	public Quantidade getQuantity() {
		checkNotNull(quantity, ServiceRecord
				.errorMessage("Suggetion > Quantity"));
		return quantity;
	}

}
