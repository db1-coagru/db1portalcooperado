package br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

@XmlEnum
public enum TypeInfo {

	@XmlEnumValue(value = "3")
	DIAGNOSTICOS(3L),

	@XmlEnumValue(value = "4")
	PRODUTOS(4L);

	private Long codigo;

	TypeInfo(Long codigo) {
		this.codigo = codigo;
	}

	public Long getCodigo() {
		return codigo;
	}

}
