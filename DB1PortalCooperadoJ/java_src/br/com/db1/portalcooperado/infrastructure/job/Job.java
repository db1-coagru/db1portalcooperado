package br.com.db1.portalcooperado.infrastructure.job;

public interface Job {

	Boolean isEnabled();

	void run();

}
