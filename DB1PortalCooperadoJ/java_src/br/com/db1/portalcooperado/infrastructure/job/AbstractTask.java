package br.com.db1.portalcooperado.infrastructure.job;

import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.webservice.FaultDetailMessage;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public abstract class AbstractTask implements Task {

	protected final JobStatistics statistics = JobStatistics.of();

	protected final JobLogger logger;

	protected AbstractTask() {
		this.logger = JobLogger.of(this.getClass().getSimpleName());
	}

	@Override
	public JobStatistics run() {
		try {
			statistics.init();
			execute();
		}
		catch (Exception ex) {
			logger.erro(String.format("Erro no processamento: %s", this.getClass().getSimpleName()), ex);
		}
		return statistics;
	}

	protected abstract void execute() throws InterruptedException, IOException, JAXBException, FaultDetailMessage;

}
