package br.com.db1.portalcooperado.infrastructure.job;

public abstract class AbstractJob implements Job {

	private final JobLogger logger;

	private final Task task;

	protected AbstractJob(Task task) {
		this.logger = JobLogger.of(this.getClass().getSimpleName());
		this.task = task;
	}

	@Override
	public void run() {
		try {
			if (isEnabled()) {
				logger.inicio();
				JobStatistics jobStatistics = task.run();
				logger.termino(jobStatistics);
			}
		}
		catch (Exception ex) {
			logger.erro(String.format("Erro ao processar job: %s", this.getClass().getSimpleName()), ex);
		}
	}

}
