package br.com.db1.portalcooperado.infrastructure.job;

public interface Task {

	JobStatistics run();
	
}
