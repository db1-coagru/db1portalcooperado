package br.com.db1.portalcooperado.infrastructure.job;

import jersey.repackaged.com.google.common.base.Objects;

public class JobStatistics {

	private Long processado = 0L;

	private Long sucesso = 0L;

	private Long erro = 0L;

	private Long ignorados = 0L;

	private JobStatistics() {

	}

	public static JobStatistics of() {
		return new JobStatistics();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JobStatistics) {
			JobStatistics that = (JobStatistics) obj;
			return Objects.equal(processado, that.processado)
					&& Objects.equal(sucesso, that.sucesso)
					&& Objects.equal(erro, that.erro)
					&& Objects.equal(ignorados, that.ignorados);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(processado, sucesso, erro, ignorados);
	}

	@Override
	public String toString() {
		return String
				.format("Processados: %s, Sucesso: %s,  Erro: %s, Ignorados: %s", processado, sucesso, erro, ignorados);
	}

	public void init() {
		processado = 0L;
		sucesso = 0L;
		erro = 0L;
		ignorados = 0L;
	}

	public void addProcessado() {
		processado++;
	}

	public void addSucesso() {
		sucesso++;
	}

	public void addErro() {
		erro++;
	}

	public void addIgnorado() {
		ignorados++;
	}

}
