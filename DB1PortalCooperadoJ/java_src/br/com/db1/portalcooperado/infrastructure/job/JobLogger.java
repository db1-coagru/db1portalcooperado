package br.com.db1.portalcooperado.infrastructure.job;

import br.com.db1.portalcooperado.util.Mensagem;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.Date;

import static br.com.db1.portalcooperado.util.DateFormatter.DD_MM_YYYY_HH_MM_SS;

public class JobLogger {

	private static final String INICIO = "%s - %s iniciado";

	private static final String ITEM_NAO_ENVIADO = "%s - Processo: %s \n Item não enviado: %s \n Motivo: %s";

	private static final String TERMINO = "%s - %s finalizado. Resultado processamento: %s";

	private final String nomeProcesso;

	private final Logger logger;

	private JobLogger(String nomeProcesso) {
		this.nomeProcesso = nomeProcesso;
		this.logger = Logger.getLogger(nomeProcesso);
		this.logger.setLevel(Level.ALL);
	}

	public static JobLogger of(String nomeDoProcesso) {
		PortalCooperadoUtil.checkNotNull(nomeDoProcesso, Mensagem.campoObrigatorioMasculino("Nome do processo"));
		return new JobLogger(nomeDoProcesso);
	}

	public void inicio() {
		info(String.format(INICIO, dataHora(), nomeProcesso));
	}

	public void termino(JobStatistics statistics) {
		info(String.format(TERMINO, dataHora(), nomeProcesso, statistics));
	}

	public void itemErro(Object object, Exception ex) {
		erro(String.format(ITEM_NAO_ENVIADO, dataHora(), nomeProcesso, object, ex.getMessage()), ex);
	}

	public void erro(String message, Exception ex) {
		logger.error(message, ex);
	}

	public void info(String message) {
		logger.info(message);
	}

	private String dataHora() {
		return DD_MM_YYYY_HH_MM_SS.format(new Date());
	}
}
