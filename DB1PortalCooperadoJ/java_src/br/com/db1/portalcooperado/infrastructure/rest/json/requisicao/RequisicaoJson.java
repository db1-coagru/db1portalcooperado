package br.com.db1.portalcooperado.infrastructure.rest.json.requisicao;

import br.com.db1.portalcooperado.model.types.Produto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.Date;

@JsonRootName("requisicao")
public class RequisicaoJson {

	@JsonProperty(value = "tipo")
	private TipoRequisicao tipo;

	@JsonProperty(value = "ip")
	private String ip;

	@JsonProperty("dataInicial")
	private Date dataInicial;

	@JsonProperty("dataFinal")
	private Date dataFinal;

	@JsonProperty("soja")
	private boolean soja;

	@JsonProperty("trigo")
	private boolean trigo;

	@JsonProperty("milho")
	private boolean milho;

	@JsonProperty("exportaPdf")
	private boolean exportaPdf;

	@JsonProperty("matricula")
	private String matricula;

	@JsonProperty("idRecomendacaoTecnica")
	private Long idRecomendacaoTecnica;

	@JsonProperty("idAvicultura")
	private Long idAvicultura;

	private RequisicaoJson() {

	}

	public boolean temAlgumTipoCultura() {
		return soja || trigo || milho;
	}

	public boolean isOf(TipoRequisicao tipoRequisicao) {
		return this.tipo.equals(tipoRequisicao);
	}

	public Long getTiposAsLong() {
		return tipo.toLongValue();
	}

	public Long getTipoProduto() {
		return Produto.buildCodigoFrom(soja, milho, trigo);
	}

	public Long getMatriculaAsLong() {
		return matricula != null ? Long.parseLong(matricula) : null;
	}

	public boolean isPosicaoFinanceira() {
		return tipo.isPosicaoFinanceira();
	}

	public TipoRequisicao getTipoRequisicao() {
		return tipo;
	}

	public String getIp() {
		return ip;
	}

	public Date getDataInicial() {
		return dataInicial;
	}

	public Date getDataFinal() {
		return dataFinal;
	}

	public boolean isExportaPdf() {
		return exportaPdf;
	}

	public String getMatricula() {
		return matricula;
	}

	public Long getIdRecomendacaoTecnica() {
		return idRecomendacaoTecnica;
	}

	public Long getIdAvicultura() {
		return idAvicultura;
	}
}
