package br.com.db1.portalcooperado.infrastructure.rest.exceptions;

import br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson;
import br.com.db1.portalcooperado.infrastructure.rest.responses.Responses;

import javax.ws.rs.WebApplicationException;

/**
 * Created by rafael.rasso on 03/08/2016.
 * Exeção disparada quando determinada requisição REST não é aceitável.
 */
public class CustomUnauthorizedWebApplicationException extends WebApplicationException {

	private static final long serialVersionUID = -8002261381483102180L;

	/**
	 * Construtor de CustomNotAcceptableException a partir de uma DB1Exception.
	 * Além de servir para disparar a exeção ele ja captura a mensagem da exceção e gera o JSON de retorno.
	 *
	 * @param exception
	 */
	public CustomUnauthorizedWebApplicationException(String message) {
		super(Responses.unauthorized(MensagemJson.of(message)));
	}

}
