package br.com.db1.portalcooperado.infrastructure.rest.json.avicultura;

import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaResposta;
import jersey.repackaged.com.google.common.collect.Lists;

import java.util.List;

public class AviculturaItemJsonBuilder {

	private String descricao;

	private AviculturaResposta resposta;

	private String observacao;

	private List<String> imagens = Lists.newLinkedList();

	private AviculturaItemJsonBuilder() {

	}

	static AviculturaItemJsonBuilder of() {
		return new AviculturaItemJsonBuilder();
	}

	public AviculturaItemJsonBuilder withDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}

	public AviculturaItemJsonBuilder withResposta(AviculturaResposta resposta) {
		this.resposta = resposta;
		return this;
	}

	public AviculturaItemJsonBuilder withObservacao(String observacao) {
		this.observacao = observacao;
		return this;
	}

	public AviculturaItemJsonBuilder withImagens(List<String> imagens) {
		this.imagens = imagens;
		return this;
	}

	public AviculturaItemJson build() {
		return new AviculturaItemJson(this);
	}

	String getDescricao() {
		return descricao;
	}

	AviculturaResposta getResposta() {
		return resposta;
	}

	String getObservacao() {
		return observacao;
	}

	List<String> getImagens() {
		return imagens;
	}
}
