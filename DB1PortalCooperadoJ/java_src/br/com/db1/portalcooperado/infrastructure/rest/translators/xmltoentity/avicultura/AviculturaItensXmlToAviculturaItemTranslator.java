package br.com.db1.portalcooperado.infrastructure.rest.translators.xmltoentity.avicultura;

import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaItemXml;
import br.com.db1.portalcooperado.infrastructure.rest.translators.ListToListTranslator;
import br.com.db1.portalcooperado.infrastructure.rest.translators.Translator;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AviculturaItensXmlToAviculturaItemTranslator
		implements Translator<List<AviculturaItemXml>, List<AviculturaItem>> {

	private final AviculturaItemXmlToAviculturaTranslator aviculturaItemXmlToAviculturaTranslator;

	@Autowired
	public AviculturaItensXmlToAviculturaItemTranslator(
			AviculturaItemXmlToAviculturaTranslator aviculturaItemXmlToAviculturaTranslator) {
		this.aviculturaItemXmlToAviculturaTranslator = aviculturaItemXmlToAviculturaTranslator;
	}

	@Override
	public List<AviculturaItem> translate(List<AviculturaItemXml> from) {
		return ListToListTranslator.translate(from, aviculturaItemXmlToAviculturaTranslator);
	}
}
