package br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("posicaoFinanceiraMensagem")
public class PosicaoFinanceiraMensagemJson {

	@JsonProperty("descricao")
	private final String descricao;

	private PosicaoFinanceiraMensagemJson(String descricao) {
		this.descricao = descricao;
	}

	public static PosicaoFinanceiraMensagemJson of(String descricao) {
		return new PosicaoFinanceiraMensagemJson(descricao);
	}

}
