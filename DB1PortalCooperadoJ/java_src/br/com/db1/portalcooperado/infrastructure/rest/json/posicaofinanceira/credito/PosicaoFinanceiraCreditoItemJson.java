package br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira.credito;

import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraCredito;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.math.BigDecimal;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.roundTwo;

@JsonRootName("creditoItem")
public class PosicaoFinanceiraCreditoItemJson {

	@JsonProperty("romaneio")
	private final String romaneio;

	@JsonProperty("sequencia")
	private final Long sequencia;

	@JsonProperty("local")
	private final String local;

	@JsonProperty("padrao")
	private final Long padrao;

	@JsonProperty("tipo")
	private final Long tipo;

	@JsonProperty("pesoLiquido")
	private final BigDecimal pesoLiquido;

	@JsonProperty("valorDia")
	private final BigDecimal valorDia;

	@JsonProperty("valorLiquido")
	private final BigDecimal valorLiquido;

	private PosicaoFinanceiraCreditoItemJson(PosicaoFinanceiraCredito posicaoFinanceiraCredito) {
		this.romaneio = posicaoFinanceiraCredito.getNrRomaneio();
		this.sequencia = posicaoFinanceiraCredito.getNrSequencia();
		this.local = posicaoFinanceiraCredito.getDsLocal();
		this.padrao = posicaoFinanceiraCredito.getNrPadrao();
		this.tipo = posicaoFinanceiraCredito.getNrTipo();
		this.pesoLiquido = roundTwo(posicaoFinanceiraCredito.getPesoLiquido());
		this.valorDia = roundTwo(posicaoFinanceiraCredito.getVlDia());
		this.valorLiquido = roundTwo(posicaoFinanceiraCredito.getVlLiquido());
	}

	public static PosicaoFinanceiraCreditoItemJson of(PosicaoFinanceiraCredito posicaoFinanceiraCredito) {
		checkNotNull(posicaoFinanceiraCredito, "Crédito de posição financeira deve ser informado para gerar o JSON.");
		return new PosicaoFinanceiraCreditoItemJson(posicaoFinanceiraCredito);
	}

	public BigDecimal getPesoLiquido() {
		return pesoLiquido;
	}

	public BigDecimal getValorLiquido() {
		return valorLiquido;
	}
}
