package br.com.db1.portalcooperado.infrastructure.rest.json.entregasproducao;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;

import java.util.List;

@JsonRootName("entregasProducaoProduto")
public class EntregasProducaoProdutoJson {

	@JsonProperty("produto")
	private final String produto;

	@JsonProperty("itens")
	private final List<EntregasProducaoProdutoItemJson> itens;

	@JsonProperty("total")
	private final EntregasProducaoTotal total;

	private EntregasProducaoProdutoJson(Builder builder) {
		this.produto = builder.produto;
		this.itens = builder.createItens();
		this.total = builder.total;
	}

	protected static Builder builder(String produto) {
		return new Builder(produto);
	}

	protected static class Builder {

		private final String produto;

		private final List<EntregasProducaoProdutoItemJson> itens = Lists.newLinkedList();

		private final EntregasProducaoTotal total = EntregasProducaoTotal.zero();

		private Builder(String produto) {
			this.produto = produto;
		}

		protected void add(EntregasProducaoProdutoItemJson item) {
			itens.add(item);
			total.add(item);
		}

		protected EntregasProducaoProdutoJson build() {
			return new EntregasProducaoProdutoJson(this);
		}

		private List<EntregasProducaoProdutoItemJson> createItens() {
			return ImmutableList.copyOf(itens);
		}
	}

}
