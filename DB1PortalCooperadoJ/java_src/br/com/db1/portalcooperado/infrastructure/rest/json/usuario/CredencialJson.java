package br.com.db1.portalcooperado.infrastructure.rest.json.usuario;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * Created by rafael.rasso on 01/08/2016.
 * <p>
 * Classe que representa o JSON de login que será recebido via requisição
 * POST na API REST.
 */
@JsonRootName("credencial")
public class CredencialJson {

	public static final String DOMAIN = "COAGRU";

	@JsonProperty("matricula")
	private String matricula;

	@JsonProperty("senha")
	private String senha;

	private CredencialJson() {

	}

	/**
	 * Retorna a matricula do login recebida via JSON.
	 *
	 * @return
	 */
	public String getMatricula() {
		return matricula;
	}

	/**
	 * Retorna a senha criptografada do login recebida via JSON.
	 *
	 * @return
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * Retorna o Dominio do login, este fixado em COAGRU pois a aplicação mobile não informará o mesmo.
	 *
	 * @return
	 */
	public String getDomain() {
		return DOMAIN;
	}

}
