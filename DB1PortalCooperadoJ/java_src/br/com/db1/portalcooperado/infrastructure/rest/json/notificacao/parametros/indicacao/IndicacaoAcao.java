package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.indicacao;

import com.fasterxml.jackson.annotation.JsonValue;

public enum IndicacaoAcao {

    INCLUSAO("I") {
        @Override
        public String getTitulo() {
            return "Nova indicação realizada";
        }

        @Override
        public String mensagemOf(String nomeIndicador) {
            return String.format("Você foi indicado pelo cooperante %s. Toque para atualizar as permissões", nomeIndicador);
        }
    },

    ALTERACAO("A") {
        @Override
        public String getTitulo() {
            return "Uma alteração das suas permissões foi realizada";
        }

        @Override
        public String mensagemOf(String nomeIndicador) {
            return String.format("Sua permissão para acesso aos dados do cooperante %s foi atualizada. " +
                    "Toque para atualizar as permissões", nomeIndicador);
        }
    },

    EXCLUSAO("E") {
        @Override
        public String getTitulo() {
            return "Suas permissões foram revogadas.";
        }

        @Override
        public String mensagemOf(String nomeIndicador) {
            return String.format("Sua indicação para visualizar dados do cooperante %s foi removida. " +
                    "Toque para atualizar as permissoes", nomeIndicador);
        }
    };

    private final String codigo;

    IndicacaoAcao(String codigo) {
        this.codigo = codigo;
    }

    public abstract String getTitulo();

    public abstract String mensagemOf(String nomeIndicador);

    @JsonValue
    public String getCodigo() {
        return codigo;
    }

}
