package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.recomendacaotecnica;

import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.AbstractParametrosJson;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioFeminino;
import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioMasculino;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class RecomendacaoTecnicaParametrosJson extends AbstractParametrosJson {

	@JsonProperty("idRecomendacaoTecnica")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final Long recomendacaoTecnica;

	private RecomendacaoTecnicaParametrosJson(Builder builder) {
		super(builder.tipoNotificacao());
		this.recomendacaoTecnica = builder.recomendacaoTecnica;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder extends AbstractParametrosJsonBuilder {

		private Long recomendacaoTecnica;

		private Builder() {

		}

		@Override
		public Builder withTipo(TipoNotificacao tipoNotificacao) {
			super.withTipo(tipoNotificacao);
			return this;
		}

		public Builder withRecomendacaoTecnica(Long recomendacaoTecnica) {
			this.recomendacaoTecnica = recomendacaoTecnica;
			return this;
		}

		public RecomendacaoTecnicaParametrosJson build() {
			checkNotNull(tipoNotificacao, campoObrigatorioMasculino("Tipo de notificação"));
			checkArgument(tipoNotificacao.isRecomendacaoTecnica(), "Tipo de notificação incorreto.");
			checkNotNull(recomendacaoTecnica, campoObrigatorioFeminino("Recomendação técnica"));
			return new RecomendacaoTecnicaParametrosJson(this);
		}

	}
}
