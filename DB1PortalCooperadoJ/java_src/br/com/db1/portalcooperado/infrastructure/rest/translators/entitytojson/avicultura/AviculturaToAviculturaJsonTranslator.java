package br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura;

import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJson;
import br.com.db1.portalcooperado.infrastructure.rest.translators.Translator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AviculturaToAviculturaJsonTranslator implements Translator<Avicultura, AviculturaJson> {

	private final AviculturaItensToAviculturaItensJsonTranslator aviculturaItensToAviculturaItensJsonTranslator;

	@Autowired
	public AviculturaToAviculturaJsonTranslator(
			AviculturaItensToAviculturaItensJsonTranslator aviculturaItensToAviculturaItensJsonTranslator) {
		this.aviculturaItensToAviculturaItensJsonTranslator = aviculturaItensToAviculturaItensJsonTranslator;
	}

	@Override
	public AviculturaJson translate(Avicultura from) {
		return AviculturaJson.builder()
				.withGalpao(from.getGalpao())
				.withData(from.getData())
				.withLinhagem(from.getLinhagem())
				.withLote(from.getLote())
				.withSexo(from.getSexo())
				.withQuantidade(from.getQuantidade())
				.withNomeTecnico(from.getNomeTecnico())
				.withItens(aviculturaItensToAviculturaItensJsonTranslator.translate(from.getItens()))
				.build();
	}

}
