package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao;

import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.util.Mensagem;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.collect.ImmutableList;

import java.util.List;

public class MatriculasEnviarNotificacao {

    private final List<MatriculaJson> values;

    private MatriculasEnviarNotificacao(List<MatriculaJson> values) {
        this.values = values;
    }

    public static MatriculasEnviarNotificacao of(Usuario usuario, PermissaoIndicado permissao) {
        ImmutableList.Builder<MatriculaJson> builder = ImmutableList.builder();
        builder.add(MatriculaJson.of(usuario.getMatricula()));
        for (String matriculaIndicado : usuario.matriculasIndicadosOf(permissao)) {
            builder.add(MatriculaJson.of(matriculaIndicado));
        }
        return new MatriculasEnviarNotificacao(builder.build());
    }

    public static MatriculasEnviarNotificacao of(Object matricula) {
        PortalCooperadoUtil.checkNotNull(matricula, Mensagem.campoObrigatorioMasculino("Matricula"));
        MatriculaJson matriculaJson = MatriculaJson.of(matricula);
        return new MatriculasEnviarNotificacao(ImmutableList.of(matriculaJson));
    }

    public List<MatriculaJson> values() {
        return values;
    }

    Boolean isEmpty() {
        return values.isEmpty();
    }


}
