package br.com.db1.portalcooperado.infrastructure.rest.json.avicultura;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.base.MoreObjects;
import jersey.repackaged.com.google.common.base.Objects;
import jersey.repackaged.com.google.common.collect.Lists;

import java.util.Date;
import java.util.List;

@JsonRootName(value = "avicultura")
public class AviculturaJson {

	@JsonProperty(value = "galpao")
	private String galpao;

	@JsonProperty(value = "lote")
	private String lote;

	@JsonProperty(value = "sexo")
	private String sexo;

	@JsonProperty(value = "quantidade")
	private Integer quantidade;

	@JsonProperty(value = "data")
	private Date data;

	@JsonProperty(value = "linhagem")
	private String linhagem;

	@JsonProperty(value = "nomeTecnico")
	private String nomeTecnico;

	@JsonProperty(value = "possuiNaoConformidade")
	private Boolean possuiNaoConformidade = Boolean.FALSE;

	@JsonProperty(value = "itens")
	private List<AviculturaItemJson> itens = Lists.newLinkedList();

	AviculturaJson(AviculturaJsonBuilder builder) {
		this.galpao = builder.getGalpao();
		this.lote = builder.getLote();
		this.sexo = builder.getSexo();
		this.quantidade = builder.getQuantidade();
		this.data = builder.getData();
		this.linhagem = builder.getLinhagem();
		this.nomeTecnico = builder.getNomeTecnico();
		this.possuiNaoConformidade = builder.getPossuiNaoConformidade();
		this.itens = builder.getItens();
	}

	public static AviculturaJsonBuilder builder() {
		return AviculturaJsonBuilder.of();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AviculturaJson) {
			AviculturaJson other = (AviculturaJson) obj;
			return Objects.equal(lote, other.lote) &&
					Objects.equal(data, other.data);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(lote, data);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("galpao", galpao)
				.add("lote", lote)
				.add("sexo", sexo)
				.add("quantidade", quantidade)
				.add("data", data)
				.add("linhagem", linhagem)
				.add("nomeTecnico", nomeTecnico)
				.toString();
	}

	String getGalpao() {
		return galpao;
	}

	String getLote() {
		return lote;
	}

	String getSexo() {
		return sexo;
	}

	Integer getQuantidade() {
		return quantidade;
	}

	Date getData() {
		return data;
	}

	String getLinhagem() {
		return linhagem;
	}

	String getNomeTecnico() {
		return nomeTecnico;
	}

	Boolean getPossuiNaoConformidade() {
		return possuiNaoConformidade;
	}

	List<AviculturaItemJson> getItens() {
		return itens;
	}

}
