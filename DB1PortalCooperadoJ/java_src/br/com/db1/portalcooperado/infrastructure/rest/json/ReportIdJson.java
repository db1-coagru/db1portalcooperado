package br.com.db1.portalcooperado.infrastructure.rest.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@JsonRootName("reportId")
public class ReportIdJson {

	@JsonProperty("value")
	private final String value;

	private ReportIdJson(String value) {
		this.value = value;
	}

	public static ReportIdJson of(String value) {
		checkNotNull(value, "Id do relatório deve ser informado.");
		return new ReportIdJson(value);
	}

}
