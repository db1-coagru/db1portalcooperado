package br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira.debito;

import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraDebitos;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.math.BigDecimal;
import java.util.Date;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.roundTwo;

@JsonRootName("debitoItem")
public class PosicaoFinanceiraDebitoItemJson {

    @JsonProperty("numeroDocumento")
    private final String numeroDocumento;

    @JsonProperty("serie")
    private final String serie;

    @JsonProperty("numeroParcela")
    private final Long numeroParcela;

    @JsonProperty("produto")
    private final String produto;

    @JsonProperty("filial")
    private final Long filial;

    @JsonProperty("lancamento")
    private final Date lancamento;

    @JsonProperty("vencimento")
    private final Date vencimento;

    @JsonProperty("valorProduto")
    private final BigDecimal valorProduto;

    @JsonProperty("valorJuros")
    private final BigDecimal valorJuros;

    @JsonProperty("valorLiquido")
    private final BigDecimal valorLiquido;

    private PosicaoFinanceiraDebitoItemJson(PosicaoFinanceiraDebitos debitos) {
        this.numeroDocumento = debitos.getNrDocumento();
        this.serie = debitos.getSerie();
        this.numeroParcela = debitos.getNrParcela();
        this.produto = debitos.getDsProduto();
        this.filial = debitos.getNrFilial();
        this.lancamento = debitos.getDtLancamento();
        this.vencimento = debitos.getDtVencimento();
        this.valorProduto = roundTwo(debitos.getVlProduto());
        this.valorJuros = roundTwo(debitos.getVlJuros());
        this.valorLiquido = roundTwo(debitos.getVlLiquido());
    }

    public static PosicaoFinanceiraDebitoItemJson of(PosicaoFinanceiraDebitos debitos) {
        return new PosicaoFinanceiraDebitoItemJson(debitos);
    }

    public BigDecimal getValorProduto() {
        return valorProduto;
    }

    public BigDecimal getValorJuros() {
        return valorJuros;
    }

    public BigDecimal getValorLiquido() {
        return valorLiquido;
    }

    @JsonIgnore
    public boolean isNaoVencido() {
        Date today = PortalCooperadoUtil.getDataAtMidnight();
        return today.equals(vencimento) || vencimento.after(today);
    }

}
