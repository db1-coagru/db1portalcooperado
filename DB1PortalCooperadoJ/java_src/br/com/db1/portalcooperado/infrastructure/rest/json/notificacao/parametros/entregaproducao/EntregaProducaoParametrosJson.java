package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.entregaproducao;

import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.AbstractParametrosJson;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioFeminino;
import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioMasculino;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class EntregaProducaoParametrosJson extends AbstractParametrosJson {

	@JsonProperty("trigo")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final Boolean trigo;

	@JsonProperty("milho")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final Boolean milho;

	@JsonProperty("soja")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final Boolean soja;

	@JsonProperty("dataInicio")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final Date inicio;

	@JsonProperty("dataFim")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final Date fim;

	private EntregaProducaoParametrosJson(Builder builder) {
		super(builder.tipoNotificacao());
		this.trigo = builder.trigo;
		this.soja = builder.soja;
		this.milho = builder.milho;
		this.inicio = builder.inicio;
		this.fim = builder.fim;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder extends AbstractParametrosJsonBuilder {

		private Boolean trigo = false;

		private Boolean milho = false;

		private Boolean soja = false;

		private Date inicio;

		private Date fim;

		private Builder() {

		}

		@Override
		public Builder withTipo(TipoNotificacao tipoNotificacao) {
			super.withTipo(tipoNotificacao);
			return this;
		}

		public Builder withTrigo() {
			trigo = true;
			return this;
		}

		public Builder withMilho() {
			milho = true;
			return this;
		}

		public Builder withSoja() {
			soja = true;
			return this;
		}

		public Builder between(Date inicio, Date fim) {
			this.inicio = inicio;
			this.fim = fim;
			return this;
		}

		public EntregaProducaoParametrosJson build() {
			checkNotNull(tipoNotificacao, campoObrigatorioMasculino("Tipo de notificação"));
			checkArgument(tipoNotificacao.isEntregaProducao(), "Tipo de notificação incorreto.");
			if (tipoNotificacao.isRomaneioAgricola()) {
				checkArgument(trigo || milho || soja, campoObrigatorioFeminino("Ao menos uma cultura"));
				checkNotNull(inicio, campoObrigatorioFeminino("Data inicial"));
				checkNotNull(fim, campoObrigatorioFeminino("Data final"));
			}
			return new EntregaProducaoParametrosJson(this);
		}

	}

}

