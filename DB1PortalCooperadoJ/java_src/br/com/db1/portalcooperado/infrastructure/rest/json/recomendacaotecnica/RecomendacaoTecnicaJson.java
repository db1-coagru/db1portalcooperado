package br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica;

import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaDiagnostico;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaImagem;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaProduto;
import br.com.db1.portalcooperado.util.DateFormatter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.collect.ImmutableList;

import java.util.List;

@JsonRootName(value = "recomendacaoTecnica")
public class RecomendacaoTecnicaJson {

	@JsonProperty(value = "produto")
	private final String produto;

	@JsonProperty(value = "talhao")
	private final String talhao;

	@JsonProperty(value = "data")
	private final String data;

	@JsonProperty(value = "rendimentoPulverizador")
	private final String rendimentoPulverizador;

	@JsonProperty(value = "diagnosticos")
	private final List<RecomendacaoTecnicaDiagnosticoJson> diagnosticos;

	@JsonProperty(value = "produtos")
	private final List<RecomendacaoTecnicaProdutoJson> produtos;

	@JsonProperty(value = "imagens")
	private final List<String> imagens;

	@JsonProperty(value = "recomendacoes")
	private final String recomendacoes;

	@JsonProperty(value = "urgente")
	private final Boolean urgente;

	@JsonProperty(value = "mensagemCustomizada")
	private final String mensagemCustomizada;

	private RecomendacaoTecnicaJson(Builder builder) {
		this.produto = builder.produto();
		this.talhao = builder.talhao();
		this.data = builder.data();
		this.rendimentoPulverizador = builder.rendimentoPulverizador();
		this.diagnosticos = builder.diagnosticos();
		this.imagens = builder.imagens();
		this.produtos = builder.produtos();
		this.recomendacoes = builder.recomendacoes();
		this.urgente = builder.getUrgente();
		this.mensagemCustomizada = builder.getMensagemCustomizada();
	}

	public static Builder builder(RecomendacaoTecnica recomendacaoTecnica) {
		return new Builder(recomendacaoTecnica);
	}

	String getProduto() {
		return produto;
	}

	String getTalhao() {
		return talhao;
	}

	String getData() {
		return data;
	}

	String getRendimentoPulverizador() {
		return rendimentoPulverizador;
	}

	List<RecomendacaoTecnicaDiagnosticoJson> getDiagnosticos() {
		return diagnosticos;
	}

	List<RecomendacaoTecnicaProdutoJson> getProdutos() {
		return produtos;
	}

	List<String> getImagens() {
		return imagens;
	}

	String getRecomendacoes() {
		return recomendacoes;
	}

	Boolean getUrgente() {
		return urgente;
	}

	String getMensagemCustomizada() {
		return mensagemCustomizada;
	}

	public static class Builder {

		private static final String CAMINHO = "rest/imagem?name=";

		private final RecomendacaoTecnica recomendacaoTecnica;

		private Builder(RecomendacaoTecnica recomendacaoTecnica) {
			this.recomendacaoTecnica = recomendacaoTecnica;
		}

		public RecomendacaoTecnicaJson build() {
			return new RecomendacaoTecnicaJson(this);
		}

		private String produto() {
			return recomendacaoTecnica.descricaoProduto();
		}

		private String talhao() {
			return recomendacaoTecnica.totalAreaAsString();
		}

		private String rendimentoPulverizador() {
			return String.format("%s alq.", recomendacaoTecnica.getEficiencia());
		}

		private String data() {
			return DateFormatter.DD_MM_YYYY.format(recomendacaoTecnica.getCriacao());
		}

		private List<RecomendacaoTecnicaDiagnosticoJson> diagnosticos() {
			ImmutableList.Builder<RecomendacaoTecnicaDiagnosticoJson> builder = ImmutableList.builder();
			for (RecomendacaoTecnicaDiagnostico diagnostico : recomendacaoTecnica.getDiagnosticos()) {
				RecomendacaoTecnicaDiagnosticoJson diagnosticoJson = RecomendacaoTecnicaDiagnosticoJson.of(diagnostico);
				builder.add(diagnosticoJson);
			}
			return builder.build();
		}

		private List<RecomendacaoTecnicaProdutoJson> produtos() {
			ImmutableList.Builder<RecomendacaoTecnicaProdutoJson> builder = ImmutableList.builder();
			for (RecomendacaoTecnicaProduto produto : recomendacaoTecnica.getProdutos()) {
				RecomendacaoTecnicaProdutoJson produtoJson = RecomendacaoTecnicaProdutoJson.of(produto);
				builder.add(produtoJson);
			}
			return builder.build();
		}

		private List<String> imagens() {
			ImmutableList.Builder<String> builder = ImmutableList.builder();
			for (RecomendacaoTecnicaImagem imagem : recomendacaoTecnica.getImagens()) {
				builder.add(String.format("%s%s", CAMINHO, imagem.getNome()));
			}
			return builder.build();
		}

		private String recomendacoes() {
			return recomendacaoTecnica.getRecomendacao();
		}

		private Boolean getUrgente() {
			return recomendacaoTecnica.getUrgente();
		}

		private String getMensagemCustomizada() {
			return recomendacaoTecnica.getMensagemCustomizada();
		}

	}

}
