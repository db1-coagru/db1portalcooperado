package br.com.db1.portalcooperado.infrastructure.rest.json.entregasproducao;

import br.com.db1.portalcooperado.model.entity.EntregaProducao;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.math.BigDecimal;
import java.util.Date;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.*;

@JsonRootName("entregasProducaoProdutoItem")
public class EntregasProducaoProdutoItemJson {

	@JsonProperty("entrega")
	private final Date entrega;

	@JsonProperty("local")
	private final String local;

	@JsonProperty("romaneio")
	private final String romaneio;

	@JsonProperty("sequencia")
	private final Long sequencia;

	@JsonProperty("numeroCadastroProduto")
	private final String numeroCadastroProduto;

	@JsonProperty("placa")
	private final String placa;

	@JsonProperty("padrao")
	private final Long padrao;

	@JsonProperty("tipo")
	private final Long tipo;

	@JsonProperty("pesoBruto")
	private final BigDecimal pesoBruto;

	@JsonProperty("pesoLiquido")
	private final BigDecimal pesoLiquido;

	@JsonProperty("percentualImpureza")
	private final BigDecimal percentualImpureza;

	@JsonProperty("percentualQuebrado")
	private final BigDecimal percentualQuebrado;

	@JsonProperty("percentualArdido")
	private final BigDecimal percentualArdido;

	@JsonProperty("percentualUmidade")
	private final BigDecimal percentualUmidade;

	private EntregasProducaoProdutoItemJson(EntregaProducao entregaProducao) {
		this.entrega = entregaProducao.getDtEntrega();
		this.local = entregaProducao.getDsLocal();
		this.romaneio = entregaProducao.getNrRomaneio();
		this.sequencia = entregaProducao.getNrSequencia();
		this.numeroCadastroProduto = entregaProducao.getNrCadPro();
		this.placa = entregaProducao.getPlaca();
		this.padrao = entregaProducao.getNrPadrao();
		this.tipo = entregaProducao.getNrTipo();
		this.pesoBruto = truncZero(entregaProducao.getPesoBruto());
		this.pesoLiquido = truncZero(entregaProducao.getPesoLiquido());
		this.percentualImpureza = roundTwo(entregaProducao.getPcImpureza());
		this.percentualQuebrado = roundTwo(entregaProducao.getPcQuebrado());
		this.percentualArdido = roundTwo(entregaProducao.getPcArdido());
		this.percentualUmidade = roundTwo(entregaProducao.getPcUmidade());

	}

	protected static EntregasProducaoProdutoItemJson of(EntregaProducao entregaProducao) {
		checkNotNull(entregaProducao, "Entrega de produção deve ser informada para gerar o JSON.");
		return new EntregasProducaoProdutoItemJson(entregaProducao);
	}

	protected BigDecimal getPesoBruto() {
		return pesoBruto;
	}

	protected BigDecimal getPesoLiquido() {
		return pesoLiquido;
	}
}
