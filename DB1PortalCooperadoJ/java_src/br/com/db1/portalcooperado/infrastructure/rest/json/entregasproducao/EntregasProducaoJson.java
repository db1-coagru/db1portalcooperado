package br.com.db1.portalcooperado.infrastructure.rest.json.entregasproducao;

import br.com.db1.portalcooperado.model.entity.EntregaProducao;
import br.com.db1.portalcooperado.model.entity.EntregasProducao;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

import static jersey.repackaged.com.google.common.base.MoreObjects.firstNonNull;

@JsonRootName("entregasProducao")
public class EntregasProducaoJson {

	@JsonProperty("itens")
	private final List<EntregasProducaoProdutoJson> itens;

	private EntregasProducaoJson(Builder builder) {
		this.itens = builder.buildItensJson();
	}

	public static Builder builder(EntregasProducao entregas) {
		return new Builder(entregas.toList());
	}

	public static class Builder {

		private final List<EntregaProducao> entregas;

		private final Map<String, EntregasProducaoProdutoJson.Builder> itemByProduto = Maps.newLinkedHashMap();

		private Builder(List<EntregaProducao> entregas) {
			this.entregas = entregas;
		}

		public EntregasProducaoJson build() {
			return new EntregasProducaoJson(this);
		}

		private List<EntregasProducaoProdutoJson> buildItensJson() {
			montaBuilders();
			ImmutableList.Builder<EntregasProducaoProdutoJson> builder = ImmutableList.builder();
			for (EntregasProducaoProdutoJson.Builder itemBuilder : itemByProduto.values()) {
				builder.add(itemBuilder.build());
			}
			return builder.build();
		}

		private void montaBuilders() {
			for (EntregaProducao entrega : entregas) {
				String produto = entrega.getDsProduto();
				EntregasProducaoProdutoJson.Builder builder = findOrCreate(produto);
				builder.add(EntregasProducaoProdutoItemJson.of(entrega));
				itemByProduto.put(produto, builder);
			}
		}

		private EntregasProducaoProdutoJson.Builder findOrCreate(String produto) {
			return firstNonNull(itemByProduto.get(produto),
					EntregasProducaoProdutoJson.builder(produto));
		}

	}
}
