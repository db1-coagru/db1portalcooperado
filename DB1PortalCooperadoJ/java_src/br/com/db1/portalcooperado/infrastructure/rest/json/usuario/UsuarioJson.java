package br.com.db1.portalcooperado.infrastructure.rest.json.usuario;

import br.com.db1.portalcooperado.model.entity.Indicacao;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.perfil.PerfilJson;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.collect.ImmutableList;

import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@JsonRootName(value = "usuario")
public class UsuarioJson {

	@JsonProperty("id")
	private final Long id;

	@JsonProperty("matricula")
	private final String matricula;

	@JsonProperty("nome")
	private final String nome;

	@JsonProperty("perfil")
	private final PerfilJson perfil;

	@JsonProperty("token")
	private final TokenJson tokenJson;

	@JsonProperty("indicacoes")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private final List<IndicacaoJson> indicacoes;

	private UsuarioJson(Builder builder) {
		this.id = builder.getId();
		this.matricula = builder.getMatricula();
		this.nome = builder.getNome();
		this.perfil = builder.createPerfilJson();
		this.tokenJson = builder.getTokenJson();
		this.indicacoes = builder.createIndicacoesJson();
	}

	public static UsuarioJson of(Usuario usuario, TokenJson token) {
		checkNotNull(usuario, "Usuário inexistente no banco de dados.");
		checkNotNull(token, "Token inexistente no banco de dados.");
		Builder builder = new Builder(usuario, token);
		return builder.build();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof UsuarioJson) {
			UsuarioJson other = (UsuarioJson) obj;
			return PortalCooperadoUtil.areEquals(this.matricula, other.matricula);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(matricula);
	}

	public String getMatricula() {
		return matricula;
	}

	public String getNome() {
		return nome;
	}

	public PerfilJson getPerfil() {
		return perfil;
	}

	public TokenJson getTokenJson() {
		return tokenJson;
	}

	public List<IndicacaoJson> getIndicacoes() {
		return indicacoes;
	}

	private static class Builder {

		private final Usuario usuario;

		private final TokenJson tokenJson;

		private Builder(Usuario usuario, TokenJson tokenJson) {
			this.usuario = usuario;
			this.tokenJson = tokenJson;
		}

		private UsuarioJson build() {
			return new UsuarioJson(this);
		}

		private PerfilJson createPerfilJson() {
			return PerfilJson.of(usuario.getProfile());
		}

		public List<IndicacaoJson> createIndicacoesJson() {
			ImmutableList.Builder<IndicacaoJson> builder = ImmutableList.builder();
			for (Indicacao indicacao : usuario.getIndicacoesIndicado()) {
				IndicacaoJson indicacaoJson = IndicacaoJson.of(indicacao);
				builder.add(indicacaoJson);
			}
			return builder.build();
		}

		public Long getId() {
			return usuario.getId();
		}

		public String getMatricula() {
			return usuario.getMatricula().toString();
		}

		public String getNome() {
			return usuario.getNome();
		}

		public TokenJson getTokenJson() {
			return tokenJson;
		}

	}
}
