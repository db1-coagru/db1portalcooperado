package br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura;

import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJson;
import br.com.db1.portalcooperado.infrastructure.rest.translators.ListToListTranslator;
import br.com.db1.portalcooperado.infrastructure.rest.translators.Translator;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AviculturaItensToAviculturaItensJsonTranslator
		implements Translator<List<AviculturaItem>, List<AviculturaItemJson>> {

	private final AviculturaItemToAviculturaItemJsonTranslator aviculturaItemToAviculturaItemJsonTranslator;

	@Autowired
	public AviculturaItensToAviculturaItensJsonTranslator(
			AviculturaItemToAviculturaItemJsonTranslator aviculturaItemToAviculturaItemJsonTranslator) {
		this.aviculturaItemToAviculturaItemJsonTranslator = aviculturaItemToAviculturaItemJsonTranslator;
	}

	@Override
	public List<AviculturaItemJson> translate(List<AviculturaItem> from) {
		return ListToListTranslator.translate(from, aviculturaItemToAviculturaItemJsonTranslator);
	}

}
