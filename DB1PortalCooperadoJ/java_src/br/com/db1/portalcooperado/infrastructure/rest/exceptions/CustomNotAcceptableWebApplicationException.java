package br.com.db1.portalcooperado.infrastructure.rest.exceptions;

import br.com.db1.exception.DB1Exception;
import br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson;
import br.com.db1.portalcooperado.infrastructure.rest.responses.Responses;

import javax.ws.rs.WebApplicationException;

/**
 * Created by rafael.rasso on 03/08/2016.
 * Exeção disparada quando determinada requisição REST não é aceitável.
 */
public class CustomNotAcceptableWebApplicationException extends WebApplicationException {

	private static final long serialVersionUID = 7563236665800945849L;

	/**
	 * Construtor de CustomNotAcceptableException a partir de uma DB1Exception.
	 * Além de servir para disparar a exeção ele ja captura a mensagem da exceção e gera o JSON de retorno.
	 *
	 * @param exception
	 */
	public CustomNotAcceptableWebApplicationException(DB1Exception exception) {
		super(Responses.notAcceptable(MensagemJson.of(exception.getMessage())));
	}

}
