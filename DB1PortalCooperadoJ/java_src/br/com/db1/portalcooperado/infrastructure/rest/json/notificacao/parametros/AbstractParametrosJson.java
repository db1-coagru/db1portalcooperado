package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros;

import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("parametros")
public abstract class AbstractParametrosJson implements ParametrosJson {

    @JsonProperty("tipo")
    private final TipoNotificacao tipoNotificacao;

    protected AbstractParametrosJson(TipoNotificacao tipoNotificacao) {
        this.tipoNotificacao = tipoNotificacao;
    }

    public abstract static class AbstractParametrosJsonBuilder {

        protected TipoNotificacao tipoNotificacao;

        protected AbstractParametrosJsonBuilder() {

        }

        protected AbstractParametrosJsonBuilder withTipo(TipoNotificacao tipoNotificacao) {
            this.tipoNotificacao = tipoNotificacao;
            return this;
        }

        public TipoNotificacao tipoNotificacao() {
            return tipoNotificacao;
        }


    }

}
