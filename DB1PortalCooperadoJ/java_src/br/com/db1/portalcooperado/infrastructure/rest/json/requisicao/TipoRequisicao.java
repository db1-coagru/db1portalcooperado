package br.com.db1.portalcooperado.infrastructure.rest.json.requisicao;

import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import jersey.repackaged.com.google.common.collect.ImmutableMap;

import static br.com.db1.portalcooperado.model.types.PermissaoIndicado.*;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;

public enum TipoRequisicao {

	POSICAO_FINANCEIRA(1L) {
		@Override
		public PermissaoIndicado getPermissao() {
			return ACTPOSICAOFINANCEIRA;
		}

	},

	ENTREGAS_PRODUCAO(2L) {
		@Override
		public PermissaoIndicado getPermissao() {
			return ACTENTREGAPRODUCAO;
		}
	},

	RECOMENDACAO_TECNICA(3L) {
		@Override
		public PermissaoIndicado getPermissao() {
			return ACTRECOMENDACAOTECNICA;
		}
	},
	AVICULTURA(4L) {
		@Override
		public PermissaoIndicado getPermissao() {
			return ACTAVICULTURA;
		}
	};

	private static ImmutableMap<Long, TipoRequisicao> valueMap;

	static {
		ImmutableMap.Builder<Long, TipoRequisicao> builder = ImmutableMap.builder();
		for (TipoRequisicao json : values()) {
			builder.put(json.longValue, json);
		}
		valueMap = builder.build();
	}

	private final Long longValue;

	TipoRequisicao(Long longValue) {
		this.longValue = longValue;
	}

	@JsonCreator
	public static TipoRequisicao fromLongValue(Long longValue) {
		checkArgument(valueMap.containsKey(longValue), "Tipo de requisição não suportado.");
		return valueMap.get(longValue);
	}

	public abstract PermissaoIndicado getPermissao();

	@JsonValue
	public Long toLongValue() {
		return longValue;
	}

	public void hasPermissaoThrowsExceptionIfFalse(Long matriculaIndicador, Usuario indicado) {
		indicado.hasPermissaoThrowsExceptionIfFalse(matriculaIndicador, getPermissao());
	}

	public boolean isPosicaoFinanceira() {
		return POSICAO_FINANCEIRA.equals(this);
	}
}
