package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao;

import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.ParametrosJson;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioFeminino;
import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioMasculino;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@JsonRootName("notificacao")
public class NotificacaoJson {

    @JsonProperty("titulo")
    private String titulo;

    @JsonProperty("mensagem")
    private String mensagem;

    @JsonProperty("matriculas")
    private List<MatriculaJson> matriculas;

    @JsonProperty("parametros")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ParametrosJson parametros;

    public NotificacaoJson(Builder builder) {
        this.titulo = builder.titulo;
        this.mensagem = builder.mensagem;
        this.matriculas = builder.matriculas.values();
        this.parametros = builder.parametros;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String titulo;

        private String mensagem;

        private MatriculasEnviarNotificacao matriculas;

        private ParametrosJson parametros;


        public Builder withTitulo(String titulo) {
            this.titulo = titulo;
            return this;
        }

        public Builder withMensagem(String mensagem) {
            this.mensagem = mensagem;
            return this;
        }

        public Builder withMatriculas(MatriculasEnviarNotificacao matriculas) {
            this.matriculas = matriculas;
            return this;
        }

        public Builder withParametros(
                ParametrosJson parametros) {
            this.parametros = parametros;
            return this;
        }

        public NotificacaoJson build() {
            checkNotNull(titulo, campoObrigatorioMasculino("Título"));
            checkNotNull(mensagem, campoObrigatorioFeminino("Mensagem"));
            checkArgument(matriculas != null && !matriculas.isEmpty(), campoObrigatorioFeminino("Matriculas"));
            return new NotificacaoJson(this);
        }

    }
}
