package br.com.db1.portalcooperado.infrastructure.rest.json;

import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@JsonRootName("mensagem")
public class MensagemJson {

	public static final String MENSAGEM_JSON_NAO_LOCALIZADO = "Requisição inválida. Objeto JSON não localizado.";

	private static final String MENSAGEM_EXCECAO = "Ocorreu uma exceção inesperada. Contate o suporte.";

	private static final String UNAUTHORIZED_MOBILE = "Usuário sem permissão de acesso Mobile.";

	@JsonProperty("descricao")
	private final String descricao;

	private MensagemJson(String descricao) {
		this.descricao = descricao;
	}

	public static MensagemJson of(String descricao) {
		checkNotNull(descricao, "Descrição da mensagem deve ser informada.");
		return new MensagemJson(descricao);
	}

	public static MensagemJson newMensagemJsonNaoLocalizado() {
		return new MensagemJson(MENSAGEM_JSON_NAO_LOCALIZADO);
	}

	public static MensagemJson newMensagemErroInesperado() {
		return new MensagemJson(MENSAGEM_EXCECAO);
	}

	public static MensagemJson newMensagemSemAcessoMobile() {
		return new MensagemJson(UNAUTHORIZED_MOBILE);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MensagemJson) {
			MensagemJson other = (MensagemJson) obj;
			return PortalCooperadoUtil.areEquals(this.descricao, other.descricao);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(descricao);
	}

	@Override
	public String toString() {
		return descricao;
	}
	
}
