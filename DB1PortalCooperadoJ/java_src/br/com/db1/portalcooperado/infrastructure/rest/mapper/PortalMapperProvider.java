package br.com.db1.portalcooperado.infrastructure.rest.mapper;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * Created by DB1 Inform�tica on 01/08/2016.
 * <p>
 * Provedor de conversao utilizado pelo jersey para converter as mensagens JSON.
 */
@Provider
public class PortalMapperProvider implements ContextResolver<ObjectMapper> {

	private final ObjectMapper objectMapper;

	/**
	 * Construtor do provedor de mapeamento do JERSEY
	 */
	public PortalMapperProvider() {
		objectMapper = createPortalCooperadoObjectMapper();
	}

	@Override
	public ObjectMapper getContext(final Class<?> type) {
		return objectMapper;
	}

	private ObjectMapper createPortalCooperadoObjectMapper() {
		return new ObjectMapper()
				.configure(SerializationFeature.WRAP_ROOT_VALUE, true)
				.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true)
				.setAnnotationIntrospector(createJaxbJacksonAnnotationIntrospector());
	}

	private static AnnotationIntrospector createJaxbJacksonAnnotationIntrospector() {
		final AnnotationIntrospector jaxbIntrospector = new JaxbAnnotationIntrospector(TypeFactory.defaultInstance());
		final AnnotationIntrospector jacksonIntrospector = new JacksonAnnotationIntrospector();
		return AnnotationIntrospector.pair(jacksonIntrospector, jaxbIntrospector);
	}

}