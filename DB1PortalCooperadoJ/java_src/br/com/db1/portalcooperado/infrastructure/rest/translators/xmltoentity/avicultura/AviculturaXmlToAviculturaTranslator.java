package br.com.db1.portalcooperado.infrastructure.rest.translators.xmltoentity.avicultura;

import br.com.db1.portalcooperado.dao.AviculturaDao;
import br.com.db1.portalcooperado.dao.UsuarioDao;
import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXml;
import br.com.db1.portalcooperado.infrastructure.rest.translators.Translator;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import jersey.repackaged.com.google.common.base.MoreObjects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AviculturaXmlToAviculturaTranslator implements Translator<AviculturaXml, Avicultura> {

	private final UsuarioDao usuarioDao;

	private final AviculturaDao aviculturaDao;

	private final AviculturaItensXmlToAviculturaItemTranslator aviculturaItensXmlToAviculturaItemTranslator;

	@Autowired
	public AviculturaXmlToAviculturaTranslator(UsuarioDao usuarioDao,
			AviculturaDao aviculturaDao,
			AviculturaItensXmlToAviculturaItemTranslator aviculturaItensXmlToAviculturaItemTranslator) {
		this.usuarioDao = usuarioDao;
		this.aviculturaDao = aviculturaDao;
		this.aviculturaItensXmlToAviculturaItemTranslator = aviculturaItensXmlToAviculturaItemTranslator;
	}

	@Override
	public Avicultura translate(AviculturaXml from) {
		Avicultura avicultura = findOneOrCreateOf(from);
		avicultura.setProdutor(usuarioDao.buscaUsuarioByCpfCnpj(from.getCpfCnpj()));
		avicultura.setSexo(from.getSexo());
		avicultura.setQuantidade(from.getQuantidade());
		avicultura.setNomeTecnico(from.getNomeTecnico());
		avicultura.setLote(from.getLote());
		avicultura.setLinhagem(from.getLinhagem());
		avicultura.setGalpao(from.getGalpao());
		avicultura.setData(from.getData());
		avicultura.addDifferentItensKeepingOld(aviculturaItensXmlToAviculturaItemTranslator.translate(from.getItens()));
		return avicultura;
	}

	private Avicultura findOneOrCreateOf(AviculturaXml from) {
		return MoreObjects.firstNonNull(aviculturaDao.findOneByAviculturaXml(from), new Avicultura());
	}

}
