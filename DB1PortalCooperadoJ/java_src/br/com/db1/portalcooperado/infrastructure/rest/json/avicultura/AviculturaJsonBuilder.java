package br.com.db1.portalcooperado.infrastructure.rest.json.avicultura;

import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaResposta;
import jersey.repackaged.com.google.common.collect.Lists;

import java.util.Date;
import java.util.List;

public class AviculturaJsonBuilder {

	private String galpao;

	private String lote;

	private String sexo;

	private Integer quantidade;

	private Date data;

	private String linhagem;

	private String nomeTecnico;

	private Boolean possuiNaoConformidade = Boolean.FALSE;

	private List<AviculturaItemJson> itens = Lists.newLinkedList();

	private AviculturaJsonBuilder() {

	}

	static AviculturaJsonBuilder of() {
		return new AviculturaJsonBuilder();
	}

	public AviculturaJsonBuilder withGalpao(String galpao) {
		this.galpao = galpao;
		return this;
	}

	public AviculturaJsonBuilder withLote(String lote) {
		this.lote = lote;
		return this;
	}

	public AviculturaJsonBuilder withSexo(String sexo) {
		this.sexo = sexo;
		return this;
	}

	public AviculturaJsonBuilder withQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
		return this;
	}

	public AviculturaJsonBuilder withData(Date data) {
		this.data = data;
		return this;
	}

	public AviculturaJsonBuilder withLinhagem(String linhagem) {
		this.linhagem = linhagem;
		return this;
	}

	public AviculturaJsonBuilder withNomeTecnico(String nomeTecnico) {
		this.nomeTecnico = nomeTecnico;
		return this;
	}

	public AviculturaJsonBuilder withItens(List<AviculturaItemJson> itens) {
		this.itens = itens;
		return this;
	}

	public AviculturaJson build() {
		calcularSePossuiNaoConformidade();
		return new AviculturaJson(this);
	}

	private void calcularSePossuiNaoConformidade() {
		for (AviculturaItemJson itemJson : itens) {
			if (itemJson.repostaIs(AviculturaResposta.NAO_CONFORMIDADE)) {
				this.possuiNaoConformidade = Boolean.TRUE;
				break;
			}
		}
	}

	String getGalpao() {
		return galpao;
	}

	String getLote() {
		return lote;
	}

	String getSexo() {
		return sexo;
	}

	Integer getQuantidade() {
		return quantidade;
	}

	Date getData() {
		return data;
	}

	String getLinhagem() {
		return linhagem;
	}

	String getNomeTecnico() {
		return nomeTecnico;
	}

	List<AviculturaItemJson> getItens() {
		return itens;
	}

	Boolean getPossuiNaoConformidade() {
		return possuiNaoConformidade;
	}
}
