package br.com.db1.portalcooperado.infrastructure.rest.json.avicultura;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.base.MoreObjects;
import jersey.repackaged.com.google.common.base.Objects;

import java.util.List;

@JsonRootName("aviculturas")
public class AviculturasJson {

	@JsonProperty(value = "itens")
	private final List<AviculturaJson> itens;

	private AviculturasJson(List<AviculturaJson> itens) {
		this.itens = itens;
	}

	public static AviculturasJson of(List<AviculturaJson> itens) {
		return new AviculturasJson(itens);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof AviculturasJson) {
			AviculturasJson that = (AviculturasJson) o;
			return Objects.equal(itens, that.itens);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(itens);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("itens", itens)
				.toString();
	}

}
