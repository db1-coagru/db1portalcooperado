package br.com.db1.portalcooperado.infrastructure.rest.translators;

import jersey.repackaged.com.google.common.collect.Lists;

import java.util.List;

public class ListToListTranslator {

	private ListToListTranslator() {

	}

	public static <F, T> List<T> translate(List<F> from, Translator<F, T> itemTranslator) {
		List<T> builder = Lists.newArrayList();
		for (F item : from) {
			T translated = itemTranslator.translate(item);
			builder.add(translated);
		}
		return builder;
	}

}
