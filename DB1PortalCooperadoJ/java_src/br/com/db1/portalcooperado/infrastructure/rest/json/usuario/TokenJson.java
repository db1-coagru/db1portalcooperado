package br.com.db1.portalcooperado.infrastructure.rest.json.usuario;

import br.com.db1.portalcooperado.model.entity.Token;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@JsonRootName("portalToken")
public class TokenJson {

	@JsonProperty("acessToken")
	private final String accessToken;

	@JsonProperty("geracao")
	private final Long geracao;

	@JsonProperty("expiracao")
	private final Long expiracao;

	private TokenJson(String accessToken, Long geracao, Long expiracao) {
		this.accessToken = accessToken;
		this.geracao = geracao;
		this.expiracao = expiracao;
	}

	public static TokenJson of(Token token) {
		checkNotNull(token, "Token deve ser informado para geraração do JSON.");
		return new TokenJson(token.getAccessToken(), token.getGeracaoMillis(), token.getExpiracaoMillis());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TokenJson) {
			TokenJson other = (TokenJson) obj;
			return PortalCooperadoUtil.areEquals(this.accessToken, other.accessToken);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(accessToken);
	}

	public String getAccessToken() {
		return accessToken;
	}

	public Long getGeracao() {
		return geracao;
	}

	public Long getExpiracao() {
		return expiracao;
	}
}
