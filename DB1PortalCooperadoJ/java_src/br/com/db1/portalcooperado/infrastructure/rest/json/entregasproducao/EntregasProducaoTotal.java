package br.com.db1.portalcooperado.infrastructure.rest.json.entregasproducao;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.math.BigDecimal;

@JsonRootName("total")
public class EntregasProducaoTotal {

	@JsonProperty("pesoBruto")
	private BigDecimal pesoBruto = BigDecimal.ZERO;

	@JsonProperty("pesoLiquido")
	private BigDecimal pesoLiquido = BigDecimal.ZERO;

	protected static EntregasProducaoTotal zero() {
		return new EntregasProducaoTotal();
	}

	protected void add(EntregasProducaoProdutoItemJson item) {
		pesoBruto = pesoBruto.add(item.getPesoBruto());
		pesoLiquido = pesoLiquido.add(item.getPesoLiquido());
	}
}
