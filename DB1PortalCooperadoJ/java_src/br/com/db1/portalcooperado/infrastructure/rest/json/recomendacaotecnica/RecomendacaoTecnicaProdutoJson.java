package br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica;

import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaProduto;
import br.com.db1.portalcooperado.util.Mensagem;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "recomendeacaoTecnicaProduto")
public class RecomendacaoTecnicaProdutoJson {

	@JsonProperty(value = "ordem")
	private Long ordem;

	@JsonProperty(value = "codigo")
	private Long codigo;

	@JsonProperty(value = "nome")
	private String nome;

	@JsonProperty(value = "dose")
	private String dose;

	@JsonProperty(value = "total")
	private String total;

	@JsonProperty(value = "bomba")
	private String bomba;

	public RecomendacaoTecnicaProdutoJson(RecomendacaoTecnicaProduto produto) {
		this.ordem = produto.getOrdem();
		this.codigo = produto.getCodigo();
		this.nome = produto.getNome();
		String unidadeMedida = produto.getUnidadeMedida();
		this.dose = String.format("%s (%s/Alq)", produto.getQuantidade(), unidadeMedida);
		this.total = String.format("%s %s", produto.getTotal(), unidadeMedida);
		this.bomba = String.format("%s %s", produto.getBomba(), unidadeMedida);
	}

	public static RecomendacaoTecnicaProdutoJson of(RecomendacaoTecnicaProduto produto) {
		PortalCooperadoUtil.checkNotNull(produto, Mensagem.campoObrigatorioMasculino("Produto da recomendação"));
		return new RecomendacaoTecnicaProdutoJson(produto);
	}
}
