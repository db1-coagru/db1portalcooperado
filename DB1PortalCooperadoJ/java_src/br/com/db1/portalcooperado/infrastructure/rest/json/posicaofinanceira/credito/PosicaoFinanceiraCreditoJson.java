package br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira.credito;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.roundTwo;

@JsonRootName("credito")
public class PosicaoFinanceiraCreditoJson {

	@JsonProperty("produto")
	private final String produto;

	@JsonProperty("itens")
	private final List<PosicaoFinanceiraCreditoItemJson> itens;

	@JsonProperty("totalPesoLiquido")
	private final BigDecimal totalPesoLiquido;

	@JsonProperty("totalValorLiquido")
	private final BigDecimal totalValorLiquido;

	private PosicaoFinanceiraCreditoJson(Builder builder) {
		this.produto = builder.produto();
		this.itens = builder.createItens();
		this.totalPesoLiquido = builder.totalPesoLiquido();
		this.totalValorLiquido = builder.totalValorLiquido();
	}

	public static Builder builder(String produto) {
		checkNotNull(produto, "Produto deve ser informado para geração do JSON.");
		return new Builder(produto);
	}

	public String getProduto() {
		return produto;
	}

	public static class Builder {

		private final String produto;

		private final List<PosicaoFinanceiraCreditoItemJson> itens = Lists.newLinkedList();

		private BigDecimal totalPesoLiquido = BigDecimal.ZERO;

		private BigDecimal totalValorLiquido = BigDecimal.ZERO;

		private Builder(String produto) {
			this.produto = produto;
		}

		public void add(PosicaoFinanceiraCreditoItemJson item) {
			itens.add(item);
			totalPesoLiquido = totalPesoLiquido.add(item.getPesoLiquido());
			totalValorLiquido = totalValorLiquido.add(item.getValorLiquido());
		}

		public PosicaoFinanceiraCreditoJson build() {
			return new PosicaoFinanceiraCreditoJson(this);
		}

		private List<PosicaoFinanceiraCreditoItemJson> createItens() {
			return ImmutableList.copyOf(itens);
		}

		private String produto() {
			return produto;
		}

		private BigDecimal totalPesoLiquido() {
			return roundTwo(totalPesoLiquido);
		}

		private BigDecimal totalValorLiquido() {
			return roundTwo(totalValorLiquido);
		}
	}

}
