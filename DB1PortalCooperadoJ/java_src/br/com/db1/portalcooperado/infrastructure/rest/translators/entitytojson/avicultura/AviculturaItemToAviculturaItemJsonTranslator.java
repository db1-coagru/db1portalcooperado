package br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura;

import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItem;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJson;
import br.com.db1.portalcooperado.infrastructure.rest.translators.Translator;
import org.springframework.stereotype.Service;

@Service
public class AviculturaItemToAviculturaItemJsonTranslator implements Translator<AviculturaItem, AviculturaItemJson> {

	@Override
	public AviculturaItemJson translate(AviculturaItem from) {
		return AviculturaItemJson.builder()
				.withDescricao(from.getDescricao())
				.withResposta(from.getResposta())
				.withObservacao(from.getObservacao())
				.withImagens(from.getNomesImagens())
				.build();
	}

}
