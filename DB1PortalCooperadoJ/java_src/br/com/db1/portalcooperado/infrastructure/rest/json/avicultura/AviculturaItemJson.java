package br.com.db1.portalcooperado.infrastructure.rest.json.avicultura;

import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaResposta;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.base.MoreObjects;
import jersey.repackaged.com.google.common.base.Objects;

import java.util.List;

@JsonRootName(value = "aviculturaItem")
public class AviculturaItemJson {

	@JsonProperty(value = "descricao")
	private final String descricao;

	@JsonProperty(value = "resposta")
	private final AviculturaResposta resposta;

	@JsonProperty(value = "observacao")
	private final String observacao;

	@JsonProperty(value = "imagens")
	private final List<String> imagens;

	AviculturaItemJson(AviculturaItemJsonBuilder builder) {
		this.descricao = builder.getDescricao();
		this.resposta = builder.getResposta();
		this.observacao = builder.getObservacao();
		this.imagens = builder.getImagens();
	}

	public static AviculturaItemJsonBuilder builder() {
		return AviculturaItemJsonBuilder.of();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof AviculturaItemJson) {
			AviculturaItemJson that = (AviculturaItemJson) o;
			return Objects.equal(descricao, that.descricao) &&
					Objects.equal(resposta, that.resposta) &&
					Objects.equal(observacao, that.observacao);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(descricao, resposta, observacao);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("descricao", descricao)
				.add("resposta", resposta)
				.add("observacao", observacao)
				.toString();
	}

	Boolean repostaIs(AviculturaResposta resposta) {
		return this.resposta.equals(resposta);
	}

	public String getDescricao() {
		return descricao;
	}

	public AviculturaResposta getResposta() {
		return resposta;
	}

	public String getObservacao() {
		return observacao;
	}

	public List<String> getImagens() {
		return imagens;
	}
}
