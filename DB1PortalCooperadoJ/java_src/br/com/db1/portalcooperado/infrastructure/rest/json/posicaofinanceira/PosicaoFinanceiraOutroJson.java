package br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira;

import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraOutros;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.math.BigDecimal;
import java.util.Date;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.roundTwo;

@JsonRootName("outro")
public class PosicaoFinanceiraOutroJson {

	@JsonProperty("tipo")
	private final String tipo;

	@JsonProperty("produto")
	private final String produto;

	@JsonProperty("valorQuantidade")
	private final BigDecimal valorQuantidade;

	@JsonProperty("valorProduto")
	private final BigDecimal valorProduto;

	@JsonProperty("vencimento")
	private final Date vencimento;

	private PosicaoFinanceiraOutroJson(PosicaoFinanceiraOutros posicaoFinanceiraOutros) {
		this.tipo = posicaoFinanceiraOutros.getDsTipo();
		this.produto = posicaoFinanceiraOutros.getDsProduto();
		this.valorQuantidade = roundTwo(posicaoFinanceiraOutros.getVlQuantidade());
		this.valorProduto = roundTwo(posicaoFinanceiraOutros.getVlProduto());
		this.vencimento = posicaoFinanceiraOutros.getDtVencimento();
	}

	public static PosicaoFinanceiraOutroJson of(PosicaoFinanceiraOutros posicaoFinanceiraOutros) {
		return new PosicaoFinanceiraOutroJson(posicaoFinanceiraOutros);
	}

}

