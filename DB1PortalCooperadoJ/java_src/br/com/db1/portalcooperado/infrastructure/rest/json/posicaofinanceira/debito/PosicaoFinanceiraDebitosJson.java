package br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira.debito;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.roundTwo;

@JsonRootName("debitos")
public class PosicaoFinanceiraDebitosJson {

	@JsonProperty("itens")
	private final List<PosicaoFinanceiraDebitoItemJson> itens;

	@JsonProperty("totalValor")
	private final BigDecimal totalValor;

	@JsonProperty("totalJuros")
	private final BigDecimal totalJuros;

	@JsonProperty("totalValorLiquido")
	private final BigDecimal totalValorLiquido;

	@JsonProperty("montanteVencido")
	private final BigDecimal montanteVencido;

	@JsonProperty("montanteVencer")
	private final BigDecimal montanteVencer;

	private PosicaoFinanceiraDebitosJson(Builder builder) {
		this.itens = builder.itens();
		this.totalValor = builder.totalValor();
		this.totalJuros = builder.totalJuros();
		this.totalValorLiquido = builder.totalValorLiquido();
		this.montanteVencido = builder.montanteVencido();
		this.montanteVencer = builder.montanteVencer();
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final List<PosicaoFinanceiraDebitoItemJson> itens = Lists.newLinkedList();

		private BigDecimal totalValor = BigDecimal.ZERO;

		private BigDecimal totalJuros = BigDecimal.ZERO;

		private BigDecimal totalValorLiquido = BigDecimal.ZERO;

		private BigDecimal montanteVencido = BigDecimal.ZERO;

		private BigDecimal montanteVencer = BigDecimal.ZERO;

		private Builder() {

		}

		public void add(PosicaoFinanceiraDebitoItemJson item) {
			itens.add(item);
			totalValor = totalValor.add(item.getValorProduto());
			totalJuros = totalJuros.add(item.getValorJuros());
			totalValorLiquido = totalValorLiquido.add(item.getValorLiquido());
			if (item.isNaoVencido()) {
				montanteVencer = montanteVencer.add(item.getValorLiquido());
			}
			else {
				montanteVencido = montanteVencido.add(item.getValorLiquido());
			}
		}

		public PosicaoFinanceiraDebitosJson build() {
			return new PosicaoFinanceiraDebitosJson(this);
		}

		public List<PosicaoFinanceiraDebitoItemJson> itens() {
			return itens;
		}

		public BigDecimal totalValor() {
			return roundTwo(totalValor);
		}

		public BigDecimal totalJuros() {
			return roundTwo(totalJuros);
		}

		public BigDecimal totalValorLiquido() {
			return roundTwo(totalValorLiquido);
		}

		public BigDecimal montanteVencido() {
			return roundTwo(montanteVencido);
		}

		public BigDecimal montanteVencer() {
			return roundTwo(montanteVencer);
		}

	}

}
