package br.com.db1.portalcooperado.infrastructure.rest.exceptions;

import br.com.db1.portalcooperado.infrastructure.rest.responses.Responses;

import javax.ws.rs.WebApplicationException;

/**
 * Created by rafael.rasso on 03/08/2016.
 * Exeção disparada quando uma requisição REST obteve erro no servidor que não era esperado.
 */
public class CustomServerErrorWebApplicationException extends WebApplicationException {

	private static final long serialVersionUID = -3324876087412940869L;

	/**
	 * Construtor de CustomServerErrorException a partir de uma Exception.
	 *
	 * @param exception
	 */
	public CustomServerErrorWebApplicationException(Exception exception) {
		super(exception, Responses.serverError());
	}
}
