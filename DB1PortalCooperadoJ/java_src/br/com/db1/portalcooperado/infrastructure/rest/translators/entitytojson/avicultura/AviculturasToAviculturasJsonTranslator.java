package br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura;

import br.com.db1.portalcooperado.infrastructure.rest.translators.ListToListTranslator;
import br.com.db1.portalcooperado.infrastructure.rest.translators.Translator;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturasJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AviculturasToAviculturasJsonTranslator implements Translator<List<Avicultura>, AviculturasJson> {

	private final AviculturaToAviculturaJsonTranslator aviculturaToAviculturaJsonTranslator;

	@Autowired
	public AviculturasToAviculturasJsonTranslator(
			AviculturaToAviculturaJsonTranslator aviculturaToAviculturaJsonTranslator) {
		this.aviculturaToAviculturaJsonTranslator = aviculturaToAviculturaJsonTranslator;
	}

	@Override
	public AviculturasJson translate(List<Avicultura> from) {
		List<AviculturaJson> translated = ListToListTranslator.translate(from, aviculturaToAviculturaJsonTranslator);
		return AviculturasJson.of(translated);
	}

}
