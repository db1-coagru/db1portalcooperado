package br.com.db1.portalcooperado.infrastructure.rest.responses;

import br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson;

import javax.ws.rs.core.Response;

/**
 * Classe responsável por centralizar a estrutura de resposta da API REST
 */
public class Responses {

	private Responses() {

	}

	/**
	 * Gera uma resposta com status 200 para o JSON recebido.
	 *
	 * @param json
	 * @return Response
	 */
	public static Response ok(Object json) {
		return Response.ok(json).build();
	}

	/**
	 * Gera uma resposta com status 406 para o JSON recebido.
	 *
	 * @param json
	 * @return Response
	 */
	public static Response notAcceptable(Object json) {
		return build(json, Response.Status.NOT_ACCEPTABLE);
	}

	/**
	 * Gera uma resposta com status 500 para o JSON recebido.
	 *
	 * @param json
	 * @return Response
	 */
	public static Response serverError() {
		return Response.serverError()
				.entity(MensagemJson.newMensagemErroInesperado())
				.build();
	}

	/**
	 * Gera uma resposta com status 401 para o JSON recebido.
	 *
	 * @param json
	 * @return Response
	 */
	public static Response unauthorized(Object json) {
		return build(json, Response.Status.UNAUTHORIZED);
	}

	private static Response build(Object json, Response.Status status) {
		return Response.status(status)
				.entity(json)
				.build();
	}

	public static Response notAcceptable(Exception exception) {
		return notAcceptable(MensagemJson.of(exception.getMessage()));
	}
}
