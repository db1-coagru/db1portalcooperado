package br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira.credito;

import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraCredito;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Maps;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.roundTwo;
import static jersey.repackaged.com.google.common.base.MoreObjects.firstNonNull;

@JsonRootName("creditos")
public class PosicaoFinanceiraCreditosJson {

	@JsonProperty("itens")
	private final List<PosicaoFinanceiraCreditoJson> itens;

	@JsonProperty("totalProducaoFixar")
	private final BigDecimal totalProducaoFixar;

	private PosicaoFinanceiraCreditosJson(Builder builder) {
		this.itens = builder.itens();
		this.totalProducaoFixar = builder.totalProducaoFixar();
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private Map<String, PosicaoFinanceiraCreditoJson.Builder> creditoByProduto = Maps.newLinkedHashMap();

		private BigDecimal totalProducaoFixar = BigDecimal.ZERO;

		private Builder() {

		}

		public void add(PosicaoFinanceiraCredito posicaoFinanceiraCredito) {
			String produto = posicaoFinanceiraCredito.getDsProduto();
			PosicaoFinanceiraCreditoItemJson item = PosicaoFinanceiraCreditoItemJson.of(posicaoFinanceiraCredito);
			PosicaoFinanceiraCreditoJson.Builder builder = findOrCreate(produto);
			builder.add(item);
			creditoByProduto.put(produto, builder);
			totalProducaoFixar = totalProducaoFixar.add(item.getValorLiquido());
		}

		private PosicaoFinanceiraCreditoJson.Builder findOrCreate(String produto) {
			return firstNonNull(creditoByProduto.get(produto),
					PosicaoFinanceiraCreditoJson.builder(produto));
		}

		public PosicaoFinanceiraCreditosJson build() {
			return new PosicaoFinanceiraCreditosJson(this);
		}

		private List<PosicaoFinanceiraCreditoJson> itens() {
			ImmutableList.Builder<PosicaoFinanceiraCreditoJson> builder = ImmutableList.builder();
			for (PosicaoFinanceiraCreditoJson.Builder creditoBuilder : creditoByProduto.values()) {
				builder.add(creditoBuilder.build());
			}
			return builder.build();
		}

		private BigDecimal totalProducaoFixar() {
			return roundTwo(totalProducaoFixar);
		}
	}

}
