package br.com.db1.portalcooperado.infrastructure.rest.translators.xmltoentity.avicultura;

import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaItemXml;
import br.com.db1.portalcooperado.infrastructure.rest.translators.Translator;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.testng.util.Strings;

@Service
public class AviculturaItemXmlToAviculturaTranslator implements Translator<AviculturaItemXml, AviculturaItem> {

	private final AviculturaItemImagemXmlToAviculturaItemImagem aviculturaItemImagemXmlToAviculturaItemImagem;

	@Autowired
	public AviculturaItemXmlToAviculturaTranslator(
			AviculturaItemImagemXmlToAviculturaItemImagem aviculturaItemImagemXmlToAviculturaItemImagem) {
		this.aviculturaItemImagemXmlToAviculturaItemImagem = aviculturaItemImagemXmlToAviculturaItemImagem;
	}

	@Override
	public AviculturaItem translate(AviculturaItemXml from) {
		AviculturaItem item = new AviculturaItem();
		item.setDescricao(from.getDescricao());
		item.setObservacao(from.getObservacao());
		item.setResposta(from.getResposta());
		if (!Strings.isNullOrEmpty(from.getImagem())) {
			item.addImagem(aviculturaItemImagemXmlToAviculturaItemImagem.translate(from.getImagem()));
		}
		return item;
	}

}
