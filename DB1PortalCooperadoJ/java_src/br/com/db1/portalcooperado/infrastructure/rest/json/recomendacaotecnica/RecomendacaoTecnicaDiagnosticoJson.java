package br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica;

import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaDiagnostico;
import br.com.db1.portalcooperado.util.Mensagem;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "recomendeacaoTecnicaDiagnostico")
public class RecomendacaoTecnicaDiagnosticoJson {

	@JsonProperty(value = "descricao")
	private String descricao;

	@JsonProperty(value = "observacao")
	private String observacao;

	public RecomendacaoTecnicaDiagnosticoJson(RecomendacaoTecnicaDiagnostico diagnostico) {
		this.descricao = diagnostico.getDescricao();
		this.observacao = diagnostico.getObservacao();
	}

	public static RecomendacaoTecnicaDiagnosticoJson of(RecomendacaoTecnicaDiagnostico diagnostico) {
		PortalCooperadoUtil.checkNotNull(diagnostico, Mensagem.campoObrigatorioMasculino("Diagnostico da recomendação"));
		return new RecomendacaoTecnicaDiagnosticoJson(diagnostico);
	}
}
