package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao;

import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioFeminino;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@JsonRootName("matricula")
public class MatriculaJson {

	@JsonProperty("matricula")
	private final Long matricula;

	private MatriculaJson(Long matricula) {
		this.matricula = matricula;
	}

	public static MatriculaJson of(Object matricula) {
		checkNotNull(matricula, campoObrigatorioFeminino("Matrícula"));
		return new MatriculaJson(Long.valueOf(matricula.toString()));
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MatriculaJson) {
			MatriculaJson other = (MatriculaJson) obj;
			return PortalCooperadoUtil.areEquals(this.matricula, other.matricula);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(matricula);
	}

	@Override
	public String toString() {
		return matricula.toString();
	}
}
