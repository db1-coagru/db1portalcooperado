package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.avicultura;

import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.AbstractParametrosJson;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioFeminino;
import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioMasculino;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class AviculturaParametrosJson extends AbstractParametrosJson {

	@JsonProperty("idAvicultura")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final Long avicultura;

	private AviculturaParametrosJson(Builder builder) {
		super(builder.tipoNotificacao());
		this.avicultura = builder.avicultura;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder extends AbstractParametrosJsonBuilder {

		private Long avicultura;

		private Builder() {

		}

		@Override
		public Builder withTipo(TipoNotificacao tipoNotificacao) {
			super.withTipo(tipoNotificacao);
			return this;
		}

		public Builder withAvicultura(Long avicultura) {
			this.avicultura = avicultura;
			return this;
		}

		public AviculturaParametrosJson build() {
			checkNotNull(tipoNotificacao, campoObrigatorioMasculino("Tipo de notificação"));
			checkArgument(tipoNotificacao.isAvicultura(), "Tipo de notificação incorreto.");
			checkNotNull(avicultura, campoObrigatorioFeminino("Avicultura"));
			return new AviculturaParametrosJson(this);
		}

	}

}
