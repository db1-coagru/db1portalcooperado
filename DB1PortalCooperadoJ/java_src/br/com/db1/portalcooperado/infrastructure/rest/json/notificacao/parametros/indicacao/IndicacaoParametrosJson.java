package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.indicacao;

import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.AbstractParametrosJson;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioMasculino;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class IndicacaoParametrosJson extends AbstractParametrosJson {

	@JsonProperty("acao")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final IndicacaoAcao acao;

	@JsonProperty("permissoes")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final List<PermissaoIndicado> permissoes;

	@JsonProperty("indicador")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long indicador;

	private IndicacaoParametrosJson(Builder builder) {
		super(builder.tipoNotificacao());
		this.acao = builder.acao;
		this.indicador = builder.indicador;
		this.permissoes = builder.permissoes;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder extends AbstractParametrosJsonBuilder {

		private IndicacaoAcao acao;

		private Long indicador;

		private List<PermissaoIndicado> permissoes;

		private Builder() {
			super();
		}

		@Override
		public Builder withTipo(TipoNotificacao tipoNotificacao) {
			super.withTipo(tipoNotificacao);
			return this;
		}

		public Builder withAcao(IndicacaoAcao acao) {
			this.acao = acao;
			return this;
		}

		public Builder withIndicador(Long indicador) {
			this.indicador = indicador;
			return this;
		}

		public Builder withPermissoes(List<PermissaoIndicado> permissoes) {
			this.permissoes = permissoes;
			return this;
		}

		public IndicacaoParametrosJson build() {
			checkNotNull(tipoNotificacao, campoObrigatorioMasculino("Tipo de notificação"));
			checkArgument(tipoNotificacao.isIndicacao(), "Tipo de notificação incorreto.");
			checkNotNull(acao, campoObrigatorioMasculino("Ação"));
			checkNotNull(indicador, campoObrigatorioMasculino("Indicador"));
			checkNotNull(permissoes, campoObrigatorioMasculino("Permissões do indicado"));
			return new IndicacaoParametrosJson(this);
		}

	}

}
