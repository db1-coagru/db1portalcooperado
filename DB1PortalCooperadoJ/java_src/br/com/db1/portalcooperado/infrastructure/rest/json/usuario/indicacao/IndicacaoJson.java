package br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao;

import br.com.db1.portalcooperado.model.entity.Indicacao;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.collect.ImmutableList;

import java.util.List;

@JsonRootName("indicacao")
public class IndicacaoJson {

	@JsonProperty("indicador")
	private final Long indicador;

	@JsonProperty("matricula")
	private final String matricula;

	@JsonProperty("nome")
	private final String nome;

	@JsonProperty("permissoes")
	private final List<PermissaoIndicado> permissoes;

	private IndicacaoJson(Indicacao indicacao, List<PermissaoIndicado> permissoes) {
		this.indicador = indicacao.getIdIndicador();
		this.matricula = indicacao.getMatriculaIndicador();
		this.nome = indicacao.getNomeIndicador();
		this.permissoes = permissoes;
	}

	public static IndicacaoJson of(Indicacao indicacao) {
		PortalCooperadoUtil.checkNotNull(indicacao, "Indicação deve ser informada.");
		List<PermissaoIndicado> permissoes = ImmutableList.copyOf(indicacao.getPermissoesIndicado());
		return new IndicacaoJson(indicacao, permissoes);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IndicacaoJson) {
			IndicacaoJson other = (IndicacaoJson) obj;
			return PortalCooperadoUtil.areEquals(this.matricula, other.matricula);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(this.matricula);
	}

	public String getMatricula() {
		return matricula;
	}

	public String getNome() {
		return nome;
	}

	public List<PermissaoIndicado> getPermissoes() {
		return permissoes;
	}
}
