package br.com.db1.portalcooperado.infrastructure.rest.translators;

public interface Translator<F, T> {

	T translate(F from);

}
