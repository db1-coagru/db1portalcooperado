package br.com.db1.portalcooperado.infrastructure.rest.translators.xmltoentity.avicultura;

import br.com.db1.portalcooperado.infrastructure.rest.translators.Translator;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItemImagem;
import org.springframework.stereotype.Service;

@Service
public class AviculturaItemImagemXmlToAviculturaItemImagem implements Translator<String, AviculturaItemImagem> {

	@Override
	public AviculturaItemImagem translate(String from) {
		return new AviculturaItemImagem(from);
	}

}
