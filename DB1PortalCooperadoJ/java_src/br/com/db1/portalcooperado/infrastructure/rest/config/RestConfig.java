package br.com.db1.portalcooperado.infrastructure.rest.config;

import br.com.db1.portalcooperado.application.AuthentcationFilter;
import br.com.db1.portalcooperado.application.resources.ImagemResource;
import br.com.db1.portalcooperado.application.resources.LoginResource;
import br.com.db1.portalcooperado.application.resources.RequisicaoResource;
import br.com.db1.portalcooperado.infrastructure.rest.mapper.PortalMapperProvider;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Created by rafael.rasso on 01/08/2016.
 * Configuracao do jersey que sera utilizada no mapeamento do servele no web.xml.
 */
public class RestConfig extends ResourceConfig {

	/**
	 * Construirá a configuração dos recursos e do jacson para uso no jersey.
	 *
	 * @return
	 * @author rafael.rasso
	 * @since 02/08//2016
	 */
	public RestConfig() {
		super(PortalMapperProvider.class,
				LoginResource.class,
				RequisicaoResource.class,
				ImagemResource.class,
				AuthentcationFilter.class,
				JacksonFeature.class);
	}

}
