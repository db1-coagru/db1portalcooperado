package br.com.db1.portalcooperado.infrastructure.rest.json.usuario.perfil;

import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import br.com.db1.security.model.entity.Profile;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@JsonRootName(value = "perfil")
public class PerfilJson {

	@JsonProperty(value = "id")
	private final Long id;

	@JsonProperty(value = "descricao")
	private final String descricao;

	@JsonProperty(value = "permissoes")
	private final List<PermissaoIndicado> permissoes;

	private PerfilJson(Long id, String descricao, List<PermissaoIndicado> permissoes) {
		this.id = id;
		this.descricao = descricao;
		this.permissoes = permissoes;
	}

	public static PerfilJson of(Profile profile) {
		checkNotNull(profile, "Perfil do usuário inexistente no banco de dados.");
		List<PermissaoIndicado> permissoes = PermissaoIndicado.valuesOf(profile.getPrivileges());
		return new PerfilJson(profile.getIdProfile(), profile.getDescription(), permissoes);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PerfilJson) {
			PerfilJson other = (PerfilJson) obj;
			return PortalCooperadoUtil.areEquals(this.descricao, other.descricao);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(descricao);
	}

	public String getDescricao() {
		return descricao;
	}

}
