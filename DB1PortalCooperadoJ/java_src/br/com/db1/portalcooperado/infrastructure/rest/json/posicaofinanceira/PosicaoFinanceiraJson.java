package br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira;

import br.com.db1.portalcooperado.model.entity.PosicaoFinanceira;
import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraCredito;
import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraDebitos;
import br.com.db1.portalcooperado.model.entity.PosicaoFinanceiraOutros;
import br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira.credito.PosicaoFinanceiraCreditosJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira.debito.PosicaoFinanceiraDebitoItemJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.posicaofinanceira.debito.PosicaoFinanceiraDebitosJson;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import jersey.repackaged.com.google.common.collect.ImmutableList;

import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@JsonRootName("posicaoFinanceira")
public class PosicaoFinanceiraJson {

	@JsonProperty("creditos")
	private final PosicaoFinanceiraCreditosJson creditos;

	@JsonProperty("debitos")
	private final PosicaoFinanceiraDebitosJson debitos;

	@JsonProperty("outrosCreditos")
	private final PosicaoFinanceiraDebitosJson outrosCreditos;

	@JsonProperty("outrosCompromissos")
	private final List<PosicaoFinanceiraOutroJson> outros;

	@JsonProperty("mensagem")
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private final PosicaoFinanceiraMensagemJson mensagem;

	private PosicaoFinanceiraJson(Builder builder) {
		this.creditos = builder.buildCreditosJson();
		this.debitos = builder.buildDebitosJson();
		this.outrosCreditos = builder.buildOutrosDebitosJson();
		this.outros = builder.buildOutrosJson();
		this.mensagem = builder.buildMensagemJson();
	}

	public static Builder builder(PosicaoFinanceira posicaoFinanceira) {
		checkNotNull(posicaoFinanceira, "Posição financeira deve ser informada para gerar o JSON.");
		return new Builder(posicaoFinanceira);
	}

	public static class Builder {

		private final PosicaoFinanceira posicaoFinanceira;

		private Builder(PosicaoFinanceira posicaoFinanceira) {
			this.posicaoFinanceira = posicaoFinanceira;
		}

		public PosicaoFinanceiraJson build() {
			return new PosicaoFinanceiraJson(this);
		}

		private PosicaoFinanceiraCreditosJson buildCreditosJson() {
			PosicaoFinanceiraCreditosJson.Builder builder = PosicaoFinanceiraCreditosJson
					.builder();
			for (PosicaoFinanceiraCredito credito : posicaoFinanceira.getCreditos()) {
				builder.add(credito);
			}
			return builder.build();
		}

		private PosicaoFinanceiraDebitosJson buildDebitosJson() {
			PosicaoFinanceiraDebitosJson.Builder builder = PosicaoFinanceiraDebitosJson.builder();
			for (PosicaoFinanceiraDebitos debito : posicaoFinanceira.geDebitos()) {
				builder.add(PosicaoFinanceiraDebitoItemJson.of(debito));
			}
			return builder.build();
		}

		public PosicaoFinanceiraDebitosJson buildOutrosDebitosJson() {
			PosicaoFinanceiraDebitosJson.Builder builder = PosicaoFinanceiraDebitosJson.builder();
			for (PosicaoFinanceiraDebitos debitoCredito : posicaoFinanceira.getDebitosCreditos()) {
				builder.add(PosicaoFinanceiraDebitoItemJson.of(debitoCredito));
			}
			return builder.build();
		}

		private List<PosicaoFinanceiraOutroJson> buildOutrosJson() {
			ImmutableList.Builder<PosicaoFinanceiraOutroJson> builder = ImmutableList.builder();
			for (PosicaoFinanceiraOutros outro : posicaoFinanceira.getOutros()) {
				builder.add(PosicaoFinanceiraOutroJson.of(outro));
			}
			return builder.build();
		}

		public PosicaoFinanceiraMensagemJson buildMensagemJson() {
			if (posicaoFinanceira.hasMensagem()) {
				return PosicaoFinanceiraMensagemJson.of(posicaoFinanceira.getMensagem());
			}
			return null;
		}

	}
}
