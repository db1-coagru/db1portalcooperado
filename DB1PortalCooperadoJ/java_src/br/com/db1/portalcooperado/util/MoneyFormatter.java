package br.com.db1.portalcooperado.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

public enum MoneyFormatter {

	REAIS {
		@Override
		public String format(BigDecimal valor) {
			NumberFormat format = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
			return format.format(valor);
		}
	};

	public abstract String format(BigDecimal valor);

}
