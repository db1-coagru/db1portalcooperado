package br.com.db1.portalcooperado.util;

import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.requisicao.ConectorBaseDados;

public final class RequisicaoWaitReturn {

	private RequisicaoWaitReturn() {

	}

	public static void waitReturn(Requisicao requisicao) {
		Long idRequisicao = requisicao.getId();
		boolean hasRetorno = ConectorBaseDados.getInstancia().existeRetorno(idRequisicao);
		while (!hasRetorno) {
			hasRetorno = ConectorBaseDados.getInstancia().existeRetorno(idRequisicao);
		}
	}

}
