package br.com.db1.portalcooperado.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public final class EnviaEmail {

	private static Session sessao = null;
    private static final String CONTENT_TYPE = "text/html; charset=ISO-8859-1;";
    //private static final String SMTP_AUTH_USER = "EMAIL_LOGIN";
    //private static final String SMTP_AUTH_PWD  = "EMAIL_SENHA";

	/*
	 * Método para enviar email em html
	 */

	public static void enviarEmailHtml(String emailDe,
			String emailPara,
			String emailAssunto,
			String emailMensagem,
			String emailServidor) throws MessagingException {

		//String emailPara = "Sistema Qualidade <naoresponda@agraria.com.br>";
		Properties mailProps = new Properties();
		mailProps.put("mail.smtp.host", emailServidor);
		//mailProps.put("mail.smtp.auth", "true");
		sessao = Session.getDefaultInstance(mailProps, null);
		
		
		//Authenticator auth = new SMTPAuthenticator();
        //sessao = Session.getDefaultInstance(mailProps, auth);
		
		//props.put("mail.smtp.auth", "true");
		//session = Session.getDefaultInstance(props, new Autenticacao());
		//sessao = Session.getDefaultInstance(mailProps, new Autenticacao());
		

		Message mensagem = new MimeMessage(sessao);
		
		// Remetente
		InternetAddress endMailFrom = new InternetAddress(emailDe);
		mensagem.setFrom(endMailFrom);

		// Destinatário
		
		InternetAddress endMailPara = new InternetAddress(emailPara);
		mensagem.addRecipient(Message.RecipientType.TO, endMailPara);

		// Assunto da mensagem
		
		mensagem.setSubject(emailAssunto);

		// Corpo da mensagem
		MimeMultipart mpRoot = new MimeMultipart("mixed");
		MimeMultipart mpContent = new MimeMultipart("alternative");
		MimeBodyPart mbp1 = new MimeBodyPart();
		mbp1.setContent(emailMensagem.toString(), CONTENT_TYPE);
		mpContent.addBodyPart(mbp1);
		MimeBodyPart contentPartRoot = new MimeBodyPart();
		contentPartRoot.setContent(mpContent);
		mpRoot.addBodyPart(contentPartRoot);
		mensagem.setContent(mpRoot);

		// Salva mensagem
		mensagem.saveChanges();

		// Envia email
		System.out.println("Conectado...");
		Transport.send(mensagem);
		System.out.println("Enviado.");
	}
	
//	private static class SMTPAuthenticator extends javax.mail.Authenticator {
//        public PasswordAuthentication getPasswordAuthentication() {
//           String username = SMTP_AUTH_USER;
//           String password = SMTP_AUTH_PWD;
//           return new PasswordAuthentication(username, password);
//        }
//    }
}

