package br.com.db1.portalcooperado.util;

import br.com.db1.exception.DB1Exception;
import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.library.datemanager.DB1DateManager;
import br.com.db1.security.model.entity.User;
import br.com.db1.session.DB1Session;
import jersey.repackaged.com.google.common.base.Objects;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;

import static java.math.BigDecimal.ROUND_HALF_EVEN;

public class PortalCooperadoUtil {

	public static final int SCALE = 2;

	public static final int TRUNC_SCALE = 0;

	private PortalCooperadoUtil() {

	}

	public static Date getDataOperacao() {
		return getDB1DateManager().getOperationDateTime().getTime();
	}

	public static DB1DateManager getDB1DateManager() {
		DB1DateManager instance = (DB1DateManager) DB1Session
				.getCurrentSessionType().getAttribute(
						DB1DateManager.SESSION_DATE_MANAGER);
		if (instance == null) {
			instance = new DB1DateManager();
			DB1Session.getCurrentSessionType().registerInSession(
					DB1DateManager.SESSION_DATE_MANAGER, instance);
		}
		return instance;
	}

	public static String getHoraOperacao() {
		return DateFormatter.HH_MM_SS.format(getDataOperacao());
	}

	public static String getHora() {
		return DateFormatter.HH_MM_SS.format(new Date());
	}

	public static String getIpOrigem() {
		try {
			InetAddress thisIp = InetAddress.getLocalHost();
			return thisIp.getHostAddress();
		}
		catch (UnknownHostException e) {
			throw new DB1Exception(e);
		}

	}

	public static Long timeInMillisOf(Date date) {
		checkNotNull(date, "Data deve ser informada para extração dos milisegundos.");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getTimeInMillis();
	}

	public static <T> T checkNotNull(T reference, Object errorMessage) {
		if (reference != null) {
			return reference;
		}
		throw new DB1ValidateException(String.valueOf(errorMessage));
	}

	public static void checkArgument(boolean expression, Object errorMessage) {
		if (!expression) {
			throw new DB1ValidateException(String.valueOf(errorMessage));
		}
	}

	public static BigDecimal roundTwo(BigDecimal value) {
		return value.setScale(SCALE, ROUND_HALF_EVEN);
	}

	public static BigDecimal truncZero(BigDecimal value) {
		return value.setScale(TRUNC_SCALE, ROUND_HALF_EVEN);
	}

	public static boolean areEquals(Object one, Object two) {
		return Objects.equal(one, two);
	}

	public static int hashCode(Object... objects) {
		return Objects.hashCode(objects);
	}

	public static String getNomeCooperativa() {
		User user = (User) DB1Session.getCurrentSessionType().getAttribute(User.USER);
		return user.getOrganization().getDescription().toUpperCase();
	}

	public static Date getDataOperacaoAtMidnight() {
		Date dataAtual = PortalCooperadoUtil.getDataOperacao();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataAtual);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static Date getDataAtMidnight() {
		Date dataAtual = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataAtual);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static Date getDataAtEndDay() {
		Date dataAtual = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataAtual);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	public static Date getDateSubtractingDays(int amount) {
		Calendar today = Calendar.getInstance();
		today.add(Calendar.DAY_OF_YEAR, amount * -1);
		return today.getTime();
	}

}
