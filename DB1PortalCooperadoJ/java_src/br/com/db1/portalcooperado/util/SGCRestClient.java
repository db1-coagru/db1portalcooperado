package br.com.db1.portalcooperado.util;

import br.com.db1.portalcooperado.requisicao.Propriedades;
import org.glassfish.jersey.client.ClientProperties;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.io.Serializable;

public class SGCRestClient implements Serializable {

    private static final long serialVersionUID = -9051681763084087997L;

    private static final int VALUE_TIMEOUT = 30000;

    private SGCRestClient() {

    }

    public static WebTarget getWebTarget() {
        return ClientBuilder.newClient()
                .property(ClientProperties.CONNECT_TIMEOUT, VALUE_TIMEOUT)
                .property(ClientProperties.READ_TIMEOUT, VALUE_TIMEOUT)
                .target(Propriedades.getPropriedadeNovo("url.api.capptan"));
    }
}
