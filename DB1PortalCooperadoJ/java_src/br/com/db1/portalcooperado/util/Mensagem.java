package br.com.db1.portalcooperado.util;

import br.com.db1.internationalization.DB1Bundle;

public class Mensagem {

	public static final String ERRO_INESPERADO = "Ocorreu um erro inesperado. Contate o suporte!";

	public static final String REPORT_IS_BLANK = "Este relatório não contém páginas a serem exibidas.";

	public static final String USUARIO_NAO_ENCONTRADO = "O usuário solicitado não está cadastrado corretamente. Por questões de segurança, o relatório não pode ser gerado.";

	private static final String RESOURCE = "br.com.db1.portalcooperado.resource.messages";

	private static final String TIPO_NAO_IMPLEMENTADO = "br.com.db1.portalcooperado.service.impl.RequisicaoServiceImpl.typeNotImplemented";

	private Mensagem() {

	}

	public static String tipoNaoImplementado() {
		return DB1Bundle.getMessage(RESOURCE, TIPO_NAO_IMPLEMENTADO);
	}

	public static String createReportProblem(String message) {
		return String.format("%s: %s", "Problemas ao tentar gerar relatório", message);
	}

	public static String campoObrigatorioMasculino(String campo) {
		return String.format("%s %s", campo, "deve ser informado.");
	}

	public static String campoObrigatorioFeminino(String campo) {
		return String.format("%s %s", campo, "deve ser informada.");
	}

}
