package br.com.db1.portalcooperado.util.notificacaostringbuilder;

public interface NotificacaoMensagemBuilder {

	public String build();

}
