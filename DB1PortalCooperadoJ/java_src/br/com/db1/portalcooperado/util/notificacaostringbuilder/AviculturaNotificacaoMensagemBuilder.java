package br.com.db1.portalcooperado.util.notificacaostringbuilder;

import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import br.com.db1.portalcooperado.util.DateFormatter;

class AviculturaNotificacaoMensagemBuilder implements NotificacaoMensagemBuilder {

	private static final String FORMAT = "Galpão: %s,  Lote: %s, Data de vistoria: %s.";

	private final Avicultura avicultura;

	private AviculturaNotificacaoMensagemBuilder(Avicultura avicultura) {
		this.avicultura = avicultura;
	}

	static NotificacaoMensagemBuilder of(Avicultura avicultura) {
		return new AviculturaNotificacaoMensagemBuilder(avicultura);
	}

	@Override
	public String build() {
		return String.format(FORMAT,
				avicultura.getGalpao(),
				avicultura.getLote(),
				DateFormatter.DD_MM_YYYY.format(avicultura.getData()));
	}

}
