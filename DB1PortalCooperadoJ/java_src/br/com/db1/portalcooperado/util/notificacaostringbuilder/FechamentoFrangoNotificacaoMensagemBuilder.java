package br.com.db1.portalcooperado.util.notificacaostringbuilder;

import br.com.db1.portalcooperado.model.entity.Notificacao;

import static br.com.db1.portalcooperado.util.MoneyFormatter.REAIS;

class FechamentoFrangoNotificacaoMensagemBuilder extends AbstractNotificacaoMensagemBuilder {

	private static final String FORMAT = "%s: valor bruto de %s pagamento %s.";

	private FechamentoFrangoNotificacaoMensagemBuilder(Notificacao notificacao) {
		super(notificacao);
	}

	static NotificacaoMensagemBuilder of(Notificacao notificacao) {
		return new FechamentoFrangoNotificacaoMensagemBuilder(notificacao);
	}

	@Override
	public String build() {
		return String.format(FORMAT,
				notificacao.descricaoTipoNotificacao(),
				REAIS.format(notificacao.getValor()),
				notificacao.dataAsString());
	}

}
