package br.com.db1.portalcooperado.util.notificacaostringbuilder;

import br.com.db1.portalcooperado.model.entity.Notificacao;

class RomaneioAgricolaNotificacaoMensagemBuilder extends AbstractNotificacaoMensagemBuilder {

    private static final String FORMATO_SEM_LOTE = "%s - Foram entregues %skg de %s em %s no dia %s às %s pelo veículo %s sob o documento %s.";

    private static final String FORMATO_COM_LOTE = "%s - Foram entregues %skg de %s em %s no dia %s às %s pelo veículo %s sob o documento %s " +
        "correspondente ao lote %s.";

    private RomaneioAgricolaNotificacaoMensagemBuilder(Notificacao notificacao) {
        super(notificacao);
    }

    static NotificacaoMensagemBuilder of(Notificacao notificacao) {
        return new RomaneioAgricolaNotificacaoMensagemBuilder(notificacao);
    }

    @Override
    public String build() {
        if (notificacao.getLote() != null) {
            return buildComLote();
        }
        return buildSemLote();
    }

    private String buildComLote() {
        return String.format(FORMATO_COM_LOTE,
            notificacao.descricaoTipoNotificacao(),
            notificacao.getPeso(),
            notificacao.getProduto().toLowerCase(),
            notificacao.getFilial().toLowerCase(),
            notificacao.dataAsString(),
            notificacao.getHora(),
            notificacao.getPlaca(),
            notificacao.getDocumento(),
            notificacao.getLote().toLowerCase());
    }

    private String buildSemLote() {
        return String.format(FORMATO_SEM_LOTE,
            notificacao.descricaoTipoNotificacao(),
            notificacao.getPeso(),
            notificacao.getProduto().toLowerCase(),
            notificacao.getFilial().toLowerCase(),
            notificacao.dataAsString(),
            notificacao.getHora(),
            notificacao.getPlaca(),
            notificacao.getDocumento());
    }

}
