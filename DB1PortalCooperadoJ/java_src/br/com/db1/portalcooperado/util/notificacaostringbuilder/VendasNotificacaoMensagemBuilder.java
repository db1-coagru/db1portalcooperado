package br.com.db1.portalcooperado.util.notificacaostringbuilder;

import br.com.db1.portalcooperado.model.entity.Notificacao;

import static br.com.db1.portalcooperado.util.MoneyFormatter.REAIS;

class VendasNotificacaoMensagemBuilder extends AbstractNotificacaoMensagemBuilder {

    private static final String FORMAT_SEM_LOTE = "%s - Realizado em %s no dia %s às %s no valor de %s sob o documento %s.";

    private static final String FORMAT_COM_LOTE = "%s - Realizado em %s no dia %s às %s no valor de %s sob o documento %s correspondente ao lote %s.";

    private VendasNotificacaoMensagemBuilder(Notificacao notificacao) {
        super(notificacao);
    }

    static NotificacaoMensagemBuilder of(Notificacao notificacao) {
        return new VendasNotificacaoMensagemBuilder(notificacao);
    }

    @Override
    public String build() {
        if (notificacao.getLote() != null) {
            return buildComLote();
        }
        return buildSemLote();
    }

    private String buildComLote() {
        return String.format(FORMAT_COM_LOTE,
            notificacao.descricaoTipoNotificacao(),
            notificacao.getFilial().toLowerCase(),
            notificacao.dataAsString(),
            notificacao.getHora(),
            REAIS.format(notificacao.getValor()),
            notificacao.getDocumento(),
            notificacao.getLote().toLowerCase());
    }

    private String buildSemLote() {
        return String.format(FORMAT_SEM_LOTE,
            notificacao.descricaoTipoNotificacao(),
            notificacao.getFilial().toLowerCase(),
            notificacao.dataAsString(),
            notificacao.getHora(),
            REAIS.format(notificacao.getValor()),
            notificacao.getDocumento());
    }
}
