package br.com.db1.portalcooperado.util.notificacaostringbuilder;

import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.util.DateFormatter;

class RecomendacaoTecnicaNotificacaoMensagemBuilder implements NotificacaoMensagemBuilder {

    private static final String FORMAT = "Produto: %s,  Talhão (alq.): %s, Data de vistoria: %s.";

    private final RecomendacaoTecnica recomendacaoTecnica;

    private RecomendacaoTecnicaNotificacaoMensagemBuilder(RecomendacaoTecnica recomendacaoTecnica) {
        this.recomendacaoTecnica = recomendacaoTecnica;
    }

    static NotificacaoMensagemBuilder of(RecomendacaoTecnica recomendacaoTecnica) {
        return new RecomendacaoTecnicaNotificacaoMensagemBuilder(recomendacaoTecnica);
    }

    @Override
    public String build() {
        return String.format(FORMAT,
                recomendacaoTecnica.getProduto().getDescricaoLowerCase(),
                recomendacaoTecnica.getTotalArea(),
                DateFormatter.DD_MM_YYYY.format(recomendacaoTecnica.getCriacao()));
    }

}
