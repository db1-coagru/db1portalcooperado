package br.com.db1.portalcooperado.util.notificacaostringbuilder;

import br.com.db1.portalcooperado.model.entity.Notificacao;

abstract class AbstractNotificacaoMensagemBuilder implements NotificacaoMensagemBuilder {

	protected final Notificacao notificacao;

	protected AbstractNotificacaoMensagemBuilder(Notificacao notificacao) {
		this.notificacao = notificacao;
	}
}
