package br.com.db1.portalcooperado.util.notificacaostringbuilder;

import br.com.db1.portalcooperado.model.entity.Notificacao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import br.com.db1.portalcooperado.util.Mensagem;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;

public class NotificacaoMensagemBuilderFactory {

	private static final String NOTIFICACAO = "Notificação";

	private NotificacaoMensagemBuilderFactory() {

	}

	public static NotificacaoMensagemBuilder romaneioAgricola(Notificacao notificacao) {
		PortalCooperadoUtil.checkNotNull(notificacao, Mensagem.campoObrigatorioFeminino(NOTIFICACAO));
		return RomaneioAgricolaNotificacaoMensagemBuilder.of(notificacao);
	}

	public static NotificacaoMensagemBuilder recomendacaoTecnica(RecomendacaoTecnica recomendacaoTecnica) {
		PortalCooperadoUtil
				.checkNotNull(recomendacaoTecnica, Mensagem.campoObrigatorioFeminino("Recomendação Técnica"));
		return RecomendacaoTecnicaNotificacaoMensagemBuilder.of(recomendacaoTecnica);
	}

	public static NotificacaoMensagemBuilder vendas(Notificacao notificacao) {
		PortalCooperadoUtil.checkNotNull(notificacao, Mensagem.campoObrigatorioFeminino(NOTIFICACAO));
		return VendasNotificacaoMensagemBuilder.of(notificacao);
	}

	public static NotificacaoMensagemBuilder fechamentoFrango(Notificacao notificacao) {
		PortalCooperadoUtil.checkNotNull(notificacao, Mensagem.campoObrigatorioFeminino(NOTIFICACAO));
		return FechamentoFrangoNotificacaoMensagemBuilder.of(notificacao);
	}

	public static NotificacaoMensagemBuilder avicultura(Avicultura avicultura) {
		PortalCooperadoUtil
				.checkNotNull(avicultura, Mensagem.campoObrigatorioFeminino("Avicultura"));
		return AviculturaNotificacaoMensagemBuilder.of(avicultura);
	}
}
