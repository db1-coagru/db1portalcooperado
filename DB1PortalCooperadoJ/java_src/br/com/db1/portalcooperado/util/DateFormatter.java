package br.com.db1.portalcooperado.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public enum DateFormatter {

	DD_MM_YYYY("dd/MM/yyyy"),

	DD_MM_YYYY_HH_MM_SS("dd/MM/yyyy HH:mm:ss"),

	HH_MM_SS("HH:mm:ss");

	private final SimpleDateFormat formatter;

	DateFormatter(String format) {
		this.formatter = new SimpleDateFormat(format);
	}

	public String format(Date date) {
		return formatter.format(date);
	}

}
