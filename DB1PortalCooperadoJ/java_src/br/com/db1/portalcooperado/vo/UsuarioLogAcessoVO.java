package br.com.db1.portalcooperado.vo;

import br.com.db1.portalcooperado.model.types.Canal;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;

import java.io.Serializable;
import java.math.BigDecimal;

public class UsuarioLogAcessoVO implements Serializable {

	private static final String SEM_NOME_CADASTRADO = "** SEM NOME CADASTRADO **";

	private String nomeCooperante = SEM_NOME_CADASTRADO;

	private static final long serialVersionUID = 1885794793142682620L;

	private Long matricula;

	private Long canal;

	private BigDecimal qtdeAcessosPosFinanceira;

	private BigDecimal qtdeAcessosExtratoProducao;

	private BigDecimal qtdeAcessosRecomendacaoTecnica;

	private BigDecimal qtdeAcessosAvicultura;

	private BigDecimal totalAcessos;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof UsuarioLogAcessoVO) {
			UsuarioLogAcessoVO other = (UsuarioLogAcessoVO) obj;
			return PortalCooperadoUtil.areEquals(this.matricula, other.matricula);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return PortalCooperadoUtil.hashCode(matricula);
	}

	public void clearMatriculaENome() {
		this.matricula = null;
		this.nomeCooperante = null;
	}

	public String getCanalAcesso() {
		return Canal.fromLongValue(canal).getDescricao();
	}

	/**
	 * Retorna o valor do campo matricula.
	 *
	 * @return valor do campo matricula.
	 */
	public Long getMatricula() {
		return matricula;
	}

	/**
	 * Atribui um novo valor para o campo matricula.
	 *
	 * @param matricula novo valor para o campo matricula.
	 */
	public void setMatricula(Long matricula) {
		this.matricula = matricula;
	}

	/**
	 * Retorna o valor do campo nomeCooperante.
	 *
	 * @return valor do campo nomeCooperante.
	 */
	public String getNomeCooperante() {
		return nomeCooperante;
	}

	/**
	 * Atribui um novo valor para o campo nomeCooperante.
	 *
	 * @param nomeCooperante novo valor para o campo nomeCooperante.
	 */
	public void setNomeCooperante(String nomeCooperante) {
		this.nomeCooperante = nomeCooperante;
	}

	public Long getCanal() {
		return canal;
	}

	public void setCanal(Long canal) {
		this.canal = canal;
	}

	/**
	 * Retorna o valor do campo qtdeAcessosPosFinanceira.
	 *
	 * @return valor do campo qtdeAcessosPosFinanceira.
	 */
	public BigDecimal getQtdeAcessosPosFinanceira() {
		return qtdeAcessosPosFinanceira;
	}

	/**
	 * Atribui um novo valor para o campo qtdeAcessosPosFinanceira.
	 *
	 * @param qtdeAcessosPosFinanceira novo valor para o campo qtdeAcessosPosFinanceira.
	 */
	public void setQtdeAcessosPosFinanceira(BigDecimal qtdeAcessosPosFinanceira) {
		this.qtdeAcessosPosFinanceira = qtdeAcessosPosFinanceira;
	}

	/**
	 * Retorna o valor do campo qtdeAcessosExtratoProducao.
	 *
	 * @return valor do campo qtdeAcessosExtratoProducao.
	 */
	public BigDecimal getQtdeAcessosExtratoProducao() {
		return qtdeAcessosExtratoProducao;
	}

	/**
	 * Atribui um novo valor para o campo qtdeAcessosExtratoProducao.
	 *
	 * @param qtdeAcessosExtratoProducao novo valor para o campo qtdeAcessosExtratoProducao.
	 */
	public void setQtdeAcessosExtratoProducao(BigDecimal qtdeAcessosExtratoProducao) {
		this.qtdeAcessosExtratoProducao = qtdeAcessosExtratoProducao;
	}

	/**
	 * Retorna o valor do campo totalAcessos.
	 *
	 * @return valor do campo totalAcessos.
	 */
	public BigDecimal getTotalAcessos() {
		return totalAcessos;
	}

	/**
	 * Atribui um novo valor para o campo totalAcessos.
	 *
	 * @param totalAcessos novo valor para o campo totalAcessos.
	 */
	public void setTotalAcessos(BigDecimal totalAcessos) {
		this.totalAcessos = totalAcessos;
	}

	public BigDecimal getQtdeAcessosRecomendacaoTecnica() {
		return qtdeAcessosRecomendacaoTecnica;
	}

	public void setQtdeAcessosRecomendacaoTecnica(BigDecimal qtdeAcessosRecomendacaoTecnica) {
		this.qtdeAcessosRecomendacaoTecnica = qtdeAcessosRecomendacaoTecnica;
	}

	public BigDecimal getQtdeAcessosAvicultura() {
		return qtdeAcessosAvicultura;
	}

	public void setQtdeAcessosAvicultura(BigDecimal qtdeAcessosAvicultura) {
		this.qtdeAcessosAvicultura = qtdeAcessosAvicultura;
	}
}
