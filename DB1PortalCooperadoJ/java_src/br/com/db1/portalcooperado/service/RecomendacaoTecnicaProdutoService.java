package br.com.db1.portalcooperado.service;

import br.com.db1.myBatisPersistence.service.GenericMyBatisService;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaProduto;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;

public interface RecomendacaoTecnicaProdutoService extends GenericMyBatisService<RecomendacaoTecnicaProduto, Long> {

	public Boolean processaRecomendacaoTecnicaProdutos(RecomendacaoTecnica recomendacaoTecnica,
			ServiceRecord serviceRecord);

}
