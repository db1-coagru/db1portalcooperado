package br.com.db1.portalcooperado.service;

import br.com.db1.portalcooperado.infrastructure.folders.files.FileReader;
import br.com.db1.portalcooperado.infrastructure.folders.files.FolderFiles;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileFilter;

public interface FolderService  {

	<T> FolderFiles<T> readFolderOnServer(String path, FileReader<T> fileReader, FileFilter fileFilter)
			throws JAXBException;

	FolderFiles<File> readFolderOnServer(String path, FileFilter fileFilter) throws JAXBException;


}
