package br.com.db1.portalcooperado.service;

import br.com.db1.myBatisPersistence.service.GenericMyBatisService;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaDiagnostico;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;

public interface RecomendacaoTecnicaDiagnosticoService
		extends GenericMyBatisService<RecomendacaoTecnicaDiagnostico, Long> {

	public Boolean processaRecomendacaoTecnicaDiagnosticos(RecomendacaoTecnica recomendacaoTecnica,
			ServiceRecord serviceRecord);

}
