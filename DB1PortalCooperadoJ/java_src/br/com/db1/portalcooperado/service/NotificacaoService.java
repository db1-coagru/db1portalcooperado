package br.com.db1.portalcooperado.service;

import br.com.db1.myBatisPersistence.service.GenericMyBatisService;
import br.com.db1.portalcooperado.model.entity.Notificacao;

public interface NotificacaoService extends GenericMyBatisService<Notificacao, Long> {

    boolean enviarNotificacaoMobileConsumindoFila(Notificacao notificacao);

}
