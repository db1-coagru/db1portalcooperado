package br.com.db1.portalcooperado.service;

import br.com.db1.myBatisPersistence.service.GenericMyBatisService;
import br.com.db1.portalcooperado.model.entity.UsuarioLog;
import br.com.db1.portalcooperado.relatorio.types.LogAcessoTipoCanal;

import java.util.Date;

/**
 * Interface que define os métodos de negócio Genéricos e que devem ser
 * customizados conforme a necessidade do <code>Model</code> que a implementa.
 * 
 * @author DB1 Informatica - Generated by DB1 Eclipse Plugin
 * 
 */
public interface UsuarioLogService extends
		GenericMyBatisService<UsuarioLog, Long> {

	/**
	 * Chamada para o relatório que exibe as ações de um funcionário no cadastro do usuário.
	 * @param matricula
	 * @param dtInicial
	 * @param dtFinal
	 * @return
	 */
	String relatorioUsuarioLog(Long matricula, Date dtInicial, Date dtFinal);
	
	/**
	 * Método responsável por construir o relatório de log de acessos de usuário.
	 * @param dtInicial data inicial do período a ser buscado o conjunto de registros de acesso
	 * @param dtFinal data final do período a ser buscado o conjunto de registros de acesso
	 * @return valor da chave do arquivo na Session do Flex
	 */
	String relatorioAcessoUsuarioLog(LogAcessoTipoCanal canal, Date dtInicial, Date dtFinal);
}