package br.com.db1.portalcooperado.service;

import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;

public interface NotificacaoRecomendacaoTecnicaService {

    void enviarNotificacaoMobile(RecomendacaoTecnica recomendacaoTecnica);

}
