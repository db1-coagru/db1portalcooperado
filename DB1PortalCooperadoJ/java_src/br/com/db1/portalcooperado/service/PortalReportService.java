package br.com.db1.portalcooperado.service;

import br.com.db1.kernel.report.exception.DB1ReportGenerationException;
import br.com.db1.portalcooperado.relatorio.types.PortalReport;

public interface PortalReportService {

	String createReport(PortalReport.Builder builder) throws DB1ReportGenerationException;

}
