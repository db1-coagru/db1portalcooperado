package br.com.db1.portalcooperado.service;

import br.com.db1.library.mail.DadosMensagem;
import br.com.db1.library.mail.SMTPServer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

public final class EnviaEmailService {

	private EnviaEmailService() {
	}

	public static void enviarEmail(DadosMensagem mensagem, final SMTPServer server) throws MessagingException {
		Properties mailProps = System.getProperties();
		mailProps.put("mail.smtp.host", server.getServerName());
		mailProps.put("mail.smtp.port", server.getPorta());
		mailProps.put("mail.smtp.auth", server.getBoAutenticacao().toString());
		Authenticator auth = criarAutenticator(server);
		Session mailSession = Session.getInstance(mailProps, auth);
		mailSession.setDebug(true);
		Multipart multipart = new MimeMultipart();
		if (mensagem.getAnexo() != null) {
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(mensagem.getMensagem(), "text/html; charset=ISO-8859-1;");
			multipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(mensagem.getAnexo());
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(mensagem.getAnexo().getName());
			multipart.addBodyPart(messageBodyPart);
		}
		Message mimeMessage = new MimeMessage(mailSession);
		mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mensagem.getPara()));
		mimeMessage.setFrom(new InternetAddress(server.getUserName()));
		mimeMessage.setReplyTo(InternetAddress.parse(mensagem.getDe()));
		mimeMessage.setSubject(mensagem.getAssunto());
		if (mensagem.getCopiaPara() != null) {
			mimeMessage.addRecipients(Message.RecipientType.CC, InternetAddress.parse(mensagem.getCopiaPara()));
		}
		if (mensagem.getAnexo() != null) {
			mimeMessage.setContent(multipart);
		} else {
			mimeMessage.setContent(mensagem.getMensagem(), "text/html; charset=ISO-8859-1;");
		}
		Transport.send(mimeMessage, mimeMessage.getAllRecipients());
	}

	private static Authenticator criarAutenticator(final SMTPServer server) {
		if (server.getBoAutenticacao()) {
			return new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(server.getUserName(), server.getPassword());
				}
			};
		}
		return null;
	}

	public static SMTPServer getSMTPServerCliente(String paramPass, String paramUser, Boolean paramAutenticado,
			String paramPorta, String paramServerName) {
		return new SMTPServer(paramServerName, paramPorta, paramAutenticado, paramUser, paramPass);
	}
}
