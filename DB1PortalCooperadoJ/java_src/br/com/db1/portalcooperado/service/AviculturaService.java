package br.com.db1.portalcooperado.service;

import br.com.db1.myBatisPersistence.service.GenericMyBatisService;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;

import java.util.Date;

public interface AviculturaService extends GenericMyBatisService<Avicultura, Long> {

	/**
	 * Processo para execução manual da integração da avicultura do agrosys com o portal. Obs:
	 * este processo é chamado
	 * pelo flex.
	 *
	 * @param dataProcessar é a data que o processamento irá buscar os dados no SOAP da agrosys
	 */
	void integrarManualmenteChecklistsDoDia(Date dataProcessar);

	void enviarNotificacaoMobile(Avicultura avicultura);

}
