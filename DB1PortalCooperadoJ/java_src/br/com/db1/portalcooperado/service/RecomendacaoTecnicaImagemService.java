package br.com.db1.portalcooperado.service;

import br.com.db1.myBatisPersistence.service.GenericMyBatisService;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaImagem;

import java.io.File;
import java.util.List;

public interface RecomendacaoTecnicaImagemService extends GenericMyBatisService<RecomendacaoTecnicaImagem, Long> {

	public void processaRecomendacaoTecnicaImagens(RecomendacaoTecnica recomendacaoTecnica, List<File> imagens);

}
