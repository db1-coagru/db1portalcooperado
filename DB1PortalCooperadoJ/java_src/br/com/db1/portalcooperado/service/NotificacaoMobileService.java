package br.com.db1.portalcooperado.service;

import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.NotificacaoJson;

public interface NotificacaoMobileService {

    void enviarNotificacaoMobile(NotificacaoJson notificacaoJson);

}
