package br.com.db1.portalcooperado.service;

import br.com.db1.myBatisPersistence.service.GenericMyBatisService;
import br.com.db1.portalcooperado.model.entity.Token;
import br.com.db1.security.model.entity.User;

/**
 * Interface que define os métodos de negócio Genéricos e que devem ser
 * customizados conforme a necessidade do <code>Model</code> que a implementa.
 *
 * @author rafael.rasso
 */
public interface PortalTokenService extends GenericMyBatisService<Token, Long> {

	/**
	 * Irá gerar o token para o usuário recebido por parâmetro
	 *
	 * @param user
	 * @return Token
	 */
	Token generateTokenTo(User user);

	/**
	 * Irá validar se o token para recebido por parâmetro existe no banco, não expirou e ainda é válido.
	 *
	 * @param token
	 * @return boolean
	 */
	boolean validateToken(String token);
	
}
