package br.com.db1.portalcooperado.service;

import br.com.db1.portalcooperado.model.entity.PosicaoFinanceira;
import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.relatorio.types.PortalReport;
import br.com.db1.portalcooperado.relatorio.types.ReportType;

import javax.ws.rs.core.Response;

public interface PosicaoFinanceiraService {

	PosicaoFinanceira posicaoFinanceiraOf(Requisicao requisicao);

	PortalReport.Builder posicaoFinanceiraReportBuilderOf(PosicaoFinanceira posicaoFinanceira, ReportType reportType);

	PortalReport.Builder posicaoFinanceiraReportBuilderOf(Requisicao requisicao, ReportType reportType);

	Response posicaoFinanceiraResponseOf(Requisicao requisicao, Boolean exportaPdf);

	String posicaoFinanceiraReportOf(Requisicao requisicao, ReportType tipoRelatorio);

}
