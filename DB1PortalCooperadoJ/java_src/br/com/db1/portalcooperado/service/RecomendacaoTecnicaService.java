package br.com.db1.portalcooperado.service;

import br.com.db1.myBatisPersistence.service.GenericMyBatisService;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;

import javax.ws.rs.core.Response;
import java.io.File;
import java.util.List;

public interface RecomendacaoTecnicaService extends GenericMyBatisService<RecomendacaoTecnica, Long> {

	RecomendacaoTecnica processaRecomendacaoTecnica(ServiceRecord serviceRecord, List<File> imagens);

	Response recomendacoesTecnicasResponseOf(Long matricula, Long idRecomendacaoTecnica);

}
