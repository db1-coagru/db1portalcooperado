package br.com.db1.portalcooperado.service;

import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXml;

import java.util.Date;
import java.util.List;

public interface AviculturaAgrosysService {

	List<AviculturaXml> findAllAviculturasOf(Date date);

	List<AviculturaXml> findAllAviculturasOf(Date begin, Date end);

}
