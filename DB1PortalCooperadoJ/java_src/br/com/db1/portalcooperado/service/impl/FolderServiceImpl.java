package br.com.db1.portalcooperado.service.impl;

import br.com.db1.portalcooperado.infrastructure.folders.files.FileReader;
import br.com.db1.portalcooperado.infrastructure.folders.files.FolderFiles;
import br.com.db1.portalcooperado.infrastructure.folders.readers.FolderReader;
import br.com.db1.portalcooperado.service.FolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileFilter;

@Service(value = "folderService")
@Transactional(rollbackFor = Exception.class)
public class FolderServiceImpl implements FolderService {

	@Autowired
	@Qualifier(value = "serverFolderReader")
	private FolderReader folderReader;

	@Override
	public <T> FolderFiles<T> readFolderOnServer(String path, FileReader<T> fileReader, FileFilter fileFilter)
			throws JAXBException {
		return folderReader.read(path, fileReader, fileFilter);
	}

	@Override
	public FolderFiles<File> readFolderOnServer(String path, FileFilter fileFilter) throws JAXBException {
		return folderReader.read(path, fileFilter);
	}

}
