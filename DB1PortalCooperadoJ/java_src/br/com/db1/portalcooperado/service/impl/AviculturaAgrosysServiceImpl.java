package br.com.db1.portalcooperado.service.impl;

import br.com.db1.exception.DB1Exception;
import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXml;
import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXmlCheckList;
import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.webservice.IntegCheckListObj;
import br.com.db1.portalcooperado.service.AviculturaAgrosysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.ws.Holder;
import java.util.Date;
import java.util.List;

import static br.com.db1.portalcooperado.util.DateFormatter.DD_MM_YYYY;

@Service(value = "aviculturaAgrosysService")
@Transactional(rollbackFor = Exception.class)
public class AviculturaAgrosysServiceImpl implements AviculturaAgrosysService {

	private final IntegCheckListObj integCheckListObj;

	@Autowired
	public AviculturaAgrosysServiceImpl(IntegCheckListObj integCheckListObj) {
		this.integCheckListObj = integCheckListObj;
	}

	@Override
	public List<AviculturaXml> findAllAviculturasOf(Date date) {
		return findAllAviculturasOf(date, date);
	}

	@Override
	public List<AviculturaXml> findAllAviculturasOf(Date begin, Date end) {
		try {
			Holder<String> result = new Holder<String>();
			Holder<String> vxmlRet = new Holder<String>();
			integCheckListObj.lnitg100(DD_MM_YYYY.format(begin), DD_MM_YYYY.format(end), result, vxmlRet);
			AviculturaXmlCheckList checkList = AviculturaXmlCheckList.of(vxmlRet.value);
			return checkList.getItens();
		}
		catch (Exception ex) {
			throw new DB1Exception(ex);
		}
	}

}
