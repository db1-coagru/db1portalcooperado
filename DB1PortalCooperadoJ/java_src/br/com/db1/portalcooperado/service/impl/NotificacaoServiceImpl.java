package br.com.db1.portalcooperado.service.impl;

import br.com.db1.myBatisPersistence.service.GenericMyBatisServiceImpl;
import br.com.db1.portalcooperado.dao.NotificacaoDao;
import br.com.db1.portalcooperado.model.entity.Notificacao;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.Produto;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.MatriculasEnviarNotificacao;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.NotificacaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.ParametrosJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.entregaproducao.EntregaProducaoParametrosJson;
import br.com.db1.portalcooperado.service.NotificacaoMobileService;
import br.com.db1.portalcooperado.service.NotificacaoService;
import br.com.db1.portalcooperado.service.UsuarioService;
import br.com.db1.portalcooperado.util.notificacaostringbuilder.NotificacaoMensagemBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service(value = "notificacaoService")
@Transactional(rollbackFor = Exception.class)
public class NotificacaoServiceImpl extends GenericMyBatisServiceImpl<Notificacao, Long>
		implements NotificacaoService {

	private NotificacaoDao notificacaoDao;

	private UsuarioService usuarioService;

	private NotificacaoMobileService notificacaoMobileService;

	@Autowired
	public NotificacaoServiceImpl(NotificacaoDao notificacaoDao,
			UsuarioService usuarioService,
			NotificacaoMobileService notificacaoMobileService) {
		this.notificacaoDao = notificacaoDao;
		this.usuarioService = usuarioService;
		this.notificacaoMobileService = notificacaoMobileService;
		setGenericDao(notificacaoDao);
	}

	@Override
	@Transactional
	public boolean enviarNotificacaoMobileConsumindoFila(Notificacao notificacao) {
		NotificacaoJson notificacaoJson = transformToJson(notificacao);
		notificacaoMobileService.enviarNotificacaoMobile(notificacaoJson);
		notificacao.setStatus(1L);
		notificacaoDao.update(notificacao);
		return true;
	}

	private NotificacaoJson transformToJson(Notificacao notificacao) {
		Usuario usuario = usuarioService.buscaUsuarioFull(notificacao.matriculaAsString());
		return NotificacaoJson.builder()
				.withTitulo(notificacao.getTitulo())
				.withMensagem(mensagemOf(notificacao))
				.withMatriculas(MatriculasEnviarNotificacao.of(usuario, notificacao.getPermissao()))
				.withParametros(parametrosOf(notificacao))
				.build();
	}

	private String mensagemOf(Notificacao notificacao) {
		if (notificacao.possuiTipo(TipoNotificacao.ROMANEIO_AGRICOLA)) {
			return NotificacaoMensagemBuilderFactory.romaneioAgricola(notificacao).build();
		}
		if (notificacao.possuiTipo(TipoNotificacao.FECHAMENTO_DE_FRANGO)) {
			return NotificacaoMensagemBuilderFactory.fechamentoFrango(notificacao).build();
		}
		return NotificacaoMensagemBuilderFactory.vendas(notificacao).build();
	}

	private ParametrosJson parametrosOf(Notificacao notificacao) {
		if (notificacao.isDeRomaneioAgricola()) {
			Date data = notificacao.getData();
			EntregaProducaoParametrosJson.Builder builder = EntregaProducaoParametrosJson.builder().between(data, data);
			Produto produto = Produto.fromStringValue(notificacao.getProduto());
			produto.append(builder);
			builder.withTipo(TipoNotificacao.ROMANEIO_AGRICOLA);
			return builder.build();
		}
		return null;
	}
}
