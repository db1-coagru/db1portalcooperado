package br.com.db1.portalcooperado.service.impl;

import br.com.db1.portalcooperado.dao.AviculturaDao;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaToAviculturaJsonTranslator;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturasToAviculturasJsonTranslator;
import br.com.db1.portalcooperado.requisicao.reponseprocessors.RequisicaoResponseProcessor;
import br.com.db1.portalcooperado.requisicao.reponseprocessors.RequisicaoResponseProcessorFactory;
import br.com.db1.portalcooperado.service.EntregaProducaoService;
import br.com.db1.portalcooperado.service.PosicaoFinanceiraService;
import br.com.db1.portalcooperado.service.RecomendacaoTecnicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RequisicaoProcessResponseFactoryService {

	private final PosicaoFinanceiraService posicaoFinanceiraService;

	private final EntregaProducaoService entregaProducaoService;

	private final RecomendacaoTecnicaService recomendacaoTecnicaService;

	private final AviculturaDao avaviculturaDao;

	private final AviculturasToAviculturasJsonTranslator aviculturasToAviculturasJsonTranslator;

	private final AviculturaToAviculturaJsonTranslator aviculturaToAviculturaJsonTranslator;

	@Autowired
	public RequisicaoProcessResponseFactoryService(PosicaoFinanceiraService posicaoFinanceiraService,
			EntregaProducaoService entregaProducaoService,
			RecomendacaoTecnicaService recomendacaoTecnicaService,
			AviculturaDao avaviculturaDao,
			AviculturasToAviculturasJsonTranslator aviculturasToAviculturasJsonTranslator,
			AviculturaToAviculturaJsonTranslator aviculturaToAviculturaJsonTranslator) {
		this.posicaoFinanceiraService = posicaoFinanceiraService;
		this.entregaProducaoService = entregaProducaoService;
		this.recomendacaoTecnicaService = recomendacaoTecnicaService;
		this.avaviculturaDao = avaviculturaDao;
		this.aviculturasToAviculturasJsonTranslator = aviculturasToAviculturasJsonTranslator;
		this.aviculturaToAviculturaJsonTranslator = aviculturaToAviculturaJsonTranslator;
	}

	RequisicaoResponseProcessor processadorOf(TipoRequisicao tipoRequisicao) {
		if (TipoRequisicao.POSICAO_FINANCEIRA.equals(tipoRequisicao)) {
			return RequisicaoResponseProcessorFactory.posicaoFinanceira(posicaoFinanceiraService);
		}
		if (TipoRequisicao.ENTREGAS_PRODUCAO.equals(tipoRequisicao)) {
			return RequisicaoResponseProcessorFactory.entregaProducao(entregaProducaoService);
		}
		if (TipoRequisicao.RECOMENDACAO_TECNICA.equals(tipoRequisicao)) {
			return RequisicaoResponseProcessorFactory.recomendacaoTecnica(recomendacaoTecnicaService);
		}
		if (TipoRequisicao.AVICULTURA.equals(tipoRequisicao)) {
			return RequisicaoResponseProcessorFactory
					.avicultura(avaviculturaDao, aviculturasToAviculturasJsonTranslator,
							aviculturaToAviculturaJsonTranslator);
		}
		return RequisicaoResponseProcessorFactory.tipoNaoSuportado();
	}

}
