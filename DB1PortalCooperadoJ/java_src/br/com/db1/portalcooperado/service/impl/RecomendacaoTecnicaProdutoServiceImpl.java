package br.com.db1.portalcooperado.service.impl;

import br.com.db1.myBatisPersistence.service.GenericMyBatisServiceImpl;
import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaProdutoDao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaProduto;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.Suggestion;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.TypeInfo;
import br.com.db1.portalcooperado.service.RecomendacaoTecnicaProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(value = "recomendacaoTecnicaProdutoService")
@Transactional(rollbackFor = Exception.class)
public class RecomendacaoTecnicaProdutoServiceImpl extends GenericMyBatisServiceImpl<RecomendacaoTecnicaProduto, Long>
		implements RecomendacaoTecnicaProdutoService {

	@Autowired
	public RecomendacaoTecnicaProdutoServiceImpl(RecomendacaoTecnicaProdutoDao recomendacaoTecnicaProdutoDao) {
		super.setGenericDao(recomendacaoTecnicaProdutoDao);
	}

	@Override
	@Transactional
	public Boolean processaRecomendacaoTecnicaProdutos(RecomendacaoTecnica recomendacaoTecnica,
			ServiceRecord serviceRecord) {
		remove(recomendacaoTecnica.getProdutos());
		Boolean temProdutos = Boolean.FALSE;

		for (Suggestion suggestion : serviceRecord.suggestionsOf(TypeInfo.PRODUTOS)) {

			RecomendacaoTecnicaProduto.Builder builder = RecomendacaoTecnicaProduto.builder();
			builder.withRecomendacaoTecnica(recomendacaoTecnica);
			builder.withCodigo(suggestion.getProductInfoVendorId());
			builder.withNome(suggestion.getProductInfoName());
			builder.withOrdem(suggestion.getOrder());
			builder.withQuantidade(suggestion.getQuantity());
			builder.withUnidadeMedida(suggestion.getUnit());
			builder.withTotalArea(serviceRecord.getTotalArea());
			builder.withEficiencia(serviceRecord.getEfficiency());
			RecomendacaoTecnicaProduto produto = builder.build();

			saveOrUpdate(produto);
			temProdutos = Boolean.TRUE;
		}
		return temProdutos;
	}

	private void remove(List<RecomendacaoTecnicaProduto> produtos) {
		for (RecomendacaoTecnicaProduto produto : produtos) {
			remove(produto);
		}
	}

}