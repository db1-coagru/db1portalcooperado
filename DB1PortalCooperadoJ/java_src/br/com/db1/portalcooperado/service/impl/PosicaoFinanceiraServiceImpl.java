package br.com.db1.portalcooperado.service.impl;

import br.com.db1.portalcooperado.dao.InformacoesComplementaresDao;
import br.com.db1.portalcooperado.dao.UsuarioDao;
import br.com.db1.portalcooperado.model.entity.InformacoesComplementares;
import br.com.db1.portalcooperado.model.entity.PosicaoFinanceira;
import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.relatorio.types.PortalReport;
import br.com.db1.portalcooperado.relatorio.types.ReportType;
import br.com.db1.portalcooperado.infrastructure.rest.json.ReportIdJson;
import br.com.db1.portalcooperado.infrastructure.rest.responses.Responses;
import br.com.db1.portalcooperado.service.PortalReportService;
import br.com.db1.portalcooperado.service.PosicaoFinanceiraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response;

import static br.com.db1.portalcooperado.relatorio.types.PosicaoFinanceiraReports.POSICAO_FINANCEIRA_NOME;
import static br.com.db1.portalcooperado.relatorio.types.ReportType.JSON;
import static br.com.db1.portalcooperado.util.DateFormatter.DD_MM_YYYY_HH_MM_SS;

@Service(value = "posicaoFinanceiraService")
@Transactional(rollbackFor = Exception.class)
public class PosicaoFinanceiraServiceImpl implements PosicaoFinanceiraService {

	@Autowired
	private UsuarioDao usuarioDao;

	@Autowired
	private InformacoesComplementaresDao informacoesComplementaresDao;

	@Autowired
	private PortalReportService portalReportService;

	@Override
	public PosicaoFinanceira posicaoFinanceiraOf(Requisicao requisicao) {
		return PosicaoFinanceira.builder(requisicao)
				.build();
	}

	@Override
	public PortalReport.Builder posicaoFinanceiraReportBuilderOf(PosicaoFinanceira posicaoFinanceira,
			ReportType reportType) {
		Requisicao requisicao = posicaoFinanceira.getRequisicao();
		Usuario usuario = usuarioDao.buscaUsuario(requisicao.getMatricula().toString());
		InformacoesComplementares info = informacoesComplementaresDao.findById(requisicao.getId());

		PortalReport.Builder builder = PortalReport.builder();
		builder.withTipo(reportType);
		builder.withCaminho(reportType.getRelatorioPosicaoFinanceiraExecutar());
		builder.addDataSource(requisicao);
		builder.withParam("PNomeRelatorio", POSICAO_FINANCEIRA_NOME);
		builder.withParam("PNomeCooperativa", reportType.getNomeCooperativa());
		builder.withParam("PRequisicao", "Req: " + requisicao.getId().toString());
		builder.withParam("PMatricula", "Mat: " + usuario.getMatricula().toString());
		builder.withParam("PNomeCooperante", "Nome: " + usuario.getNome().toUpperCase());
		builder.withParam("PCpfCnpj", "CPF/CNPJ: " + usuario.getCpfCnpj());
		builder.withParam("PDataHora", "Data e Hora: " + DD_MM_YYYY_HH_MM_SS.format(reportType.getDataOperacao()));
		builder.withParam("pData", reportType.getDataOperacaoAtMidnight());
		builder.withParam("pListDebitos", posicaoFinanceira.geDebitos());
		builder.withParam("pListCreditos", posicaoFinanceira.getCreditos());
		builder.withParam("pListDebitosCreditos", posicaoFinanceira.getDebitosCreditos());
		builder.withParam("pListOutros", posicaoFinanceira.getOutros());
		reportType.appendSubReports(builder);
		if (info != null) {
			builder.withParam("PLocalAcerto", "Local de Acerto:" + info.getLocalOrigem());
		}
		if (posicaoFinanceira.hasMensagem()) {
			builder.withParam("PMensagem", posicaoFinanceira.getMensagem());
		}
		return builder;
	}

	@Override
	public PortalReport.Builder posicaoFinanceiraReportBuilderOf(Requisicao requisicao, ReportType reportType) {
		PosicaoFinanceira posicaoFinanceira = posicaoFinanceiraOf(requisicao);
		return posicaoFinanceiraReportBuilderOf(posicaoFinanceira, reportType);
	}

	@Override
	public Response posicaoFinanceiraResponseOf(Requisicao requisicao, Boolean exportaPdf) {
		PosicaoFinanceira posicaoFinanceira = posicaoFinanceiraOf(requisicao);
		if (exportaPdf) {
			PortalReport.Builder builder = posicaoFinanceiraReportBuilderOf(posicaoFinanceira, JSON);
			String idReport = portalReportService.createReport(builder);
			return Responses.ok(ReportIdJson.of(idReport));
		}
		return Responses.ok(posicaoFinanceira.toJson());
	}

	@Override
	public String posicaoFinanceiraReportOf(Requisicao requisicao, ReportType tipoRelatorio) {
		PortalReport.Builder builder = posicaoFinanceiraReportBuilderOf(requisicao, tipoRelatorio);
		return portalReportService.createReport(builder);
	}

}
