package br.com.db1.portalcooperado.service.impl;

import br.com.db1.myBatisPersistence.service.GenericMyBatisServiceImpl;
import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaDiagnosticoDao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaDiagnostico;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.Suggestion;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.TypeInfo;
import br.com.db1.portalcooperado.service.RecomendacaoTecnicaDiagnosticoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(value = "recomendacaoTecnicaDiagnosticoService")
@Transactional(rollbackFor = Exception.class)
public class RecomendacaoTecnicaDiagnosticoServiceImpl
		extends GenericMyBatisServiceImpl<RecomendacaoTecnicaDiagnostico, Long>
		implements RecomendacaoTecnicaDiagnosticoService {

	@Autowired
	public RecomendacaoTecnicaDiagnosticoServiceImpl(
			RecomendacaoTecnicaDiagnosticoDao recomendacaoTecnicaDiagnosticoDao) {
		super.setGenericDao(recomendacaoTecnicaDiagnosticoDao);
	}

	@Override
	@Transactional
	public Boolean processaRecomendacaoTecnicaDiagnosticos(RecomendacaoTecnica recomendacaoTecnica,
			ServiceRecord serviceRecord) {
		remove(recomendacaoTecnica.getDiagnosticos());
		Boolean temDiagnosticos = Boolean.FALSE;
		for (Suggestion suggestion : serviceRecord.suggestionsOf(TypeInfo.DIAGNOSTICOS)) {

			RecomendacaoTecnicaDiagnostico diagnostico = RecomendacaoTecnicaDiagnostico.of();
			diagnostico.setRecomendacaoTecnica(recomendacaoTecnica);
			diagnostico.setDescricao(suggestion.getSuggestionTypeInfoDescripton());
			diagnostico.setObservacao(suggestion.getNote());

			saveOrUpdate(diagnostico);
			temDiagnosticos = Boolean.TRUE;
		}
		return temDiagnosticos;
	}

	private void remove(List<RecomendacaoTecnicaDiagnostico> diagnosticos) {
		for (RecomendacaoTecnicaDiagnostico diagnostico : diagnosticos) {
			remove(diagnostico);
		}
	}

}