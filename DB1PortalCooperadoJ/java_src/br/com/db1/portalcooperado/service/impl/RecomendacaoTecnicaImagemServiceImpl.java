package br.com.db1.portalcooperado.service.impl;

import br.com.db1.myBatisPersistence.service.GenericMyBatisServiceImpl;
import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaImagemDao;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnicaImagem;
import br.com.db1.portalcooperado.service.RecomendacaoTecnicaImagemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;

@Service(value = "recomendacaoTecnicaImagemervice")
@Transactional(rollbackFor = Exception.class)
public class RecomendacaoTecnicaImagemServiceImpl extends GenericMyBatisServiceImpl<RecomendacaoTecnicaImagem, Long>
		implements RecomendacaoTecnicaImagemService {

	@Autowired
	public RecomendacaoTecnicaImagemServiceImpl(RecomendacaoTecnicaImagemDao recomendacaoTecnicItemaDao) {
		super.setGenericDao(recomendacaoTecnicItemaDao);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void processaRecomendacaoTecnicaImagens(RecomendacaoTecnica recomendacaoTecnica, List<File> imagens) {
		remove(recomendacaoTecnica.getImagens());
		for (File imagem : imagens) {
			RecomendacaoTecnicaImagem recomendacaoTecnicaImagem = RecomendacaoTecnicaImagem.of();
			recomendacaoTecnicaImagem.setRecomendacaoTecnica(recomendacaoTecnica);
			recomendacaoTecnicaImagem.setNome(imagem.getName());
			saveOrUpdate(recomendacaoTecnicaImagem);
		}
	}

	private void remove(List<RecomendacaoTecnicaImagem> imagens) {
		for (RecomendacaoTecnicaImagem imagem : imagens) {
			remove(imagem);
		}
	}

}