package br.com.db1.portalcooperado.service.impl;

import br.com.db1.myBatisPersistence.service.GenericMyBatisServiceImpl;
import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaDao;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson;
import br.com.db1.portalcooperado.infrastructure.rest.responses.Responses;
import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.Produto;
import br.com.db1.portalcooperado.service.*;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response;
import java.io.File;
import java.util.List;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@Service(value = "recomendacaoTecnicaService")
@Transactional(rollbackFor = Exception.class)
public class RecomendacaoTecnicaServiceImpl extends GenericMyBatisServiceImpl<RecomendacaoTecnica, Long>
		implements RecomendacaoTecnicaService {

	private RecomendacaoTecnicaDao recomendacaoTecnicaDao;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private RecomendacaoTecnicaProdutoService recomendacaoTecnicaProdutoService;

	@Autowired
	private RecomendacaoTecnicaDiagnosticoService recomendacaoTecnicaDiagnosticoService;

	@Autowired
	private RecomendacaoTecnicaImagemService recomendacaoTecnicaImagemService;

	@Autowired
	private NotificacaoRecomendacaoTecnicaService notificacaoRecomendacaoTecnicaService;

	@Autowired
	public RecomendacaoTecnicaServiceImpl(RecomendacaoTecnicaDao recomendacaoTecnicaDao) {
		this.recomendacaoTecnicaDao = recomendacaoTecnicaDao;
		super.setGenericDao(recomendacaoTecnicaDao);
	}

	@Override
	@Transactional
	public RecomendacaoTecnica processaRecomendacaoTecnica(ServiceRecord serviceRecord, List<File> imagens) {
		RecomendacaoTecnica recomendacaoTecnica = geraRecomendacaoTecnica(serviceRecord, imagens);
		notificacaoRecomendacaoTecnicaService.enviarNotificacaoMobile(recomendacaoTecnica);
		return recomendacaoTecnica;
	}

	@Override
	public Response recomendacoesTecnicasResponseOf(Long matricula, Long idRecomendacaoTecnica) {
		if (idRecomendacaoTecnica != null) {
			return recomendacaoTecnicaResponseOf(idRecomendacaoTecnica);
		}
		return recomendacoesTecnicasUltimosNoventaDiasOf(matricula);
	}

	private RecomendacaoTecnica geraRecomendacaoTecnica(ServiceRecord serviceRecord, List<File> imagens) {
		String matricula = serviceRecord.getCustomerId();
		Usuario usuario = usuarioService.buscaUsuario(matricula);
		checkNotNull(usuario, "Usuário inexistente na base do portal do cooperado");

		Produto produto = Produto.fromStringValue(serviceRecord.getCultureInfoDescription());
		RecomendacaoTecnica recomendacaoTecnica = recomendacaoTecnicaOf(serviceRecord, usuario, produto);
		RecomendacaoTecnica saved = saveOrUpdate(recomendacaoTecnica);

		recomendacaoTecnicaDiagnosticoService.processaRecomendacaoTecnicaDiagnosticos(saved, serviceRecord);
		recomendacaoTecnicaProdutoService.processaRecomendacaoTecnicaProdutos(saved, serviceRecord);
		recomendacaoTecnicaImagemService.processaRecomendacaoTecnicaImagens(recomendacaoTecnica, imagens);
		return saved;
	}

	private RecomendacaoTecnica recomendacaoTecnicaOf(ServiceRecord serviceRecord, Usuario usuario, Produto produto) {
		RecomendacaoTecnica recomendacaoTecnica = findOrCreateRecomendacao(serviceRecord.getFileName());
		recomendacaoTecnica.setUsuario(usuario);
		recomendacaoTecnica.setCriacao(serviceRecord.getOperationInfoCreateDate());
		recomendacaoTecnica.setEficiencia(serviceRecord.getEfficiency());
		recomendacaoTecnica.setNomeArquivo(serviceRecord.getFileName());
		recomendacaoTecnica.setRecomendacao(serviceRecord.getRecommendation());
		recomendacaoTecnica.setTotalArea(serviceRecord.getTotalArea());
		recomendacaoTecnica.setVolume(serviceRecord.getVolume());
		recomendacaoTecnica.setProduto(produto);
		recomendacaoTecnica.setUrgente(serviceRecord.getUrgent());
		recomendacaoTecnica.setMensagemCustomizada(serviceRecord.getCustomMessage());
		return recomendacaoTecnica;
	}

	private RecomendacaoTecnica findOrCreateRecomendacao(String fileName) {
		RecomendacaoTecnica recomendacaoTecnica = recomendacaoTecnicaDao.findOneByFileName(fileName);
		if (recomendacaoTecnica != null) {
			return recomendacaoTecnica;
		}
		return RecomendacaoTecnica.of();
	}

	private Response recomendacaoTecnicaResponseOf(Long id) {
		RecomendacaoTecnica recomendacaoTecnica = recomendacaoTecnicaDao.findById(id);
		if (recomendacaoTecnica != null) {
			RecomendacaoTecnicaJson recomendacaoTecnicaJson = RecomendacaoTecnicaJson
					.builder(recomendacaoTecnica)
					.build();
			return Responses.ok(recomendacaoTecnicaJson);
		}
		MensagemJson mensagemJson = MensagemJson
				.of("Nenhuma recomendação encontrada para o id: " + id + ". Contate o suporte!");
		return Responses.ok(mensagemJson);
	}

	private Response recomendacoesTecnicasUltimosNoventaDiasOf(Long matricula) {
		List<RecomendacaoTecnicaJson> recomendacaoTecnicaJsons = recomendacoesTecnicasJsonUltimosNoventaDiasOf(
				matricula);
		return Responses.ok(recomendacaoTecnicaJsons);
	}

	private List<RecomendacaoTecnicaJson> recomendacoesTecnicasJsonUltimosNoventaDiasOf(Long matricula) {
		ImmutableList.Builder<RecomendacaoTecnicaJson> builder = ImmutableList.builder();
		List<RecomendacaoTecnica> recomendacoes = recomendacaoTecnicaDao.findLastNinetyDaysByMatricula(matricula);
		for (RecomendacaoTecnica recomendacaoTecnica : recomendacoes) {
			RecomendacaoTecnicaJson json = RecomendacaoTecnicaJson.builder(recomendacaoTecnica).build();
			builder.add(json);
		}
		return builder.build();
	}

}