package br.com.db1.portalcooperado.service.impl;

import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.MatriculasEnviarNotificacao;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.NotificacaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.ParametrosJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.recomendacaotecnica.RecomendacaoTecnicaParametrosJson;
import br.com.db1.portalcooperado.service.NotificacaoMobileService;
import br.com.db1.portalcooperado.service.NotificacaoRecomendacaoTecnicaService;
import br.com.db1.portalcooperado.service.UsuarioService;
import br.com.db1.portalcooperado.util.notificacaostringbuilder.NotificacaoMensagemBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "notificacaoRecomendacaoTecnicaService")
@Transactional(rollbackFor = Exception.class)
public class NotificacaoRecomendacaoTecnicaServiceImpl implements NotificacaoRecomendacaoTecnicaService {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private NotificacaoMobileService notificacaoMobileService;

    @Override
    public void enviarNotificacaoMobile(RecomendacaoTecnica recomendacaoTecnica) {
        String mensagem = NotificacaoMensagemBuilderFactory.recomendacaoTecnica(recomendacaoTecnica).build();
        Usuario usuario = usuarioService.buscaUsuarioFull(recomendacaoTecnica.matriculaAsString());
        NotificacaoJson notificacaoJson = NotificacaoJson.builder()
                .withTitulo("Nova recomendação técnica")
                .withMensagem(mensagem)
                .withMatriculas(MatriculasEnviarNotificacao.of(usuario, PermissaoIndicado.ACTRECOMENDACAOTECNICA))
                .withParametros(parametrosJsonOf(recomendacaoTecnica))
                .build();
        notificacaoMobileService.enviarNotificacaoMobile(notificacaoJson);
    }

    private ParametrosJson parametrosJsonOf(RecomendacaoTecnica recomendacaoTecnica) {
        return RecomendacaoTecnicaParametrosJson.builder()
                .withTipo(TipoNotificacao.RECOMENDACAO_TECNICA)
                .withRecomendacaoTecnica(recomendacaoTecnica.getId())
                .build();
    }

}