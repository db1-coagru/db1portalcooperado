package br.com.db1.portalcooperado.service.impl;

import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.NotificacaoJson;
import br.com.db1.portalcooperado.service.NotificacaoMobileService;
import br.com.db1.portalcooperado.util.SGCRestClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;

@Service(value = "notificacaoMobileService")
@Transactional(rollbackFor = Exception.class)
public class NotificacaoMobileServiceImpl implements NotificacaoMobileService {

    @Override
    public void enviarNotificacaoMobile(NotificacaoJson notificacaoJson) {
        Response response = SGCRestClient.getWebTarget()
                .path("coagru")
                .path("integracao")
                .path("db1")
                .path("notificacao_automatica")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(notificacaoJson, MediaType.APPLICATION_JSON));

        int status = response.getStatus();
        Response.StatusType statusInfo = response.getStatusInfo();
        String mensagem = response.readEntity(String.class);
        checkArgument(status == 200, String.format("%s - %s : %s", status, statusInfo, mensagem));
    }

}
