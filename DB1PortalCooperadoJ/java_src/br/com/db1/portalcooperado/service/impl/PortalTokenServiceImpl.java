package br.com.db1.portalcooperado.service.impl;

import br.com.db1.myBatisPersistence.service.GenericMyBatisServiceImpl;
import br.com.db1.portalcooperado.dao.TokenDao;
import br.com.db1.portalcooperado.model.entity.Token;
import br.com.db1.portalcooperado.model.types.StatusToken;
import br.com.db1.portalcooperado.service.PortalTokenService;
import br.com.db1.security.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementação da <code>Interface</code> que define os métodos de negócio
 * Genéricos. Define o nome do <code>Service</code> que será criado no
 * <code>Spring</code> e o modelo <code>Transacional</code>. <br/>
 * <p>
 * Esta classe deve conter a implementação das regras de negócios necessária
 * para o <code>Model</code>.
 *
 * @author rafael.rasso
 */
@Service(value = "tokenService")
@Transactional(rollbackFor = Exception.class)
public class PortalTokenServiceImpl extends GenericMyBatisServiceImpl<Token, Long>
		implements PortalTokenService {

	@Autowired
	private TokenDao tokenDao;

	/**
	 * Irá gerar o token para o usuário recebido por parâmetro
	 *
	 * @param user
	 * @return Token
	 */
	@Override
	public Token generateTokenTo(User user) {
		Token token = Token.builder(user).build();
		Token saved = tokenDao.save(token);
		invalidarTokens(user, saved);
		return saved;
	}

	/**
	 * Irá validar se o token para recebido por parâmetro existe no banco, não expirou e ainda é válido.
	 *
	 * @param accessToken
	 * @return boolean
	 */
	@Override
	public boolean validateToken(String accessToken) {
		Token token = tokenDao.findByAccessToken(accessToken);
		return token != null && !token.isExpirado() && token.isValido();
	}

	private void invalidarTokens(User user, Token saved) {
		Object[] condicao = { "pIdSecUser", user.getIdUser(),
				"pIdToken", saved.getId(), "pStatus", StatusToken.VALIDO };

		List<Token> tokensValidos = tokenDao.findByCondition("ID_SEC_USER = :pIdSecUser " +
				"and ID_TOKEN != :pIdToken " +
				"and TP_STATUS = :pStatus", condicao);

		for (Token tokenAtivo : tokensValidos) {
			tokenAtivo.invalidar();
			tokenDao.update(tokenAtivo);
		}
	}

}
