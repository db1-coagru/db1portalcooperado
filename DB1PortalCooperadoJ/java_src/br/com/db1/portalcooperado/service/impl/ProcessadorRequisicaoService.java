package br.com.db1.portalcooperado.service.impl;

import br.com.db1.portalcooperado.model.builder.RequisicaoBuilder;
import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.Canal;
import br.com.db1.portalcooperado.requisicao.reponseprocessors.RequisicaoResponseProcessor;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.service.RequisicaoService;
import br.com.db1.portalcooperado.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;

import static br.com.db1.portalcooperado.relatorio.types.ReportType.JSON;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@Service
public class ProcessadorRequisicaoService {

	private final RequisicaoService requisicaoService;

	private final UsuarioService usuarioService;

	private final RequisicaoProcessResponseFactoryService factory;

	@Autowired
	public ProcessadorRequisicaoService(RequisicaoService requisicaoService, UsuarioService usuarioService,
			RequisicaoProcessResponseFactoryService factory) {
		this.requisicaoService = requisicaoService;
		this.usuarioService = usuarioService;
		this.factory = factory;
	}

	public Response processResponse(RequisicaoJson requisicaoJson, String authorization) {
		validateJson(requisicaoJson);
		Usuario usuario = usuarioService.buscaUsuarioByAccessToken(authorization);
		Requisicao requisicao = criaRequisicao(requisicaoJson, usuario);
		TipoRequisicao tipoRequisicao = requisicaoJson.getTipoRequisicao();
		RequisicaoResponseProcessor requisicaoResponseProcessor = factory.processadorOf(tipoRequisicao);
		return requisicaoResponseProcessor.processar(requisicao, requisicaoJson);
	}

	private void validateJson(RequisicaoJson json) {
		TipoRequisicao tipoRequisicao = json.getTipoRequisicao();
		String ip = json.getIp();
		checkNotNull(tipoRequisicao, "Tipo da requisição deve ser informado.");
		checkArgument(ip != null && !ip.trim().isEmpty(), "IP do usuário da requisição deve ser informado.");
		if (TipoRequisicao.ENTREGAS_PRODUCAO.equals(tipoRequisicao)) {
			checkNotNull(json.getDataInicial(), "Data inicial deve ser informada.");
			checkNotNull(json.getDataFinal(), "Data final deve ser informada.");
			checkArgument(json.temAlgumTipoCultura(), "Alguma cultura deve ser informada.");
		}
	}

	private Requisicao criaRequisicao(RequisicaoJson json, Usuario usuario) {
		RequisicaoBuilder builder = Requisicao.builder(Canal.MOBILE);
		builder.setMatriculaUsuario(usuario.getMatricula());
		builder.setTipoRequisicao(json.getTiposAsLong());
		builder.setTipoProduto(json.getTipoProduto());
		builder.setDataInicial(json.getDataInicial());
		builder.setDataFinal(json.getDataFinal());
		builder.setReportType(JSON);
		builder.setIp(json.getIp());
		builder.setUsuario(usuario);
		builder.setMatriculaIndicador(json.getMatriculaAsLong());
		return requisicaoService.geraRequisicao(builder);
	}

}
