package br.com.db1.portalcooperado.service.impl;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.kernel.report.exception.DB1ReportGenerationException;
import br.com.db1.kernel.report.service.DB1ReportService;
import br.com.db1.portalcooperado.relatorio.types.PortalReport;
import br.com.db1.portalcooperado.service.PortalReportService;
import br.com.db1.portalcooperado.util.Mensagem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

@Service(value = "portalReportService")
@Transactional(rollbackFor = Exception.class)
public class PortalReportServiceImpl implements PortalReportService {

	private static final Log logger = LogFactory.getLog(PortalReportService.class);

	private final DB1ReportService db1ReportService;

	@Autowired
	public PortalReportServiceImpl(DB1ReportService db1ReportService) {
		this.db1ReportService = db1ReportService;
	}

	@Override
	public String createReport(PortalReport.Builder builder) {
		checkNotNull(builder, Mensagem.campoObrigatorioMasculino("Relatório"));
		try {
			PortalReport portalReport = builder.build();
			return db1ReportService.createReport(portalReport.getCaminho(),
					portalReport.getExportableValue(),
					portalReport.getDataSource(),
					portalReport.getParameters());
		}
		catch (DB1ValidateException e) {
			String reportProblem = e.getMessage();
			logger.error(reportProblem, e);
			throw new DB1ValidateException(reportProblem, e);
		}
		catch (DB1ReportGenerationException e) {
			String reportProblem = Mensagem.createReportProblem(e.getMessage());
			logger.error(reportProblem, e);
			throw new DB1ReportGenerationException(reportProblem, e);
		}
		catch (Exception ex) {
			logger.error(Mensagem.ERRO_INESPERADO);
			throw new DB1ReportGenerationException(Mensagem.ERRO_INESPERADO, ex);
		}
	}
}
