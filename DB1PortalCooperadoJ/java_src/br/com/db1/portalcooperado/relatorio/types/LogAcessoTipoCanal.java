package br.com.db1.portalcooperado.relatorio.types;

public enum LogAcessoTipoCanal {

	TODOS {
		@Override
		public void append(StringBuilder builder) {
			// usuado para setar qual canal usar, quando thodos nao faz nada
		}
	},

	PORTAL {
		@Override
		public void append(StringBuilder builder) {
			builder.append(" AND req.tp_canal = 1");
		}
	},

	MOBILE {
		@Override
		public void append(StringBuilder builder) {
			builder.append(" AND req.tp_canal = 2");
		}
	};

	public abstract void append(StringBuilder builder);

	public boolean isTodos() {
		return TODOS.equals(this);
	}
}
