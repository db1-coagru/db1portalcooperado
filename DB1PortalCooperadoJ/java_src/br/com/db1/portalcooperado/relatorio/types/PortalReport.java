package br.com.db1.portalcooperado.relatorio.types;

import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;

import java.util.List;

import static br.com.db1.portalcooperado.util.Mensagem.REPORT_IS_BLANK;
import static br.com.db1.portalcooperado.util.Mensagem.campoObrigatorioMasculino;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public class PortalReport {

	private final String caminho;

	private final ReportType reportType;

	private final List<Object> dataSource;

	private final List<Object> parameters;

	private PortalReport(Builder builder) {
		this.caminho = builder.caminho;
		this.reportType = builder.reportType;
		this.dataSource = builder.dataSource();
		this.parameters = builder.parameters();
	}

	public static Builder builder() {
		return new Builder();
	}

	public String getTipo() {
		return reportType.getValor();
	}

	public String getExportableValue() {
		return reportType.getExportableValue();
	}

	public String getCaminho() {
		return caminho;
	}

	public List<Object> getDataSource() {
		return dataSource;
	}

	public List<Object> getParameters() {
		return parameters;
	}

	public static class Builder {

		private final List<Object> parameters = Lists.newLinkedList();

		private final List<Object> dataSource = Lists.newLinkedList();

		private String caminho;

		private ReportType reportType;

		private Builder() {

		}

		public PortalReport build() {
			checkNotNull(caminho, campoObrigatorioMasculino("Caminho do relatório"));
			checkNotNull(reportType, campoObrigatorioMasculino("Tipo do relatório"));
			checkArgument(!dataSource.isEmpty(), REPORT_IS_BLANK);
			return new PortalReport(this);
		}

		public Builder withCaminho(String caminho) {
			this.caminho = caminho;
			return this;
		}

		public Builder withTipo(ReportType reportType) {
			this.reportType = reportType;
			return this;
		}

		public Builder withParam(Object paramName, Object paramValue) {
			checkNotNull(paramName, "Nome do parâmetro do relatório deve ser informado");
			parameters.add(paramName);
			parameters.add(paramValue);
			return this;
		}

		public Builder addDataSource(Object object) {
			this.dataSource.add(object);
			return this;
		}

		public Builder addDataSource(List<? extends Object> objects) {
			this.dataSource.addAll(objects);
			return this;
		}

		private List<Object> dataSource() {
			return ImmutableList.copyOf(dataSource);
		}

		private List<Object> parameters() {
			return ImmutableList.copyOf(parameters);
		}

	}

}
