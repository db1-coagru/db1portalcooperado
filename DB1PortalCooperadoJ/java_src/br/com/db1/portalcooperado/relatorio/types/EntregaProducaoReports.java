package br.com.db1.portalcooperado.relatorio.types;

public class EntregaProducaoReports {

	public static final String ENTREGA_PRODUCAO_PAISAGEM = "/br/com/db1/portalcooperado/relatorio/EntregaProducaoPaisagem.jasper";

	public static final String ENTREGA_PRODUCAO = "/br/com/db1/portalcooperado/relatorio/EntregaProducao.jasper";


	private EntregaProducaoReports() {

	}


	
}
