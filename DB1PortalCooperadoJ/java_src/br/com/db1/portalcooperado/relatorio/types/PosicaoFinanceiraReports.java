package br.com.db1.portalcooperado.relatorio.types;

public class PosicaoFinanceiraReports {

	public static final String POSICAO_FINANCEIRA_NOME = "EXTRATO DE POSIÇÃO FINANCEIRA";

	public static final String POSICAO_FINANCEIRA_PAISAGEM = "/br/com/db1/portalcooperado/relatorio/PosicaoFinanceiraPaisagem.jasper";

	public static final String POSICAO_FINANCEIRA = "/br/com/db1/portalcooperado/relatorio/PosicaoFinanceira.jasper";

	public static final String POSICAO_FINANCEIRA_CREDITO = "/br/com/db1/portalcooperado/relatorio/PosicaoFinanceiraCreditos.jasper";

	public static final String POSICAO_FINANCEIRA_DEBITO = "/br/com/db1/portalcooperado/relatorio/PosicaoFinanceiraDebitos.jasper";

	public static final String POSICAO_FINANCEIRA_DEBITO_CREDITO = "/br/com/db1/portalcooperado/relatorio/PosicaoFinanceiraDebitoCredito.jasper";

	public static final String POSICAO_FINANCEIRA_OUTROS = "/br/com/db1/portalcooperado/relatorio/PosicaoFinanceiraOutros.jasper";

	public static final String POSICAO_FINANCEIRA_CREDITO_PAISAGEM = "/br/com/db1/portalcooperado/relatorio/PosicaoFinanceiraCreditosPaisagem.jasper";

	public static final String POSICAO_FINANCEIRA_DEBITO_PAISAGEM = "/br/com/db1/portalcooperado/relatorio/PosicaoFinanceiraDebitosPaisagem.jasper";

	public static final String POSICAO_FINANCEIRA_DEBITO_CREDITO_PAISAGEM = "/br/com/db1/portalcooperado/relatorio/PosicaoFinanceiraDebitoCreditoPaisagem.jasper";

	public static final String POSICAO_FINANCEIRA_OUTROS_PAISAGEM = "/br/com/db1/portalcooperado/relatorio/PosicaoFinanceiraOutrosPaisagem.jasper";

	private PosicaoFinanceiraReports() {

	}

}
