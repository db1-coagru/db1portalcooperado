package br.com.db1.portalcooperado.relatorio.types;

import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.collect.ImmutableMap;

import java.util.Date;
import java.util.Map;

import static br.com.db1.portalcooperado.relatorio.types.EntregaProducaoReports.ENTREGA_PRODUCAO;
import static br.com.db1.portalcooperado.relatorio.types.EntregaProducaoReports.ENTREGA_PRODUCAO_PAISAGEM;
import static br.com.db1.portalcooperado.relatorio.types.PosicaoFinanceiraReports.*;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkArgument;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.checkNotNull;

public enum ReportType {

	HTML("html", "html") {
		@Override
		public void appendSubReports(PortalReport.Builder builder) {
			builder.withParam("subPosicaoFinanceiraCreditos", POSICAO_FINANCEIRA_CREDITO_PAISAGEM);
			builder.withParam("subPosicaoFinanceiraDebitos", POSICAO_FINANCEIRA_DEBITO_PAISAGEM);
			builder.withParam("subPosicaoFinanceiraDebitoCredito", POSICAO_FINANCEIRA_DEBITO_CREDITO_PAISAGEM);
			builder.withParam("subPosicaoFinanceiraOutros", POSICAO_FINANCEIRA_OUTROS_PAISAGEM);
		}

		@Override
		public String getRelatorioPosicaoFinanceiraExecutar() {
			return POSICAO_FINANCEIRA_PAISAGEM;
		}

		@Override
		public String getRelatorioEntregaProducaoExecutar() {
			return ENTREGA_PRODUCAO_PAISAGEM;
		}
	},

	PDF("pdf", "pdf") {
		@Override
		public void appendSubReports(PortalReport.Builder builder) {
			builder.withParam("subPosicaoFinanceiraCreditos", POSICAO_FINANCEIRA_CREDITO);
			builder.withParam("subPosicaoFinanceiraDebitos", POSICAO_FINANCEIRA_DEBITO);
			builder.withParam("subPosicaoFinanceiraDebitoCredito", POSICAO_FINANCEIRA_DEBITO_CREDITO);
			builder.withParam("subPosicaoFinanceiraOutros", POSICAO_FINANCEIRA_OUTROS);
		}

		@Override
		public String getRelatorioPosicaoFinanceiraExecutar() {
			return POSICAO_FINANCEIRA;
		}

		@Override
		public String getRelatorioEntregaProducaoExecutar() {
			return ENTREGA_PRODUCAO;
		}
	},

	JSON("json", "pdf") {
		@Override
		public void appendSubReports(PortalReport.Builder builder) {
			PDF.appendSubReports(builder);
		}

		@Override
		public String getRelatorioPosicaoFinanceiraExecutar() {
			return PDF.getRelatorioPosicaoFinanceiraExecutar();
		}

		@Override
		public String getRelatorioEntregaProducaoExecutar() {
			return PDF.getRelatorioEntregaProducaoExecutar();
		}

		@Override
		public Date getDataOperacaoAtMidnight() {
			return PortalCooperadoUtil.getDataAtMidnight();
		}

		@Override
		public String getNomeCooperativa() {
			return "COAGRU COOPERATIVA AGROINDUSTRIAL UNIÃO";
		}

		@Override
		public Date getDataOperacao() {
			return new Date();
		}
	};

	private static final Map<String, ReportType> valueMap;

	static {
		ImmutableMap.Builder<String, ReportType> builder = ImmutableMap.builder();
		for (ReportType reportType : values()) {
			builder.put(reportType.valor, reportType);
		}
		valueMap = builder.build();
	}

	private final String valor;

	private final String exportableValue;

	ReportType(String valor, String exportableValue) {
		this.valor = valor;
		this.exportableValue = exportableValue;
	}

	public static ReportType fromString(String valor) {
		checkNotNull(valor, "Tipo de relatório deve ser informado.");
		checkArgument(valueMap.containsKey(valor), String.format("Tipo de relatório informado %s inválido.", valor));
		return valueMap.get(valor);
	}

	public abstract void appendSubReports(PortalReport.Builder builder);

	public abstract String getRelatorioPosicaoFinanceiraExecutar();

	public abstract String getRelatorioEntregaProducaoExecutar();

	public Date getDataOperacaoAtMidnight() {
		return PortalCooperadoUtil.getDataOperacaoAtMidnight();
	}

	public String getNomeCooperativa() {
		return PortalCooperadoUtil.getNomeCooperativa();
	}

	public Date getDataOperacao() {
		return PortalCooperadoUtil.getDataOperacao();
	}

	public String getValor() {
		return valor;
	}

	public String getExportableValue() {
		return exportableValue;
	}

}
