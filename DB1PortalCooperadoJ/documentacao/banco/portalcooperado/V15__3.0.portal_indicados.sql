-- DROP SEQUENCE SEQ_INDICACAO;
-- DROP SEQUENCE SEQ_INDICACAO_PERMISSAO;
-- DROP TABLE COOP_INDICACAO_PERMISSAO;
-- DROP TABLE COOP_INDICACAO;

CREATE SEQUENCE SEQ_INDICACAO MINVALUE 1 MAXVALUE 9999999999999999999 INCREMENT BY 1 START WITH 4000 CACHE 20 NOORDER NOCYCLE;
CREATE SEQUENCE SEQ_INDICACAO_PERMISSAO MINVALUE 1 MAXVALUE 9999999999999999999 INCREMENT BY 1 START WITH 4000 CACHE 20 NOORDER NOCYCLE;

CREATE TABLE COOP_INDICACAO (
  ID_INDICACAO NUMBER(19) NOT NULL PRIMARY KEY,
  ID_INDICADOR NUMBER(19) NOT NULL,
  ID_INDICADO  NUMBER(19) NOT NULL
);

ALTER TABLE COOP_INDICACAO
  ADD CONSTRAINT FK_INDICACAO_INDICADOR FOREIGN KEY (ID_INDICADOR)
REFERENCES COOP_USUARIO (ID_USUARIO);

ALTER TABLE COOP_INDICACAO
  ADD CONSTRAINT FK_INDICACAO_INDICADO FOREIGN KEY (ID_INDICADO)
REFERENCES COOP_USUARIO (ID_USUARIO);

CREATE INDEX IDX_INDICACAO_INDICADOR
  ON COOP_INDICACAO (ID_INDICADOR);
CREATE INDEX IDX_INDICACAO_INDICADO
  ON COOP_INDICACAO (ID_INDICADO);
CREATE UNIQUE INDEX IDX_IND_INDICADOR_INDICADO
  ON COOP_INDICACAO (ID_INDICADOR, ID_INDICADO);


CREATE TABLE COOP_INDICACAO_PERMISSAO (
  ID_INDICACAO_PERMISSAO NUMBER(19) NOT NULL PRIMARY KEY,
  ID_INDICACAO NUMBER(19) NOT NULL,
  DS_PERMISSAO VARCHAR2 (30)  NOT NULL
);

CREATE INDEX IDX_INDIC_PERMISSAO_INDICACAO
ON COOP_INDICACAO_PERMISSAO (ID_INDICACAO);