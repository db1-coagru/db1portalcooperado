/*
*****************************************************************************
*****************************************************************************
****************************** ATENÇÃO: *************************************
** UTILIZAR SOMENTE EM AMBIENTE DE TESTE/HOMOLOGACAO PARA GERAR DADOS PARA **
**         GERAÇÃO DOS EXTRATO NÃO RODAR EM AMBIENTE DE PRODUÇÃO.          **
*****************************************************************************
*****************************************************************************
*****************************************************************************
*/

-- DELETE FROM SEC_SYSTEM_INSTANCE;
-- DELETE FROM SEC_SYSTEM;
Insert into SEC_SYSTEM (ID,NAME,DESCRIPTION,ORGANIZATIONAL_STRUCTURE_NAME) values ('1','DB1PORTALCOOPERADO','DB1PORTALCOOPERADO',null);
Insert into SEC_SYSTEM (ID,NAME,DESCRIPTION,ORGANIZATIONAL_STRUCTURE_NAME) values ('2','CORESECURITY','CORESECURITY',null);
Insert into SEC_SYSTEM_INSTANCE (ID,DESCRIPTION,IDENTIFICATION,ID_SYSTEM,ID_SECURITY_SYSTEM_INSTANCE) values ('2','GERAL SECURITU','SECURITYBASE','2',null);
Insert into SEC_SYSTEM_INSTANCE (ID,DESCRIPTION,IDENTIFICATION,ID_SYSTEM,ID_SECURITY_SYSTEM_INSTANCE) values ('1','GERAL PORTALCOOPERADO ','BASEDESENV','1','2');
