/*
*****************************************************************************
*****************************************************************************
****************************** ATENÇÃO: *************************************
** UTILIZAR SOMENTE EM AMBIENTE DE TESTE/HOMOLOGACAO PARA GERAR DADOS PARA **
**         GERAÇÃO DOS EXTRATO NÃO RODAR EM AMBIENTE DE PRODUÇÃO.          **
*****************************************************************************
*****************************************************************************
*****************************************************************************
*/

-- DELETE FROM COOP_POS_FIN_CRED;
-- DELETE FROM COOP_POS_FIN_CRED_OBS;
-- DELETE FROM COOP_POS_FIN_DEB;
-- DELETE FROM COOP_POS_FIN_OUT;
-- DELETE FROM COOP_ENT_PROD_AGR;
BEGIN
  FOR i IN 1..1000 LOOP
    INSERT INTO COOP_POS_FIN_CRED (ID_REQUISICAO, NR_ROMANEIO, NR_SEQUENCIA, DS_LOCAL, DS_PRODUTO, NR_PADRAO, NR_TIPO, PS_LIQUIDO, VL_DIA, VL_LIQUIDO)
    VALUES (i, i, 1, 'Yolanda', 'Soja', 1, 2, i + 2 + 100, i + 2 + 200, i + 2 + 300);

    INSERT INTO COOP_POS_FIN_CRED (ID_REQUISICAO, NR_ROMANEIO, NR_SEQUENCIA, DS_LOCAL, DS_PRODUTO, NR_PADRAO, NR_TIPO, PS_LIQUIDO, VL_DIA, VL_LIQUIDO)
    VALUES (i, i, 2, 'Anahy', 'Soja', 1, 2, i + 2 + 100, i + 2 + 200, i + 2 + 300);

    INSERT INTO COOP_POS_FIN_CRED (ID_REQUISICAO, NR_ROMANEIO, NR_SEQUENCIA, DS_LOCAL, DS_PRODUTO, NR_PADRAO, NR_TIPO, PS_LIQUIDO, VL_DIA, VL_LIQUIDO)
    VALUES (i, i, 3, 'Ubirata', 'Milho', 2, 3, i + 3 + 100, i + 3 + 200, i + 3 + 300);

    INSERT INTO COOP_POS_FIN_CRED (ID_REQUISICAO, NR_ROMANEIO, NR_SEQUENCIA, DS_LOCAL, DS_PRODUTO, NR_PADRAO, NR_TIPO, PS_LIQUIDO, VL_DIA, VL_LIQUIDO)
    VALUES (i, i, 4, 'Anahy', 'Trigo', 3, 4, i + 4 + 100, i + 4 + 200, i + 4 + 300);
  END LOOP;
END;
/
BEGIN
  FOR i IN 1..1000 LOOP
    IF MOD(i, 2) = 0
    THEN
      INSERT INTO COOP_POS_FIN_CRED_OBS (ID_REQUISICAO, MSG_CREDITO) VALUES (i, 'Mensagem Credito teste ' || i);
    END IF;

  END LOOP;
END;
/

BEGIN
  FOR i IN 1..1000 LOOP
    FOR j IN 1..5 LOOP

      INSERT INTO COOP_POS_FIN_DEB (ID_REQUISICAO, DS_PRODUTO, NR_DOCUMENTO, SERIE, NR_PARCELA, NR_FILIAL, DT_LANCAMENTO, DT_VENCIMENTO, VL_PRODUTO, VL_JUROS, VL_LIQUIDO, BO_CREDITO)
      VALUES
        (i, 'PRODUTO PRODUZIDO PARA TESTES NO AMBIENTE HOMOLOGACAO ' || i, i + j, i, 1, 1, sysdate + j, sysdate + j, 100 + i + j, 0.0001 * i + j, (200 + i + j) + 0.001 * i + j, '2');

    END LOOP;

  END LOOP;
END;
/

BEGIN
  FOR i IN 1..1000 LOOP
    FOR j IN 1..3 LOOP

      INSERT INTO COOP_POS_FIN_DEB (ID_REQUISICAO, DS_PRODUTO, NR_DOCUMENTO, SERIE, NR_PARCELA, NR_FILIAL, DT_LANCAMENTO, DT_VENCIMENTO, VL_PRODUTO, VL_JUROS, VL_LIQUIDO, BO_CREDITO)
      VALUES
        (i, 'PRODUTO PRODUZIDO PARA TESTES NO AMBIENTE HOMOLOGACAO ' || i, i + j, i, 1, 1, sysdate + j, sysdate + j, 100 + i + j, 0.0001 * i + j, (200 + i + j) + 0.001 * i + j, '1');

    END LOOP;

  END LOOP;
END;
/

BEGIN
  FOR i IN 1..1000 LOOP
    IF MOD(i, 2) = 0
    THEN
      INSERT INTO COOP_POS_FIN_OUT (ID_REQUISICAO, DS_TIPO, DS_PRODUTO, VL_QUANTIDADE, DT_VENCIMENTO, VL_PRODUTO)
      VALUES (i, 'Tipo teste ' || i, 'Produto teste ' || i, 100 + i, sysdate, 100.01 + i);
    END IF;

  END LOOP;
END;
/

BEGIN
  FOR i IN 1..1000 LOOP
    INSERT INTO COOP_ENT_PROD_AGR (ID_REQUISICAO, DT_ENTREGA, DS_PRODUTO, CD_ORDENACAO, DS_LOCAL, NR_ROMANEIO, NR_SEQUENCIA, NR_CAD_PRO, PL_VEICULO, NR_PADRAO,
                                   NR_TIPO, PS_BRUTO, PS_LIQUIDO, PC_IMPUREZA, PC_QUEBRADO, PC_ARDIDO, PC_UMIDADE)
    VALUES (i, sysdate + 1, 'Soja', 2, 'Yolanda', i, 1, 2, 'AAA1000', 1, 2, 15.55 * i,
            15.55 * i - 2, 15.55 * 2, 1.62 * 2, 0.15 * 2, 0.05 * 2);

    INSERT INTO COOP_ENT_PROD_AGR (ID_REQUISICAO, DT_ENTREGA, DS_PRODUTO, CD_ORDENACAO, DS_LOCAL, NR_ROMANEIO, NR_SEQUENCIA, NR_CAD_PRO, PL_VEICULO, NR_PADRAO,
                                   NR_TIPO, PS_BRUTO, PS_LIQUIDO, PC_IMPUREZA, PC_QUEBRADO, PC_ARDIDO, PC_UMIDADE)
    VALUES (i, sysdate + 2, 'Soja', 2, 'Anahy', i, 2, 2, 'AAA1000', 1, 2, 15.55 * i,
            15.55 * i - 2, 15.55 * 2, 1.62 * 2, 0.15 * 2, 0.05 * 2);

    INSERT INTO COOP_ENT_PROD_AGR (ID_REQUISICAO, DT_ENTREGA, DS_PRODUTO, CD_ORDENACAO, DS_LOCAL, NR_ROMANEIO, NR_SEQUENCIA, NR_CAD_PRO, PL_VEICULO, NR_PADRAO,
                                   NR_TIPO, PS_BRUTO, PS_LIQUIDO, PC_IMPUREZA, PC_QUEBRADO, PC_ARDIDO, PC_UMIDADE)
    VALUES (i, sysdate + 2, 'Milho', 3, 'Ubirata', i, 3, 3, 'AAA1001', 2, 3, 15.55 * i,
            15.55 * i - 3, 15.55 * 3, 1.62 * 3, 0.15 * 3, 0.05 * 3);

    INSERT INTO COOP_ENT_PROD_AGR (ID_REQUISICAO, DT_ENTREGA, DS_PRODUTO, CD_ORDENACAO, DS_LOCAL, NR_ROMANEIO, NR_SEQUENCIA, NR_CAD_PRO, PL_VEICULO, NR_PADRAO,
                                   NR_TIPO, PS_BRUTO, PS_LIQUIDO, PC_IMPUREZA, PC_QUEBRADO, PC_ARDIDO, PC_UMIDADE)
    VALUES (i, sysdate + 3, 'Trigo', 3, 'Anahy', i, 4, 4, 'AAA1002', 3, 4, 15.55 * i,
            15.55 * i - 4, 15.55 * 4, 1.62 * 4, 0.15 * 4, 0.05 * 4);

  END LOOP;
END;
/

BEGIN
  FOR i IN 36000..37000 LOOP
      INSERT INTO COOP_INF_COMPL (ID_REQUISICAO, DS_RETORNO_LOCAL_ORIGEM, BO_ACESSA_USUARIO) VALUES (i, 'Ds Retorno local origem ' || i, '1');
  END LOOP;
END;
/

COMMIT;