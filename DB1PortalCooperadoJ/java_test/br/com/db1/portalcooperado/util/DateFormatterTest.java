package br.com.db1.portalcooperado.util;

import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class DateFormatterTest {

	@Test
	public void formatDayMonthYearIsIncorrect() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2016, Calendar.AUGUST, 12);
		Date date = calendar.getTime();
		assertThat(DateFormatter.DD_MM_YYYY.format(date)).isEqualTo("12/08/2016");
	}

	@Test
	public void formatDayMonthYearHourMinutesSecondsIsIncorrect() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2016, Calendar.AUGUST, 12, 13, 44, 10);
		Date date = calendar.getTime();
		assertThat(DateFormatter.DD_MM_YYYY_HH_MM_SS.format(date)).isEqualTo("12/08/2016 13:44:10");
	}

}