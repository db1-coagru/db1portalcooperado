package br.com.db1.portalcooperado.util.notificacaostringbuilder;


import br.com.db1.portalcooperado.model.entity.Notificacao;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class FechamentoFrangoNotificacaoMensagemBuilderTest {

    @Test
    public void montagemMensagemFechamentoFrangoEstaIncorreta() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(2010, 9, 10);

        Notificacao notificacao = Notificacao.of();
        notificacao.setTipo(TipoNotificacao.FECHAMENTO_DE_FRANGO);
        notificacao.setFilial("YOLANDA");
        notificacao.setValor(BigDecimal.valueOf(20.00));
        notificacao.setData(calendar.getTime());
        notificacao.setHora("12:12");
        notificacao.setDocumento(1234L);

        NotificacaoMensagemBuilder builder = FechamentoFrangoNotificacaoMensagemBuilder.of(notificacao);
        String expected = "Fechamento de frango: valor bruto de R$ 20,00 pagamento 10/10/2010.";
        assertThat(builder.build()).isEqualTo(expected);
    }

}