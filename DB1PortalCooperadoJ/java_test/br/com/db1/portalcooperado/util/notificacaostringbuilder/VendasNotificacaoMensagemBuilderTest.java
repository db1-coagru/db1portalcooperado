package br.com.db1.portalcooperado.util.notificacaostringbuilder;


import br.com.db1.portalcooperado.model.entity.Notificacao;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class VendasNotificacaoMensagemBuilderTest {

    @Test
    public void deveMontarMensagemSemLote() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(2010, 9, 10);

        Notificacao notificacao = Notificacao.of();
        notificacao.setTipo(TipoNotificacao.VENDA_PECAS);
        notificacao.setFilial("YOLANDA");
        notificacao.setValor(BigDecimal.valueOf(20.00));
        notificacao.setData(calendar.getTime());
        notificacao.setHora("12:12");
        notificacao.setDocumento(1234L);

        NotificacaoMensagemBuilder builder = VendasNotificacaoMensagemBuilder.of(notificacao);
        String expected = "Venda de peças - Realizado em yolanda no dia 10/10/2010 às 12:12 no valor de R$ 20,00 sob o documento 1234.";
        assertThat(builder.build()).isEqualTo(expected);
    }

    @Test
    public void deveMontarMensagemComLote() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(2010, 9, 10);

        Notificacao notificacao = Notificacao.of();
        notificacao.setTipo(TipoNotificacao.VENDA_PECAS);
        notificacao.setFilial("YOLANDA");
        notificacao.setValor(BigDecimal.valueOf(20.00));
        notificacao.setData(calendar.getTime());
        notificacao.setHora("12:12");
        notificacao.setDocumento(1234L);
        notificacao.setLote("CABO ALTO");

        NotificacaoMensagemBuilder builder = VendasNotificacaoMensagemBuilder.of(notificacao);
        String expected = "Venda de peças - Realizado em yolanda no dia 10/10/2010 às 12:12 no valor de R$ 20,00 sob o documento 1234 " +
            "correspondente ao lote cabo alto.";
        assertThat(builder.build()).isEqualTo(expected);
    }


}