package br.com.db1.portalcooperado.util.notificacaostringbuilder;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.model.entity.Notificacao;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NotificacaoMensagemBuilderFactoryTest {

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Notificação deve ser informada.")
	public void romaneioNaoDeveriaTerCriadoMensagemBuilderSemNotificacao() throws Exception {
		NotificacaoMensagemBuilderFactory.romaneioAgricola(null);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
		expectedExceptionsMessageRegExp = "Recomendação Técnica deve ser informada.")
	public void recomendacaoTecnicaNaoDeveriaTerCriadoMensagemBuilderSemNotificacao() throws Exception {
		NotificacaoMensagemBuilderFactory.recomendacaoTecnica(null);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Notificação deve ser informada.")
	public void vendasNaoDeveriaTerCriadoMensagemBuilderSemNotificacao() throws Exception {
		NotificacaoMensagemBuilderFactory.vendas(null);
	}

	@Test
	public void deveriaTerCriadoMensagemBuilderDeRomaneio() throws Exception {
		Notificacao notificacao = mock(Notificacao.class);
		when(notificacao.isDeRomaneioAgricola()).thenReturn(true);
		NotificacaoMensagemBuilder builder = NotificacaoMensagemBuilderFactory.romaneioAgricola(notificacao);
		assertThat(builder).isInstanceOf(RomaneioAgricolaNotificacaoMensagemBuilder.class);
	}

	@Test
	public void deveriaTerCriadoMensagemBuilderDeVendas() throws Exception {
		Notificacao notificacao = mock(Notificacao.class);
		NotificacaoMensagemBuilder builder = NotificacaoMensagemBuilderFactory.vendas(notificacao);
		assertThat(builder).isInstanceOf(VendasNotificacaoMensagemBuilder.class);
	}

	@Test
	public void buildRomaneioNaoCriouStringCorretamente() throws Exception {
		Notificacao notificacao = notifiacaoOf(TipoNotificacao.ROMANEIO_AGRICOLA);
		NotificacaoMensagemBuilder builder = NotificacaoMensagemBuilderFactory.romaneioAgricola(notificacao);

		String expected = "Romaneio agrícola - Foram entregues 100kg de soja em ubiratã no dia 19/09/2016 às 10:10:10" +
						" pelo veículo AAA0000 sob o documento 12.";

		assertThat(builder.build()).isEqualTo(expected);
	}

	@Test
	public void buildVendasNaoCriouStringCorretamente() throws Exception {
		Notificacao notificacao = notifiacaoOf(TipoNotificacao.VENDA_INSUMOS);
		NotificacaoMensagemBuilder builder = NotificacaoMensagemBuilderFactory.vendas(notificacao);
		String expected = "Venda de insumos - Realizado em ubiratã no dia 19/09/2016 às 10:10:10 no valor de R$ 10,00 sob o documento 12.";
		assertThat(builder.build()).isEqualTo(expected);
	}

	private Notificacao notifiacaoOf(TipoNotificacao tipo) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.DAY_OF_MONTH, 19);
		calendar.set(Calendar.MONTH, 8);
		calendar.set(Calendar.YEAR, 2016);
		calendar.set(Calendar.HOUR_OF_DAY, 0);

		Notificacao notificacao = Notificacao.of();
		notificacao.setTipo(tipo);
		notificacao.setPeso(100L);
		notificacao.setProduto("Soja");
		notificacao.setFilial("Ubiratã");
		notificacao.setData(calendar.getTime());
		notificacao.setHora("10:10:10");
		notificacao.setPlaca("AAA0000");
		notificacao.setValor(BigDecimal.TEN);
		notificacao.setDocumento(12L);
		return notificacao;
	}
}