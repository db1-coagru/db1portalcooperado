package br.com.db1.portalcooperado.util.notificacaostringbuilder;


import br.com.db1.portalcooperado.model.entity.Notificacao;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class RomaneioAgricolaNotificacaoMensagemBuilderTest {

    @Test
    public void deveMontarMensagemSemLote() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(2010, 9, 10);

        Notificacao notificacao = Notificacao.of();
        notificacao.setTipo(TipoNotificacao.ROMANEIO_AGRICOLA);
        notificacao.setFilial("YOLANDA");
        notificacao.setPeso(10L);
        notificacao.setProduto("SOJA");
        notificacao.setData(calendar.getTime());
        notificacao.setHora("12:12");
        notificacao.setPlaca("AHM-1520");
        notificacao.setDocumento(1234L);

        NotificacaoMensagemBuilder builder = RomaneioAgricolaNotificacaoMensagemBuilder.of(notificacao);
        String expected = "Romaneio agrícola - Foram entregues 10kg de soja em yolanda no dia 10/10/2010 " +
            "às 12:12 pelo veículo AHM-1520 sob o documento 1234.";
        assertThat(builder.build()).isEqualTo(expected);
    }

    @Test
    public void deveMontarMensagemComLote() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(2010, 9, 10);

        Notificacao notificacao = Notificacao.of();
        notificacao.setTipo(TipoNotificacao.ROMANEIO_AGRICOLA);
        notificacao.setFilial("YOLANDA");
        notificacao.setPeso(10L);
        notificacao.setProduto("SOJA");
        notificacao.setData(calendar.getTime());
        notificacao.setHora("12:12");
        notificacao.setPlaca("AHM-1520");
        notificacao.setDocumento(1234L);
        notificacao.setLote("CABO ALTO");

        NotificacaoMensagemBuilder builder = RomaneioAgricolaNotificacaoMensagemBuilder.of(notificacao);
        String expected = "Romaneio agrícola - Foram entregues 10kg de soja em yolanda no dia 10/10/2010 " +
            "às 12:12 pelo veículo AHM-1520 sob o documento 1234 correspondente ao lote cabo alto.";
        assertThat(builder.build()).isEqualTo(expected);
    }


}