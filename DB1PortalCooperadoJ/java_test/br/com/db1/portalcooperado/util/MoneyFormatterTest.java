package br.com.db1.portalcooperado.util;

import org.testng.annotations.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class MoneyFormatterTest {

	@Test
	public void reaisFormatIncorreto() throws Exception {
		assertThat(MoneyFormatter.REAIS.format(BigDecimal.valueOf(10594.599))).isEqualTo("R$ 10.594,60");
	}

}