package br.com.db1.portalcooperado.dao;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDao;
import br.com.db1.portalcooperado.infrastructure.assertions.ClasseAssert;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class AbstractDaoTest {

	private final Class<?> klazz;

	AbstractDaoTest(Class<?> klazz) {
		this.klazz = klazz;
	}

	@Test
	public void deveSerUmaInterface() throws Exception {
		assertThat(klazz).isInterface();
	}

	@Test
	public void deveEstenderDeGenericMyBatisDao() throws Exception {
		ClasseAssert.assertThat(klazz).isHeritageOf(GenericMyBatisDao.class);
	}

}
