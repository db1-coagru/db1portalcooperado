package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaProdutoDao;

public class RecomendacaoTecnicaProdutoDaoImplTest extends AbstractDaoImplTest {

	protected RecomendacaoTecnicaProdutoDaoImplTest() {
		super(RecomendacaoTecnicaProdutoDaoImpl.class, RecomendacaoTecnicaProdutoDao.class);
	}

}