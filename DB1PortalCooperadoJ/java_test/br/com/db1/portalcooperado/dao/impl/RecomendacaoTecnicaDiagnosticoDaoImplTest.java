package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaDiagnosticoDao;

public class RecomendacaoTecnicaDiagnosticoDaoImplTest extends AbstractDaoImplTest {

	protected RecomendacaoTecnicaDiagnosticoDaoImplTest() {
		super(RecomendacaoTecnicaDiagnosticoDaoImpl.class, RecomendacaoTecnicaDiagnosticoDao.class);
	}

}