package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaImagemDao;

public class RecomendacaoTecnicaImagemDaoImplTest extends AbstractDaoImplTest {

	protected RecomendacaoTecnicaImagemDaoImplTest() {
		super(RecomendacaoTecnicaImagemDaoImpl.class, RecomendacaoTecnicaImagemDao.class);
	}

}