package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.EntregaProducaoDao;

public class EntregaProducaoDaoImplTest extends AbstractDaoImplTest {

	protected EntregaProducaoDaoImplTest() {
		super(EntregaProducaoDaoImpl.class, EntregaProducaoDao.class);
	}

}