package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.RecomendacaoTecnicaDao;

public class RecomendacaoTecnicaDaoImplTest extends AbstractDaoImplTest {

	protected RecomendacaoTecnicaDaoImplTest() {
		super(RecomendacaoTecnicaDaoImpl.class, RecomendacaoTecnicaDao.class);
	}

}