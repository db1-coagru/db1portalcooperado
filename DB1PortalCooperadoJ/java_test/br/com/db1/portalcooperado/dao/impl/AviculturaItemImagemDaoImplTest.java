package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.AviculturaItemImagemDao;

public class AviculturaItemImagemDaoImplTest extends AbstractDaoImplTest {

	protected AviculturaItemImagemDaoImplTest() {
		super(AviculturaItemImagemDaoImpl.class, AviculturaItemImagemDao.class);
	}

}