package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.AviculturaItemDao;

public class AviculturaItemDaoImplTest extends AbstractDaoImplTest {

	protected AviculturaItemDaoImplTest() {
		super(AviculturaItemDaoImpl.class, AviculturaItemDao.class);
	}

}