package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.IndicacaoPermissaoDao;

public class IndicacaoPermissaoDaoImplTest extends AbstractDaoImplTest {

	protected IndicacaoPermissaoDaoImplTest() {
		super(IndicacaoPermissaoDaoImpl.class, IndicacaoPermissaoDao.class);
	}

}