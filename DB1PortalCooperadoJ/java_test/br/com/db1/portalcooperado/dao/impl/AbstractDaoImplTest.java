package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.myBatisPersistence.dao.GenericMyBatisDaoImpl;
import br.com.db1.portalcooperado.infrastructure.assertions.ClasseAssert;
import org.springframework.stereotype.Repository;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class AbstractDaoImplTest {

	private final Class<?> klazz;

	private final Class<?> superklazz;

	AbstractDaoImplTest(Class<?> klazz, Class<?> superklazz) {
		this.klazz = klazz;
		this.superklazz = superklazz;
	}

	@Test
	public void desteEstarAnotadoComRepository() throws Exception {
		assertThat(klazz).hasAnnotation(Repository.class);
	}

	@Test
	public void deveEstenderDeGenericMyBatisDaoImpl() throws Exception {
		ClasseAssert.assertThat(klazz).isHeritageOf(GenericMyBatisDaoImpl.class);
	}

	@Test
	public void deveImplementarInterfaceQueEstendeDeGenericaMyBastisDao() throws Exception {
		assertThat(klazz.getInterfaces()).contains(superklazz);
	}

}
