package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.InformacoesComplementaresDao;

public class InformacoesComplementaresDaoImplTest extends AbstractDaoImplTest {

	protected InformacoesComplementaresDaoImplTest() {
		super(InformacoesComplementaresDaoImpl.class, InformacoesComplementaresDao.class);
	}

}