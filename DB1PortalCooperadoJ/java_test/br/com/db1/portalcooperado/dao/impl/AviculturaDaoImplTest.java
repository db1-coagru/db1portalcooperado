package br.com.db1.portalcooperado.dao.impl;

import br.com.db1.portalcooperado.dao.AviculturaDao;

public class AviculturaDaoImplTest extends AbstractDaoImplTest {

	protected AviculturaDaoImplTest() {
		super(AviculturaDaoImpl.class, AviculturaDao.class);
	}

}