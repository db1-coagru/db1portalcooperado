package br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.suggestion.suggestiontypeinfo;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.ServiceRecordDataProvider;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.Suggestion;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.TypeInfo;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SuggestionTypeInfoTest {

	@Test
	public void recuperacaoDaSuggestionTypeInfoDescriptionEstaIncorreta() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		List<Suggestion> suggestions = serviceRecord.suggestionsOf(TypeInfo.DIAGNOSTICOS);

		List<String> actual = Lists.newLinkedList();
		for (Suggestion suggestion : suggestions) {
			actual.add(suggestion.getSuggestionTypeInfoDescripton());
		}

		List<String> expected = ImmutableList.of("Ervas Daninhas");

		assertThat(actual).isEqualTo(expected);
	}


	@Test
	public void recuperacaoDoNoteEstaIncorreto() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		List<Suggestion> suggestions = serviceRecord.suggestionsOf(TypeInfo.DIAGNOSTICOS);

		List<String> actual = Lists.newLinkedList();
		for (Suggestion suggestion : suggestions) {
			actual.add(suggestion.getNote());
		}

		List<String> expected = ImmutableList.of("buva, cochao, nabo");

		assertThat(actual).isEqualTo(expected);
	}



}