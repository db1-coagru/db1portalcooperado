package br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;

public class ServiceRecordDataProvider {

	public static final String _00942620160817GRE = "java_test\\br\\com\\db1\\portalcooperado\\schedules\\recomendacaotecnica\\xml\\arquivos\\00942620160817GRE.xml";

	public static ServiceRecord serviceRecord1()
			throws JAXBException, IOException {
		File file = new File(_00942620160817GRE);
		return ServiceRecord.of(file);
	}
}
