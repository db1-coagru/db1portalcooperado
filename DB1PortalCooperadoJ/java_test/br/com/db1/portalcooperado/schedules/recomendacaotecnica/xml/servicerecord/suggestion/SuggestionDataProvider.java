package br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.suggestion;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.Suggestion;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.TypeInfo;
import org.testng.annotations.DataProvider;

import static org.mockito.Mockito.mock;

public class SuggestionDataProvider {

	public static final String FILTERS = "filters";

	public static final String TYPE_INFO_IS = "typeInfoIs";

	public static final String TYPE_INFO_NOT_IS = "typeInfoNotIs";

	@DataProvider(name = FILTERS)
	public static Object[][] filters() {
		return new Object[][] {
				{ TypeInfo.DIAGNOSTICOS, 1 },
				{ TypeInfo.PRODUTOS, 5 }
		};
	}

	@DataProvider(name = TYPE_INFO_IS)
	public static Object[][] typeInfoIs() {
		return new Object[][] {
				{ Suggestion.of(
						TypeInfo.DIAGNOSTICOS), TypeInfo.DIAGNOSTICOS },
				{ Suggestion.of(
						TypeInfo.PRODUTOS), TypeInfo.PRODUTOS }
		};
	}

	@DataProvider(name = TYPE_INFO_NOT_IS)
	public static Object[][] typeInfoNotIs() {
		Suggestion objetoRecuperadoViaReflectionJaxb = mock(
				Suggestion.class);
		return new Object[][] {
				{ objetoRecuperadoViaReflectionJaxb, TypeInfo.DIAGNOSTICOS },
				{ objetoRecuperadoViaReflectionJaxb, TypeInfo.PRODUTOS },
				{ Suggestion.of(
						TypeInfo.DIAGNOSTICOS), TypeInfo.PRODUTOS },
				{ Suggestion.of(
						TypeInfo.PRODUTOS), TypeInfo.DIAGNOSTICOS }
		};
	}
}
