package br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.cultureinfo;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.ServiceRecordDataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CultureInfoTest {

	@Test
	public void recuperacaoDoProdutoIncorreto() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		assertThat(serviceRecord.getCultureInfoDescription()).isEqualTo("SOJA");
	}

}