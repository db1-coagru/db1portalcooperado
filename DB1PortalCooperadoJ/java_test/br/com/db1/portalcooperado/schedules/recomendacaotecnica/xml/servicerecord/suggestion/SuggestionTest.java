package br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.suggestion;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.Suggestion;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.TypeInfo;
import br.com.db1.portalcooperado.model.types.Quantidade;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.ServiceRecordDataProvider;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Test(dataProviderClass = SuggestionDataProvider.class)
public class SuggestionTest {

	@Test(dataProvider = SuggestionDataProvider.TYPE_INFO_IS)
	public void typeInfoIsFalseQuandoDeveriaSerTrue(
			Suggestion suggestion, TypeInfo typeInfo) throws Exception {
		assertThat(suggestion.typeInfoIs(typeInfo)).isTrue();
	}

	@Test(dataProvider = SuggestionDataProvider.TYPE_INFO_NOT_IS)
	public void typeInfoIsTrueQuandoDeveriaSerFalse(
			Suggestion suggestion, TypeInfo typeInfo) throws Exception {
		assertThat(suggestion.typeInfoIs(typeInfo)).isFalse();
	}

	@Test
	public void recuperacaoDoOrderEstaIncorreta() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		List<Suggestion> suggestions = serviceRecord.suggestionsOf(
				TypeInfo.PRODUTOS);

		List<Long> actual = Lists.newLinkedList();
		for (Suggestion suggestion : suggestions) {
			actual.add(suggestion.getOrder());
		}

		List<Long> expected = ImmutableList.of(1L, 3L, 4L, 2L, 5L);

		assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void recuperacaoDaQuantityEstaIncorreta() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		List<Suggestion> suggestions = serviceRecord.suggestionsOf(
				TypeInfo.PRODUTOS);

		List<Quantidade> actual = Lists.newLinkedList();
		for (Suggestion suggestion : suggestions) {
			actual.add(suggestion.getQuantity());
		}

		List<Quantidade> expected = Lists.newLinkedList();
		expected.add(Quantidade.zero());
		expected.add(Quantidade.of(4.00, 2));
		expected.add(Quantidade.of(2.00, 2));
		expected.add(Quantidade.zero());
		expected.add(Quantidade.of(2.00, 2));

		assertThat(actual).isEqualTo(expected);
	}

}