package br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.suggestion.productinfo;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.ServiceRecordDataProvider;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.Suggestion;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.TypeInfo;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductInfoTest {

	@Test
	public void recuperacaoDoProductInfoVendorIdEstaIncorreta() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		List<Suggestion> suggestions = serviceRecord.suggestionsOf(TypeInfo.PRODUTOS);

		List<Long> actual = Lists.newLinkedList();
		for (Suggestion suggestion : suggestions) {
			actual.add(suggestion.getProductInfoVendorId());
		}

		List<Long> expected = ImmutableList.of(181092L, 177893L, 163779L, 179705L, 98000L);

		assertThat(actual).isEqualTo(expected);
	}

	@Test
	public void recuperacaoDoProductInfoNomeEstaIncorreta() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		List<Suggestion> suggestions = serviceRecord.suggestionsOf(TypeInfo.PRODUTOS);

		List<String> actual = Lists.newLinkedList();
		for (Suggestion suggestion : suggestions) {
			actual.add(suggestion.getProductInfoName());
		}

		List<String> expected = Lists.newLinkedList();
		expected.add("OROBOR N=1 B=0,1 Gl");
		expected.add("CRUCIAL (H) RG08912 CT I bd 20");
		expected.add("U-46 PRIME (H) RG02704 CTI BD20");
		expected.add("KROMO 250 WG (H) RG04208 CTIV PCT 500gr");
		expected.add("NIMBUS (J) RG004997 CTIV GL");

		assertThat(actual).isEqualTo(expected);
	}

}