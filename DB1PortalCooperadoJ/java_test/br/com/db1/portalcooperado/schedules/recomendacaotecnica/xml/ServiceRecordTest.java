package br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml;

import br.com.db1.portalcooperado.model.types.Quantidade;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.ServiceRecordDataProvider;
import br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.suggestion.SuggestionDataProvider;
import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.TypeInfo;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ServiceRecordTest {

	@Test
	public void recuperacaoDoTotalAreaIncorreto() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		assertThat(serviceRecord.getTotalArea()).isEqualTo(Quantidade.of(10.000, 3));
	}

	@Test
	public void recuperacaoDaEfficiencyIncorreto() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		assertThat(serviceRecord.getEfficiency()).isEqualTo(Quantidade.of(6.000, 3));
	}

	@Test
	public void recuperacaoDoVolumneIncorreto() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		assertThat(serviceRecord.getVolume()).isEqualTo(Quantidade.of(2000.000, 3));
	}

	@Test
	public void recuperacaoDaRecommendationIncorreta() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		assertThat(serviceRecord.getRecommendation()).isEqualTo("É importante fazer igual ta descrito");
	}

	@Test(dataProvider = SuggestionDataProvider.FILTERS, dataProviderClass = SuggestionDataProvider.class)
	public void filterSuggestionsEstaIncorreto(TypeInfo typeInfo, Integer sizeExpected) throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		assertThat(serviceRecord.suggestionsOf(typeInfo)).hasSize(sizeExpected);
	}

	@Test
	public void recuperacaoDoUrgentIncorreto() throws Exception {
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		assertThat(serviceRecord.getUrgent()).isTrue();
	}

	@Test
	public void recuperacaoDaCustomMessageIncorreta() throws Exception {
		String expected = "Em visita técnica realizada na sua área de SOJA foram avaliadas as condições atuais de " +
				"desenvolvimento e ocorrências de ordem geral. Entre em contato com " +
				"o Engº Agrº Anderson Carlos da Silva Cruz no Departamento Técnico da COAGRU para saber " +
				"os detalhes da avaliação.";
		ServiceRecord serviceRecord = ServiceRecordDataProvider.serviceRecord1();
		assertThat(serviceRecord.getCustomMessage()).isEqualTo(expected);
	}
}