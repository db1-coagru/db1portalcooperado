package br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.suggestion;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.suggestion.TypeInfo;
import org.testng.annotations.Test;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import java.lang.reflect.Field;

import static org.assertj.core.api.Assertions.assertThat;

public class TypeInfoTest {

	@Test
	public void naoEstaMappeadoComXmlEnum() throws Exception {
		assertThat(
				TypeInfo.class).hasAnnotation(XmlEnum.class);
	}

	@Test
	public void typeInfoDiagnosticosNaoEstaMappeadoComXmlEnumValue() throws Exception {
		Field field = TypeInfo.class.getField(
				TypeInfo.DIAGNOSTICOS.name());
		assertThat(field.getAnnotation(XmlEnumValue.class)).isNotNull();
	}

	@Test
	public void typeInfoProdutosNaoEstaMappeadoComXmlEnumValue() throws Exception {
		Field field = TypeInfo.class.getField(
				TypeInfo.PRODUTOS.name());
		assertThat(field.getAnnotation(XmlEnumValue.class)).isNotNull();
	}

	@Test
	public void valorXmlEnumValueDoDiagnosticosEstaIncorreto() throws Exception {
		Field field = TypeInfo.class.getField(
				TypeInfo.DIAGNOSTICOS.name());
		XmlEnumValue annotation = field.getAnnotation(XmlEnumValue.class);
		assertThat(annotation.value()).isEqualTo("3");
	}

	@Test
	public void valorXmlEnumValueDoProdutosEstaIncorreto() throws Exception {
		Field field = TypeInfo.class.getField(
				TypeInfo.PRODUTOS.name());
		XmlEnumValue annotation = field.getAnnotation(XmlEnumValue.class);
		assertThat(annotation.value()).isEqualTo("4");
	}

}