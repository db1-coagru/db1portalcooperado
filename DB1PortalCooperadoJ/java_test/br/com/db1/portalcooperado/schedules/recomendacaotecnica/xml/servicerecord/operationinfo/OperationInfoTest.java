package br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.operationinfo;

import br.com.db1.portalcooperado.infrastructure.greensoft.recomendacaotecnica.xml.servicerecord.ServiceRecord;
import br.com.db1.portalcooperado.util.DateFormatter;
import org.testng.annotations.Test;

import java.io.File;
import java.util.Calendar;

import static br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.ServiceRecordDataProvider._00942620160817GRE;
import static org.assertj.core.api.Assertions.assertThat;

public class OperationInfoTest {

	@Test
	public void recuperacaoDaDataDeCriacaoIncorreta() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 23);
		calendar.set(Calendar.MONTH, 6);
		calendar.set(Calendar.YEAR, 2016);
		calendar.set(Calendar.HOUR_OF_DAY, 11);
		calendar.set(Calendar.MINUTE, 39);
		calendar.set(Calendar.SECOND, 59);

		File file = new File(_00942620160817GRE);
		ServiceRecord serviceRecord = ServiceRecord.of(file);

		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(serviceRecord.getOperationInfoCreateDate());

		String expected = DateFormatter.DD_MM_YYYY_HH_MM_SS.format(calendar.getTime());
		String actual = DateFormatter.DD_MM_YYYY_HH_MM_SS.format(calendar2.getTime());

		assertThat(actual).isEqualTo(expected);
	}

}