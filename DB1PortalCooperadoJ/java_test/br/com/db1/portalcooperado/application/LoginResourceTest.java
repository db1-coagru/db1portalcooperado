package br.com.db1.portalcooperado.application;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.application.resources.LoginResource;
import br.com.db1.portalcooperado.model.entity.Token;
import br.com.db1.portalcooperado.model.entity.UserDataProvider;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.entity.UsuarioDataProvider;
import br.com.db1.portalcooperado.infrastructure.rest.exceptions.CustomNotAcceptableWebApplicationException;
import br.com.db1.portalcooperado.infrastructure.rest.exceptions.CustomServerErrorWebApplicationException;
import br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.CredencialJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson;
import br.com.db1.portalcooperado.service.PortalTokenService;
import br.com.db1.portalcooperado.service.UsuarioService;
import br.com.db1.security.model.entity.User;
import br.com.db1.security.service.UserService;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LoginResourceTest {

	private LoginResource loginResource;

	private Usuario usuario1;

	private Token tokenUser1;

	@BeforeClass
	public void setUp() {
		DB1ValidateException exception = new DB1ValidateException("Exception");
		User user = UserDataProvider.user1();
		User user2 = UserDataProvider.user2();

		usuario1 = UsuarioDataProvider.usuario1();
		Usuario usuario2 = UsuarioDataProvider.usuario2();

		tokenUser1 = Token.builder(user).build();

		UserService userServiceMock = mock(UserService.class);
		when(userServiceMock.authenticate(null, "123", "COAGRU")).thenThrow(exception);
		when(userServiceMock.authenticate("123", null, "COAGRU")).thenThrow(exception);
		when(userServiceMock.authenticate("unspeck", "123", "COAGRU")).thenThrow(new NullPointerException());
		when(userServiceMock.authenticate("123", "123", null)).thenThrow(exception);
		when(userServiceMock.authenticate("123", "123", "COAGRU")).thenReturn(user);
		when(userServiceMock.authenticate("1234", "123", "COAGRU")).thenReturn(user2);

		UsuarioService usuarioServiceMock = mock(UsuarioService.class);
		when(usuarioServiceMock.buscaUsuarioFull("123")).thenReturn(usuario1);
		when(usuarioServiceMock.buscaUsuarioFull("1234")).thenReturn(usuario2);

		PortalTokenService portalTokenService = mock(PortalTokenService.class);
		when(portalTokenService.generateTokenTo(user)).thenReturn(tokenUser1);

		loginResource = new LoginResource(userServiceMock, portalTokenService, usuarioServiceMock);
	}

	@Test
	public void credencialIsNull() throws Exception {
		Response response = new LoginResource(null, null, null).authenticate(null);
		MensagemJson actual = (MensagemJson) response.getEntity();
		assertThat(actual).isEqualTo(MensagemJson.newMensagemJsonNaoLocalizado());
	}

	@Test(expectedExceptions = CustomNotAcceptableWebApplicationException.class,
			expectedExceptionsMessageRegExp = "HTTP 406 Not Acceptable")
	public void userInexistenteWhenMatriculaIsNull() throws Exception {
		CredencialJson credencialJson = mock(CredencialJson.class);
		when(credencialJson.getSenha()).thenReturn("123");
		when(credencialJson.getDomain()).thenReturn("COAGRU");
		loginResource.authenticate(credencialJson);
	}

	@Test(expectedExceptions = CustomNotAcceptableWebApplicationException.class,
			expectedExceptionsMessageRegExp = "HTTP 406 Not Acceptable")
	public void userInexistenteWhenSenhaIsNull() throws Exception {
		CredencialJson credencialJson = mock(CredencialJson.class);
		when(credencialJson.getMatricula()).thenReturn("123");
		when(credencialJson.getDomain()).thenReturn("COAGRU");
		loginResource.authenticate(credencialJson);
	}

	@Test(expectedExceptions = CustomNotAcceptableWebApplicationException.class,
			expectedExceptionsMessageRegExp = "HTTP 406 Not Acceptable")
	public void userInexistenteWhenDominioIsNull() throws Exception {
		CredencialJson credencialJson = mock(CredencialJson.class);
		when(credencialJson.getMatricula()).thenReturn("123");
		when(credencialJson.getSenha()).thenReturn("123");
		loginResource.authenticate(credencialJson);
	}

	@Test
	public void usuarioJsonGeneratedIsIncorrect() throws Exception {
		CredencialJson credencialJson = mock(CredencialJson.class);
		when(credencialJson.getMatricula()).thenReturn("123");
		when(credencialJson.getSenha()).thenReturn("123");
		when(credencialJson.getDomain()).thenReturn("COAGRU");

		Response authenticate = loginResource.authenticate(credencialJson);
		UsuarioJson actual = (UsuarioJson) authenticate.getEntity();

		UsuarioJson expected = UsuarioJson.of(usuario1, TokenJson.of(tokenUser1));

		assertThat(actual).isEqualToComparingFieldByField(expected);
	}

	@Test
	public void usuarioJsonGeneratedIsEqualWhenShouldBeNot() throws Exception {
		CredencialJson credencialJson = mock(CredencialJson.class);
		when(credencialJson.getMatricula()).thenReturn("123");
		when(credencialJson.getSenha()).thenReturn("123");
		when(credencialJson.getDomain()).thenReturn("COAGRU");

		Response authenticate = loginResource.authenticate(credencialJson);
		UsuarioJson actual = (UsuarioJson) authenticate.getEntity();

		User user = UserDataProvider.user2();
		Usuario usuario = UsuarioDataProvider.usuario2();
		Token tokenUser = Token.builder(user).build();
		TokenJson tokenJson = TokenJson.of(tokenUser);

		UsuarioJson expected = UsuarioJson.of(usuario, tokenJson);
		assertThat(actual).isNotEqualTo(expected);
	}

	@Test(expectedExceptions = CustomServerErrorWebApplicationException.class,
			expectedExceptionsMessageRegExp = "HTTP 500 Internal Server Error")
	public void unexpectedException() throws Exception {
		CredencialJson credencialJson = mock(CredencialJson.class);
		when(credencialJson.getMatricula()).thenReturn("unspeck");
		when(credencialJson.getSenha()).thenReturn("123");
		when(credencialJson.getDomain()).thenReturn("COAGRU");

		loginResource.authenticate(credencialJson);
	}

	@Test
	public void usuarioAuthorizedMobileWhenShouldBeNot() throws Exception {
		CredencialJson credencialJson = mock(CredencialJson.class);
		when(credencialJson.getMatricula()).thenReturn("1234");
		when(credencialJson.getSenha()).thenReturn("123");
		when(credencialJson.getDomain()).thenReturn("COAGRU");

		Response authenticate = loginResource.authenticate(credencialJson);

		MensagemJson actual = (MensagemJson) authenticate.getEntity();
		MensagemJson expected = MensagemJson.newMensagemSemAcessoMobile();
		assertThat(actual).isEqualTo(expected);
	}
}