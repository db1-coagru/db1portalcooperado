package br.com.db1.portalcooperado.model.types;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Test(dataProviderClass = PermissaoIndicadoDataProvider.class)
public class PermissaoIndicadoTest {

	@Test(dataProvider = PermissaoIndicadoDataProvider.CODIGOS)
	public void deveRetornarOpcaoParaCodigoInformado(PermissaoIndicado permissaoIndicado, Integer codigo) {
		assertThat(PermissaoIndicado.fromCodigo(codigo)).isEqualTo(permissaoIndicado);
	}

	@Test(dataProvider = PermissaoIndicadoDataProvider.CODIGOS)
	public void devePossuirCodigoCorreto(PermissaoIndicado permissaoIndicado, Integer codigo) {
		assertThat(permissaoIndicado.getCodigo()).isEqualTo(codigo);
	}

	@Test(dataProvider = PermissaoIndicadoDataProvider.DESCRICOES)
	public void devePossuirDescricaoCorreta(PermissaoIndicado permissaoIndicado, String descricao) {
		assertThat(permissaoIndicado.getDescricao()).isEqualTo(descricao);
	}

}