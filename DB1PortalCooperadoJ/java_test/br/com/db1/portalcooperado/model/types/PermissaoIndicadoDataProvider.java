package br.com.db1.portalcooperado.model.types;

import org.testng.annotations.DataProvider;

public class PermissaoIndicadoDataProvider {

	public static final String CODIGOS = "codigos";

	public static final String DESCRICOES = "descricoes";

	private PermissaoIndicadoDataProvider() {

	}

	@DataProvider(name = CODIGOS)
	public static Object[][] codigos() {
		return new Object[][] {
				{ PermissaoIndicado.ACTEVENTOS, 0 },
				{ PermissaoIndicado.ACTPOSICAOFINANCEIRA, 3 },
				{ PermissaoIndicado.ACTENTREGAPRODUCAO, 2 },
				{ PermissaoIndicado.ACTFIXACAOFRANGO, 11 },
				{ PermissaoIndicado.ACTNOTICIAS, 1 },
				{ PermissaoIndicado.ACTRECOMENDACAOTECNICA, 4 },
				{ PermissaoIndicado.ACTVENDACAVACOMARAVALHA, 10 },
				{ PermissaoIndicado.ACTVENDAINSUMOS, 6 },
				{ PermissaoIndicado.ACTVENDAPECAS, 8 },
				{ PermissaoIndicado.ACTVENDARACAOSUPLEMENTO, 9 },
				{ PermissaoIndicado.ACTVENDAVETERINARIA, 7 },
				{ PermissaoIndicado.ACTAVICULTURA, 12 }
		};
	}

	@DataProvider(name = DESCRICOES)
	public static Object[][] descricoes() {
		return new Object[][] {
				{ PermissaoIndicado.ACTEVENTOS, "Eventos" },
				{ PermissaoIndicado.ACTPOSICAOFINANCEIRA, "Extrato Financeiro" },
				{ PermissaoIndicado.ACTENTREGAPRODUCAO, "Extrato Produção Agricola" },
				{ PermissaoIndicado.ACTFIXACAOFRANGO, "Fechamento de Frango" },
				{ PermissaoIndicado.ACTNOTICIAS, "Notícias" },
				{ PermissaoIndicado.ACTRECOMENDACAOTECNICA, "Recomendação Técnica" },
				{ PermissaoIndicado.ACTVENDACAVACOMARAVALHA, "Venda de Cavaco/Maravalha" },
				{ PermissaoIndicado.ACTVENDAINSUMOS, "Venda de Insumos" },
				{ PermissaoIndicado.ACTVENDAPECAS, "Venda de peças" },
				{ PermissaoIndicado.ACTVENDARACAOSUPLEMENTO, "Venda de Ração/Suplemento Mineral" },
				{ PermissaoIndicado.ACTVENDAVETERINARIA, "Venda veterinária" },
				{ PermissaoIndicado.ACTAVICULTURA, "Avicultura" }
		};
	}

}
