package br.com.db1.portalcooperado.model.types;

import br.com.db1.exception.validate.DB1ValidateException;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Test(dataProviderClass = TipoNotificacaoDataProvider.class)
public class TipoNotificacaoTest {

	@Test(dataProvider = TipoNotificacaoDataProvider.CODIGOS)
	public void fromCodigoEstaIncorreto(TipoNotificacao expected, Long value) throws Exception {
		assertThat(TipoNotificacao.fromCodigo(value)).isEqualTo(expected);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Tipo de notificação deve ser informado.")
	public void fromCodigoNuloNaoDisparouErro() throws Exception {
		TipoNotificacao.fromCodigo(null);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Tipo de notificação inexistente.")
	public void fromCodigoInexistenteNaoDisparouErro() throws Exception {
		TipoNotificacao.fromCodigo(20L);
	}

	@Test(dataProvider = TipoNotificacaoDataProvider.DESCRICOES)
	public void descricaoIncorreta(TipoNotificacao tipoNotificacao, String expected) throws Exception {
		assertThat(tipoNotificacao.getDescricao()).isEqualTo(expected);
	}

	@Test(dataProvider = TipoNotificacaoDataProvider.CODIGOS)
	public void codigoIncorreto(TipoNotificacao tipoNotificacao, Long expected) throws Exception {
		assertThat(tipoNotificacao.getCodigo()).isEqualTo(expected);
	}

	@Test(dataProvider = TipoNotificacaoDataProvider.TIPOS_EXCETO_ROMANEIO)
	public void isRomaneioTrueQuandoDeveriaSerFalse(TipoNotificacao tipoNotificacao) throws Exception {
		assertThat(!tipoNotificacao.isRomaneioAgricola());
	}

	@Test
	public void isRomaneioFalseQuandoDeveriaSerTrue() throws Exception {
		assertThat(TipoNotificacao.ROMANEIO_AGRICOLA.isRomaneioAgricola());
	}

	@Test(dataProvider = TipoNotificacaoDataProvider.TITULOS)
	public void tituloIncorreta(TipoNotificacao tipoNotificacao, String expected) throws Exception {
		assertThat(tipoNotificacao.getTitulo()).isEqualTo(expected);
	}

}