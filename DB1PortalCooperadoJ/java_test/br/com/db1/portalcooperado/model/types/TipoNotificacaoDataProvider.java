package br.com.db1.portalcooperado.model.types;

import org.testng.annotations.DataProvider;

public class TipoNotificacaoDataProvider {

	public static final String DESCRICOES = "descricoes";

	public static final String CODIGOS = "codigos";

	public static final String TIPOS_EXCETO_ROMANEIO = "tiposExcetoRomaneio";

	public static final String TITULOS = "titulos";

	@DataProvider(name = DESCRICOES)
	public static Object[][] descricoes() {
		return new Object[][] {
				{ TipoNotificacao.ROMANEIO_AGRICOLA, "Romaneio agrícola" },
				{ TipoNotificacao.VENDA_INSUMOS, "Venda de insumos" },
				{ TipoNotificacao.VENDA_VETERINARIA, "Venda veterinária" },
				{ TipoNotificacao.VENDA_PECAS, "Venda de peças" },
				{ TipoNotificacao.VENDA_RACAO_SUPLEMENTO, "Venda de ração/suplemento mineral" },
				{ TipoNotificacao.VENDA_CAVACO_MARAVALHA, "Venda de cavaco/maravalha" },
				{ TipoNotificacao.FECHAMENTO_DE_FRANGO, "Fechamento de frango" },
				{ TipoNotificacao.RECOMENDACAO_TECNICA, "Recomendação técnica" }
		};
	}

	@DataProvider(name = CODIGOS)
	public static Object[][] codigos() {
		return new Object[][] {
				{ TipoNotificacao.ROMANEIO_AGRICOLA, 1L },
				{ TipoNotificacao.VENDA_INSUMOS, 2L },
				{ TipoNotificacao.VENDA_VETERINARIA, 3L },
				{ TipoNotificacao.VENDA_PECAS, 4L },
				{ TipoNotificacao.VENDA_RACAO_SUPLEMENTO, 5L },
				{ TipoNotificacao.VENDA_CAVACO_MARAVALHA, 6L },
				{ TipoNotificacao.FECHAMENTO_DE_FRANGO, 7L },
				{ TipoNotificacao.RECOMENDACAO_TECNICA, 10L }
		};
	}

	@DataProvider(name = TIPOS_EXCETO_ROMANEIO)
	public static Object[][] tiposExcetoRomaneio() {
		return new Object[][] {
				{ TipoNotificacao.ROMANEIO_AGRICOLA },
				{ TipoNotificacao.VENDA_INSUMOS },
				{ TipoNotificacao.VENDA_VETERINARIA },
				{ TipoNotificacao.VENDA_PECAS },
				{ TipoNotificacao.VENDA_RACAO_SUPLEMENTO },
				{ TipoNotificacao.VENDA_CAVACO_MARAVALHA },
				{ TipoNotificacao.FECHAMENTO_DE_FRANGO },
				{ TipoNotificacao.RECOMENDACAO_TECNICA }
		};
	}

	@DataProvider(name = TITULOS)
	public static Object[][] titulos() {
		return new Object[][] {
				{ TipoNotificacao.ROMANEIO_AGRICOLA, "Novo romaneio agrícola" },
				{ TipoNotificacao.VENDA_INSUMOS, "Nova venda de insumos" },
				{ TipoNotificacao.VENDA_VETERINARIA, "Nova venda veterinária" },
				{ TipoNotificacao.VENDA_PECAS, "Nova venda de peças" },
				{ TipoNotificacao.VENDA_RACAO_SUPLEMENTO, "Nova venda de ração/suplemento mineral" },
				{ TipoNotificacao.VENDA_CAVACO_MARAVALHA, "Nova venda de cavaco/maravalha" },
				{ TipoNotificacao.FECHAMENTO_DE_FRANGO, "Novo fechamento de frango" },
				{ TipoNotificacao.RECOMENDACAO_TECNICA, "Nova recomendação técnica" },
				{ TipoNotificacao.INDICACOES, "" }
		};
	}
}
