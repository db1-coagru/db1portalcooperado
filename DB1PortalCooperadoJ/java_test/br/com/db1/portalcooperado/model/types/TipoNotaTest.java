package br.com.db1.portalcooperado.model.types;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TipoNotaTest {

	@Test
	public void descricaoVendaIncorreta() throws Exception {
		assertThat(TipoNota.V.getDescricao()).isEqualTo("Venda");
	}

	@Test
	public void descricaoDevolucaoIncorreta() throws Exception {
		assertThat(TipoNota.D.getDescricao()).isEqualTo("Devolução");
	}

}