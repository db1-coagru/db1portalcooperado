package br.com.db1.portalcooperado.model.types;

import br.com.db1.exception.validate.DB1ValidateException;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CanalTest {

	@Test
	public void portalLongValueIsIncorrect() throws Exception {
		assertThat(Canal.PORTAL.longValue()).isEqualTo(1L);
	}

	@Test
	public void mobileLongValueIsIncorrect() throws Exception {
		assertThat(Canal.MOBILE.longValue()).isEqualTo(2L);
	}

	@Test
	public void portalDescricaoIsIncorrect() throws Exception {
		assertThat(Canal.PORTAL.getDescricao()).isEqualTo("Portal");
	}

	@Test
	public void mobileDescricaoIsIncorrect() throws Exception {
		assertThat(Canal.MOBILE.getDescricao()).isEqualTo("Mobile");
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Tipo de canal não suportado.")
	public void unsupportedLongValueNotThrowingException() throws Exception {
		Canal.fromLongValue(3L);
	}

	@Test
	public void fromLongValuePortalIsIncorrect() throws Exception {
		assertThat(Canal.fromLongValue(1L)).isEqualTo(Canal.PORTAL);
	}

	@Test
	public void fromLongValueMobileIsIncorrect() throws Exception {
		assertThat(Canal.fromLongValue(2L)).isEqualTo(Canal.MOBILE);
	}

	@Test
	public void defineIpMobileIncorreto() throws Exception {
		assertThat(Canal.MOBILE.defineIp("123.456.789.10")).isEqualTo("123.456.789.10");
	}
}