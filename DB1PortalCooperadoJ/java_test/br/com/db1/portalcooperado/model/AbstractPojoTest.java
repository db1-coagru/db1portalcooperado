package br.com.db1.portalcooperado.model;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.Rule;
import com.openpojo.validation.rule.impl.NoPrimitivesRule;
import com.openpojo.validation.rule.impl.NoPublicFieldsExceptStaticFinalRule;
import com.openpojo.validation.rule.impl.NoStaticExceptFinalRule;
import com.openpojo.validation.rule.impl.SerializableMustHaveSerialVersionUIDRule;
import com.openpojo.validation.test.Tester;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.testng.annotations.Test;

/**
 * Abstração dos testes de pojos utilizando o open pojo
 *
 */
public abstract class AbstractPojoTest {

	private final PojoClass pojoClass;

	public AbstractPojoTest(Class<?> pojoClass) {
		this.pojoClass = PojoClassFactory.getPojoClass(pojoClass);
	}

	@Test
	public void should_set_correct_value() {
		validate(new SetterTester());
	}

	@Test
	public void should_get_correct_value() {
		validate(new GetterTester());
	}

	@Test
	public void should_not_have_primitive_attributes() throws Exception {
		validate(new NoPrimitivesRule());
	}

	@Test
	public void should_not_exists_static_attributes_without_final_modifier() throws Exception {
		validate(new NoStaticExceptFinalRule());
	}

	@Test
	public void should_not_exists_serial_version_uid_to_serializable() throws Exception {
		validate(new SerializableMustHaveSerialVersionUIDRule());
	}

	@Test
	public void should_not_exists_non_statics_attributes_with_public_modifier() throws Exception {
		validate(new NoPublicFieldsExceptStaticFinalRule());
	}

	private void validate(Rule rule) {
		ValidatorBuilder.create()
				.with(rule)
				.build()
				.validate(pojoClass);
	}

	private void validate(Tester tester) {
		ValidatorBuilder.create()
				.with(tester)
				.build()
				.validate(pojoClass);
	}
}
