package br.com.db1.portalcooperado.model.specifications;

import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.Date;

import static br.com.db1.portalcooperado.relatorio.types.LogAcessoTipoCanal.*;
import static br.com.db1.portalcooperado.util.DateFormatter.DD_MM_YYYY_HH_MM_SS;
import static org.assertj.core.api.Assertions.assertThat;

public class UsuariosLogsTest {

	@Test
	public void withCanalTodosAndBetweenIsIncorrect() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2016, Calendar.AUGUST, 12);
		Date dtInicial = calendar.getTime();
		calendar.set(2016, Calendar.AUGUST, 13);
		Date dtFinal = calendar.getTime();

		String expected = " WHERE req.dt_requisicao BETWEEN " + "to_date('"
				+ DD_MM_YYYY_HH_MM_SS.format(dtInicial)
				+ "','dd/MM/yyyy hh24:mi:ss') AND to_date('"
				+ DD_MM_YYYY_HH_MM_SS.format(dtFinal) + "','dd/MM/yyyy hh24:mi:ss'))"
				+ " GROUP BY matricula, nome, canal)"
				+ " ORDER BY TRIM(nome), canal";
		assertThat(UsuariosLogs.withCanalAndBetween(TODOS, dtInicial, dtFinal)).isEqualTo(expected);
	}

	@Test
	public void withCanalPortalAndBetweenIsIncorrect() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2016, Calendar.AUGUST, 12);
		Date dtInicial = calendar.getTime();
		calendar.set(2016, Calendar.AUGUST, 13);
		Date dtFinal = calendar.getTime();

		String expected = " WHERE req.dt_requisicao BETWEEN " + "to_date('"
				+ DD_MM_YYYY_HH_MM_SS.format(dtInicial)
				+ "','dd/MM/yyyy hh24:mi:ss') AND to_date('"
				+ DD_MM_YYYY_HH_MM_SS.format(dtFinal) + "','dd/MM/yyyy hh24:mi:ss')"
				+ " AND req.tp_canal = 1)"
				+ " GROUP BY matricula, nome, canal)"
				+ " ORDER BY TRIM(nome), canal";
		assertThat(UsuariosLogs.withCanalAndBetween(PORTAL, dtInicial, dtFinal)).isEqualTo(expected);
	}

	@Test
	public void withCanalMobileAndBetweenIsIncorrect() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2016, Calendar.AUGUST, 12);
		Date dtInicial = calendar.getTime();
		calendar.set(2016, Calendar.AUGUST, 13);
		Date dtFinal = calendar.getTime();

		String expected = " WHERE req.dt_requisicao BETWEEN " + "to_date('"
				+ DD_MM_YYYY_HH_MM_SS.format(dtInicial)
				+ "','dd/MM/yyyy hh24:mi:ss') AND to_date('"
				+ DD_MM_YYYY_HH_MM_SS.format(dtFinal) + "','dd/MM/yyyy hh24:mi:ss')"
				+ " AND req.tp_canal = 2)"
				+ " GROUP BY matricula, nome, canal)"
				+ " ORDER BY TRIM(nome), canal";

		assertThat(UsuariosLogs.withCanalAndBetween(MOBILE, dtInicial, dtFinal)).isEqualTo(expected);
	}

}