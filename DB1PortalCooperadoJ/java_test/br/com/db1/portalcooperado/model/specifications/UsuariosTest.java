package br.com.db1.portalcooperado.model.specifications;

import br.com.db1.myBatisPersistence.util.MyBatisUtil;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class UsuariosTest {

	public static final String CONSTANTES = "constantes";

	@DataProvider(name = CONSTANTES)
	public static Object[][] constantes() {
		return new Object[][] {
				{ Usuarios.QUERY_EXISTE_CPF_CNPJ, "existeCpfCnpj" },
				{ Usuarios.QUERY_EXISTE_MATRICULA, "existeMatricula" },
				{ Usuarios.QUERY_BUSCAR_USUARIO_FULL, "buscarUsuarioFull" },
				{ Usuarios.QUERY_BUSCAR_USUARIO_COM_TOKEN, "buscarUsuarioComToken" },
				{ Usuarios.PARAM_ID, "id" },
				{ Usuarios.PARAM_CPF_CNPJ, "cpfCnpj" },
				{ Usuarios.PARAM_MATRICULA, "matricula" },
				{ Usuarios.PARAM_ACCESS_TOKEN, "accessToken" },
		};
	}

	@Test(dataProvider = CONSTANTES)
	public void constanteIncorreta(String constant, String expected) throws Exception {
		assertThat(constant).isEqualTo(expected);
	}

	@Test
	public void withCpfCnpjIncorreto() throws Exception {
		Map<String, Object> expected = MyBatisUtil.createMap("cpfCnpj", "123.456.789.10", "id", 1L);
		assertThat(Usuarios.withCpfCnpj("123.456.789.10", 1L)).isEqualTo(expected);
	}

	@Test
	public void withLoginIdUsuarioIncorreto() throws Exception {
		Map<String, Object> expected = MyBatisUtil.createMap("matricula", "123", "id", 1L);
		assertThat(Usuarios.withLogin("123", 1L)).isEqualTo(expected);
	}

	@Test
	public void withLoginIncorreto() throws Exception {
		Map<String, Object> expected = MyBatisUtil.createMap("matricula", "123");
		assertThat(Usuarios.withLogin("123")).isEqualTo(expected);
	}

	@Test
	public void withAccessTokenIncorreto() throws Exception {
		Map<String, Object> expected = MyBatisUtil.createMap("accessToken", "123");
		assertThat(Usuarios.withAccessToken("123")).isEqualTo(expected);
	}

	@Test
	public void withMatriculaIncorreto() throws Exception {
		Map<String, Object> expected = MyBatisUtil.createMap("matricula", "123");
		assertThat(Usuarios.withMatricula("123")).isEqualTo(expected);
	}
}