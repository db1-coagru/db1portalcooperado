package br.com.db1.portalcooperado.model.specifications;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RequisicoesTest {

	@Test
	public void queryRelatorioAcessoCooperadoIsIncorrect() throws Exception {
		String expected = "MAT_USUARIO = :pMatricula " +
				"AND DT_REQUISICAO BETWEEN :pDtInicial " +
				"AND :pDtFinal " +
				"ORDER BY TP_REQUISICAO, DT_REQUISICAO";
		assertThat(Requisicoes.relatorioAcessoCooperado()).isEqualTo(expected);
	}
}