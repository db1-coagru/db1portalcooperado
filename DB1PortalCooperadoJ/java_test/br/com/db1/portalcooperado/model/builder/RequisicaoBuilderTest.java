package br.com.db1.portalcooperado.model.builder;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.entity.UsuarioDataProvider;
import br.com.db1.portalcooperado.model.types.Canal;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class RequisicaoBuilderTest {

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Matrícula da requisição deve ser informada.")
	public void builderSemMatriculaUsuario() throws Exception {
		RequisicaoBuilder builder = Requisicao.builder(Canal.MOBILE);
		builder.build();
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Tipo da requisição deve ser informado.")
	public void builderSemTipoRequisicao() throws Exception {
		RequisicaoBuilder builder = Requisicao.builder(Canal.MOBILE);
		builder.setMatriculaUsuario(123L);
		builder.build();
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Tipo do relatório deve ser informado.")
	public void builderSemReportType() throws Exception {
		RequisicaoBuilder builder = Requisicao.builder(Canal.MOBILE);
		builder.setMatriculaUsuario(123L);
		builder.setTipoRequisicao(1L);
		builder.build();
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Usuário deve ser informado.")
	public void builderSemUsuario() throws Exception {
		RequisicaoBuilder builder = Requisicao.builder(Canal.MOBILE);
		builder.setMatriculaUsuario(123L);
		builder.setTipoRequisicao(1L);
		builder.setReportType("html");
		builder.build();
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Canal de emissão da requisição deve ser informado.")
	public void builderSemCanal() throws Exception {
		RequisicaoBuilder builder = Requisicao.builder(null);
		builder.setMatriculaUsuario(123L);
		builder.setTipoRequisicao(1L);
		builder.setReportType("html");
		builder.setUsuario(mock(Usuario.class));
		builder.build();
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Você não possui permissão de acesso a essa funcionalidade.")
	public void buildNaoEstaVerificandoPrivilegios() throws Exception {
		RequisicaoBuilder builder = Requisicao.builder(Canal.MOBILE);
		builder.setMatriculaUsuario(123L);
		builder.setTipoRequisicao(1L);
		builder.setReportType("html");
		builder.setUsuario(UsuarioDataProvider.usuario1());
		builder.build();
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Você não possui permissão de acesso a essa funcionalidade.")
	public void buildNaoEstaVerificandoPermissoesIndicados() throws Exception {
		RequisicaoBuilder builder = Requisicao.builder(Canal.MOBILE);
		builder.setMatriculaUsuario(123L);
		builder.setTipoRequisicao(1L);
		builder.setReportType("html");
		builder.setUsuario(UsuarioDataProvider.usuario3("1234"));
		builder.build();
	}

	@Test
	public void matriculaEstaIncorretaQuandoGeradaPorIndicacao() throws Exception {
		RequisicaoBuilder builder = Requisicao.builder(Canal.MOBILE);
		builder.setMatriculaUsuario(123L);
		builder.setTipoRequisicao(1L);
		builder.setReportType("json");
		builder.setUsuario(mock(Usuario.class));
		builder.setMatriculaIndicador(1234L);
		builder.setIp("123.456.789.10");
		Requisicao requisicao = builder.build();
		assertThat(requisicao.getMatricula()).isEqualTo(1234L);
	}

	@Test
	public void matriculaEstaIncorretaQuandoGeradaPorUsuario() throws Exception {
		Requisicao requisicao = createRequisicaoUsingBuilder();
		assertThat(requisicao.getMatricula()).isEqualTo(123L);
	}

	@Test
	public void tipoRequisicaoEstaIncorreto() throws Exception {
		Requisicao requisicao = createRequisicaoUsingBuilder();
		assertThat(requisicao.getTpRequisicao()).isEqualTo(2L);
	}

	@Test
	public void tipoProdutoEstaIncorreto() throws Exception {
		Requisicao requisicao = createRequisicaoUsingBuilder();
		assertThat(requisicao.getTpProduto()).isEqualTo(234L);
	}

	@Test
	public void dataInicialEstaIncorreta() throws Exception {
		Requisicao requisicao = createRequisicaoUsingBuilder();
		assertThat(requisicao.getDtInicial()).isEqualTo(PortalCooperadoUtil.getDataAtMidnight());
	}

	@Test
	public void dataFinalEstaIncorreta() throws Exception {
		Requisicao requisicao = createRequisicaoUsingBuilder();
		assertThat(requisicao.getDtFinal()).isEqualTo(PortalCooperadoUtil.getDataAtMidnight());
	}

	@Test
	public void ipEstaIncorreto() throws Exception {
		Requisicao requisicao = createRequisicaoUsingBuilder();
		assertThat(requisicao.getIpUsuario()).isEqualTo("123.456.789.10");
	}

	@Test
	public void canalEstaIncorreto() throws Exception {
		Requisicao requisicao = createRequisicaoUsingBuilder();
		assertThat(requisicao.getTpCanal()).isEqualTo(2L);
	}

	private Requisicao createRequisicaoUsingBuilder() {
		RequisicaoBuilder builder = Requisicao.builder(Canal.MOBILE);
		builder.setMatriculaUsuario(123L);
		builder.setTipoRequisicao(2L);
		builder.setTipoProduto(234L);
		builder.setDataInicial(PortalCooperadoUtil.getDataAtMidnight());
		builder.setDataFinal(PortalCooperadoUtil.getDataAtMidnight());
		builder.setReportType("json");
		builder.setUsuario(mock(Usuario.class));
		builder.setIp("123.456.789.10");
		return builder.build();
	}

}