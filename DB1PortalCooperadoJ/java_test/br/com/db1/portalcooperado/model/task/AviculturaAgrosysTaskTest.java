package br.com.db1.portalcooperado.model.task;

import br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXml;
import br.com.db1.portalcooperado.infrastructure.job.JobStatistics;
import br.com.db1.portalcooperado.infrastructure.rest.translators.xmltoentity.avicultura.AviculturaXmlToAviculturaTranslator;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import br.com.db1.portalcooperado.service.AviculturaAgrosysService;
import br.com.db1.portalcooperado.service.AviculturaService;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;

import static br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXmlDataProvider.AVICULTURA_XML_DOIS;
import static br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXmlDataProvider.AVICULTURA_XML_UM;
import static br.com.db1.portalcooperado.model.entity.avicultura.AviculturaDataProvider.aviculturaDois;
import static br.com.db1.portalcooperado.model.entity.avicultura.AviculturaDataProvider.aviculturaUm;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AviculturaAgrosysTaskTest {

	@Test
	public void deve_executar_importacao_para_todos_arquivos() {
		AviculturaAgrosysService aviculturaAgrosysService = mockarAviculturaAgrosysService();
		AviculturaXmlToAviculturaTranslator translator = mockarTranslator();
		AviculturaService aviculturaService = mockAviculturaService();

		AviculturaAgrosysTask task = new AviculturaAgrosysTask(aviculturaAgrosysService, translator, aviculturaService);
		ReflectionTestUtils.setField(task, "quantidadeDiasRetroativos", 6);
		JobStatistics jobStatistics = task.run();

		JobStatistics esperado = JobStatistics.of();
		esperado.addProcessado();
		esperado.addProcessado();
		esperado.addSucesso();
		esperado.addSucesso();

		assertThat(jobStatistics).isEqualTo(esperado);
	}

	private AviculturaAgrosysService mockarAviculturaAgrosysService() {
		AviculturaAgrosysService aviculturaAgrosysService = mock(AviculturaAgrosysService.class);
		List<AviculturaXml> aviculturas = ImmutableList.of(AVICULTURA_XML_UM, AVICULTURA_XML_DOIS);
		when(aviculturaAgrosysService.findAllAviculturasOf(any(Date.class), any(Date.class))).thenReturn(aviculturas);
		return aviculturaAgrosysService;
	}

	private AviculturaXmlToAviculturaTranslator mockarTranslator() {
		AviculturaXmlToAviculturaTranslator translator = mock(AviculturaXmlToAviculturaTranslator.class);
		when(translator.translate(AVICULTURA_XML_UM)).thenReturn(aviculturaUm());
		when(translator.translate(AVICULTURA_XML_DOIS)).thenReturn(aviculturaDois());
		return translator;
	}

	private AviculturaService mockAviculturaService() {
		Avicultura aviculturaUm = aviculturaUm();
		Avicultura aviculturaDois = aviculturaDois();
		AviculturaService aviculturaService = mock(AviculturaService.class);
		when(aviculturaService.save(aviculturaUm)).thenReturn(aviculturaUm);
		when(aviculturaService.save(aviculturaDois)).thenReturn(aviculturaDois);
		return aviculturaService;
	}

}