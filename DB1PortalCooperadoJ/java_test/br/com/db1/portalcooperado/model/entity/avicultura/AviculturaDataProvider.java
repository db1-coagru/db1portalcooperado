package br.com.db1.portalcooperado.model.entity.avicultura;

import br.com.db1.portalcooperado.model.entity.UsuarioDataProvider;
import jersey.repackaged.com.google.common.collect.ImmutableList;

import java.util.Date;

import static br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItemDataProvider.aviculturaItemDois;
import static br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItemDataProvider.aviculturaItemUm;

public class AviculturaDataProvider {

	private AviculturaDataProvider() {

	}

	public static Avicultura aviculturaUm() {
		Avicultura avicultura = new Avicultura();
		avicultura.setProdutor(UsuarioDataProvider.usuario1());
		avicultura.setData(new Date());
		avicultura.setGalpao("galpao um");
		avicultura.setLinhagem("linhagem um");
		avicultura.setLote("lote um");
		avicultura.setNomeTecnico("nome do tecnico um");
		avicultura.setQuantidade(100);
		avicultura.setSexo("M");
		avicultura.setItens(ImmutableList.of(aviculturaItemUm()));
		return avicultura;
	}

	public static Avicultura aviculturaDois() {
		Avicultura avicultura = new Avicultura();
		avicultura.setProdutor(UsuarioDataProvider.usuario2());
		avicultura.setData(new Date());
		avicultura.setGalpao("galpao dois");
		avicultura.setLinhagem("linhagem dois");
		avicultura.setLote("lote dois");
		avicultura.setNomeTecnico("nome do tecnico dois");
		avicultura.setQuantidade(100);
		avicultura.setSexo("F");
		avicultura.setItens(ImmutableList.of(aviculturaItemUm(), aviculturaItemDois()));
		return avicultura;
	}
}
