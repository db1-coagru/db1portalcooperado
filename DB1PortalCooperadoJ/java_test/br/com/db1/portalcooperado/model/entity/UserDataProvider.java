package br.com.db1.portalcooperado.model.entity;

import br.com.db1.security.model.entity.User;

public class UserDataProvider {

	public static User user1() {
		User user = new User();
		user.setIdUser(1L);
		user.setName("User 1");
		user.setLogin("123");
		return user;
	}

	public static User user2() {
		User user = new User();
		user.setIdUser(2L);
		user.setName("User 2");
		user.setLogin("1234");
		return user;
	}

}
