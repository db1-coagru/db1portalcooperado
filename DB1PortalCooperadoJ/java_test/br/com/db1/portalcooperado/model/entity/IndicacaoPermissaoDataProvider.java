package br.com.db1.portalcooperado.model.entity;

import br.com.db1.portalcooperado.model.types.PermissaoIndicado;

public class IndicacaoPermissaoDataProvider {

	public static IndicacaoPermissao actEventos(Indicacao indicacao) {
		IndicacaoPermissao indicacaoPermissao = new IndicacaoPermissao();
		indicacaoPermissao.setIndicacao(indicacao);
		indicacaoPermissao.setPermissao(PermissaoIndicado.ACTEVENTOS);
		return indicacaoPermissao;
	}

	public static IndicacaoPermissao actNoticias(Indicacao indicacao) {
		IndicacaoPermissao indicacaoPermissao = new IndicacaoPermissao();
		indicacaoPermissao.setIndicacao(indicacao);
		indicacaoPermissao.setPermissao(PermissaoIndicado.ACTNOTICIAS);
		return indicacaoPermissao;
	}

}
