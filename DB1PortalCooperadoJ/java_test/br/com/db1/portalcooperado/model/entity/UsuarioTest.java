package br.com.db1.portalcooperado.model.entity;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.model.AbstractPojoTest;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import br.com.db1.security.model.entity.Profile;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static br.com.db1.portalcooperado.model.entity.IndicacaoPermissaoDataProvider.actEventos;
import static br.com.db1.portalcooperado.model.entity.IndicacaoPermissaoDataProvider.actNoticias;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class UsuarioTest extends AbstractPojoTest {

	public static final String PERMISSOES_INDICADO = "permissoesIndicado";

	public UsuarioTest() {
		super(Usuario.class);
	}

	@DataProvider(name = PERMISSOES_INDICADO)
	public static Object[][] hasPermissaoTo() {
		Object[][] result = new Object[PermissaoIndicado.values().length][1];
		int count = 0;
		for (PermissaoIndicado permissaoIndicado : PermissaoIndicado.values()) {
			result[count][0] = permissaoIndicado;
			count++;
		}
		return result;
	}

	@Test(dataProvider = PERMISSOES_INDICADO, expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Você não possui permissão de acesso a essa funcionalidade.")
	public void usuarioComPermissaoQuandoDeveriaDispararException(PermissaoIndicado permissaoIndicado)
			throws Exception {
		Usuario usuario = UsuarioDataProvider.usuario4();
		usuario.hasPermissaoThrowsExceptionIfFalse(1234L, permissaoIndicado);
	}

	@Test(dataProvider = PERMISSOES_INDICADO, expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Você não possui permissão de acesso a essa funcionalidade.")
	public void usuarioIndicadoComPermissaoQuandoDeveriaDispararException(PermissaoIndicado permissaoIndicado)
			throws Exception {
		Usuario usuario = new Usuario();
		usuario.setMatricula(123L);
		usuario.setProfile(mock(Profile.class));
		usuario.hasPermissaoThrowsExceptionIfFalse(1234L, permissaoIndicado);
	}

	@Test(dataProvider = PERMISSOES_INDICADO)
	public void usuarioComPermissaoDisparandoExceptionQuandoNaoDeveria(PermissaoIndicado permissaoIndicado)
			throws Exception {
		Usuario usuario = new Usuario();
		usuario.setMatricula(12345L);
		usuario.setProfile(ProfileDataProvider.profile2());
		assertThat(usuario.hasPermissaoThrowsExceptionIfFalse(12345L, permissaoIndicado)).isTrue();
	}

	@Test(dataProvider = PERMISSOES_INDICADO)
	public void usuarioIndicadoComPermissaoDisparandoExceptionQuandoNaoDeveria(PermissaoIndicado permissaoIndicado)
			throws Exception {
		Usuario usuario = UsuarioDataProvider.usuario3("1234");
		assertThat(usuario.hasPermissaoThrowsExceptionIfFalse(1234L, permissaoIndicado)).isTrue();
	}

	@Test
	public void naoConseguiuAdicionarIndicacaoQuandoDeveria() throws Exception {
		Indicacao indicacao1 = IndicacaoDataProvider.indicacao1();
		Indicacao indicacao2 = IndicacaoDataProvider.indicacao2();

		Usuario usuario = new Usuario();
		usuario.add(indicacao1);
		usuario.add(indicacao2);
		assertThat(usuario.getIndicacoes()).containsExactly(indicacao1, indicacao2);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Já existe uma indicação para este usuário.")
	public void conseguiuAdicionarIndicacaoParaIndicadoJaAdicionado() throws Exception {
		Indicacao indicacao1 = IndicacaoDataProvider.indicacao1();

		Usuario usuario = new Usuario();
		usuario.add(indicacao1);
		usuario.add(indicacao1);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Indicação deve ser informada.")
	public void conseguiuAdicionarIndicacaoNula() throws Exception {
		Usuario usuario = new Usuario();
		usuario.add(null);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Indicação deve ser informada.")
	public void conseguiuEditarIndicacaoNula() throws Exception {
		Usuario usuario = new Usuario();
		usuario.edit(null);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Impossível editar. Indicação inexistente para este usuário.")
	public void conseguiuEditarIndicacaoInexistente() throws Exception {
		Usuario usuario = new Usuario();
		usuario.edit(IndicacaoDataProvider.indicacao1());
	}

	@Test
	public void naoEditouIndicacaoCorretamente() throws Exception {
		Indicacao indicacao2 = IndicacaoDataProvider.indicacao2();

		Usuario usuario = new Usuario();
		usuario.setMatricula(1L);
		usuario.add(IndicacaoDataProvider.indicacao1());
		usuario.add(indicacao2);
		usuario.add(IndicacaoDataProvider.indicacao3());

		Indicacao indicacao2Editada = IndicacaoDataProvider.indicacao2();
		indicacao2Editada.setIndicador(usuario);
		IndicacaoPermissao indicacaoPermissao = actEventos(indicacao2Editada);
		IndicacaoPermissao indicacaoPermissao2 = actNoticias(indicacao2Editada);
		indicacao2Editada.setPermissoes(ImmutableList.of(indicacaoPermissao, indicacaoPermissao2));
		usuario.edit(indicacao2Editada);

		assertThat(usuario.getIndicacoes().get(1)).isEqualToComparingFieldByField(indicacao2Editada);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Indicação deve ser informada.")
	public void conseguiuRemoverIndicacaoNula() throws Exception {
		Usuario usuario = new Usuario();
		usuario.remove(null);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Impossível remover. Indicação inexistente para este usuário.")
	public void conseguiuRemoverIndicacaoInexistente() throws Exception {
		Usuario usuario = new Usuario();
		usuario.remove(IndicacaoDataProvider.indicacao1());
	}

	@Test
	public void naoRemoveuIndicacaoCorretamente() throws Exception {
		Indicacao indicacao1 = IndicacaoDataProvider.indicacao1();
		Indicacao indicacao2 = IndicacaoDataProvider.indicacao2();
		Indicacao indicacao3 = IndicacaoDataProvider.indicacao3();

		Usuario usuario = new Usuario();
		usuario.setMatricula(1L);
		usuario.add(indicacao1);
		usuario.add(indicacao2);
		usuario.add(indicacao3);
		usuario.remove(indicacao2);

		assertThat(usuario.getIndicacoes()).containsExactly(indicacao1, indicacao3);
	}

	@Test
	public void equalsEstaIncorreto() throws Exception {
		Usuario usuario1 = new Usuario();
		usuario1.setMatricula(1l);

		Usuario usuario2 = new Usuario();
		usuario2.setMatricula(1l);

		Usuario usuario3 = new Usuario();
		usuario3.setMatricula(2l);

		assertThat(usuario1).isEqualTo(usuario2).isNotEqualTo(usuario3).isNotEqualTo(new Object());
	}

	@Test
	public void hashCodeEstaIncorreto() throws Exception {
		int expected = PortalCooperadoUtil.hashCode(123L);
		int notExpected = PortalCooperadoUtil.hashCode(1234L);

		Usuario usuario1 = new Usuario();
		usuario1.setMatricula(123l);

		assertThat(usuario1.hashCode()).isEqualTo(expected).isNotEqualTo(notExpected);
	}
}