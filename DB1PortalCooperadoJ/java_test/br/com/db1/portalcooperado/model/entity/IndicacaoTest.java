package br.com.db1.portalcooperado.model.entity;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.model.AbstractPojoTest;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IndicacaoTest extends AbstractPojoTest {

	public IndicacaoTest() {
		super(Indicacao.class);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Ao menos uma permissão deve ser informada.")
	public void permissoesIsEmptyWhenEditPermissoes() throws Exception {
		Indicacao indicacao = new Indicacao();
		indicacao.editPermissoes(ImmutableList.<Integer>of());
	}

	@Test
	public void editPermissoesIsIncorrect() throws Exception {
		Usuario indicador = UsuarioDataProvider.usuario1();
		Usuario indicado = UsuarioDataProvider.usuario2();

		List<Integer> permissoes = ImmutableList.of(0, 2, 4);

		Indicacao indicacao = new Indicacao();
		indicacao.setIndicador(indicador);
		indicacao.setIndicado(indicado);
		indicacao.editPermissoes(permissoes);

		Indicacao indicacao2 = new Indicacao();
		indicacao2.setIndicador(indicador);
		indicacao2.setIndicado(indicado);

		List<IndicacaoPermissao> expected = Lists.newLinkedList();
		expected.add(createIndicacaoPermissao(0, indicacao2));
		expected.add(createIndicacaoPermissao(2, indicacao2));
		expected.add(createIndicacaoPermissao(4, indicacao2));

		assertThat(indicacao.getPermissoes()).isEqualTo(expected);
	}

	@Test
	public void permissoesIndicadoIsIncorrect() throws Exception {
		Usuario indicador = UsuarioDataProvider.usuario1();
		Usuario indicado = UsuarioDataProvider.usuario2();

		Indicacao indicacao = new Indicacao();
		indicacao.setIndicador(indicador);
		indicacao.setIndicado(indicado);
		indicacao.editPermissoes(ImmutableList.of(0, 2, 4));

		List<PermissaoIndicado> expected = Lists.newLinkedList();
		expected.add(PermissaoIndicado.ACTEVENTOS);
		expected.add(PermissaoIndicado.ACTENTREGAPRODUCAO);
		expected.add(PermissaoIndicado.ACTRECOMENDACAOTECNICA);
		assertThat(indicacao.getPermissoesIndicado()).isEqualTo(expected);
	}

	@Test
	public void matriculaIndicadorIsIncorrect() throws Exception {
		Usuario indicador = mock(Usuario.class);
		when(indicador.getMatricula()).thenReturn(13L);

		Indicacao indicacao = new Indicacao();
		indicacao.setIndicador(indicador);

		assertThat(indicacao.getMatriculaIndicador()).isEqualTo("13");
	}

	@Test
	public void nomeIndicadorInIncorrect() throws Exception {
		Usuario indicador = mock(Usuario.class);
		when(indicador.getNome()).thenReturn("Teste");

		Indicacao indicacao = new Indicacao();
		indicacao.setIndicador(indicador);

		assertThat(indicacao.getNomeIndicador()).isEqualTo("Teste");
	}

	private IndicacaoPermissao createIndicacaoPermissao(int permissaoIndicado, Indicacao indicacao) {
		IndicacaoPermissao indicacaoPermissao = new IndicacaoPermissao();
		indicacaoPermissao.setIndicacao(indicacao);
		indicacaoPermissao.setPermissao(permissaoIndicado);
		return indicacaoPermissao;
	}
}