package br.com.db1.portalcooperado.model.entity;

import br.com.db1.portalcooperado.model.AbstractPojoTest;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import org.testng.annotations.Test;

import static br.com.db1.portalcooperado.model.types.TipoNotificacao.ROMANEIO_AGRICOLA;
import static org.assertj.core.api.Assertions.assertThat;

@Test(dataProviderClass = NotificacaoDataProvider.class)
public class NotificacaoTest extends AbstractPojoTest {

	public NotificacaoTest() {
		super(Notificacao.class);
	}

	@Test(dataProvider = NotificacaoDataProvider.NOT_EQUALS)
	public void equalsEstaTrueQuandoDeveriaSerFalso(Object other) throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setMatricula(1234l);
		notificacao.setData(PortalCooperadoUtil.getDataAtMidnight());
		notificacao.setHora("10:10:10");
		notificacao.setTipo(ROMANEIO_AGRICOLA);
		assertThat(notificacao).isNotEqualTo(other);
	}

	@Test
	public void equalsEstaFalseQuandoDeveriaSerTrue() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setMatricula(1234l);
		notificacao.setData(PortalCooperadoUtil.getDataAtMidnight());
		notificacao.setHora("10:10:10");
		notificacao.setTipo(ROMANEIO_AGRICOLA);
		assertThat(notificacao).isEqualTo(notificacao);
	}

	@Test
	public void hashCodeEstaErrado() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setMatricula(1234l);
		notificacao.setData(PortalCooperadoUtil.getDataAtMidnight());
		notificacao.setHora("10:10:10");
		notificacao.setTipo(ROMANEIO_AGRICOLA);

		int expected = PortalCooperadoUtil
				.hashCode(1234l, PortalCooperadoUtil.getDataAtMidnight(), "10:10:10", ROMANEIO_AGRICOLA);
		assertThat(notificacao.hashCode()).isEqualTo(expected);
	}

	@Test
	public void possuiLote() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setLote("123");
		assertThat(notificacao.possuiLote()).isTrue();
	}

	@Test
	public void deveNaoPossuirLoteQuandoOMesmoForStringVazia() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setLote("");
		assertThat(notificacao.possuiLote()).isFalse();
	}

	@Test
	public void deveNaoPossuirLoteQuandoOMesmoPossuirSomenteEspacos() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setLote("          ");
		assertThat(notificacao.possuiLote()).isFalse();
	}

	@Test
	public void deveNaoPossuirLoteQuandoOMesmoForNulo() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setLote(null);
		assertThat(notificacao.possuiLote()).isFalse();
	}

	@Test
	public void deveRetornarAFilialEmLowerCase() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setFilial("FILIAL DE TESTE");
		assertThat(notificacao.getFilialLowercase()).isEqualTo("filial de teste");
	}

	@Test
	public void deveRetornarStringVaziaQuandoFilialNaoExistirAoPegarLowercase() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setFilial(null);
		assertThat(notificacao.getFilialLowercase()).isEqualTo("");
	}

	@Test
	public void deveRetornarStringVaziaQuandoFilialForStringVaziaAoPegarLowercase() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setFilial("");
		assertThat(notificacao.getFilialLowercase()).isEqualTo("");
	}

	@Test
	public void deveRetornarStringVaziaQuandoFilialForStringComApenasEspacosBrancosAoPegarLowercase() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setFilial("       ");
		assertThat(notificacao.getFilialLowercase()).isEqualTo("");
	}

	@Test
	public void deveRetornarLoteEmLowerCase() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setLote("FILIAL DE TESTE");
		assertThat(notificacao.getLoteLowercase()).isEqualTo("filial de teste");
	}

	@Test
	public void deveRetornarStringVaziaQuandoLoteNaoExistirAoPegarLowercase() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setLote(null);
		assertThat(notificacao.getLoteLowercase()).isEqualTo("");
	}

	@Test
	public void deveRetornarStringVaziaQuandoLoteForStringVaziaAoPegarLowercase() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setLote("");
		assertThat(notificacao.getLoteLowercase()).isEqualTo("");
	}

	@Test
	public void deveRetornarStringVaziaQuandoLoteForStringComApenasEspacosBrancosAoPegarLowercase() throws Exception {
		Notificacao notificacao = Notificacao.of();
		notificacao.setLote("       ");
		assertThat(notificacao.getLoteLowercase()).isEqualTo("");
	}

}