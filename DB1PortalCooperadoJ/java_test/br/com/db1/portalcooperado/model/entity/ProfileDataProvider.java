package br.com.db1.portalcooperado.model.entity;

import br.com.db1.security.model.entity.Privilege;
import br.com.db1.security.model.entity.Profile;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;

import java.util.List;

import static br.com.db1.portalcooperado.model.entity.PrivilegeDataProvider.*;

public class ProfileDataProvider {

	private ProfileDataProvider() {

	}

	public static Profile profile1() {
		Profile profile = new Profile();
		profile.setIdProfile(1L);
		profile.setDescription("Perfil 1");
		profile.setPrivileges(ImmutableList.of(actEventos()));
		return profile;
	}

	public static Profile profile2() {
		Profile profile = new Profile();
		profile.setIdProfile(2L);
		profile.setDescription("Perfil 2");
		List<Privilege> privilegios = Lists.newLinkedList();
		privilegios.add(actEventos());
		privilegios.add(actNoticias());
		privilegios.add(actEntregaProducao());
		privilegios.add(actPosicaoFinanceira());
		privilegios.add(actRecomendacaoTecnica());
		privilegios.add(actRomaneioAgricola());
		privilegios.add(actVendaInsumos());
		privilegios.add(actVendaVeterinaria());
		privilegios.add(actVendaPecas());
		privilegios.add(actVendaRacaoSuplemento());
		privilegios.add(actVendaCavacoMaravalha());
		privilegios.add(actFixacaoFrango());
		privilegios.add(actAvicultura());
		profile.setPrivileges(privilegios);
		return profile;
	}

	public static Profile profile3() {
		Profile profile = new Profile();
		profile.setIdProfile(2L);
		profile.setDescription("Perfil 2");
		profile.setPrivileges(ImmutableList.<Privilege>of());
		return profile;
	}
}
