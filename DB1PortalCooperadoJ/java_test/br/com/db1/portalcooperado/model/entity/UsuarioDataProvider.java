package br.com.db1.portalcooperado.model.entity;

import jersey.repackaged.com.google.common.collect.ImmutableList;

import java.util.List;

public class UsuarioDataProvider {

	private UsuarioDataProvider() {

	}

	public static Usuario usuario1() {
		Usuario usuario = new Usuario();
		usuario.setId(1L);
		usuario.setIdUser(1L);
		usuario.setNome("Usuario 1");
		usuario.setMatricula(123L);
		usuario.setMobile(true);
		usuario.setProfile(ProfileDataProvider.profile1());
		return usuario;
	}

	public static Usuario usuario2() {
		Usuario usuario = new Usuario();
		usuario.setId(2L);
		usuario.setIdUser(2L);
		usuario.setNome("Usuario 2");
		usuario.setMatricula(1234L);
		usuario.setMobile(false);
		usuario.setProfile(ProfileDataProvider.profile1());
		return usuario;
	}

	public static Usuario usuario3(String matriculaIndicador) {
		List<Indicacao> indicacoesIndicado = ImmutableList.of(IndicacaoDataProvider.mockIndicacao1(matriculaIndicador));

		Usuario usuario = new Usuario();
		usuario.setId(3L);
		usuario.setIdUser(3L);
		usuario.setNome("Usuario 3");
		usuario.setMatricula(12345L);
		usuario.setMobile(true);
		usuario.setProfile(ProfileDataProvider.profile1());
		usuario.setIndicacoesIndicado(indicacoesIndicado);
		return usuario;
	}

	public static Usuario usuario4() {
		Usuario usuario = new Usuario();
		usuario.setId(4L);
		usuario.setIdUser(4L);
		usuario.setNome("Usuario 4");
		usuario.setMatricula(123456L);
		usuario.setMobile(false);
		usuario.setProfile(ProfileDataProvider.profile3());
		return usuario;
	}

	public static Usuario usuario5() {
		Usuario usuario = new Usuario();
		usuario.setId(5L);
		usuario.setIdUser(5L);
		usuario.setNome("Usuario 5");
		usuario.setMatricula(5L);
		usuario.setMobile(true);
		usuario.setProfile(ProfileDataProvider.profile2());
		return usuario;
	}

}
