package br.com.db1.portalcooperado.model.entity;

import br.com.db1.portalcooperado.util.PortalCooperadoUtil;

public class RequisicaoDataProvider {

	private RequisicaoDataProvider() {

	}

	public static Requisicao requisicao1() {
		Requisicao requisicao = new Requisicao();
		requisicao.setId(1L);
		requisicao.setTpRequisicao(2L);
		requisicao.setTpProduto(1L);
		requisicao.setDtInicial(PortalCooperadoUtil.getDataAtMidnight());
		requisicao.setDtFinal(PortalCooperadoUtil.getDataAtMidnight());
		requisicao.setIpUsuario("123.456.789.10");
		return requisicao;
	}

	public static Requisicao requisicao2() {
		Requisicao requisicao = new Requisicao();
		requisicao.setId(2L);
		requisicao.setTpRequisicao(1L);
		requisicao.setTpProduto(1L);
		requisicao.setDtInicial(PortalCooperadoUtil.getDataAtMidnight());
		requisicao.setDtFinal(PortalCooperadoUtil.getDataAtMidnight());
		requisicao.setIpUsuario("123.456.789.10");
		return requisicao;
	}
}
