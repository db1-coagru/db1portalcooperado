package br.com.db1.portalcooperado.model.entity;

import br.com.db1.portalcooperado.model.AbstractPojoTest;
import br.com.db1.portalcooperado.model.builder.RequisicaoBuilder;
import br.com.db1.portalcooperado.model.types.Canal;
import br.com.db1.portalcooperado.relatorio.types.ReportType;
import br.com.db1.portalcooperado.requisicao.Propriedades;
import br.com.db1.portalcooperado.util.DateFormatter;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import org.testng.annotations.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class RequisicaoTest extends AbstractPojoTest {

	public RequisicaoTest() {
		super(Requisicao.class);
	}

	@Test
	public void descricaoWhenCanalPortalIsIncorrect() throws Exception {
		Requisicao requisicao = new Requisicao();
		requisicao.setTpCanal(Canal.PORTAL);
		assertThat(requisicao.getTpCanalDescricao()).isEqualTo("Portal");
	}

	@Test
	public void descricaoWhenCanalMobileIsIncorrect() throws Exception {
		Requisicao requisicao = new Requisicao();
		requisicao.setTpCanal(Canal.MOBILE);
		assertThat(requisicao.getTpCanalDescricao()).isEqualTo("Mobile");
	}

	@Test
	public void comandoRequisicaoEntregaProducaoErrado() throws Exception {
		Date data = PortalCooperadoUtil.getDataAtMidnight();
		RequisicaoBuilder builder = RequisicaoBuilder.of(Canal.MOBILE);
		builder.setIp("123.456.789.10");
		builder.setMatriculaUsuario(5L);
		builder.setUsuario(UsuarioDataProvider.usuario5());
		builder.setTipoRequisicao(2L);
		builder.setDataInicial(data);
		builder.setDataFinal(data);
		builder.setReportType(ReportType.HTML);
		builder.setTipoProduto(234L);
		Requisicao requisicao = builder.build();
		requisicao.setId(1L);

		String propriedade = Propriedades.getInstancia().getPropriedade(Propriedades.PROP_LINHA_COMANDO_PROGRESS);
		String dataAsString = DateFormatter.DD_MM_YYYY.format(data);
		String expected = String.format("%s '5;1;2;234;%s;%s'", propriedade, dataAsString, dataAsString);
		assertThat(requisicao.montarComando()).isEqualTo(expected);
	}

	@Test
	public void comandoRequisicaoPosicaoFinanceiraErrado() throws Exception {
		RequisicaoBuilder builder = RequisicaoBuilder.of(Canal.MOBILE);
		builder.setIp("123.456.789.10");
		builder.setMatriculaUsuario(5L);
		builder.setUsuario(UsuarioDataProvider.usuario5());
		builder.setTipoRequisicao(1L);
		builder.setReportType(ReportType.HTML);
		Requisicao requisicao = builder.build();
		requisicao.setId(1L);

		String propriedade = Propriedades.getInstancia().getPropriedade(Propriedades.PROP_LINHA_COMANDO_PROGRESS);
		String expected = String.format("%s '5;1;1'", propriedade);
		assertThat(requisicao.montarComando()).isEqualTo(expected);
	}
}