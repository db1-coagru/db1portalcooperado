package br.com.db1.portalcooperado.model.entity;

import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import org.testng.annotations.DataProvider;

import java.util.Date;

public class NotificacaoDataProvider {

	public static final String NOT_EQUALS = "notEquals";

	public static final String TITULOS = "titulos";

	@DataProvider(name = NOT_EQUALS)
	public static Object[][] notEquals() {
		Notificacao notificacao1 = Notificacao.of();
		notificacao1.setMatricula(123l);
		notificacao1.setData(PortalCooperadoUtil.getDataAtMidnight());
		notificacao1.setHora("10:10:10");
		notificacao1.setTipo(TipoNotificacao.ROMANEIO_AGRICOLA);

		Notificacao notificacao2 = Notificacao.of();
		notificacao2.setMatricula(1234l);
		notificacao2.setData(PortalCooperadoUtil.getDataAtMidnight());
		notificacao2.setHora("10:10:11");
		notificacao2.setTipo(TipoNotificacao.ROMANEIO_AGRICOLA);

		Notificacao notificacao3 = Notificacao.of();
		notificacao3.setMatricula(1234l);
		notificacao3.setData(PortalCooperadoUtil.getDataAtMidnight());
		notificacao3.setHora("10:10:10");
		notificacao3.setTipo(TipoNotificacao.VENDA_INSUMOS);

		Notificacao notificacao4 = Notificacao.of();
		notificacao4.setMatricula(1234l);
		notificacao4.setData(new Date());
		notificacao4.setHora("10:10:10");
		notificacao4.setTipo(TipoNotificacao.ROMANEIO_AGRICOLA);

		return new Object[][] {
				{ notificacao1 },
				{ notificacao2 },
				{ notificacao3 },
				{ notificacao4 },
				{ new Object() },
		};
	}

	@DataProvider(name = TITULOS)
	public static Object[][] titulos() {
		Notificacao notificacao1 = Notificacao.of();
		notificacao1.setTipo(TipoNotificacao.ROMANEIO_AGRICOLA);

		Notificacao notificacao2 = Notificacao.of();
		notificacao2.setTipo(TipoNotificacao.VENDA_INSUMOS);

		Notificacao notificacao3 = Notificacao.of();
		notificacao3.setTipo(TipoNotificacao.VENDA_VETERINARIA);

		Notificacao notificacao4 = Notificacao.of();
		notificacao4.setTipo(TipoNotificacao.VENDA_PECAS);

		Notificacao notificacao5 = Notificacao.of();
		notificacao5.setTipo(TipoNotificacao.VENDA_RACAO_SUPLEMENTO);

		Notificacao notificacao6 = Notificacao.of();
		notificacao6.setTipo(TipoNotificacao.VENDA_CAVACO_MARAVALHA);

		Notificacao notificacao7 = Notificacao.of();
		notificacao7.setTipo(TipoNotificacao.FECHAMENTO_DE_FRANGO);

		return new Object[][] {
				{ notificacao1 },
				{ notificacao2 },
				{ notificacao3 },
				{ notificacao4 },
				{ notificacao5 },
				{ notificacao6 },
				{ notificacao7 }
		};
	}

}
