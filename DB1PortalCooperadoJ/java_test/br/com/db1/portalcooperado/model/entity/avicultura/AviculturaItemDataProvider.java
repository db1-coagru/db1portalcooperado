package br.com.db1.portalcooperado.model.entity.avicultura;

import jersey.repackaged.com.google.common.collect.ImmutableList;

import static br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItemImagemDataProvider.*;

public class AviculturaItemDataProvider {

	private AviculturaItemDataProvider(){

	}

	public static AviculturaItem aviculturaItemUm() {
		AviculturaItem aviculturaItem = new AviculturaItem();
		aviculturaItem.setDescricao("descricao um");
		aviculturaItem.setObservacao("observacao um");
		aviculturaItem.setResposta(AviculturaResposta.NAO_CONFORMIDADE);
		aviculturaItem.setImagens(ImmutableList.of(aviculturaItemImagemUm(), aviculturaItemImagemDois()));
		return aviculturaItem;
	}

	public static AviculturaItem aviculturaItemDois() {
		AviculturaItem aviculturaItem = new AviculturaItem();
		aviculturaItem.setDescricao("descricao dois");
		aviculturaItem.setObservacao("observacao dois");
		aviculturaItem.setResposta(AviculturaResposta.CONFORMIDADE);
		aviculturaItem.setImagens(ImmutableList.of(aviculturaItemImagemTres(), aviculturaItemImagemQuatro()));
		return aviculturaItem;
	}

}
