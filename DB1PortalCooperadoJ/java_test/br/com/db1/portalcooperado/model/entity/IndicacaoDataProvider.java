package br.com.db1.portalcooperado.model.entity;

import jersey.repackaged.com.google.common.collect.ImmutableList;

import static br.com.db1.portalcooperado.model.entity.IndicacaoPermissaoDataProvider.actEventos;
import static br.com.db1.portalcooperado.model.types.PermissaoIndicado.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IndicacaoDataProvider {

	public static Indicacao mockIndicacao1(String matricula) {
		Indicacao indicacao = mock(Indicacao.class);
		when(indicacao.hasPermissaoTo(matricula, ACTEVENTOS)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTNOTICIAS)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTENTREGAPRODUCAO)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTPOSICAOFINANCEIRA)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTRECOMENDACAOTECNICA)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTFIXACAOFRANGO)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTVENDACAVACOMARAVALHA)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTVENDAINSUMOS)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTVENDAPECAS)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTVENDARACAOSUPLEMENTO)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTVENDAVETERINARIA)).thenReturn(true);
		when(indicacao.hasPermissaoTo(matricula, ACTAVICULTURA)).thenReturn(true);
		return indicacao;
	}

	public static Indicacao indicacao1() {
		Indicacao indicacao = new Indicacao();
		indicacao.setIndicado(UsuarioDataProvider.usuario1());
		indicacao.setPermissoes(ImmutableList.of(actEventos(indicacao)));
		return indicacao;
	}

	public static Indicacao indicacao2() {
		Indicacao indicacao = new Indicacao();
		indicacao.setIndicado(UsuarioDataProvider.usuario2());
		indicacao.setPermissoes(ImmutableList.of(actEventos(indicacao)));
		return indicacao;
	}

	public static Indicacao indicacao3() {
		Indicacao indicacao = new Indicacao();
		indicacao.setIndicado(UsuarioDataProvider.usuario4());
		indicacao.setPermissoes(ImmutableList.of(actEventos(indicacao)));
		return indicacao;
	}
}
