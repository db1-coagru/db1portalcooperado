package br.com.db1.portalcooperado.model.entity;

import br.com.db1.security.model.entity.Privilege;

public class PrivilegeDataProvider {

	private PrivilegeDataProvider() {

	}

	public static Privilege actEventos() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTEVENTOS");
		return privilege;
	}

	public static Privilege actNoticias() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTNOTICIAS");
		return privilege;
	}

	public static Privilege actEntregaProducao() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTENTREGAPRODUCAO");
		return privilege;
	}

	public static Privilege actPosicaoFinanceira() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTPOSICAOFINANCEIRA");
		return privilege;
	}

	public static Privilege actRecomendacaoTecnica() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTRECOMENDACAOTECNICA");
		return privilege;
	}

	public static Privilege actRomaneioAgricola() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTROMANEIOAGRICOLA");
		return privilege;
	}

	public static Privilege actVendaInsumos() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTVENDAINSUMOS");
		return privilege;
	}

	public static Privilege actVendaVeterinaria() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTVENDAVETERINARIA");
		return privilege;
	}

	public static Privilege actVendaPecas() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTVENDAPECAS");
		return privilege;
	}

	public static Privilege actVendaRacaoSuplemento() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTVENDARACAOSUPLEMENTO");
		return privilege;
	}

	public static Privilege actVendaCavacoMaravalha() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTVENDACAVACOMARAVALHA");
		return privilege;
	}

	public static Privilege actFixacaoFrango() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTFIXACAOFRANGO");
		return privilege;
	}

	public static Privilege actAvicultura() {
		Privilege privilege = new Privilege();
		privilege.setIdentifier("ACTAVICULTURA");
		return privilege;
	}

}
