package br.com.db1.portalcooperado.model.entity.avicultura;

class AviculturaItemImagemDataProvider {

	private AviculturaItemImagemDataProvider() {

	}

	static AviculturaItemImagem aviculturaItemImagemUm() {
		AviculturaItemImagem aviculturaItemImagem = new AviculturaItemImagem();
		aviculturaItemImagem.setNome("imagem1");
		return aviculturaItemImagem;
	}

	static AviculturaItemImagem aviculturaItemImagemDois() {
		AviculturaItemImagem aviculturaItemImagem = new AviculturaItemImagem();
		aviculturaItemImagem.setNome("imagem2");
		return aviculturaItemImagem;
	}

	static AviculturaItemImagem aviculturaItemImagemTres() {
		AviculturaItemImagem aviculturaItemImagem = new AviculturaItemImagem();
		aviculturaItemImagem.setNome("imagem3");
		return aviculturaItemImagem;
	}

	static AviculturaItemImagem aviculturaItemImagemQuatro() {
		AviculturaItemImagem aviculturaItemImagem = new AviculturaItemImagem();
		aviculturaItemImagem.setNome("imagem4");
		return aviculturaItemImagem;
	}

}
