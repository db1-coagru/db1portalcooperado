package br.com.db1.portalcooperado.infrastructure.agrosys.avicultura;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public final class AviculturaItemXmlDataProvider {

	public static final AviculturaItemXml AVICULTURA_ITEM_XML_UM = aviculturaItemXmlUm();

	public static final AviculturaItemXml AVICULTURA_ITEM_XML_DOIS = aviculturaItemXmlDois();

	public static final AviculturaItemXml AVICULTURA_ITEM_XML_TRES = aviculturaItemXmlTres();

	private AviculturaItemXmlDataProvider() {

	}

	private static AviculturaItemXml aviculturaItemXmlUm() {
		AviculturaItemXml aviculturaItemXml = mock(AviculturaItemXml.class);
		when(aviculturaItemXml.getResposta()).thenReturn("NAO CONFORMIDADE");
		when(aviculturaItemXml.getObservacao()).thenReturn("fora do padrão sugerido");
		when(aviculturaItemXml.getDescricao()).thenReturn("Descrição de testes do item um");
		return aviculturaItemXml;
	}

	private static AviculturaItemXml aviculturaItemXmlDois() {
		AviculturaItemXml aviculturaItemXml = mock(AviculturaItemXml.class);
		when(aviculturaItemXml.getResposta()).thenReturn("CONFORMIDADE");
		when(aviculturaItemXml.getObservacao()).thenReturn("fora do padrão sugerido também");
		when(aviculturaItemXml.getDescricao()).thenReturn("Descrição de testes do item dois");
		return aviculturaItemXml;
	}

	private static AviculturaItemXml aviculturaItemXmlTres() {
		AviculturaItemXml aviculturaItemXml = mock(AviculturaItemXml.class);
		when(aviculturaItemXml.getResposta()).thenReturn("CONFORMIDADE");
		when(aviculturaItemXml.getDescricao()).thenReturn("Descrição de testes do item três");
		return aviculturaItemXml;
	}

}
