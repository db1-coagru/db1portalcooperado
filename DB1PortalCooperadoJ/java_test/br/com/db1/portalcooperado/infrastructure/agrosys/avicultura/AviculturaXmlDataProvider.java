package br.com.db1.portalcooperado.infrastructure.agrosys.avicultura;

import jersey.repackaged.com.google.common.collect.ImmutableList;

import java.util.Date;

import static br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaItemXmlDataProvider.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public final class AviculturaXmlDataProvider {

	public static final AviculturaXml AVICULTURA_XML_UM = aviculturaXmlUm();

	public static final AviculturaXml AVICULTURA_XML_DOIS = aviculturaXmlDois();

	private AviculturaXmlDataProvider() {

	}

	private static AviculturaXml aviculturaXmlUm() {
		AviculturaXml aviculturaXml = mock(AviculturaXml.class);
		when(aviculturaXml.getCpfCnpj()).thenReturn("1154845000158");
		when(aviculturaXml.getData()).thenReturn(new Date(1495052542000L));
		when(aviculturaXml.getSexo()).thenReturn("F");
		when(aviculturaXml.getQuantidade()).thenReturn(1000);
		when(aviculturaXml.getNomeTecnico()).thenReturn("Nome do técnico");
		when(aviculturaXml.getLote()).thenReturn("Lote 15");
		when(aviculturaXml.getLinhagem()).thenReturn("Linhagem nova");
		when(aviculturaXml.getGalpao()).thenReturn("Galpão novo");
		ImmutableList<AviculturaItemXml> itens = ImmutableList.of(AVICULTURA_ITEM_XML_UM,
				AVICULTURA_ITEM_XML_DOIS,
				AVICULTURA_ITEM_XML_TRES);
		when(aviculturaXml.getItens()).thenReturn(itens);
		return aviculturaXml;
	}

	private static AviculturaXml aviculturaXmlDois() {
		AviculturaXml aviculturaXml = mock(AviculturaXml.class);
		when(aviculturaXml.getCpfCnpj()).thenReturn("023156518484");
		when(aviculturaXml.getData()).thenReturn(new Date(1495052542000L));
		when(aviculturaXml.getSexo()).thenReturn("M");
		when(aviculturaXml.getQuantidade()).thenReturn(500);
		when(aviculturaXml.getNomeTecnico()).thenReturn("Nome do técnico dois");
		when(aviculturaXml.getLote()).thenReturn("Lote 30");
		when(aviculturaXml.getLinhagem()).thenReturn("Linhagem nova dois");
		when(aviculturaXml.getGalpao()).thenReturn("Galpão novo dois");
		ImmutableList<AviculturaItemXml> itens = ImmutableList.of(AVICULTURA_ITEM_XML_TRES);
		when(aviculturaXml.getItens()).thenReturn(itens);
		return aviculturaXml;
	}

}