package br.com.db1.portalcooperado.infrastructure.assertions;

import org.assertj.core.api.AbstractClassAssert;
import org.assertj.core.util.Sets;

import java.util.Arrays;
import java.util.Set;

public class ClasseAssert<S extends AbstractClassAssert<S>> extends AbstractClassAssert<S> {

	private final Set<Class> actualSuperclasses;

	private ClasseAssert(Class actual) {
		super(actual, ClasseAssert.class);
		this.actualSuperclasses = getActualSuperclasses();
	}

	public static ClasseAssert assertThat(Class actual) {
		return new ClasseAssert(actual);
	}

	private Set<Class> getActualSuperclasses() {
		Set<Class> classes = Sets.newHashSet();
		classes.addAll(Arrays.asList(actual.getInterfaces()));
		classes.addAll(Arrays.asList(actual.getClasses()));
		classes.add(actual.getSuperclass());
		return classes;
	}

	public ClasseAssert isHeritageOf(Class klazz) {
		isNotNull();
		if (!actualSuperclasses.contains(klazz)) {
			failWithMessage("Expected <%s> to be chidren of <%s> but was not", actual.getName(), klazz.getName());
		}
		return this;
	}

}
