package br.com.db1.portalcooperado.infrastructure.mybatis.typehandler;

import br.com.db1.portalcooperado.model.types.Produto;
import org.testng.annotations.Test;

import java.sql.CallableStatement;
import java.sql.ResultSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProdutoTypeHandlerTest {

	@Test
	public void naoRetornouProdutoCorretoNoResultSet() throws Exception {
		ProdutoTypeHandler produtoTypeHandler = new ProdutoTypeHandler();
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getString("DS_PRODUTO")).thenReturn("Soja");
		assertThat(produtoTypeHandler.getNullableResult(resultSet, "DS_PRODUTO")).isEqualTo(Produto.SOJA);
	}

	@Test
	public void naoRetornouNuloNoResultSet() throws Exception {
		ProdutoTypeHandler produtoTypeHandler = new ProdutoTypeHandler();
		ResultSet resultSet = mock(ResultSet.class);
		assertThat(produtoTypeHandler.getNullableResult(resultSet, "DS_TESTE")).isNull();
	}

	@Test
	public void naoRetornouProdutoCorretoNoCallableStatement() throws Exception {
		ProdutoTypeHandler produtoTypeHandler = new ProdutoTypeHandler();
		CallableStatement callableStatement = mock(CallableStatement.class);
		when(callableStatement.getString(1)).thenReturn("Soja");
		assertThat(produtoTypeHandler.getNullableResult(callableStatement, 1)).isEqualTo(Produto.SOJA);
	}

	@Test
	public void naoRetornouNuloNoResultCallableStatement() throws Exception {
		ProdutoTypeHandler produtoTypeHandler = new ProdutoTypeHandler();
		CallableStatement callableStatement = mock(CallableStatement.class);
		assertThat(produtoTypeHandler.getNullableResult(callableStatement, 2)).isNull();
	}


}