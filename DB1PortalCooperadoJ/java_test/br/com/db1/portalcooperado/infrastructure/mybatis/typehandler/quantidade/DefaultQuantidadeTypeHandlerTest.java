package br.com.db1.portalcooperado.infrastructure.mybatis.typehandler.quantidade;

import br.com.db1.portalcooperado.model.types.Quantidade;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.ResultSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultQuantidadeTypeHandlerTest {

	@Test
	public void naoRetornouQuantidadeCorretaNoResultSet() throws Exception {
		DefaultDecimaisQuantidadeTypeHandler defaultDecimaisQuantidadeTypeHandler = new DefaultDecimaisQuantidadeTypeHandler();
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getBigDecimal("QT_QUANTIDADE")).thenReturn(BigDecimal.TEN);

		Quantidade expected = Quantidade.of(BigDecimal.TEN);
		assertThat(defaultDecimaisQuantidadeTypeHandler.getNullableResult(resultSet, "QT_QUANTIDADE"))
				.isEqualTo(expected);
	}

	@Test
	public void naoRetornouQuantidadeCorretaNoCallableStatement() throws Exception {
		DefaultDecimaisQuantidadeTypeHandler defaultDecimaisQuantidadeTypeHandler = new DefaultDecimaisQuantidadeTypeHandler();
		CallableStatement callableStatement = mock(CallableStatement.class);
		when(callableStatement.getBigDecimal(1)).thenReturn(BigDecimal.TEN);

		Quantidade expected = Quantidade.of(BigDecimal.TEN);
		assertThat(defaultDecimaisQuantidadeTypeHandler.getNullableResult(callableStatement, 1)).isEqualTo(expected);
	}

	@Test
	public void naoRetornouNuloNoResultSetQuandoNaoExiste() throws Exception {
		DefaultDecimaisQuantidadeTypeHandler defaultDecimaisQuantidadeTypeHandler = new DefaultDecimaisQuantidadeTypeHandler();
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getBigDecimal("QT_QUANTIDADE")).thenReturn(null);

		assertThat(defaultDecimaisQuantidadeTypeHandler.getNullableResult(resultSet, "QT_QUANTIDADE")).isNull();
	}

	@Test
	public void naoRetornouNuloNoCallableStatementQuandoNaoExiste() throws Exception {
		DefaultDecimaisQuantidadeTypeHandler defaultDecimaisQuantidadeTypeHandler = new DefaultDecimaisQuantidadeTypeHandler();
		CallableStatement callableStatement = mock(CallableStatement.class);
		when(callableStatement.getBigDecimal(1)).thenReturn(null);

		assertThat(defaultDecimaisQuantidadeTypeHandler.getNullableResult(callableStatement, 1)).isNull();
	}

}