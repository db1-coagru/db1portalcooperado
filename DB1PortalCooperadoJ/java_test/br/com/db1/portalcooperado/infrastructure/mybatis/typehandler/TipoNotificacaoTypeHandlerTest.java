package br.com.db1.portalcooperado.infrastructure.mybatis.typehandler;

import org.testng.annotations.Test;

import java.sql.CallableStatement;
import java.sql.ResultSet;

import static br.com.db1.portalcooperado.model.types.TipoNotificacao.ROMANEIO_AGRICOLA;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TipoNotificacaoTypeHandlerTest {

	@Test
	public void naoRetornouTipoNotificacaoCorretoNoResultSet() throws Exception {
		TipoNotificacaoTypeHandler tipoNotificacaoTypeHandler = new TipoNotificacaoTypeHandler();
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getLong("TP_TIPO")).thenReturn(1L);
		assertThat(tipoNotificacaoTypeHandler.getNullableResult(resultSet, "TP_TIPO")).isEqualTo(ROMANEIO_AGRICOLA);
	}

	@Test
	public void naoRetornouTipoNotificacaoCorretaNoCallableStatement() throws Exception {
		TipoNotificacaoTypeHandler tipoNotificacaoTypeHandler = new TipoNotificacaoTypeHandler();
		CallableStatement callableStatement = mock(CallableStatement.class);
		when(callableStatement.getLong(1)).thenReturn(1L);
		assertThat(tipoNotificacaoTypeHandler.getNullableResult(callableStatement, 1)).isEqualTo(ROMANEIO_AGRICOLA);
	}


}