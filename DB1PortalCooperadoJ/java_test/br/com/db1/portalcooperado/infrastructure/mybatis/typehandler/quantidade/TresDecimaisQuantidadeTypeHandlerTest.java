package br.com.db1.portalcooperado.infrastructure.mybatis.typehandler.quantidade;

import br.com.db1.portalcooperado.model.types.Quantidade;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.ResultSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TresDecimaisQuantidadeTypeHandlerTest {

	@Test
	public void naoRetornouQuantidadeCorretaNoResultSet() throws Exception {
		TresDecimaisQuantidadeTypeHandler tresDecimaisQuantidadeTypeHandler = new TresDecimaisQuantidadeTypeHandler();
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getBigDecimal("QT_QUANTIDADE")).thenReturn(BigDecimal.TEN);

		Quantidade expected = Quantidade.of(BigDecimal.TEN, 3);
		assertThat(tresDecimaisQuantidadeTypeHandler.getNullableResult(resultSet, "QT_QUANTIDADE")).isEqualTo(expected);
	}

	@Test
	public void naoRetornouQuantidadeCorretaNoCallableStatement() throws Exception {
		TresDecimaisQuantidadeTypeHandler tresDecimaisQuantidadeTypeHandler = new TresDecimaisQuantidadeTypeHandler();
		CallableStatement callableStatement = mock(CallableStatement.class);
		when(callableStatement.getBigDecimal(1)).thenReturn(BigDecimal.TEN);

		Quantidade expected = Quantidade.of(BigDecimal.TEN, 3);
		assertThat(tresDecimaisQuantidadeTypeHandler.getNullableResult(callableStatement, 1)).isEqualTo(expected);
	}

	@Test
	public void naoRetornouNuloNoResultSetQuandoNaoExiste() throws Exception {
		TresDecimaisQuantidadeTypeHandler tresDecimaisQuantidadeTypeHandler = new TresDecimaisQuantidadeTypeHandler();
		ResultSet resultSet = mock(ResultSet.class);
		when(resultSet.getBigDecimal("QT_QUANTIDADE")).thenReturn(null);

		assertThat(tresDecimaisQuantidadeTypeHandler.getNullableResult(resultSet, "QT_QUANTIDADE")).isNull();
	}

	@Test
	public void naoRetornouNuloNoCallableStatementQuandoNaoExiste() throws Exception {
		TresDecimaisQuantidadeTypeHandler tresDecimaisQuantidadeTypeHandler = new TresDecimaisQuantidadeTypeHandler();
		CallableStatement callableStatement = mock(CallableStatement.class);
		when(callableStatement.getBigDecimal(1)).thenReturn(null);

		assertThat(tresDecimaisQuantidadeTypeHandler.getNullableResult(callableStatement, 1)).isNull();
	}

}