package br.com.db1.portalcooperado.infrastructure.folders.files;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.schedules.recomendacaotecnica.xml.servicerecord.ServiceRecordDataProvider;
import org.testng.annotations.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FileMoverTest {

	private final String fileName = "teste.txt";

	private final String originalFolderName = "java_test\\br\\com\\db1\\portalcooperado\\infrastructure\\folders\\files\\origem";

	private final File originalFolderFile = new File(originalFolderName);

	private final File originalFile = new File(originalFolderName, fileName);

	private final String destinoFolderName = "java_test\\br\\com\\db1\\portalcooperado\\infrastructure\\folders\\files\\destino";

	private final File destinoFolderFile = new File(destinoFolderName);

	private final File destinoFile = new File(destinoFolderFile, fileName);

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Arquivo deve ser informado.")
	public void naoDeuExcpetionAoReceberArquivoNulo() throws Exception {
		File file = null;
		FileMover.move(file, "Teste");
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Destino do arquivo deve ser informado.")
	public void naoDeuExcpetionAoReceberDestinoNulo() throws Exception {
		FileMover.move(mock(File.class), null);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Arquivo informado é inválido.")
	public void naoDeuExcpetionAoReceberArquivoInvalido() throws Exception {
		File file = mock(File.class);
		when(file.exists()).thenReturn(true);
		when(file.isFile()).thenReturn(false);
		FileMover.move(file, "caminho");
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Arquivo informado é inexistente.")
	public void naoDeuExcpetionAoReceberArquivoQueNaoExiste() throws Exception {
		File file = mock(File.class);
		when(file.exists()).thenReturn(false);
		FileMover.move(file, "caminho");
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Destino informado é inexistente.")
	public void naoDeuExcpetionAoReceberDestinoQueNaoExiste() throws Exception {
		File file = mock(File.class);
		when(file.exists()).thenReturn(true);
		when(file.isFile()).thenReturn(true);
		FileMover.move(file, "caminhoqualquer");
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Destino informado é inválido. Somente pastas podem ser informadas.")
	public void naoDeuExcpetionAoReceberNoDestinoUmFileAoInvesDeFolder() throws Exception {
		File file = mock(File.class);
		when(file.exists()).thenReturn(true);
		when(file.isFile()).thenReturn(true);
		FileMover.move(file, ServiceRecordDataProvider._00942620160817GRE);
	}

	@Test
	public void arquivoOriginalPermanceuNaOrigem() throws Exception {
		originalFolderFile.mkdir();
		destinoFolderFile.mkdir();
		originalFile.createNewFile();

		FileMover.move(originalFile, destinoFolderName);
		assertThat(originalFile.exists()).isFalse();

		destinoFile.delete();
		originalFolderFile.delete();
		destinoFolderFile.delete();
	}

	@Test
	public void arquivoNaoFoiMovidoParaDestino() throws Exception {
		originalFolderFile.mkdir();
		destinoFolderFile.mkdir();
		originalFile.createNewFile();

		FileMover.move(originalFile, destinoFolderName);
		assertThat(destinoFile.exists()).isTrue();

		destinoFile.delete();
		originalFolderFile.delete();
		destinoFolderFile.delete();
	}

	@Test
	public void naoSobrescreveuRegistroExistenteNoDestino() throws Exception {
		originalFolderFile.mkdir();
		destinoFolderFile.mkdir();
		originalFile.createNewFile();
		destinoFile.createNewFile();

		FileMover.move(originalFile, destinoFolderName);
		assertThat(originalFile.exists()).isFalse();
		assertThat(destinoFile.exists()).isTrue();

		destinoFile.delete();
		originalFolderFile.delete();
		destinoFolderFile.delete();
	}

}