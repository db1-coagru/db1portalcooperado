package br.com.db1.portalcooperado.infrastructure.folders.readers;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.infrastructure.folders.files.FileReader;
import br.com.db1.portalcooperado.infrastructure.folders.files.FolderFiles;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import org.testng.annotations.Test;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileFilter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class ServerFolderReaderTest {

	private final FolderReader reader = FolderReaderFactory.newServerFolderReader();

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "O caminho informado não existe. pastaerrada")
	public void erroPastaInexistenteNaoOcorreu() throws Exception {
		reader.read("pastaerrada", mock(FileReader.class), mock(FileFilter.class));
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "O caminho informado é inválido.")
	public void erroDeValidacaoDeDiretorioNaoOcorreu() throws Exception {
		reader.read(
				"java_test\\br\\com\\db1\\portalcooperado\\schedules\\recomendacaotecnica\\xml\\arquivos\\00942620160817GRE.xml",
				mock(FileReader.class), mock(FileFilter.class));
	}

	@Test
	public void conversaoEstaIncorreta() throws Exception {

		FileReader<String> converter = new FileReader<String>() {
			@Override
			public String read(File file) throws JAXBException {
				return file.getName();
			}
		};

		FileFilter filter = new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return true;
			}
		};

		FolderFiles<String> files = reader
				.read("java_test\\br\\com\\db1\\portalcooperado\\infrastructure\\folders", converter, filter);

		List<String> expected = ImmutableList.of("files", "readers");

		assertThat(files.values()).isEqualTo(expected);
	}

	@Test
	public void filtragemEstaIncorreta() throws Exception {

		FileReader<String> converter = new FileReader<String>() {
			@Override
			public String read(File file) throws JAXBException {
				return file.getName();
			}
		};

		FileFilter filter = new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return "readers".equals(pathname.getName());
			}
		};

		FolderFiles<String> files = reader
				.read("java_test\\br\\com\\db1\\portalcooperado\\infrastructure\\folders", converter, filter);

		List<String> expected = ImmutableList.of("readers");

		assertThat(files.values()).isEqualTo(expected);
	}
}