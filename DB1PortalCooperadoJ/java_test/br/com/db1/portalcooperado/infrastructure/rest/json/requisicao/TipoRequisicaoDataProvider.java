package br.com.db1.portalcooperado.infrastructure.rest.json.requisicao;

import org.testng.annotations.DataProvider;

public class TipoRequisicaoDataProvider {

	public static final String TIPOS_REQUISICAO = "tiposRequisicao";

	private TipoRequisicaoDataProvider(){

	}

	@DataProvider(name = TIPOS_REQUISICAO)
	public static Object[][] tiposRequisicao() {
		return new Object[][]{
				{ br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao.ENTREGAS_PRODUCAO},
				{ br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao.POSICAO_FINANCEIRA},
				{ br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao.RECOMENDACAO_TECNICA},
				{ br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao.AVICULTURA},
		};
	}
}
