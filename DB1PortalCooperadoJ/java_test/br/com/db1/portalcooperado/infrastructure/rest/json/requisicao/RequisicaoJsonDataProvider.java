package br.com.db1.portalcooperado.infrastructure.rest.json.requisicao;

import java.util.Date;

import static br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao.ENTREGAS_PRODUCAO;
import static br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao.POSICAO_FINANCEIRA;
import static br.com.db1.portalcooperado.util.PortalCooperadoUtil.getDataAtMidnight;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RequisicaoJsonDataProvider {

	private RequisicaoJsonDataProvider() {

	}

	public static br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson mockRequisicao1() {
		Date date = getDataAtMidnight();

		br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson requisicaoJson = mock(
				br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson.class);
		when(requisicaoJson.getIp()).thenReturn("123.456.789.10");
		when(requisicaoJson.getTipoRequisicao()).thenReturn(ENTREGAS_PRODUCAO);
		when(requisicaoJson.getDataInicial()).thenReturn(date);
		when(requisicaoJson.getDataFinal()).thenReturn(date);
		when(requisicaoJson.getTipoProduto()).thenReturn(1L);
		when(requisicaoJson.getTiposAsLong()).thenReturn(2L);
		when(requisicaoJson.temAlgumTipoCultura()).thenReturn(true);
		return requisicaoJson;
	}

	public static br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson mockRequisicao2() {
		Date date = getDataAtMidnight();

		br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson requisicaoJson = mock(
				br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson.class);
		when(requisicaoJson.getIp()).thenReturn("123.456.789.10");
		when(requisicaoJson.getTipoRequisicao()).thenReturn(POSICAO_FINANCEIRA);
		when(requisicaoJson.getDataInicial()).thenReturn(date);
		when(requisicaoJson.getDataFinal()).thenReturn(date);
		when(requisicaoJson.getTipoProduto()).thenReturn(1L);
		when(requisicaoJson.getTiposAsLong()).thenReturn(2L);
		return requisicaoJson;
	}
}
