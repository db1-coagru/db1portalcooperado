package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao;

import br.com.db1.exception.validate.DB1ValidateException;
import org.testng.annotations.Test;

public class NotificacaoJsonBuilderTest {

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Título deve ser informado.")
	public void fezBuildSemTitulo() throws Exception {
		NotificacaoJson.builder().build();
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Mensagem deve ser informada.")
	public void fezBuilderSemMensagem() throws Exception {
		NotificacaoJson.builder()
				.withTitulo("titulo")
				.build();
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Matriculas deve ser informada.")
	public void fezBuildeSemMatriculas() throws Exception {
		NotificacaoJson.builder()
				.withTitulo("titulo")
				.withMensagem("mensagem")
				.build();
	}

}