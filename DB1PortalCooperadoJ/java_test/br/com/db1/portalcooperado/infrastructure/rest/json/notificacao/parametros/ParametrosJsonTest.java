package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros;

import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.entregaproducao.EntregaProducaoParametrosJson;
import br.com.db1.portalcooperado.infrastructure.rest.mapper.PortalMapperProvider;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class ParametrosJsonTest {

	@Test
	public void serializacaoParametrosJsonIncorreta() throws Exception {
		PortalMapperProvider portalMapperProvider = new PortalMapperProvider();
		ObjectMapper mapper = portalMapperProvider.getContext(Usuario.class);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.DAY_OF_MONTH, 19);
		calendar.set(Calendar.MONTH, 8);
		calendar.set(Calendar.YEAR, 2016);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		EntregaProducaoParametrosJson parametrosJson = EntregaProducaoParametrosJson
				.builder()
				.withTrigo()
				.withMilho()
				.withSoja()
				.withTipo(TipoNotificacao.ROMANEIO_AGRICOLA)
				.between(calendar.getTime(), calendar.getTime())
				.build();

		String expected =
				"{\"parametros\":{\"tipo\":\"ROMANEIO_AGRICOLA\",\"trigo\":true,\"milho\":true,\"soja\":true" +
						",\"dataInicio\":1474254000000,\"dataFim\":1474254000000}}";

		String json = mapper.writeValueAsString(parametrosJson);
		assertThat(json).isEqualTo(expected);
	}

}