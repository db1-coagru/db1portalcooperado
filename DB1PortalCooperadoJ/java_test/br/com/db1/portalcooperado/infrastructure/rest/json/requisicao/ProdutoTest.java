package br.com.db1.portalcooperado.infrastructure.rest.json.requisicao;

import br.com.db1.portalcooperado.model.types.Produto;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Test(dataProviderClass = TipoProdutoDataProvider.class)
public class ProdutoTest {

	@Test(dataProvider = TipoProdutoDataProvider.CULTURAS)
	public void buildCodigoFromIsIncorrect(Boolean soja, Boolean milho, Boolean trigo, Long expected) throws Exception {
		assertThat(Produto.buildCodigoFrom(soja, milho, trigo)).isEqualTo(expected);
	}

	@Test(dataProvider = TipoProdutoDataProvider.STRINGS)
	public void fromStringIsIncorrect(String value, Produto exptected) throws Exception {
		assertThat(Produto.fromStringValue(value)).isEqualTo(exptected);
	}

}