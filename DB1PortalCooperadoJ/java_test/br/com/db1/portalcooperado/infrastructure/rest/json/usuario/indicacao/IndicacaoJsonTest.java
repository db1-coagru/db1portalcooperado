package br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.model.entity.Indicacao;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.Lists;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IndicacaoJsonTest {

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Indicação deve ser informada.")
	public void indicacaoIsNull() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson.of(null);
	}

	@Test
	public void matriculaIsIncorrect() throws Exception {
		Indicacao indicacao = mock(Indicacao.class);
		when(indicacao.getMatriculaIndicador()).thenReturn("12345");

		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson indicacaoJson = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson
				.of(indicacao);
		assertThat(indicacaoJson.getMatricula()).isEqualTo("12345");
	}

	@Test
	public void nomeIsIncorrect() throws Exception {
		Indicacao indicacao = mock(Indicacao.class);
		when(indicacao.getNomeIndicador()).thenReturn("teste");

		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson indicacaoJson = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson
				.of(indicacao);
		assertThat(indicacaoJson.getNome()).isEqualTo("teste");
	}

	@Test
	public void permissoesAreIncorrect() throws Exception {
		List<PermissaoIndicado> permissoes = Lists.newLinkedList();
		permissoes.add(PermissaoIndicado.ACTEVENTOS);
		permissoes.add(PermissaoIndicado.ACTENTREGAPRODUCAO);
		permissoes.add(PermissaoIndicado.ACTPOSICAOFINANCEIRA);

		Indicacao indicacao = mock(Indicacao.class);
		when(indicacao.getPermissoesIndicado()).thenReturn(permissoes);

		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson indicacaoJson = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson
				.of(indicacao);
		assertThat(indicacaoJson.getPermissoes()).isEqualTo(permissoes);
	}

	@Test
	public void permissoesNotAreImmutable() throws Exception {
		Indicacao indicacao = mock(Indicacao.class);
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson indicacaoJson = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson
				.of(indicacao);
		assertThat(indicacaoJson.getPermissoes()).isInstanceOf(ImmutableList.class);
	}

	@Test
	public void equalsIsIncorect() throws Exception {
		Indicacao indicacao = mock(Indicacao.class);
		when(indicacao.getMatriculaIndicador()).thenReturn("12345");

		Indicacao indicacao2 = mock(Indicacao.class);
		when(indicacao2.getMatriculaIndicador()).thenReturn("123456");

		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson indicacaoJson = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson
				.of(indicacao);
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson indicacaoJson2 = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson
				.of(indicacao);
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson indicacaoJson3 = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson
				.of(indicacao2);

		assertThat(indicacaoJson).isEqualTo(indicacaoJson2).isNotEqualTo(indicacaoJson3).isNotEqualTo(new Object());
	}

	@Test
	public void hashCodeIsIncorrect() throws Exception {
		int expected = PortalCooperadoUtil.hashCode("123456");
		int notExpected = PortalCooperadoUtil.hashCode("12345");

		Indicacao indicacao = mock(Indicacao.class);
		when(indicacao.getMatriculaIndicador()).thenReturn("123456");

		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson indicacaoJson = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson
				.of(indicacao);

		assertThat(indicacaoJson.hashCode()).isEqualTo(expected).isNotEqualTo(notExpected);
	}

}