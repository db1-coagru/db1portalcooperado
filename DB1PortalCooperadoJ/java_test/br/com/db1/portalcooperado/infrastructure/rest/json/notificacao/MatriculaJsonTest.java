package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.infrastructure.rest.mapper.PortalMapperProvider;
import br.com.db1.portalcooperado.model.entity.Usuario;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MatriculaJsonTest {

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Matrícula deve ser informada.")
	public void matriculaConstruidaSemNumeroMatricula() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.MatriculaJson.of(null);
	}

	@Test
	public void serializacaoMatriculaJsonIncorreta() throws Exception {
		PortalMapperProvider portalMapperProvider = new PortalMapperProvider();
		ObjectMapper mapper = portalMapperProvider.getContext(Usuario.class);

		MatriculaJson matriculaJson = MatriculaJson
				.of(1234L);

		String expected = "{\"matricula\":{\"matricula\":1234}}";

		String json = mapper.writeValueAsString(matriculaJson);
		assertThat(json).isEqualTo(expected);
	}
}