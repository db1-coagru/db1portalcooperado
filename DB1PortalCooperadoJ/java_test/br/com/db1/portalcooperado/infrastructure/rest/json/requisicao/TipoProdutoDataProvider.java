package br.com.db1.portalcooperado.infrastructure.rest.json.requisicao;

import br.com.db1.portalcooperado.model.types.Produto;
import org.testng.annotations.DataProvider;

public class TipoProdutoDataProvider {

    public static final String CULTURAS = "culturas";

    public static final String STRINGS = "strings";

    @DataProvider(name = CULTURAS)
    public static Object[][] culturas() {
        return new Object[][]{
                {true, true, true, 234L},
                {true, true, false, 23L},
                {true, false, false, 2L},
                {false, false, false, 0L},
                {false, true, false, 3L},
                {false, true, true, 34L},
                {true, false, true, 24L},
                {false, false, true, 4L}
        };
    }

    @DataProvider(name = STRINGS)
    public static Object[][] strings() {
        return new Object[][]{
                {"SOJA", Produto.SOJA},
                {"SOJa", Produto.SOJA},
                {"SOjA", Produto.SOJA},
                {"SoJA", Produto.SOJA},
                {"sOJA", Produto.SOJA},
                {"soJA", Produto.SOJA},
                {"sojA", Produto.SOJA},
                {"soja", Produto.SOJA},
                {"SoJa", Produto.SOJA},
                {"sOjA", Produto.SOJA},
                {"soJA", Produto.SOJA},
                {"SOja", Produto.SOJA},
                {"TRIGO", Produto.TRIGO},
                {"TRIGo", Produto.TRIGO},
                {"TRIgo", Produto.TRIGO},
                {"TRigo", Produto.TRIGO},
                {"Trigo", Produto.TRIGO},
                {"trigo", Produto.TRIGO},
                {"tRigo", Produto.TRIGO},
                {"trIgo", Produto.TRIGO},
                {"triGo", Produto.TRIGO},
                {"trigO", Produto.TRIGO},
                {"triGO", Produto.TRIGO},
                {"trIgO", Produto.TRIGO},
                {"tRigO", Produto.TRIGO},
                {"TrigO", Produto.TRIGO},
                {"TRigO", Produto.TRIGO},
                {"TRigO", Produto.TRIGO},
                {"MILHO", Produto.MILHO},
                {"MILHo", Produto.MILHO},
                {"MILho", Produto.MILHO},
                {"MIlho", Produto.MILHO},
                {"Milho", Produto.MILHO},
                {"milho", Produto.MILHO},
                {"mIlho", Produto.MILHO},
                {"miLho", Produto.MILHO},
                {"milHo", Produto.MILHO},
                {"milhO", Produto.MILHO},
                {"milHO", Produto.MILHO},
                {"MilhO", Produto.MILHO},
                {"MIlhO", Produto.MILHO},
                {"MIlHO", Produto.MILHO},
                {"soja industria", Produto.SOJA},
                {"Soja Industria", Produto.SOJA},
                {"SOJA INDUSTRIA", Produto.SOJA},
                {"SojA sEMente", Produto.SOJA},
                {"SOJA SEMENTE", Produto.SOJA},
                {"soja semente", Produto.SOJA},
                {"TRIGUILHO", Produto.TRIGO},
                {"triguilho", Produto.TRIGO},
                {"triGuiLho", Produto.TRIGO},
                {"trigo semente", Produto.TRIGO},
                {"TRIGO SEMENTE", Produto.TRIGO},
                {"TRIGO semente", Produto.TRIGO},
                {"TRIGO INDUSTRIA", Produto.TRIGO},
                {"TRIGO industria", Produto.TRIGO},
                {"trigo industria", Produto.TRIGO},
                {"quebrado de milho", Produto.MILHO},
                {"QUEBRADO DE MILHO", Produto.MILHO},
                {"queBradO de MiLHo", Produto.MILHO}

        };
    }

}
