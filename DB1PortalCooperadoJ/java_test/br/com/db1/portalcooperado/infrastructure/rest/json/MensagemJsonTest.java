package br.com.db1.portalcooperado.infrastructure.rest.json;

import br.com.db1.exception.validate.DB1ValidateException;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MensagemJsonTest {

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Descrição da mensagem deve ser informada.")
	public void descricaoIsNull() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson.of(null);
	}

	@Test
	public void descricaoIsIncorrect() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson json = br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson
				.of("123");
		assertThat(json.toString()).isEqualTo("123");
	}

	@Test
	public void mensagemJsonNaoLocalizadoIsIncorrect() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson json = br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson
				.newMensagemJsonNaoLocalizado();
		assertThat(json.toString()).isEqualTo("Requisição inválida. Objeto JSON não localizado.");
	}

	@Test
	public void mensagemErroInesperadoIsIncorrect() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson json = br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson
				.newMensagemErroInesperado();
		assertThat(json.toString()).isEqualTo("Ocorreu uma exceção inesperada. Contate o suporte.");
	}

	@Test
	public void mensagemSemAcessoMobileIsIncorrect() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson json = br.com.db1.portalcooperado.infrastructure.rest.json.MensagemJson
				.newMensagemSemAcessoMobile();
		assertThat(json.toString()).isEqualTo("Usuário sem permissão de acesso Mobile.");
	}

}