package br.com.db1.portalcooperado.infrastructure.rest.json.avicultura;

import jersey.repackaged.com.google.common.collect.ImmutableList;

import java.util.Date;

import static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJsonDataProvider.aviculturaItemJsonDois;
import static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJsonDataProvider.aviculturaItemJsonUm;

public class AviculturaJsonDataProvider {

	private AviculturaJsonDataProvider() {

	}

	public static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJson aviculturaJsonUm() {
		return br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJson.builder()
				.withData(new Date())
				.withGalpao("galpao um")
				.withLinhagem("linhagem um")
				.withLote("lote um")
				.withNomeTecnico("nome do tecnico um")
				.withQuantidade(100)
				.withSexo("M")
				.withItens(ImmutableList.of(aviculturaItemJsonUm()))
				.build();
	}

	public static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJson aviculturaJsonDois() {
		return br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJson.builder()
				.withData(new Date())
				.withGalpao("galpao dois")
				.withLinhagem("linhagem dois")
				.withLote("lote dois")
				.withNomeTecnico("nome do tecnico dois")
				.withQuantidade(100)
				.withSexo("F")
				.withItens(ImmutableList.of(aviculturaItemJsonUm(), aviculturaItemJsonDois()))
				.build();
	}

}
