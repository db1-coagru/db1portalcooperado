package br.com.db1.portalcooperado.infrastructure.rest.json.usuario.perfil;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.model.entity.ProfileDataProvider;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import br.com.db1.security.model.entity.Profile;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PerfilJsonTest {

	private Profile profile;

	private br.com.db1.portalcooperado.infrastructure.rest.json.usuario.perfil.PerfilJson perfilJson;

	@BeforeClass
	public void setUp() throws Exception {
		profile = ProfileDataProvider.profile1();
		perfilJson = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.perfil.PerfilJson.of(profile);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Perfil do usuário inexistente no banco de dados.")
	public void profileIsNull() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.perfil.PerfilJson.of(null);
	}

	@Test
	public void perfisArentEquals() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.perfil.PerfilJson jsonTwo = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.perfil.PerfilJson
				.of(profile);
		assertThat(perfilJson).isEqualTo(jsonTwo);
	}

	@Test
	public void hashCodeIsIncorrect() throws Exception {
		int expected = PortalCooperadoUtil.hashCode("Perfil 1");
		int notExpected = PortalCooperadoUtil.hashCode("Perfil 2");
		assertThat(perfilJson.hashCode()).isEqualTo(expected).isNotEqualTo(notExpected);
	}

	@Test
	public void descricaoIsIncorrect() throws Exception {
		assertThat(perfilJson.getDescricao()).isEqualTo("Perfil 1");
	}

}