package br.com.db1.portalcooperado.infrastructure.rest.translators.avicultura;

import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJson;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaItemToAviculturaItemJsonTranslator;
import org.testng.annotations.Test;

import static br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItemDataProvider.aviculturaItemUm;
import static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJsonDataProvider.aviculturaItemJsonUm;
import static org.assertj.core.api.Assertions.assertThat;

public class AviculturaItemToAviculturaItemJsonTranslatorTest {

	@Test
	public void deveTraduzirTodosCamposCorretamente() throws Exception {
		AviculturaItemToAviculturaItemJsonTranslator translator = new AviculturaItemToAviculturaItemJsonTranslator();
		AviculturaItemJson atual = translator.translate(aviculturaItemUm());
		AviculturaItemJson esperado = aviculturaItemJsonUm();
		assertThat(atual).isEqualToComparingFieldByField(esperado);
	}

}