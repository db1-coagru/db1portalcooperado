package br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica;

import br.com.db1.portalcooperado.model.entity.RecomendacaoTecnica;
import br.com.db1.portalcooperado.model.types.Produto;
import br.com.db1.portalcooperado.model.types.Quantidade;
import org.testng.annotations.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RecomendacaoTecnicaJsonBuilderTest {

	@Test
	public void produtoNaoFoiContruidoCorretamente() {
		RecomendacaoTecnica recomendacaoTecnica = mockRecomendacaoTecnica();
		br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson recomendacaoTecnicaJson = br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson
				.builder(recomendacaoTecnica).build();
		assertThat(recomendacaoTecnicaJson.getProduto()).isEqualTo("Soja");
	}

	@Test
	public void talhaoNaoFoiContruidoCorretamente() {
		RecomendacaoTecnica recomendacaoTecnica = mockRecomendacaoTecnica();
		br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson recomendacaoTecnicaJson = br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson
				.builder(recomendacaoTecnica).build();
		assertThat(recomendacaoTecnicaJson.getTalhao()).isEqualTo("Total area teste");
	}

	@Test
	public void mensagemCustomizadaoNaoFoiContruidaCorretamente() {
		RecomendacaoTecnica recomendacaoTecnica = mockRecomendacaoTecnica();
		br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson recomendacaoTecnicaJson = br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson
				.builder(recomendacaoTecnica).build();
		assertThat(recomendacaoTecnicaJson.getMensagemCustomizada()).isEqualTo("Mensagem customizada teste");
	}

	@Test
	public void flagUrgenteNaoFoiContruidaCorretamente() {
		RecomendacaoTecnica recomendacaoTecnica = mockRecomendacaoTecnica();
		br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson recomendacaoTecnicaJson = br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson
				.builder(recomendacaoTecnica).build();
		assertThat(recomendacaoTecnicaJson.getUrgente()).isTrue();
	}

	@Test
	public void rendimentoPulverizadoNaoFoiContruidaCorretamente() {
		RecomendacaoTecnica recomendacaoTecnica = mockRecomendacaoTecnica();
		br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson recomendacaoTecnicaJson = br.com.db1.portalcooperado.infrastructure.rest.json.recomendacaotecnica.RecomendacaoTecnicaJson
				.builder(recomendacaoTecnica).build();
		assertThat(recomendacaoTecnicaJson.getRendimentoPulverizador()).isEqualTo("10.10 alq.");
	}

	private RecomendacaoTecnica mockRecomendacaoTecnica() {
		RecomendacaoTecnica recomendacaoTecnica = mock(RecomendacaoTecnica.class);
		when(recomendacaoTecnica.descricaoProduto()).thenReturn(Produto.SOJA.getDescricao());
		when(recomendacaoTecnica.getUrgente()).thenReturn(Boolean.TRUE);
		when(recomendacaoTecnica.getCriacao()).thenReturn(new Date());
		when(recomendacaoTecnica.getMensagemCustomizada()).thenReturn("Mensagem customizada teste");
		when(recomendacaoTecnica.totalAreaAsString()).thenReturn("Total area teste");
		when(recomendacaoTecnica.getEficiencia()).thenReturn(Quantidade.of(10.10));
		return recomendacaoTecnica;
	}

}