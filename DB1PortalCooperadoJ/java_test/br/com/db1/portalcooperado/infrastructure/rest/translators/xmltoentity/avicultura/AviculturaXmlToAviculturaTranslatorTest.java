package br.com.db1.portalcooperado.infrastructure.rest.translators.xmltoentity.avicultura;

import br.com.db1.portalcooperado.dao.AviculturaDao;
import br.com.db1.portalcooperado.dao.UsuarioDao;
import br.com.db1.portalcooperado.model.entity.UsuarioDataProvider;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Date;

import static br.com.db1.portalcooperado.infrastructure.agrosys.avicultura.AviculturaXmlDataProvider.AVICULTURA_XML_UM;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AviculturaXmlToAviculturaTranslatorTest {

	private Avicultura aviculturaNova;

	@BeforeClass
	public void setUp() throws Exception {
		AviculturaXmlToAviculturaTranslator translator = criarTranslator();
		aviculturaNova = translator.translate(AVICULTURA_XML_UM);
	}

	@Test
	public void deveConverterProdutorAoCriarNovaAvicultura() throws Exception {
		assertThat(aviculturaNova.getProdutor()).isEqualTo(UsuarioDataProvider.usuario1());
	}

	@Test
	public void deveConverterSexoAoCriarNovaAvicultura() throws Exception {
		assertThat(aviculturaNova.getSexo()).isEqualTo("F");
	}

	@Test
	public void deveConverterQuantidadeAoCriarNovaAvicultura() throws Exception {
		assertThat(aviculturaNova.getQuantidade()).isEqualTo(1000);
	}

	@Test
	public void deveConverterNomeTecnicoAoCriarNovaAvicultura() throws Exception {
		assertThat(aviculturaNova.getNomeTecnico()).isEqualTo("Nome do técnico");
	}

	@Test
	public void deveConverterLoteAoCriarNovaAvicultura() throws Exception {
		assertThat(aviculturaNova.getLote()).isEqualTo("Lote 15");
	}

	@Test
	public void deveConverterLinhagemAoCriarNovaAvicultura() throws Exception {
		assertThat(aviculturaNova.getLinhagem()).isEqualTo("Linhagem nova");
	}

	@Test
	public void deveConverterGalpaoAoCriarNovaAvicultura() throws Exception {
		assertThat(aviculturaNova.getGalpao()).isEqualTo("Galpão novo");
	}

	@Test
	public void deveConverterDataAoCriarNovaAvicultura() throws Exception {
		assertThat(aviculturaNova.getData()).isEqualTo(new Date(1495052542000L));
	}

	@Test
	public void deveConverterItensAoCriarNovaAvicultura() throws Exception {
		assertThat(aviculturaNova.getItens()).hasSize(3);
	}

	private AviculturaXmlToAviculturaTranslator criarTranslator() throws Exception {
		UsuarioDao usuarioDao = mockarUsuarioDao();
		AviculturaDao aviculturaDao = mock(AviculturaDao.class);
		AviculturaItensXmlToAviculturaItemTranslator itemTranslator = newItensTranslator();
		return new AviculturaXmlToAviculturaTranslator(usuarioDao, aviculturaDao, itemTranslator);
	}

	private UsuarioDao mockarUsuarioDao() {
		UsuarioDao usuarioDao = mock(UsuarioDao.class);
		when(usuarioDao.buscaUsuarioByCpfCnpj("1154845000158")).thenReturn(UsuarioDataProvider.usuario1());
		return usuarioDao;
	}

	private AviculturaItensXmlToAviculturaItemTranslator newItensTranslator() {
		AviculturaItemImagemXmlToAviculturaItemImagem itemImagem = new AviculturaItemImagemXmlToAviculturaItemImagem();
		AviculturaItemXmlToAviculturaTranslator itemTranslator = new AviculturaItemXmlToAviculturaTranslator(itemImagem);
		return new AviculturaItensXmlToAviculturaItemTranslator(itemTranslator);
	}
}