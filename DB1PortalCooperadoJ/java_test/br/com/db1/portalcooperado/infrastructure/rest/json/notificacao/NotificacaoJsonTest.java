package br.com.db1.portalcooperado.infrastructure.rest.json.notificacao;

import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.ParametrosJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.parametros.entregaproducao.EntregaProducaoParametrosJson;
import br.com.db1.portalcooperado.infrastructure.rest.mapper.PortalMapperProvider;
import br.com.db1.portalcooperado.model.entity.Indicacao;
import br.com.db1.portalcooperado.model.entity.Notificacao;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.model.types.TipoNotificacao;
import com.fasterxml.jackson.databind.ObjectMapper;
import jersey.repackaged.com.google.common.collect.Lists;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NotificacaoJsonTest {

	@Test
	public void serializacaoNotificacaoJsonIncorreta() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.DAY_OF_MONTH, 19);
		calendar.set(Calendar.MONTH, 8);
		calendar.set(Calendar.YEAR, 2016);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		PortalMapperProvider portalMapperProvider = new PortalMapperProvider();
		ObjectMapper mapper = portalMapperProvider.getContext(Usuario.class);

		Indicacao indicacao1 = mock(Indicacao.class);
		when(indicacao1.getMatriculaIndicador()).thenReturn("1");
		when(indicacao1.getMatriculaIndicado()).thenReturn("2");
		when(indicacao1.hasPermissaoTo("1", PermissaoIndicado.ACTENTREGAPRODUCAO)).thenReturn(true);

		Indicacao indicacao2 = mock(Indicacao.class);
		when(indicacao2.getMatriculaIndicador()).thenReturn("1");
		when(indicacao2.getMatriculaIndicado()).thenReturn("3");
		when(indicacao2.hasPermissaoTo("1", PermissaoIndicado.ACTENTREGAPRODUCAO)).thenReturn(true);

		List<Indicacao> indicacoes = Lists.newLinkedList();
		indicacoes.add(indicacao1);
		indicacoes.add(indicacao2);

		Usuario usuario = mock(Usuario.class);
		when(usuario.getMatricula()).thenReturn(1L);
		when(usuario.getIndicacoes()).thenReturn(indicacoes);

		ParametrosJson parametrosJson = EntregaProducaoParametrosJson
				.builder()
				.withMilho()
				.withTipo(TipoNotificacao.ROMANEIO_AGRICOLA)
				.between(calendar.getTime(), calendar.getTime())
				.build();

		Notificacao notificacao = mock(Notificacao.class);
		when(notificacao.isDeRomaneioAgricola()).thenReturn(true);
		when(notificacao.getProduto()).thenReturn("Milho");
		when(notificacao.getData()).thenReturn(calendar.getTime());
		when(notificacao.getPermissao()).thenReturn(PermissaoIndicado.ACTENTREGAPRODUCAO);

		List<br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.MatriculaJson> matriculas = Lists
				.newLinkedList();
		matriculas.add(MatriculaJson.of(1));
		matriculas.add(MatriculaJson.of(2));
		matriculas.add(MatriculaJson.of(3));

		br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.MatriculasEnviarNotificacao matriculasNotificacao = mock(
				br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.MatriculasEnviarNotificacao.class);
		when(matriculasNotificacao.values()).thenReturn(matriculas);

		br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.NotificacaoJson notificacaoJson = br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.NotificacaoJson
				.builder()
				.withTitulo("Nova entrega de produção.")
				.withMensagem("Mensagem de teste.")
				.withMatriculas(matriculasNotificacao)
				.withParametros(parametrosJson)
				.build();

		String json = mapper.writeValueAsString(notificacaoJson);
		String expected = "{\"notificacao\":" +
				"{\"titulo\":\"Nova entrega de produção.\"" +
				",\"mensagem\":\"Mensagem de teste.\"" +
				",\"matriculas\":[{\"matricula\":1},{\"matricula\":2},{\"matricula\":3}]" +
				",\"parametros\":{\"tipo\":\"ROMANEIO_AGRICOLA\"" +
				",\"trigo\":false" +
				",\"milho\":true" +
				",\"soja\":false" +
				",\"dataInicio\":1474254000000" +
				",\"dataFim\":1474254000000}}}";

		assertThat(json).isEqualTo(expected);

	}

	@Test
	public void serializacaoNotificacaoJsonSemParametrosIncorreta() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.DAY_OF_MONTH, 19);
		calendar.set(Calendar.MONTH, 8);
		calendar.set(Calendar.YEAR, 2016);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		PortalMapperProvider portalMapperProvider = new PortalMapperProvider();
		ObjectMapper mapper = portalMapperProvider.getContext(Usuario.class);

		Indicacao indicacao1 = mock(Indicacao.class);
		when(indicacao1.getMatriculaIndicador()).thenReturn("1");
		when(indicacao1.getMatriculaIndicado()).thenReturn("2");
		when(indicacao1.hasPermissaoTo("1", PermissaoIndicado.ACTENTREGAPRODUCAO)).thenReturn(true);

		Indicacao indicacao2 = mock(Indicacao.class);
		when(indicacao2.getMatriculaIndicador()).thenReturn("1");
		when(indicacao2.getMatriculaIndicado()).thenReturn("3");
		when(indicacao2.hasPermissaoTo("1", PermissaoIndicado.ACTENTREGAPRODUCAO)).thenReturn(true);

		List<Indicacao> indicacoes = Lists.newLinkedList();
		indicacoes.add(indicacao1);
		indicacoes.add(indicacao2);

		Usuario usuario = mock(Usuario.class);
		when(usuario.getMatricula()).thenReturn(1L);
		when(usuario.getIndicacoes()).thenReturn(indicacoes);

		Notificacao notificacao = mock(Notificacao.class);
		when(notificacao.isDeRomaneioAgricola()).thenReturn(false);
		when(notificacao.getPermissao()).thenReturn(PermissaoIndicado.ACTENTREGAPRODUCAO);

		List<MatriculaJson> matriculas = Lists.newLinkedList();
		matriculas.add(MatriculaJson.of(1));
		matriculas.add(MatriculaJson.of(2));
		matriculas.add(MatriculaJson.of(3));

		br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.MatriculasEnviarNotificacao matriculasNotificacao = mock(
				br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.MatriculasEnviarNotificacao.class);
		when(matriculasNotificacao.values()).thenReturn(matriculas);

		br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.NotificacaoJson notificacaoJson = br.com.db1.portalcooperado.infrastructure.rest.json.notificacao.NotificacaoJson
				.builder()
				.withTitulo("Nova entrega de produção.")
				.withMensagem("Mensagem de teste.")
				.withMatriculas(matriculasNotificacao)
				.build();

		String json = mapper.writeValueAsString(notificacaoJson);
		String expected =
				"{\"notificacao\":{\"titulo\":\"Nova entrega de produção.\",\"mensagem\":\"Mensagem de teste.\"," +
						"\"matriculas\":[{\"matricula\":1},{\"matricula\":2},{\"matricula\":3}]}}";

		assertThat(json).isEqualTo(expected);

	}

}