package br.com.db1.portalcooperado.infrastructure.rest.translators.avicultura;

import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaToAviculturaJsonTranslator;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaDataProvider;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJsonDataProvider;
import org.testng.annotations.Test;

import static br.com.db1.portalcooperado.infrastructure.rest.translators.avicultura.AviculturaTranslatorDataProvider.newAviculturaToAviculturaJsonTranslator;
import static org.assertj.core.api.Assertions.assertThat;

public class AviculturaToAviculturaJsonTranslatorTest {

	@Test
	public void deveTraduzirTodosCamposCorretamente() throws Exception {
		AviculturaToAviculturaJsonTranslator translator = newAviculturaToAviculturaJsonTranslator();

		AviculturaJson atual = translator.translate(AviculturaDataProvider.aviculturaUm());
		AviculturaJson esperado = AviculturaJsonDataProvider.aviculturaJsonUm();
		assertThat(atual).isEqualToComparingFieldByField(esperado);
	}

}