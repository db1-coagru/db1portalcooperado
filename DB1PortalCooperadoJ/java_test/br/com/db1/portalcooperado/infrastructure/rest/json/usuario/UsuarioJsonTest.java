package br.com.db1.portalcooperado.infrastructure.rest.json.usuario;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson;
import br.com.db1.portalcooperado.model.entity.Indicacao;
import br.com.db1.portalcooperado.model.entity.ProfileDataProvider;
import br.com.db1.portalcooperado.model.entity.Usuario;
import br.com.db1.portalcooperado.model.types.PermissaoIndicado;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.indicacao.IndicacaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.usuario.perfil.PerfilJson;
import br.com.db1.portalcooperado.util.PortalCooperadoUtil;
import jersey.repackaged.com.google.common.collect.Lists;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UsuarioJsonTest {

	private Usuario usuario;

	private br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson usuarioJson;

	@BeforeClass
	public void setUp() throws Exception {
		usuario = mockUsuario();
		usuarioJson = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson
				.of(usuario, mock(br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson.class));
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Usuário inexistente no banco de dados.")
	public void usuarioIsNull() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson
				.of(null, mock(br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson.class));
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Token inexistente no banco de dados.")
	public void tokenIsNull() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson.of(mock(Usuario.class), null);
	}

	@Test
	public void matriculaIsIncorrect() throws Exception {
		assertThat(usuarioJson.getMatricula()).isEqualTo("123");
	}

	@Test
	public void nomeIsIncorrect() throws Exception {
		assertThat(usuarioJson.getNome()).isEqualTo("teste");
	}

	@Test
	public void perfilIsIncorrect() throws Exception {
		PerfilJson perfilJosn = PerfilJson.of(ProfileDataProvider.profile1());
		assertThat(usuarioJson.getPerfil()).isEqualTo(perfilJosn);
	}

	@Test
	public void tokenIsIncorrect() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson token = mock(
				br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson.class);
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson usuarioJson = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson
				.of(usuario, token);
		assertThat(usuarioJson.getTokenJson()).isSameAs(token);
	}

	@Test
	public void indicacoesAreIncorrect() throws Exception {
		Indicacao indicacao1 = mockIndicacao("1", "Indicador 1");
		Indicacao indicacao2 = mockIndicacao("2", "Indicador 2");
		IndicacaoJson indicacaoJson1 = IndicacaoJson.of(indicacao1);
		IndicacaoJson indicacaoJson2 = IndicacaoJson.of(indicacao2);

		List<IndicacaoJson> expected = Lists.newLinkedList();
		expected.add(indicacaoJson1);
		expected.add(indicacaoJson2);
		assertThat(usuarioJson.getIndicacoes()).isEqualTo(expected);
	}

	@Test
	public void equalsIsIncorect() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson usuarioJson2 = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson
				.of(usuario, mock(br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson.class));

		Usuario usuario2 = mock(Usuario.class);
		when(usuario2.getMatricula()).thenReturn(1234L);
		when(usuario2.getNome()).thenReturn("testes");
		when(usuario2.getProfile()).thenReturn(ProfileDataProvider.profile1());

		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.UsuarioJson usuarioJson3 = UsuarioJson
				.of(usuario2, mock(br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson.class));
		assertThat(usuarioJson).isEqualTo(usuarioJson2).isNotEqualTo(usuarioJson3).isNotEqualTo(mock(Object.class));
	}

	@Test
	public void hashCodeIsIncorrect() throws Exception {
		int exptected = PortalCooperadoUtil.hashCode("123");
		int notExptected = PortalCooperadoUtil.hashCode("124");
		assertThat(usuarioJson.hashCode()).isEqualTo(exptected).isNotEqualTo(notExptected);
	}

	private Usuario mockUsuario() {
		Usuario usuario = mock(Usuario.class);
		when(usuario.getMatricula()).thenReturn(123L);
		when(usuario.getNome()).thenReturn("teste");
		when(usuario.getProfile()).thenReturn(ProfileDataProvider.profile1());

		Indicacao indicacao1 = mockIndicacao("1", "Indicador 1");
		Indicacao indicacao2 = mockIndicacao("2", "Indicador 2");
		List<Indicacao> indicacoesIndicado = Lists.newLinkedList();
		indicacoesIndicado.add(indicacao1);
		indicacoesIndicado.add(indicacao2);
		when(usuario.getIndicacoesIndicado()).thenReturn(indicacoesIndicado);
		return usuario;
	}

	private Indicacao mockIndicacao(String matricula, String nome) {
		Indicacao indicacao = mock(Indicacao.class);
		when(indicacao.getMatriculaIndicador()).thenReturn(matricula);
		when(indicacao.getNomeIndicador()).thenReturn(nome);
		when(indicacao.getPermissoesIndicado()).thenReturn(createPermissoesIndicados());
		return indicacao;
	}

	private List<PermissaoIndicado> createPermissoesIndicados() {
		List<PermissaoIndicado> permissoesIndicados = Lists.newLinkedList();
		permissoesIndicados.add(PermissaoIndicado.ACTEVENTOS);
		permissoesIndicados.add(PermissaoIndicado.ACTPOSICAOFINANCEIRA);
		return permissoesIndicados;
	}

}