package br.com.db1.portalcooperado.infrastructure.rest.translators.avicultura;

import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaItensToAviculturaItensJsonTranslator;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaToAviculturaJsonTranslator;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturasToAviculturasJsonTranslator;

class AviculturaTranslatorDataProvider {

	private AviculturaTranslatorDataProvider() {

	}

	static AviculturasToAviculturasJsonTranslator newAviculturasToAviculturasJsonTranslator() {
		AviculturaToAviculturaJsonTranslator aviculturaTranslator = newAviculturaToAviculturaJsonTranslator();
		return new AviculturasToAviculturasJsonTranslator(aviculturaTranslator);
	}

	static AviculturaToAviculturaJsonTranslator newAviculturaToAviculturaJsonTranslator() {
		AviculturaItensToAviculturaItensJsonTranslator itensTranslator = newAviculturaItensToAviculturaItensJsonTranslator();
		return new AviculturaToAviculturaJsonTranslator(itensTranslator);
	}

	static AviculturaItensToAviculturaItensJsonTranslator newAviculturaItensToAviculturaItensJsonTranslator() {
		br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaItemToAviculturaItemJsonTranslator itemTranslator = new br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaItemToAviculturaItemJsonTranslator();
		return new AviculturaItensToAviculturaItensJsonTranslator(itemTranslator);
	}

}
