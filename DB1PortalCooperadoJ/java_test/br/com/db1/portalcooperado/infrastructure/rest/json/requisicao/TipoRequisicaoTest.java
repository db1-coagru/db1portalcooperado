package br.com.db1.portalcooperado.infrastructure.rest.json.requisicao;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.model.entity.UsuarioDataProvider;
import org.testng.annotations.Test;

import static br.com.db1.portalcooperado.model.types.PermissaoIndicado.ACTENTREGAPRODUCAO;
import static br.com.db1.portalcooperado.model.types.PermissaoIndicado.ACTPOSICAOFINANCEIRA;
import static br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao.*;
import static org.assertj.core.api.Assertions.assertThat;

@Test(dataProviderClass = TipoRequisicaoDataProvider.class)
public class TipoRequisicaoTest {

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Tipo de requisição não suportado.")
	public void fromLongValueRetornouTipoQuandoNaoDeveria() throws Exception {
		fromLongValue(5L);
	}

	@Test
	public void fromLongValuePosicaoFinanceiraIncorreto() throws Exception {
		assertThat(fromLongValue(1L)).isEqualTo(POSICAO_FINANCEIRA);
	}

	@Test
	public void fromLongValueEntregaProducaoIncorreto() throws Exception {
		assertThat(fromLongValue(2L)).isEqualTo(ENTREGAS_PRODUCAO);
	}

	@Test
	public void permissaoIndicadoPosicaoFinanceiraIncorreto() throws Exception {
		assertThat(POSICAO_FINANCEIRA.getPermissao()).isEqualTo(ACTPOSICAOFINANCEIRA);
	}

	@Test
	public void permissaoIndicadoEntregaProducaoIncorreto() throws Exception {
		assertThat(ENTREGAS_PRODUCAO.getPermissao()).isEqualTo(ACTENTREGAPRODUCAO);
	}

	@Test
	public void isPosicaoFinanceiraIncorretoWhenPosicaoFinanceira() throws Exception {
		assertThat(POSICAO_FINANCEIRA.isPosicaoFinanceira()).isTrue();
	}

	@Test
	public void isPosicaoFinanceiraIncorretoWhenEntregaProducao() throws Exception {
		assertThat(ENTREGAS_PRODUCAO.isPosicaoFinanceira()).isFalse();
	}

	@Test
	public void longValuePosicaoFinanceiraIncorreto() throws Exception {
		assertThat(POSICAO_FINANCEIRA.toLongValue()).isEqualTo(1L);
	}

	@Test
	public void longValueEntregaProducaoIncorreto() throws Exception {
		assertThat(ENTREGAS_PRODUCAO.toLongValue()).isEqualTo(2L);
	}

	@Test(dataProvider = TipoRequisicaoDataProvider.TIPOS_REQUISICAO,
			expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Você não possui permissão de acesso a essa funcionalidade.")
	public void deveDispararExceptionQuandoUsuarioNaoPossuirPermissao(
			br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao tipoRequisicao) throws Exception {
		tipoRequisicao.hasPermissaoThrowsExceptionIfFalse(123L, UsuarioDataProvider.usuario1());
	}

	@Test(dataProvider = TipoRequisicaoDataProvider.TIPOS_REQUISICAO)
	public void naoDeveDispararExceptionQuandoUsuarioPossuirPermissao(
			br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao tipoRequisicao) throws Exception {
		tipoRequisicao.hasPermissaoThrowsExceptionIfFalse(1234L, UsuarioDataProvider.usuario3("1234"));
	}

}