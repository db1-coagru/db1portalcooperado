package br.com.db1.portalcooperado.infrastructure.rest.translators.avicultura;

import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaItensToAviculturaItensJsonTranslator;
import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItem;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJson;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import org.testng.annotations.Test;

import java.util.List;

import static br.com.db1.portalcooperado.infrastructure.rest.translators.avicultura.AviculturaTranslatorDataProvider.*;
import static br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItemDataProvider.aviculturaItemDois;
import static br.com.db1.portalcooperado.model.entity.avicultura.AviculturaItemDataProvider.aviculturaItemUm;
import static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJsonDataProvider.aviculturaItemJsonDois;
import static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJsonDataProvider.aviculturaItemJsonUm;
import static org.assertj.core.api.Assertions.assertThat;

public class AviculturaItensToAviculturaItensJsonTranslatorTest {

	@Test
	public void deveTraduzirTodosCamposCorretamente() throws Exception {
		AviculturaItensToAviculturaItensJsonTranslator translator = newAviculturaItensToAviculturaItensJsonTranslator();

		List<AviculturaItem> itensATraduzir = ImmutableList.of(aviculturaItemUm(), aviculturaItemDois());
		List<AviculturaItemJson> itensTraduzidos = translator.translate(itensATraduzir);
		List<AviculturaItemJson> traducaoEsperada = ImmutableList.of(aviculturaItemJsonUm(), aviculturaItemJsonDois());

		assertThat(itensTraduzidos).containsExactlyElementsOf(traducaoEsperada);
	}

}