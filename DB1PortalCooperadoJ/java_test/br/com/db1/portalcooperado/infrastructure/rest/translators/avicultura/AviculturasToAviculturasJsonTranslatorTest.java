package br.com.db1.portalcooperado.infrastructure.rest.translators.avicultura;

import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturasToAviculturasJsonTranslator;
import br.com.db1.portalcooperado.model.entity.avicultura.Avicultura;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturasJson;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import org.testng.annotations.Test;

import java.util.List;

import static br.com.db1.portalcooperado.infrastructure.rest.translators.avicultura.AviculturaTranslatorDataProvider.newAviculturasToAviculturasJsonTranslator;
import static br.com.db1.portalcooperado.model.entity.avicultura.AviculturaDataProvider.aviculturaDois;
import static br.com.db1.portalcooperado.model.entity.avicultura.AviculturaDataProvider.aviculturaUm;
import static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJsonDataProvider.aviculturaJsonDois;
import static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaJsonDataProvider.aviculturaJsonUm;
import static org.assertj.core.api.Assertions.assertThat;

public class AviculturasToAviculturasJsonTranslatorTest {

	@Test
	public void deveTraduzirTodosCamposCorretamente() throws Exception {
		AviculturasToAviculturasJsonTranslator translator = newAviculturasToAviculturasJsonTranslator();

		List<Avicultura> itensATraduzir = ImmutableList.of(aviculturaUm(), aviculturaDois());
		AviculturasJson itensTraduzidos = translator.translate(itensATraduzir);
		List<AviculturaJson> traducaoEsperada = ImmutableList.of(aviculturaJsonUm(), aviculturaJsonDois());

		assertThat(itensTraduzidos).isEqualToComparingFieldByField(AviculturasJson.of(traducaoEsperada));
	}

}