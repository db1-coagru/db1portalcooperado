package br.com.db1.portalcooperado.infrastructure.rest.json.usuario;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.model.entity.Token;
import com.fasterxml.jackson.annotation.JsonRootName;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TokenJsonTest {

	@Test(expectedExceptions = DB1ValidateException.class, expectedExceptionsMessageRegExp = "Token deve ser informado para geraração do JSON.")
	public void tokenIsNull() throws Exception {
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson.of(null);
	}

	@Test
	public void accessTokenValueIsIncorrect() throws Exception {
		Token token = mock(Token.class);
		when(token.getAccessToken()).thenReturn("123456789");
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson json = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson
				.of(token);
		assertThat(json.getAccessToken()).isEqualTo("123456789");
	}

	@Test
	public void expiracaoValueIsIncorrect() throws Exception {
		Token token = mock(Token.class);
		when(token.getExpiracaoMillis()).thenReturn(123456789L);
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson json = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson
				.of(token);
		assertThat(json.getExpiracao()).isEqualTo(123456789L);
	}

	@Test
	public void geracaoValueIsIncorrect() throws Exception {
		Token token = mock(Token.class);
		when(token.getGeracaoMillis()).thenReturn(123456789L);
		br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson json = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson
				.of(token);
		assertThat(json.getGeracao()).isEqualTo(123456789L);
	}

	@Test
	public void notContainsRootName() throws Exception {
		assertThat(br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson.class).hasAnnotation(JsonRootName.class);
	}

	@Test
	public void rootNameValueIsIncorrect() throws Exception {
		JsonRootName rootName = br.com.db1.portalcooperado.infrastructure.rest.json.usuario.TokenJson.class.getAnnotation(JsonRootName.class);
		assertThat(rootName.value()).isEqualTo("portalToken");
	}

}