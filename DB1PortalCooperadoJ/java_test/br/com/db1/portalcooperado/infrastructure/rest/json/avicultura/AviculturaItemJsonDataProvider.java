package br.com.db1.portalcooperado.infrastructure.rest.json.avicultura;

import br.com.db1.portalcooperado.model.entity.avicultura.AviculturaResposta;
import jersey.repackaged.com.google.common.collect.ImmutableList;

public class AviculturaItemJsonDataProvider {

	private AviculturaItemJsonDataProvider() {

	}

	public static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJson aviculturaItemJsonUm() {
		return br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJson.builder()
				.withDescricao("descricao um")
				.withObservacao("observacao um")
				.withResposta(AviculturaResposta.NAO_CONFORMIDADE)
				.withImagens(ImmutableList.of("imagem1", "imagem2"))
				.build();
	}

	public static br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJson aviculturaItemJsonDois() {
		return br.com.db1.portalcooperado.infrastructure.rest.json.avicultura.AviculturaItemJson.builder()
				.withDescricao("descricao dois")
				.withObservacao("observacao dois")
				.withResposta(AviculturaResposta.CONFORMIDADE)
				.withImagens(ImmutableList.of("imagem3", "imagem4"))
				.build();
	}
}