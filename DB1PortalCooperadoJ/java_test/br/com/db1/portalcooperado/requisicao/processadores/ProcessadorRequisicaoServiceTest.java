package br.com.db1.portalcooperado.requisicao.processadores;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.portalcooperado.service.impl.ProcessadorRequisicaoService;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.service.RequisicaoService;
import br.com.db1.portalcooperado.service.UsuarioService;
import br.com.db1.portalcooperado.service.impl.RequisicaoProcessResponseFactoryService;
import org.testng.annotations.Test;

import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProcessadorRequisicaoServiceTest {

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Data inicial deve ser informada.")
	public void validateRequisicaoJsonEntregaProducaoNaoDeuExceptionPorFaltarDataInicial() throws Exception {
		ProcessadorRequisicaoService service = newExecutorService();

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);
		when(requisicaoJson.getIp()).thenReturn("1234");
		service.processResponse(requisicaoJson, "123");
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Data final deve ser informada.")
	public void validateRequisicaoJsonEntregaProducaoNaoDeuExceptionPorFaltarDataFinal() throws Exception {
		ProcessadorRequisicaoService service = newExecutorService();

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);
		when(requisicaoJson.getIp()).thenReturn("1234");
		when(requisicaoJson.getDataInicial()).thenReturn(new Date());
		service.processResponse(requisicaoJson, "123");
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Alguma cultura deve ser informada.")
	public void validateRequisicaoJsonEntregaProducaoNaoDeuExceptionPorFaltarAlgumaCultura() throws Exception {
		ProcessadorRequisicaoService service = newExecutorService();

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);
		when(requisicaoJson.getIp()).thenReturn("1234");
		when(requisicaoJson.getDataInicial()).thenReturn(new Date());
		when(requisicaoJson.getDataFinal()).thenReturn(new Date());
		service.processResponse(requisicaoJson, "123");

	}

	private ProcessadorRequisicaoService newExecutorService() {
		RequisicaoService requisicaoService = mock(RequisicaoService.class);
		UsuarioService usuarioService = mock(UsuarioService.class);
		RequisicaoProcessResponseFactoryService factory = mock(RequisicaoProcessResponseFactoryService.class);
		return new ProcessadorRequisicaoService(requisicaoService, usuarioService, factory);
	}

}