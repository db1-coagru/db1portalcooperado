package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import org.testng.annotations.DataProvider;

public class RequisicaoResponseProcessorDataProvider {

	public static final String INSTANCIAS_POR_TIPO_REQUISICAO = "instanciasPorTipoRequisicao";

	@DataProvider(name = INSTANCIAS_POR_TIPO_REQUISICAO)
	public static Object[][] instanciasPorTipoRequisicao() {
		return new Object[][] {
				{ TipoRequisicao.POSICAO_FINANCEIRA, PosicaoFinanceiraRequisicaoResponseProcessor.class },
				{ TipoRequisicao.ENTREGAS_PRODUCAO, EntregaProducaoRequisicaoResponseProcessor.class },
				{ TipoRequisicao.RECOMENDACAO_TECNICA, RecomendacaoTecnicaRequisicaoResponseProcessor.class },
				{ TipoRequisicao.AVICULTURA, AviculturaRequisicaoResponseProcessor.class },
		};
	}
}