package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.service.PosicaoFinanceiraService;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PosicaoFinanceiraRequisicaoResponseProcessorTest {

	@Test(expectedExceptions = IllegalArgumentException.class,
			expectedExceptionsMessageRegExp = "Tipos incompatíves. Esperado POSICAO_FINANCEIRA - Recebido ENTREGAS_PRODUCAO")
	public void naoDeveProcessarRequisicaoComTipoDiferenteAvicultura() throws Exception {
		PosicaoFinanceiraService service = mock(PosicaoFinanceiraService.class);
		PosicaoFinanceiraRequisicaoResponseProcessor processor = PosicaoFinanceiraRequisicaoResponseProcessor
				.of(service);

		Requisicao requisicao = mock(Requisicao.class);
		when(requisicao.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.POSICAO_FINANCEIRA);

		processor.processar(requisicao, requisicaoJson);
	}

	@Test(expectedExceptions = IllegalArgumentException.class,
			expectedExceptionsMessageRegExp = "Tipos incompatíves. Esperado POSICAO_FINANCEIRA - Recebido ENTREGAS_PRODUCAO")
	public void naoDeveProcessarRequisicaoJsonComTipoDiferenteAvicultura() throws Exception {
		PosicaoFinanceiraService service = mock(PosicaoFinanceiraService.class);
		PosicaoFinanceiraRequisicaoResponseProcessor processor = PosicaoFinanceiraRequisicaoResponseProcessor
				.of(service);

		Requisicao requisicao = mock(Requisicao.class);
		when(requisicao.getTipoRequisicao()).thenReturn(TipoRequisicao.POSICAO_FINANCEIRA);

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);

		processor.processar(requisicao, requisicaoJson);
	}

}