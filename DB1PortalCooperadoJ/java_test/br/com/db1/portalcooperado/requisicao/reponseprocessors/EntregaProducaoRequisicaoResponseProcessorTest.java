package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.service.EntregaProducaoService;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EntregaProducaoRequisicaoResponseProcessorTest {

	@Test(expectedExceptions = IllegalArgumentException.class,
			expectedExceptionsMessageRegExp = "Tipos incompatíves. Esperado ENTREGAS_PRODUCAO - Recebido AVICULTURA")
	public void naoDeveProcessarRequisicaoComTipoDiferenteAvicultura() throws Exception {
		EntregaProducaoService service = mock(EntregaProducaoService.class);
		EntregaProducaoRequisicaoResponseProcessor processor = EntregaProducaoRequisicaoResponseProcessor.of(service);

		Requisicao requisicao = mock(Requisicao.class);
		when(requisicao.getTipoRequisicao()).thenReturn(TipoRequisicao.AVICULTURA);

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);

		processor.processar(requisicao, requisicaoJson);
	}

	@Test(expectedExceptions = IllegalArgumentException.class,
			expectedExceptionsMessageRegExp = "Tipos incompatíves. Esperado ENTREGAS_PRODUCAO - Recebido AVICULTURA")
	public void naoDeveProcessarRequisicaoJsonComTipoDiferenteAvicultura() throws Exception {
		EntregaProducaoService service = mock(EntregaProducaoService.class);
		EntregaProducaoRequisicaoResponseProcessor processor = EntregaProducaoRequisicaoResponseProcessor.of(service);

		Requisicao requisicao = mock(Requisicao.class);
		when(requisicao.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.AVICULTURA);

		processor.processar(requisicao, requisicaoJson);
	}

}