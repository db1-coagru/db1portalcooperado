package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.dao.AviculturaDao;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaToAviculturaJsonTranslator;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturasToAviculturasJsonTranslator;
import br.com.db1.portalcooperado.model.entity.Requisicao;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AviculturaRequisicaoResponseProcessorTest {

	@Test(expectedExceptions = IllegalArgumentException.class,
			expectedExceptionsMessageRegExp = "Tipos incompatíves. Esperado AVICULTURA - Recebido ENTREGAS_PRODUCAO")
	public void naoDeveProcessarRequisicaoComTipoDiferenteAvicultura() throws Exception {
		AviculturaDao dao = mock(AviculturaDao.class);
		AviculturasToAviculturasJsonTranslator translator = mock(AviculturasToAviculturasJsonTranslator.class);
		AviculturaToAviculturaJsonTranslator translatorItem = mock(AviculturaToAviculturaJsonTranslator.class);
		AviculturaRequisicaoResponseProcessor processor = AviculturaRequisicaoResponseProcessor
				.of(dao, translator, translatorItem);

		Requisicao requisicao = mock(Requisicao.class);
		when(requisicao.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.AVICULTURA);

		processor.processar(requisicao, requisicaoJson);
	}

	@Test(expectedExceptions = IllegalArgumentException.class,
			expectedExceptionsMessageRegExp = "Tipos incompatíves. Esperado AVICULTURA - Recebido ENTREGAS_PRODUCAO")
	public void naoDeveProcessarRequisicaoJsonComTipoDiferenteAvicultura() throws Exception {
		AviculturaDao dao = mock(AviculturaDao.class);
		AviculturasToAviculturasJsonTranslator translator = mock(AviculturasToAviculturasJsonTranslator.class);
		AviculturaToAviculturaJsonTranslator translatorItem = mock(AviculturaToAviculturaJsonTranslator.class);

		AviculturaRequisicaoResponseProcessor processor = AviculturaRequisicaoResponseProcessor
				.of(dao, translator, translatorItem);

		Requisicao requisicao = mock(Requisicao.class);
		when(requisicao.getTipoRequisicao()).thenReturn(TipoRequisicao.AVICULTURA);

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);

		processor.processar(requisicao, requisicaoJson);
	}
}