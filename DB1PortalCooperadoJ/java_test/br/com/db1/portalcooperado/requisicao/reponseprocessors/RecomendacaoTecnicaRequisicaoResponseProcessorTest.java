package br.com.db1.portalcooperado.requisicao.reponseprocessors;

import br.com.db1.portalcooperado.model.entity.Requisicao;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.RequisicaoJson;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.service.RecomendacaoTecnicaService;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RecomendacaoTecnicaRequisicaoResponseProcessorTest {

	@Test(expectedExceptions = IllegalArgumentException.class,
			expectedExceptionsMessageRegExp = "Tipos incompatíves. Esperado RECOMENDACAO_TECNICA - Recebido ENTREGAS_PRODUCAO")
	public void naoDeveProcessarRequisicaoComTipoDiferenteAvicultura() throws Exception {
		RecomendacaoTecnicaService service = mock(RecomendacaoTecnicaService.class);
		RecomendacaoTecnicaRequisicaoResponseProcessor processor = RecomendacaoTecnicaRequisicaoResponseProcessor.of(service);

		Requisicao requisicao = mock(Requisicao.class);
		when(requisicao.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.RECOMENDACAO_TECNICA);

		processor.processar(requisicao, requisicaoJson);
	}

	@Test(expectedExceptions = IllegalArgumentException.class,
			expectedExceptionsMessageRegExp = "Tipos incompatíves. Esperado RECOMENDACAO_TECNICA - Recebido ENTREGAS_PRODUCAO")
	public void naoDeveProcessarRequisicaoJsonComTipoDiferenteAvicultura() throws Exception {
		RecomendacaoTecnicaService service = mock(RecomendacaoTecnicaService.class);
		RecomendacaoTecnicaRequisicaoResponseProcessor processor = RecomendacaoTecnicaRequisicaoResponseProcessor.of(service);

		Requisicao requisicao = mock(Requisicao.class);
		when(requisicao.getTipoRequisicao()).thenReturn(TipoRequisicao.RECOMENDACAO_TECNICA);

		RequisicaoJson requisicaoJson = mock(RequisicaoJson.class);
		when(requisicaoJson.getTipoRequisicao()).thenReturn(TipoRequisicao.ENTREGAS_PRODUCAO);

		processor.processar(requisicao, requisicaoJson);
	}

}