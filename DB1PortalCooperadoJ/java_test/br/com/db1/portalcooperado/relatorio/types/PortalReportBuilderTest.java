package br.com.db1.portalcooperado.relatorio.types;

import br.com.db1.exception.validate.DB1ValidateException;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PortalReportBuilderTest {

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Caminho do relatório deve ser informado.")
	public void reportWithoutCaminho() throws Exception {
		PortalReport.builder().build();
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Tipo do relatório deve ser informado.")
	public void reportWithoutTipo() throws Exception {
		PortalReport.builder()
				.withCaminho("Caminho de teste")
				.build();
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Este relatório não contém páginas a serem exibidas.")
	public void builderWithoutDataSource() throws Exception {
		PortalReport.builder()
				.withCaminho("Caminho de teste")
				.withTipo(ReportType.PDF)
				.build();
	}

	@Test
	public void caminhoIsIncorrect() throws Exception {
		PortalReport report = PortalReport
				.builder()
				.withCaminho("Caminho de teste")
				.withTipo(ReportType.PDF)
				.addDataSource(ImmutableList.of("Campo teste", "Valor teste"))
				.build();
		assertThat(report.getCaminho()).isEqualTo("Caminho de teste");
	}

	@Test
	public void tipoIsIncorrect() throws Exception {
		PortalReport report = PortalReport
				.builder()
				.withCaminho("Caminho de teste")
				.withTipo(ReportType.PDF)
				.addDataSource(ImmutableList.of("Campo teste", "Valor teste"))
				.build();
		assertThat(report.getTipo()).isEqualTo("pdf");
	}

	@Test
	public void dataSourceIsIncorrect() throws Exception {
		PortalReport report = PortalReport
				.builder()
				.withCaminho("Caminho de teste")
				.withTipo(ReportType.PDF)
				.addDataSource(ImmutableList.of("Teste 1", "Teste 2", "Teste 3"))
				.build();
		assertThat(report.getDataSource()).containsExactly("Teste 1", "Teste 2", "Teste 3");
	}

	@Test
	public void parametersIsIncorrect() throws Exception {
		PortalReport report = PortalReportDataProvider.reportOne();
		assertThat(report.getParameters()).containsExactly("ParamTeste", "Param teste", "ParamTeste2", "Param teste 2");
	}

	@Test
	public void dataSourceIsNotImmutableWhenShouldBe() throws Exception {
		PortalReport report = PortalReportDataProvider.reportOne();
		assertThat(report.getDataSource()).isInstanceOf(ImmutableList.class);
	}

	@Test
	public void parametersIsNotImmutableWhenShouldBe() throws Exception {
		PortalReport report = PortalReportDataProvider.reportOne();
		assertThat(report.getParameters()).isInstanceOf(ImmutableList.class);
	}

}