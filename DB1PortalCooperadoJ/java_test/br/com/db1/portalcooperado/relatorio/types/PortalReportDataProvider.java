package br.com.db1.portalcooperado.relatorio.types;

import jersey.repackaged.com.google.common.collect.ImmutableList;

public class PortalReportDataProvider {

	private PortalReportDataProvider() {

	}

	public static br.com.db1.portalcooperado.relatorio.types.PortalReport reportOne() {
		return br.com.db1.portalcooperado.relatorio.types.PortalReport.builder()
				.withCaminho("Caminho de teste")
				.withTipo(ReportType.PDF)
				.withParam("ParamTeste", "Param teste")
				.withParam("ParamTeste2", "Param teste 2")
				.addDataSource(ImmutableList.of("Teste 1", "Teste 2", "Teste 3"))
				.build();
	}
}
