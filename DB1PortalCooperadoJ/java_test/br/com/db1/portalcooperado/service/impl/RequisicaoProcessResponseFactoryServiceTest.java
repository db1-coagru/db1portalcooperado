package br.com.db1.portalcooperado.service.impl;

import br.com.db1.portalcooperado.dao.AviculturaDao;
import br.com.db1.portalcooperado.infrastructure.rest.json.requisicao.TipoRequisicao;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturaToAviculturaJsonTranslator;
import br.com.db1.portalcooperado.infrastructure.rest.translators.entitytojson.avicultura.AviculturasToAviculturasJsonTranslator;
import br.com.db1.portalcooperado.requisicao.reponseprocessors.RequisicaoResponseProcessorDataProvider;
import br.com.db1.portalcooperado.service.EntregaProducaoService;
import br.com.db1.portalcooperado.service.PosicaoFinanceiraService;
import br.com.db1.portalcooperado.service.RecomendacaoTecnicaService;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class RequisicaoProcessResponseFactoryServiceTest {

	@Test(dataProviderClass = RequisicaoResponseProcessorDataProvider.class,
			dataProvider = RequisicaoResponseProcessorDataProvider.INSTANCIAS_POR_TIPO_REQUISICAO)
	public void deveRetornarProcessorCorretoParaTipoRequisicaoInformado(TipoRequisicao tipoRequisicao,
			Class<?> klazz) throws Exception {
		RequisicaoProcessResponseFactoryService service = newService();
		assertThat(service.processadorOf(tipoRequisicao)).isInstanceOf(klazz);
	}

	private RequisicaoProcessResponseFactoryService newService() {
		return new RequisicaoProcessResponseFactoryService(mock(PosicaoFinanceiraService.class),
				mock(EntregaProducaoService.class),
				mock(RecomendacaoTecnicaService.class),
				mock(AviculturaDao.class),
				mock(AviculturasToAviculturasJsonTranslator.class),
				mock(AviculturaToAviculturaJsonTranslator.class));
	}

}