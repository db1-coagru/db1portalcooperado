package br.com.db1.portalcooperado.service.impl;

import br.com.db1.exception.validate.DB1ValidateException;
import br.com.db1.kernel.report.exception.DB1ReportGenerationException;
import br.com.db1.kernel.report.service.DB1ReportService;
import br.com.db1.portalcooperado.relatorio.types.PortalReport;
import br.com.db1.portalcooperado.relatorio.types.ReportType;
import br.com.db1.portalcooperado.service.PortalReportService;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PortalReportServiceImplTest {

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Relatório deve ser informado.")
	public void portalReportIsNull() throws Exception {
		DB1ReportService db1ReportService = mock(DB1ReportService.class);
		PortalReportService portalReportService = new PortalReportServiceImpl(db1ReportService);
		portalReportService.createReport(null);
	}

	@Test(expectedExceptions = DB1ReportGenerationException.class,
			expectedExceptionsMessageRegExp = "Problemas ao tentar gerar relatório: Exception de teste")
	public void portalReportCannotBeCreate() throws Exception {
		PortalReport.Builder builder = PortalReport.builder()
				.withCaminho("Caminho de teste")
				.withTipo(ReportType.PDF)
				.withParam("ParamTeste", "Param teste")
				.withParam("ParamTeste2", "Param teste 2")
				.addDataSource(ImmutableList.of("Teste 1", "Teste 2", "Teste 3"));
		PortalReport portalReport = builder.build();

		DB1ReportService db1ReportService = mock(DB1ReportService.class);
		when(db1ReportService.createReport(portalReport.getCaminho(),
				portalReport.getTipo(),
				portalReport.getDataSource(),
				portalReport.getParameters()))
				.thenThrow(new DB1ReportGenerationException("Exception de teste"));

		PortalReportService portalReportService = new PortalReportServiceImpl(db1ReportService);
		portalReportService.createReport(builder);
	}

	@Test(expectedExceptions = DB1ValidateException.class,
			expectedExceptionsMessageRegExp = "Este relatório não contém páginas a serem exibidas.")
	public void portalReportCannotBeCreateWithoutDataSource() throws Exception {
		PortalReport.Builder builder = PortalReport.builder()
				.withCaminho("Caminho de teste")
				.withTipo(ReportType.PDF)
				.withParam("ParamTeste", "Param teste")
				.withParam("ParamTeste2", "Param teste 2");

		PortalReportService portalReportService = new PortalReportServiceImpl(mock(DB1ReportService.class));
		portalReportService.createReport(builder);
	}

}